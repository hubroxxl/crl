﻿using CRL.Core;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CRL.Cache
{
    public interface IDelegateCacheContainer
    {
        CacheContainerType CacheContainerType { get; }
        void Write(string key, Type dataType, cacheDataItem data);
        bool Get(string key, Type dataType, out cacheDataItem data);
        void Remove(string key, Type dataType);
        List<string> GetAllKeys();
    }
    public class cacheDataItem
    {
        public string key;
        public object args;
        public DateTime expTime;
        public object data;
        public T GetData<T>(CacheContainerType cacheContainerType)
        {
            if (cacheContainerType == CacheContainerType.Memory)
            {
                return (T)data;
            }
            if (data == null)
            {
                return default(T);
            }
            return SerializeHelper.DeserializeFromJson<T>(data.ToString());
        }
    }

    class CacheObjInfo
    {
        //public object data;
        public DateTime expTime;
        public Type dataType;
        //public Func<object> dataFunc;
        public MethodInfo Method;
        public object[] funcArgs;
        public bool needUpdate;
        public Exception lastException;
        public bool autoUpdate;
        public double expMinute;
        internal Delegate funcDelegate;
    }
}
