﻿/**
* CRL
*/
using CRL.Core;

using CRL.Core.Extension;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CRL.Cache
{
    class MemoryDelegateCacheContainer : IDelegateCacheContainer
    {
        static ConcurrentDictionary<string, cacheDataItem> caches = new ConcurrentDictionary<string, cacheDataItem>();
        public CacheContainerType CacheContainerType => CacheContainerType.Memory;
        public bool Get(string key, Type dataType, out cacheDataItem data)
        {
            var a = caches.TryGetValue(key, out data);
            return a;
        }

        public List<string> GetAllKeys()
        {
            return caches.Keys.ToList();
        }

        public void Remove(string key, Type dataType)
        {
            caches.TryRemove(key, out var data);
        }

        public void Write(string key, Type dataType, cacheDataItem data)
        {
            caches.TryRemove(key, out var data2);
            caches.TryAdd(key, data);
        }
    }
    class MemoryDelegateCache : DelegateCacheBase
    {
        public MemoryDelegateCache() : base(new MemoryDelegateCacheContainer())
        {

        }
        static MemoryDelegateCache()
        {
            instance = new MemoryDelegateCache();
        }
        protected static DelegateCacheBase instance;
        internal static DelegateCacheBase getInstance()
        {
            return instance;
        }
    }
}
