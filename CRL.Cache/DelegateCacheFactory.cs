﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRL.Cache
{
    public class DelegateCacheFactory
    {
        public static DelegateCacheBase GetInstance(CacheContainerType cacheContainerType)
        {
            if (cacheContainerType == CacheContainerType.Redis)
            {
                return RedisDelegateCache.getInstance();
            }
            else if (cacheContainerType == CacheContainerType.Memory)
            {
                return MemoryDelegateCache.getInstance();
            }
            throw new Exception($"未实现{cacheContainerType}");
        }
        public static DelegateCacheBase GetMemoryInstance()
        {
            return GetInstance(CacheContainerType.Memory);
        }
        public static DelegateCacheBase GetRedisInstance()
        {
            return GetInstance(CacheContainerType.Redis);
        }
    }
    public enum CacheContainerType
    {
        Memory,
        Redis
    }
}
