﻿using CRL.Data;
using CRL.Data.DBAccess;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRL.Sqlite
{
    public static class Extension
    {
        public static IDbConfigRegister UseSqlite(this IDbConfigRegister iBuilder)
        {
            iBuilder.SetDbProviderFactory(SQLiteFactory.Instance);
            return iBuilder;
        }
    }
}
