﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRL.Data;
using CRL.Elasticsearch.ESEx;
using Nest;

namespace esTest
{
    public class GoodsService:BaseProvider<GoodsInfo>
    {
        public void CreateEsIndex()
        {
            var db = DBExtend as ESExtend;
            //定义索引设置
            Func<CreateIndexDescriptor, ICreateIndexRequest> selector = c => c
      .Index<AbsDocument>()
      .Map<AbsDocument>(m => m
          .RoutingField(r => r.Required())
          .AutoMap<GoodsInfo>()
          .AutoMap<GoodsInfoChild>()
          .Properties(props => props
              .Join(j => j
                  .Name(p => p.GoodsJoinField)
                  .Relations(r => r
                      .Join<GoodsInfo, GoodsInfoChild>()
                  )
              )
          )
      );
            db.CreateEsIndex<GoodsInfo>(selector);
        }
    }

    public class GoodsInfoChildService : BaseProvider<GoodsInfoChild>
    {
    }
}
