﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace esTest
{
    public abstract class AbsDocument
    {
        [Keyword]
        public string Id { get; set; }
        //public string IdOrigin { get; set; }
        [Keyword]
        public string DataType { get; set; }
        public JoinField GoodsJoinField { get; set; }
    }
    public class GoodsInfo : AbsDocument
    {
        public GoodsInfo()
        {
            GoodsJoinField = JoinField.Root<GoodsInfo>();
        }
        [Text(Analyzer = "ik_max_word")]
        public string Name { get; set; }
        public string Name2 { get; set; }

        public decimal Number { get; set; }
        public string remark { get; set; }
        public bool Enable { get; set; }
        public int Sort { get; set; }
        public override string ToString()
        {
            return $"{Id}_{Name}";
        }
    }
    public class GoodsInfoChild : AbsDocument
    {
        public GoodsInfoChild()
        {

        }
        public GoodsInfoChild(string goodsId, string id, string dataType)
        {
            GoodsJoinField = JoinField.Link<GoodsInfoChild>(goodsId);
            DataType = dataType.ToString();
            Id = $"{DataType[0]}{id}";
            ParentId = goodsId;
        }
        [Keyword]
        public string GroupCode { get; set; }
        /// <summary>
        /// 父ID(goodsInfo.Id)
        /// </summary>
        public string ParentId { get; set; }
        public override string ToString()
        {
            return $"{Id}";
        }
    }
}
