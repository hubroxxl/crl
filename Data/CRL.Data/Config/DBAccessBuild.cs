﻿/**
* CRL
*/
using CRL.Data.DBAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace CRL.Data
{
    public class DBAccessBuild
    {
        internal DBType _DBType;
        public string ConnectionString;
        internal IDbConnection _connection;
        internal IDbTransaction _dbTransaction;
        public object Data;
        public DBAccessBuild(DBType dbType, string connectionString)
        {
            _DBType = dbType;
            connectionString.CheckNull("参数为空 connectionString");
            ConnectionString = connectionString;
        }
        public DBAccessBuild(DBType dbType, IDbConnection connection, IDbTransaction dbTransaction = null)
        {
            connection.CheckNull("参数为空 connection");
            _DBType = dbType;
            _connection = connection;
            _dbTransaction = dbTransaction;
        }
    }
}
