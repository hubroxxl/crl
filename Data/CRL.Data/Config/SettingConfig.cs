﻿/**
* CRL
*/
using CRL.Data.DBAccess;
using CRL.Data.LambdaQuery;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRL.Data
{
    #region delegate
    /// <summary>
    /// as bool TransMethod(out string error);
    /// </summary>
    /// <param name="error"></param>
    /// <returns></returns>
    public delegate bool TransMethod(out string error);
    #endregion

    /// <summary>
    /// 框架部署,请实现委托
    /// </summary>
    public class SettingConfig
    {
        static SettingConfig()
        {
            var builder = DBConfigRegister.GetInstance();
        }
        #region 设置

        /// <summary>
        /// string字段默认长度
        /// </summary>
        public static int StringFieldLength { get; set; } = 30;
        /// <summary>
        /// 是否检测表结构,生产服务器可将此值设为FALSE
        /// </summary>
        public static bool CheckModelTableMaping { get; set; } = true;

        /// <summary>
        /// 是否使用主从读写分离
        /// 启用后,不会自动检查表结构
        /// 在事务范围内,查询按主库
        /// </summary>
        public static bool UseReadSeparation { get; set; } = false;

        /// <summary>
        /// 是否记录SQL语句调用
        /// </summary>
        public static bool LogSql { get; set; } = false;
        /// <summary>
        /// 生成参数是否与字段名一致
        /// </summary>
        public static bool FieldParameName { get; set; } = false;
        /// <summary>
        /// 是否替换SQL拼接参数
        /// </summary>
        public static bool ReplaceSqlParameter { get; set; } = false;//生成存储过程时不能替换
        /// <summary>
        /// 默认nolock
        /// </summary>
        public static bool QueryWithNoLock { get; set; } = true;
        /// <summary>
        /// 分页是否编译存储过程
        /// </summary>
        public static bool CompileSp { get; set; } = false;

        public static bool UpdateModelCache = true;

        /// <summary>
        /// 对象属性序列化
        /// </summary>
        public static Func<object, string> ValueObjSerializer = (obj) =>
        {
            return Core.SerializeHelper.SerializerToJson(obj);
        };
        public static Func<string, Type, object> ValueObjDeserialize = (str, type) =>
        {
            return Core.SerializeHelper.DeserializeFromJson(str, type);
        };
        /// <summary>
        /// 混淆字符串
        /// </summary>
        public static string ObfuscateChars = "cfoztprwedimbajqgvhlknsyux";
        public static bool UseDpaGenerator { get; set; } = false;
        public static bool IsAot = false;
        #endregion
    }
}
