﻿/**
* CRL
*/
using CRL.Data.DBAccess;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRL.Data
{
    /// <summary>
    /// 对运行时间计时
    /// </summary>
    public class SqlStopWatch
    {
        static void debugAttachLog(string msg)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                Console.WriteLine($"Debugger.IsAttached: {msg}");
            }
        }
        public static int Execute(DBHelper __DbHelper, string sql)
        {
            int n = 0;
            var el = Run(() =>
            {
                debugAttachLog(sql);
                n = __DbHelper.Execute(sql);
            });
            SQLTrack.SaveSQLElapsedTime(sql, el);
            return n;
        }
        public static async Task<int> ExecuteAsync(DBHelper __DbHelper, string sql)
        {
            debugAttachLog(sql);
            return await __DbHelper.ExecuteAsync(sql); 
        }

        public static object ExecScalar(DBHelper __DbHelper, string sql)
        {
            object obj = null;
            var el = Run(() =>
            {
                obj = __DbHelper.ExecScalar(sql);
            });
            debugAttachLog(sql);
            SQLTrack.SaveSQLElapsedTime(sql, el);
            return obj;
        }
        public static Task<object> ExecScalarAsync(DBHelper __DbHelper, string sql)
        {
            return __DbHelper.ExecScalarAsync(sql);
        }

        internal static T ReturnList<T>(Func<T> func, string sql) where T : ICollection
        {
            T list = default(T);
            var el = Run(() =>
            {
                list = func();
            });
            var n = 0;
            if (list != null)
            {
                n = list.Count;
            }
            SQLTrack.SaveSQLElapsedTime(sql, el, n);
            return list;
        }
        internal static T ReturnData<T>(Func<CallBackDataReader> readFunc, Func<CallBackDataReader, T> dataConvertFunc) where T : ICollection
        {
            T list = default(T);
            string sql = "";
            var el = Run(() =>
             {
                 var reader = readFunc();
                 if (reader != null)
                 {
                     sql = reader.Sql;
                     list = dataConvertFunc(reader);
                 }
                 else
                 {
                     list = Activator.CreateInstance<T>();
                 }
             });
            var n = 0;
            if(list!=null)
            {
                n = list.Count;
            }
            SQLTrack.SaveSQLElapsedTime(sql, el, n);
            return list;
        }
        public static long Run(Action act)
        {
            var time = DateTime.Now;
            act();
            var ts = DateTime.Now - time;
            return (long)ts.TotalMilliseconds;
        }
    }
}
