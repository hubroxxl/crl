﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CRL.Data
{
    /// <summary>
    /// 函数类型
    /// </summary>
    public enum FunctionType
    {
        MAX,
        MIN,
        COUNT,
        SUM,
        AVG
    }
}
