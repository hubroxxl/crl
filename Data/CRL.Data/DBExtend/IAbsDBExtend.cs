﻿/**
* CRL
*/
using CRL.Data.LambdaQuery;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CRL.Data
{
    public partial interface IAbsDBExtend
    {
        #region 参数
        void AddOutParam(string name, object value = null);
        void AddParame(string name, object value);
        void ClearParame();
        object GetOutParam(string name);

        //T GetOutParam<T>(string name);
        int GetReturnValue();
        #endregion

        #region insert
        void InsertFromObj<TModel>(TModel obj) where TModel : class;
        void BatchInsert<TModel>(List<TModel> details, bool keepIdentity = false) where TModel : class;

        #endregion

        void BeginTran(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted);
        void CommitTran();
        void RollbackTran();
        void CreateTableIndex<TModel>() where TModel : class;
        void DropTable<TModel>();
        void CheckTableCreated(Type type);

        #region delete
        int Delete<T>(ILambdaQuery<T> query);

        int Delete<TModel, TJoin>(Expression<Func<TModel, TJoin, bool>> expression);

        int Delete<TModel>(Expression<Func<TModel, bool>> expression);

        int Delete<TModel>(object id);
        #endregion

        #region sql
        Dictionary<TKey, TValue> ExecDictionary<TKey, TValue>(string sql);

        List<dynamic> ExecDynamicList(string sql);

        List<T> ExecList<T>(string sql) where T : class, new();

        T ExecObject<T>(string sql) where T : class, new();

        object ExecScalar(string sql);

        T ExecScalar<T>(string sql);

        int Execute(string sql);
        #endregion

        #region func
        //TType GetFunction<TType, TModel>(Expression<Func<TModel, bool>> expression, Expression<Func<TModel, TType>> selectField, FunctionType functionType, bool compileSp = false) ;
        TResult Max<TResult, TModel>(Expression<Func<TModel, bool>> expression, Expression<Func<TModel, TResult>> field, bool compileSp = false);

        TResult Min<TResult, TModel>(Expression<Func<TModel, bool>> expression, Expression<Func<TModel, TResult>> field, bool compileSp = false);
        TResult Sum<TResult, TModel>(Expression<Func<TModel, bool>> expression, Expression<Func<TModel, TResult>> field, bool compileSp = false);
        int Count<TModel>(Expression<Func<TModel, bool>> expression, bool compileSp = false) where TModel : class;
        #endregion

        #region query
        List<dynamic> QueryDynamic(LambdaQueryBase query);

        TModel QueryItem<TModel>(Expression<Func<TModel, bool>> expression, bool idDest = true, bool compileSp = false);

        TModel QueryItem<TModel>(object id);

        List<TModel> QueryList<TModel>(Expression<Func<TModel, bool>> expression = null, bool compileSp = false);

        List<TModel> QueryList<TModel>(ILambdaQuery<TModel> query);

        List<TModel> QueryOrFromCache<TModel>(ILambdaQuery<TModel> query, out string cacheKey);

        //List<TResult> QueryResult<TResult>(LambdaQueryBase query);

        List<TResult> QueryResult<TResult>(LambdaQueryBase query, NewExpression newExpression = null);

        dynamic QueryScalar<TModel>(ILambdaQuery<TModel> query);

        Dictionary<TKey, TValue> ToDictionary<TModel, TKey, TValue>(ILambdaQuery<TModel> query);
        #endregion

        #region sp
        int Run(string sp);

        List<dynamic> RunDynamicList(string sp);

        List<T> RunList<T>(string sp) where T : class, new();

        T RunObject<T>(string sp) where T : class, new();

        object RunScalar(string sp);
        #endregion

        #region update
        int Update<TModel, TJoin>(Expression<Func<TModel, TJoin, bool>> expression, ParameCollection updateValue);
        int Update<TModel>(Expression<Func<TModel, bool>> expression, dynamic updateValue);

        int Update<TModel>(Expression<Func<TModel, bool>> expression, ParameCollection setValue);

        int Update<TModel>(Expression<Func<TModel, bool>> expression, TModel model);

        int Update<TModel>(ILambdaQuery<TModel> query, ParameCollection updateValue);

        int Update<TModel>(List<TModel> objs);
        void BeginTracking<TModel>(TModel obj);
		void BeginTracking<TModel>(List<TModel> objs);
		int Update<TModel>(BatchUpdate<TModel> batchUpdate);

		int Update<TModel>(TModel obj);
        void InsertOrUpdate<TModel>(List<TModel> items, InsertOrUpdateOption option = null);
        #endregion


    }
}
