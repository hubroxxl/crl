﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using CRL.Data.LambdaQuery;
namespace CRL.Data.DBExtend.RelationDB
{
    public sealed partial class DBExtend
    {
        #region insert
        /// <summary>
        /// 批量插入,并指定是否保持自增主键
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="details"></param>
        /// <param name="keepIdentity"></param>
        public override void BatchInsert<TModel>(List<TModel> details,bool keepIdentity=false)
        {
            CheckTableCreated<TModel>();
            if (details.Count == 0)
                return;
            foreach (TModel item in details)
            {
                //item.CheckRepeatedInsert = false;
                CheckData(item, false);
            }
            try
            {
                _DBAdapter.BatchInsert(dbContext, details, keepIdentity);
            }
            catch (Exception ero)
            {
                ClearParame();
                var ero2 = _DBAdapter.CheckFieldLength(dbContext, typeof(TModel), ero, details);
                throw ero2;
            }
            ClearParame();
            var updateModel = MemoryDataCache.CacheService.GetCacheTypeKey(typeof(TModel), __DbHelper.DatabaseName);
            foreach (var item in details)
            {
                foreach (var key in updateModel)
                {
                    MemoryDataCache.CacheService.UpdateCacheItem(key, item, null);
                }
            }
        }
       
        /// <summary>
        /// 单个插入
        /// </summary>
        /// <param name="obj"></param>
        public override void InsertFromObj<TModel>(TModel obj)
        {
            //var Reflection = ReflectionHelper.GetInfo<TModel>();
            CheckTableCreated<TModel>();
            var primaryKey = TypeCache.GetTable(obj.GetType()).PrimaryKey;
            object val;
            CheckData(obj, false);
            try
            {
                val = _DBAdapter.InsertObject(dbContext, obj);
            }
            catch(Exception ero)
            {
                var ero2 = _DBAdapter.CheckFieldLength(dbContext, typeof(TModel), ero, new TModel[] { obj });
                ClearParame();
                throw ero2;
            }
            if (primaryKey != null && primaryKey.KeepIdentity == false)
            {
                //Reflection.GetAccessor(primaryKey.Name).Set((TModel)obj, index);
                val = ObjectConvert.ConvertObject(primaryKey.PropertyType, val);
                primaryKey.SetValue(obj, val);
            }
            ClearParame();
            //obj.SetOriginClone();
            UpdateCacheItem<TModel>(obj, null, true);
            //return index;
        }
        #endregion
    }
}
