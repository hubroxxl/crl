﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using CRL.Core;
using CRL.Data.LambdaQuery;
namespace CRL.Data.DBExtend.RelationDB
{
    public sealed partial class DBExtend
    {
        #region sql to sp

        static Dictionary<string, Dictionary<string, int>> spCahe = new Dictionary<string, Dictionary<string, int>>();
        /// <summary>
        /// 将SQL语句编译成储存过程
        /// </summary>
        /// <param name="template">模版</param>
        /// <param name="sql">SQL语句</param>
        /// <param name="templateParame">模版替换参数</param>
        /// <returns></returns>
        string CompileSqlToSp(string template, string sql, Dictionary<string, string> templateParame = null)
        {
            if (!_DBAdapter.CanCompileSP)
            {
                throw new Exception("当前数据库不支持动态编译");
            }
            sql = _DBAdapter.SqlFormat(sql);
            var dbName = this.__DbHelper.DatabaseName;
            lock (lockObj)
            {
                if (!spCahe.ContainsKey(dbName))//初始已编译过的存储过程
                {
                    var db = GetBackgroundDBExtend();
                    //BackupParams();
                    var dic = db.ExecDictionary<string, int>(_DBAdapter.GetAllSPSql(this.__DbHelper.DatabaseName));
                    if (!spCahe.ContainsKey(dbName))
                    {
                        spCahe.Add(dbName, dic);
                    }
                    //RecoveryParams();
                }
            }
            string fields = "";
            if (templateParame != null)
            {
                if (templateParame.ContainsKey("fields"))
                {
                    fields = templateParame["fields"];
                }
                if (templateParame.ContainsKey("sort"))
                {
                    fields += "_" + templateParame["sort"];
                }
                if (templateParame.ContainsKey("rowOver"))
                {
                    fields += "_" + templateParame["rowOver"];
                }
            }
            string spPrex = $"ZautoSp_";
            string sp;
            if(SettingConfig.FieldParameName)
            {
                spPrex += "H_";
            }
            sp = StringHelper.EncryptMD5(fields + "_" + sql.Trim());
            sp = spPrex + sp.Substring(8, 16);

            var dbCahce = spCahe[dbName];
            if (!dbCahce.ContainsKey(sp))
            {
                //sql = __DbHelper.FormatWithNolock(sql);
                var db = GetBackgroundDBExtend();
                //FillParame(dbContext.DBHelper);
                string spScript = SqlToProcedure(template, dbContext, sql, sp, templateParame);
                try
                {
                    //BackupParams();
                    this.__DbHelper.Execute(spScript);
                    //RecoveryParams();
                    lock (lockObj)
                    {
                        if (!dbCahce.ContainsKey(sp))
                        {
                            dbCahce.Add(sp, 0);
                        }
                    }
                    string log = string.Format("创建存储过程:{0}\r\n{1}", sp, spScript);
                    EventLog.Log(log, "sqlToSp", false);
                }
                catch (System.Exception ero)
                {
                    //RecoveryParams();
                    throw new Exception("动态创建存储过程时发生错误:" + ero.Message);
                }
            }
            return sp;
        }

        /// <summary>
        /// SQL语句转换为存储过程
        /// </summary>
        /// <param name="template"></param>
        /// <param name="dbContext"></param>
        /// <param name="sql"></param>
        /// <param name="procedureName"></param>
        /// <param name="templateParame"></param>
        /// <returns></returns>
        internal static string SqlToProcedure(string template, DbContextInner dbContext, string sql, string procedureName, Dictionary<string, string> templateParame = null)
        {
            var adpater = DBAdapter.DBAdapterBase.GetDBAdapterBase(dbContext);
            template = template.Trim();
            Regex r = new Regex(@"\@(\w+)", RegexOptions.IgnoreCase);//like @parame
            Match m;
            List<string> pars = new List<string>();
            for (m = r.Match(sql); m.Success; m = m.NextMatch())
            {
                string par = m.Groups[1].ToString();
                if (!pars.Contains(par))
                {
                    pars.Add(par);
                }
            }
            //string template = Properties.Resources.pageTemplate.Trim();
            sql = sql.Replace("'", "''");//单引号过滤
            template = template.Replace("{name}", procedureName);
            template = template.Replace("{sql}", sql);
            string parames = "";
            //构造参数
            if (dbContext.DBHelper.Params != null)
            {
                foreach (var p in dbContext.DBHelper.Params)
                {
                    string key = p.Key.Replace("@", "");
                    var t = p.Value.GetType();
                    var par = adpater.GetDBColumnType(t);
                    if (t == typeof(System.String))
                    {
                        par = string.Format(par, 500);
                    }
                    parames += adpater.SpParameFormat(key, par, false);
                }
            }
            if (dbContext.DBHelper.OutParams != null)
            {
                foreach (var p in dbContext.DBHelper.OutParams)
                {
                    string key = p.Key;
                    var t = p.Value.GetType();
                    var par = adpater.GetDBColumnType(t);
                    parames += adpater.SpParameFormat(key, par, true);
                }
            }
            if (parames.Length > 0)
            {
                parames = "(" + parames.Substring(0, parames.Length - 1) + ")";
            }
            template = template.Replace("{parame}", parames);
            if (templateParame != null)
            {
                foreach (var item in templateParame)
                {
                    var value = item.Value;
                    value = value.Replace("'", "''");//单引号过滤
                    template = template.Replace("{" + item.Key + "}", value);
                }
            }

            template = adpater.GetCreateSpScript(procedureName, template);
            return template;
        }
        #endregion
    }
}
