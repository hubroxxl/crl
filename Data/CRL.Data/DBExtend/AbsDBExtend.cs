﻿/**
* CRL
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CRL.Core;
using CRL.Data.DBAccess;
using CRL.Data.LambdaQuery;
//using MongoDB.Driver.Linq;

namespace CRL.Data
{
    public abstract partial class AbsDBExtend : IAbsDBExtend
    {
        /// <summary>
        /// 构造DBExtend
        /// </summary>
        /// <param name="_dbContext"></param>
        public AbsDBExtend(DbContextInner _dbContext)
        {
            dbContext = _dbContext;
            var _helper = _dbContext.DBHelper;
            if (_helper == null)
            {
                throw new Exception("数据访问对象未实例化,请实现SettingConfig.GetDbAccess");
            }
            GUID = Guid.NewGuid();
            __DbHelper = _helper;
        }
        protected DBHelper GetDBHelper(DataAccessType accessType = DataAccessType.Default)
        {
            if (!SettingConfig.UseReadSeparation)//不使用主从时
            {
                FillParame(__DbHelper);
                return __DbHelper;
            }
            var _useTransactionScope = CallContext.GetData<bool>(Base.UseTransactionScopeName);
            if (_useTransactionScope)//使用TransactionScope
            {
                FillParame(__DbHelper);
                return __DbHelper;
            }
            var _useCRLContext = CallContext.GetData<bool>(Base.UseCRLContextFlagName);
            if (_useCRLContext)//对于数据库事务,只创建一个上下文
            {
                FillParame(__DbHelper);
                return __DbHelper;
            }
            var db = dbContext.GetDBHelper(accessType);
            FillParame(db);
            __DbHelper = db;
            return db;
        }
        /// <summary>
        /// 创建当前数据库类型查询
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <returns></returns>
        protected abstract ILambdaQuery<TModel> CreateLambdaQuery<TModel>() ;
        #region 属性
        /// <summary>
        /// 数据库架构类型
        /// </summary>
        internal DataBaseArchitecture DataBaseArchitecture
        {
            get
            {
                return dbContext.DataBaseArchitecture;
            }
        }
        ///// <summary>
        ///// 对象被更新时,是否通知缓存服务器
        ///// </summary>
        //internal bool OnUpdateNotifyCacheServer
        //{
        //    get;
        //    set;
        //}
        internal enum TranStatus
        {
            未开始,
            已开始
        }
        /// <summary>
        /// 事务状态
        /// </summary>
        internal TranStatus currentTransStatus = TranStatus.未开始;
        internal Guid GUID;

        AbsDBExtend backgroundDBExtend;
        /// <summary>
        /// 仅用来检查表结构
        /// </summary>
        /// <returns></returns>
        internal AbsDBExtend GetBackgroundDBExtend()
        {
            if (backgroundDBExtend == null)
            {
                backgroundDBExtend = copyDBExtend();
            }
            return backgroundDBExtend;
        }
        internal AbsDBExtend copyDBExtend()
        {
            var helper = DBConfigRegister.GetDBHelper(dbContext.DBLocation);
            var dbContext2 = new DbContextInner(helper, dbContext.DBLocation);
            //dbContext2.ShardingMainDataIndex = dbContext.ShardingMainDataIndex;
            dbContext2.UseSharding = dbContext.UseSharding;
            return DBExtendFactory.CreateDBExtend(dbContext2);
        }

        internal DBHelper __DbHelper;
        internal void CloseConn(bool a)
        {
            __DbHelper.CloseConn(a);
        }
        //internal string DatabaseName
        //{
        //    get
        //    {
        //        return __DbHelper.DatabaseName;
        //    }
        //}
        /// <summary>
        /// 库名
        /// </summary>
        internal string DatabaseName
        {
            get
            {
                if (!string.IsNullOrEmpty(__DbHelper.ConnectionString))
                {
                    return StringHelper.EncryptMD5(__DbHelper.ConnectionString);
                }
                return __DbHelper.DatabaseName;
                //return StringHelper.EncryptMD5(__DbHelper.ConnectionString);
            }
        }
        DBAdapter.DBAdapterBase __DBAdapter;
        /// <summary>
        /// 当前数据库适配器
        /// </summary>
        internal DBAdapter.DBAdapterBase _DBAdapter
        {
            get
            {
                //return Base.CurrentDBAdapter;
                if (__DBAdapter == null)
                {
                    __DBAdapter = DBAdapter.DBAdapterBase.GetDBAdapterBase(dbContext);
                }
                return __DBAdapter;
            }
        }

        /// <summary>
        /// lockObj
        /// </summary>
        static protected object lockObj = new object();
        public DbContextInner dbContext;
        #endregion
        /// <summary>
        /// 检测数据
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="checkRepeated"></param>
        internal void CheckData(object obj, bool checkRepeated)
        {
            //var types = TypeCache.GetProperties(obj.GetType(), true).Values;
            //var types = TypeCache.GetTable(obj.GetType()).Fields;
            string msg = "";
            //var sb = new StringBuilder();
            ////检测数据约束
            //foreach (Attribute.FieldInnerAttribute p in types)
            //{
            //    string value = p.GetValue(obj) + "";
            //    if (!string.IsNullOrEmpty(value) && p.MemberName != "AddTime" && checkRepeated)
            //    {
            //        sb.Append(value.GetHashCode().ToString());
            //    }
            //    if (p.PropertyType == typeof(System.String))
            //    {
            //        if (p.NotNull && string.IsNullOrEmpty(value))
            //        {
            //            msg = string.Format("对象{0}属性{1}值不能为空", obj.GetType(), p.MemberName);
            //            throw new Exception(msg);
            //        }
            //        if (value.Length > p.Length && p.Length < 3000)
            //        {
            //            msg = string.Format("对象{0}属性{1}长度超过了设定值{2}[{3}]", obj.GetType(), p.MemberName, p.Length, value);
            //            throw new Exception(msg);
            //        }
            //    }
            //}
            //if (checkRepeated)
            //{
            //    //string concurrentKey = "insertRepeatedCheck_" + StringHelper.EncryptMD5(sb.ToString());
            //    string concurrentKey = "insertRepeatedCheck_" + sb.ToString().GetHashCode();
            //    if (!ConcurrentControl.Check(concurrentKey, 1))
            //    {
            //        throw new Exception("检测到有重复提交的数据,在" + obj.GetType());
            //    }
            //}
            //校验数据
            if (obj is IModel)
            {
                msg = (obj as IModel).CheckData();
            }
            if (!string.IsNullOrEmpty(msg))
            {
                msg = string.Format("数据校验证失败,在类型{0} {1} 请核对校验规则", obj.GetType(), msg);
                throw new Exception(msg);
            }
        }

        #region 更新缓存

        /// <summary>
        /// 按表达式更新缓存中项
        /// 当前类型有缓存时才会进行查询
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="expression"></param>
        /// <param name="c"></param>
        internal void UpdateCacheItem<TModel>(Expression<Func<TModel, bool>> expression, ParameCollection c) 
        {
            if (!SettingConfig.UpdateModelCache)//ef 扩展不能调用此方法
            {
                return;
            }
            if (DataBaseArchitecture == DataBaseArchitecture.NotRelation)
            {
                //非关系型没有缓存
                return;
            }
            //事务开启不执行查询
            if (currentTransStatus == TranStatus.已开始)
            {
                return;
            }
            Type type = typeof(TModel);
            var updateModel = MemoryDataCache.CacheService.GetCacheTypeKey(typeof(TModel), __DbHelper.DatabaseName);
            var db = copyDBExtend();//使用新的访问对象
            foreach (var key in updateModel)
            {
                var list = db.QueryList<TModel>(expression);
                foreach (var item in list)
                {
                    MemoryDataCache.CacheService.UpdateCacheItem(key, item, c);
                    //NotifyCacheServer(item);//远端缓存暂无法更新
                }
            }
        }
        /// <summary>
        /// 更新缓存中的一项
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="newObj"></param>
        /// <param name="c"></param>
        /// <param name="checkInsert"></param>
        internal void UpdateCacheItem<TModel>(TModel newObj, ParameCollection c, bool checkInsert = false) 
        {
            if (DataBaseArchitecture == DataBaseArchitecture.NotRelation)
            {
                //非关系型没有缓存
                return;
            }
            var updateModel = MemoryDataCache.CacheService.GetCacheTypeKey(typeof(TModel), __DbHelper.DatabaseName);
            foreach (var key in updateModel)
            {
                MemoryDataCache.CacheService.UpdateCacheItem(key, newObj, c, checkInsert);
            }
            //Type type = typeof(TModel);
            //if (TypeCache.ModelKeyCache.ContainsKey(type))
            //{
            //    string key = TypeCache.ModelKeyCache[type];
            //    MemoryDataCache.UpdateCacheItem(key,newObj);
            //}
        }

        #endregion
        /// <summary>
        /// ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{2} {0} {1}", GUID, DatabaseName, __DbHelper.DatabaseName);
        }

        #region 参数处理
        Dictionary<string, object> _Parame = new Dictionary<string, object>();
        Dictionary<string, object> _OutParame;
        internal void FillParame(DBHelper db)
        {
            db.ClearParams();
            foreach (var kv in _Parame)
            {
                db.AddParam(kv.Key, kv.Value);
            }
            if (_OutParame != null)
            {
                foreach (var kv in _OutParame)
                {
                    db.AddOutParam(kv.Key, kv.Value);
                }
                _OutParame.Clear();
            }
            _Parame.Clear();
        }
        /// <summary>
        /// 清除参数
        /// </summary>
        public void ClearParame()
        {
            _Parame.Clear();
            if (_OutParame != null)
            {
                _OutParame.Clear();
            }
            __DbHelper.ClearParams();
            dbContext.parIndex = 1;
        }
        /// <summary>
        /// 增加参数
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public void AddParame(string name, object value)
        {
            value = ObjectConvert.CheckNullValue(value);
            //__DbHelper.AddParam(name, value);
            _Parame.Add(name, value);
        }
        public void SetParam(string name, object value)
        {
            value = ObjectConvert.CheckNullValue(value);
            _Parame[name] = value;
        }
        /// <summary>
        /// 增加输出参数
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value">对应类型任意值</param>
        public void AddOutParam(string name, object value = null)
        {
            if (_OutParame == null)
                _OutParame = new Dictionary<string, object>();
            //__DbHelper.AddOutParam(name, value);
            _OutParame.Add(name, value);
        }
        /// <summary>
        /// 获取存储过程return的值
        /// </summary>
        /// <returns></returns>
        public int GetReturnValue()
        {
            return __DbHelper.GetReturnValue();
        }
        /// <summary>
        /// 获取OUTPUT的值
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public object GetOutParam(string name)
        {
            return __DbHelper.GetOutParam(name);
        }
        /// <summary>
        /// 获取OUT值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name"></param>
        /// <returns></returns>
        public T GetOutParam<T>(string name)
        {
            var obj = GetOutParam(name);
            return ObjectConvert.ConvertObject<T>(obj);
        }
        #endregion

        #region 事务
        /// <summary>
        /// 开始事务
        /// </summary>
        public abstract void BeginTran(System.Data.IsolationLevel isolationLevel = System.Data.IsolationLevel.ReadCommitted);
        /// <summary>
        /// 回滚事务
        /// </summary>
        public abstract void RollbackTran();
        /// <summary>
        /// 提交事务
        /// </summary>
        public abstract void CommitTran();
        #endregion
        /// <summary>
        /// 检查表是否创建了
        /// </summary>
        /// <param name="type"></param>
        public abstract void CheckTableCreated(Type type);
        #region Insert
        /// <summary>
        /// 插入
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="obj"></param>
        public abstract void InsertFromObj<TModel>(TModel obj) where TModel : class;
        /// <summary>
        /// 批量插入
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="details"></param>
        /// <param name="keepIdentity"></param>
        public abstract void BatchInsert<TModel>(List<TModel> details, bool keepIdentity = false) where TModel : class;
        #endregion

        #region 删除
        /// <summary>
        /// 按完整查询删除
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public abstract int Delete<T>(ILambdaQuery<T> query);
        /// <summary>
        /// 关联删除
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TJoin"></typeparam>
        /// <param name="expression"></param>
        /// <returns></returns>
        public int Delete<TModel, TJoin>(Expression<Func<TModel, TJoin, bool>> expression)
        {
            var query = CreateLambdaQuery<TModel>();
            query.Join(expression);
            return Delete(query);
        }
        /// <summary>
        /// 按条件删除
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="expression"></param>
        /// <returns></returns>
        public int Delete<TModel>(Expression<Func<TModel, bool>> expression) 
        {
            var query = CreateLambdaQuery<TModel>();
            query.Where(expression);
            return Delete(query);
        }
        /// <summary>
        /// 按主键删除
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        public int Delete<TModel>(object id) 
        {
            var query = CreateLambdaQuery<TModel>();
            var exp = Base.GetQueryIdExpression<TModel>(id);
            query.Where(exp);
            return Delete(query);
        }
        #endregion

        #region 杂项查询
        /// <summary>
        /// 返回字典
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="sql"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        public abstract Dictionary<TKey, TValue> ExecDictionary<TKey, TValue>(string sql);
        /// <summary>
        /// 返回字典
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public abstract Dictionary<TKey, TValue> ToDictionary<TModel, TKey, TValue>(ILambdaQuery<TModel> query) ;

        /// <summary>
        /// 返回首行首列
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public abstract dynamic QueryScalar<TModel>(ILambdaQuery<TModel> query) ;
        /// <summary>
        /// 返回动态类型
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        public abstract List<dynamic> ExecDynamicList(string sql);
        /// <summary>
        /// 返回自定义类型
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        public abstract List<T> ExecList<T>(string sql) where T : class, new();
        /// <summary>
        /// 返回自定义类型
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        public abstract T ExecObject<T>(string sql) where T : class, new();
        /// <summary>
        /// 返回首行首个结果
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        public abstract object ExecScalar(string sql);
        /// <summary>
        /// 返回首行首个结果
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sql"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        public abstract T ExecScalar<T>(string sql);
        /// <summary>
        /// 执行一条语句
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="types"></param>
        /// <returns></returns>
        public abstract int Execute(string sql);
        #endregion

        #region 函数
        #region count
        /// <summary>
        /// count
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="expression"></param>
        /// <param name="compileSp"></param>
        /// <returns></returns>
        public int Count<T>(Expression<Func<T, bool>> expression, bool compileSp = false) where T : class
        {
            return GetFunction(expression, b => 0, FunctionType.COUNT, compileSp);
        }

        #endregion

        #region Min
        /// <summary>
        /// 最小值
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="expression"></param>
        /// <param name="field"></param>
        /// <param name="compileSp"></param>
        /// <returns></returns>
        public TResult Min<TResult, TModel>(Expression<Func<TModel, bool>> expression, Expression<Func<TModel, TResult>> field, bool compileSp = false) 
        {
            return GetFunction(expression, field, FunctionType.MIN, compileSp);
        }
        #endregion

        #region Max
        /// <summary>
        /// 最大值
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="expression"></param>
        /// <param name="field"></param>
        /// <param name="compileSp"></param>
        /// <returns></returns>
        public TResult Max<TResult, TModel>(Expression<Func<TModel, bool>> expression, Expression<Func<TModel, TResult>> field, bool compileSp = false) 
        {
            return GetFunction(expression, field, FunctionType.MAX, compileSp);
        }

        #endregion

        #region SUM
        /// <summary>
        /// 合计
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="expression"></param>
        /// <param name="field"></param>
        /// <param name="compileSp"></param>
        /// <returns></returns>
        public TResult Sum<TResult, TModel>(Expression<Func<TModel, bool>> expression, Expression<Func<TModel, TResult>> field, bool compileSp = false) 
        {
            return GetFunction(expression, field, FunctionType.SUM, compileSp);
        }
        //public TType Sum<TType, TModel>(Expression<Func<TModel, bool>> expression, string field, bool compileSp = false) 
        //{
        //    return GetFunction<TType, TModel>(expression, field, FunctionType.SUM, compileSp);
        //}
        #endregion

        public abstract TResult GetFunction<TResult, TModel>(Expression<Func<TModel, bool>> expression, Expression<Func<TModel, TResult>> selectField, FunctionType functionType, bool compileSp = false) ;
        #endregion


        #region QueryDynamic
        /// <summary>
        /// 返回动态对象
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public abstract List<dynamic> QueryDynamic(LambdaQuery.LambdaQueryBase query);

        ///// <summary>
        ///// 返回指定对象
        ///// </summary>
        ///// <typeparam name="TResult"></typeparam>
        ///// <param name="query"></param>
        ///// <returns></returns>
        //public abstract List<TResult> QueryResult<TResult>(LambdaQuery.LambdaQueryBase query);

        /// <summary>
        /// 按筛选返回匿名对象
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="query"></param>
        /// <param name="newExpression"></param>
        /// <returns></returns>
        public abstract List<TResult> QueryResult<TResult>(LambdaQuery.LambdaQueryBase query, NewExpression newExpression = null);
        #endregion

        #region query item
        /// <summary>
        /// 按ID查询
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        public TModel QueryItem<TModel>(object id) 
        {
            var type = typeof(TModel);
            var table = TypeCache.GetTable(type);
            string where = _DBAdapter.FieldNameFormat(table.PrimaryKey) + "=@par1";
            AddParame("par1", id);
            var query = CreateLambdaQuery<TModel>();
            query.Where(where);
            return QueryList(query).FirstOrDefault();
            //var expression = Base.GetQueryIdExpression<TModel>(id);
            //return QueryItem<TModel>(expression);
        }
        /// <summary>
        /// 查询返回单个结果
        /// 如果只查询ID,调用QueryItem(id)
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="expression"></param>
        /// <param name="idDest">是否按主键倒序</param>
        /// <param name="compileSp">是否编译成存储过程</param>
        /// <returns></returns>
        public TModel QueryItem<TModel>(Expression<Func<TModel, bool>> expression, bool idDest = true, bool compileSp = false) 
        {
            var query = CreateLambdaQuery<TModel>();
            query.Top(1);
            query.Where(expression);
            query.CompileToSp(compileSp);
            query.OrderByPrimaryKey(idDest);
            var result = QueryList<TModel>(query);
            return result.FirstOrDefault();
        }
        #endregion

        #region QueryList
        /// <summary>
        /// 使用lamada设置条件查询
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="expression"></param>
        /// <param name="compileSp">是否编译成储过程</param>
        /// <returns></returns>
        public List<TModel> QueryList<TModel>(Expression<Func<TModel, bool>> expression = null, bool compileSp = false) 
        {
            var query = CreateLambdaQuery<TModel>();
            query.Where(expression);
            query.CompileToSp(compileSp);
            return QueryList<TModel>(query);
        }


        /// <summary>
        /// 使用完整的LamadaQuery查询
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public List<TModel> QueryList<TModel>(ILambdaQuery<TModel> query) 
        {
            string key;
            var list = QueryOrFromCache<TModel>(query, out key);
            list.ForEach(b =>
            {
                if (b is IModel)
                {
                    (b as IModel).DbContext = dbContext;
                }
            });
            return list;
        }
        /// <summary>
        /// 返回多项结果
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="query"></param>
        /// <param name="cacheKey"></param>
        /// <returns></returns>
        public abstract List<TModel> QueryOrFromCache<TModel>(ILambdaQuery<TModel> query, out string cacheKey) ;

        public virtual void InvokeReaderCallback(LambdaQueryBase query, Action<IDataReader> readFunc)
        {

        }
        #endregion

        #region 存储过程
        /// <summary>
        /// 执行存储过程
        /// </summary>
        /// <param name="sp"></param>
        /// <returns></returns>
        public abstract int Run(string sp);
        /// <summary>
        /// 执行存储过程并返回动态对象
        /// </summary>
        /// <param name="sp"></param>
        /// <returns></returns>
        public abstract List<dynamic> RunDynamicList(string sp);
        /// <summary>
        /// 执行存储过程反回自定义对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sp"></param>
        /// <returns></returns>
        public abstract List<T> RunList<T>(string sp) where T : class, new();
        /// <summary>
        /// 执行存储过程并返回首个对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sp"></param>
        /// <returns></returns>
        public abstract T RunObject<T>(string sp) where T : class, new();
        /// <summary>
        /// 执行存储过程返回首行首列
        /// </summary>
        /// <param name="sp"></param>
        /// <returns></returns>
        public abstract object RunScalar(string sp);

        #endregion

        #region Update
        /// <summary>
        /// 关联更新
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <typeparam name="TJoin"></typeparam>
        /// <param name="expression"></param>
        /// <param name="updateValue"></param>
        /// <returns></returns>
        public int Update<TModel, TJoin>(System.Linq.Expressions.Expression<Func<TModel, TJoin, bool>> expression, ParameCollection updateValue)
        {
            var query = CreateLambdaQuery<TModel>();
            query.Join(expression);
            return Update(query, updateValue);
        }
        /// <summary>
        /// 使用完整查询更新
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="query"></param>
        /// <param name="updateValue"></param>
        /// <returns></returns>
        public abstract int Update<TModel>(ILambdaQuery<TModel> query, ParameCollection updateValue) ;
        /// <summary>
        /// 指定条件和参数进行更新
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="expression"></param>
        /// <param name="setValue"></param>
        /// <returns></returns>
        public int Update<TModel>(Expression<Func<TModel, bool>> expression, ParameCollection setValue) 
        {
            var query = CreateLambdaQuery<TModel>();
            query.Where(expression);
            var n = Update(query, setValue);
            UpdateCacheItem<TModel>(expression, setValue);
            return n;
        }

        /// <summary>
        /// 指定条件并按对象差异更新
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="expression"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public int Update<TModel>(Expression<Func<TModel, bool>> expression, TModel model) 
        {
            var c = GetUpdateField(model);
            if (c.Count == 0)
            {
                return 0;
                //throw new CRLException("更新集合为空");
            }
            var n = Update(expression, c);
            if (model is IModel)
            {
                (model as IModel).CleanChanges();
            }
            return n;
        }

        /// <summary>
        /// 按匿名对象更新
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="expression"></param>
        /// <param name="updateValue"></param>
        /// <returns></returns>
        public int Update<TModel>(Expression<Func<TModel, bool>> expression, dynamic updateValue) 
        {
            var properties = updateValue.GetType().GetProperties();
            var c = new ParameCollection();
            foreach (var p in properties)
            {
                c.Add(p.Name, p.GetValue(updateValue));
            }
            return Update(expression, c);
        }

        /// <summary>
        /// 按对象差异更新
        /// </summary>
        /// <typeparam name="TModel"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int Update<TModel>(TModel obj)
        {
            ParameCollection c;
            var trackingDiff = GetTrackingDiff(obj);
            if (trackingDiff.Any())
            {
                c = trackingDiff;
            }
            else
            {
                c = GetUpdateField(obj);
            }
            if (c.Count == 0)
            {
                return 0;
                //throw new CRLException("更新集合为空");
            }
            var primaryKey = TypeCache.GetTable(obj.GetType()).PrimaryKey;
            var keyValue = primaryKey.GetValue(obj);
            var query = CreateLambdaQuery<TModel>();
            var exp = Base.GetQueryIdExpression<TModel>(keyValue);
            query.Where(exp);
            var n = Update(query, c);
            UpdateCacheItem(obj, c);
            if (n == 0)
            {
                //throw new Exception("更新失败,找不到主键为 " + keyValue + " 的记录");
            }
            //obj.CleanChanges();
            return n;
        }
		#region modelTracking
		internal Dictionary<string, ModelTracking> modelTrackings;
        public void BeginTracking<TModel>(TModel obj)
        {
            modelTrackings = modelTrackings ?? new Dictionary<string, ModelTracking>();
            var table = TypeCache.GetTable(typeof(TModel));
            var primaryKey = table.PrimaryKey;
            var keyValue = primaryKey.GetValue(obj);
            var modelKey = $"{table.Type}_{keyValue}";
            modelTrackings[modelKey] = new ModelTracking(obj);
        }
        public void BeginTracking<TModel>(List<TModel> objs)
        {
            modelTrackings = modelTrackings ?? new Dictionary<string, ModelTracking>();
            foreach (var obj in objs)
            {
                BeginTracking(obj);
            }
        }
        internal ParameCollection GetTrackingDiff<T>(T obj)
        {
            var table = TypeCache.GetTable(obj.GetType());
            var primaryKey = table.PrimaryKey;
            var keyValue = primaryKey.GetValue(obj);
            var modelKey = $"{table.Type}_{keyValue}";
            modelTrackings = modelTrackings ?? new Dictionary<string, ModelTracking>();
            modelTrackings.TryGetValue(modelKey, out var tracking);
            var diff = tracking?.GetDiff() ?? new ParameCollection();
            modelTrackings.Remove(modelKey);
            return diff;
        }
		#endregion
		/// <summary>
		/// 批量更新
		/// </summary>
		/// <typeparam name="TModel"></typeparam>
		/// <param name="objs"></param>
		/// <returns></returns>
		public abstract int Update<TModel>(List<TModel> objs) ;
        public abstract int Update<TModel>(BatchUpdate<TModel> batchUpdate);

		public virtual void InsertOrUpdate<TModel>(List<TModel> items, InsertOrUpdateOption option = null)
        {
            throw new NotImplementedException();
        }
        #endregion


        internal ParameCollection GetUpdateField<TModel>(TModel obj) 
        {
            ParameCollection c = new ParameCollection();
            if (obj is IModel)
            {
                c = (obj as IModel).GetUpdateField(_DBAdapter);//手动指定变更
                if (c.Count > 0)
                {
                    return c;
                }
            }

            var fields = TypeCache.GetProperties(typeof(TModel), true);//全量更新
            foreach (var f in fields.Values)
            {
                if (f.IsPrimaryKey)
                {
                    continue;
                }
                var v = f.GetValue(obj);
                if (v is bool || v is Enum)
                {
                    if (_DBAdapter.DBType != DBType.NPGSQL || v is Enum)
                    {
                        v = Convert.ToInt32(v);
                    }
                }
                c.Add(f.MemberName, v);
            }
            return c;

        }
        ///// <summary>
        ///// 获取Mongo查询
        ///// </summary>
        ///// <typeparam name="TModel"></typeparam>
        ///// <returns></returns>
        //public abstract IMongoQueryable<TModel> GetMongoQueryable<TModel>();
        public virtual void CreateTableIndex<TModel>() where TModel : class
        {
            var type = typeof(TModel);
            ModelCheck.CheckIndexExists(type, this);
        }

        public virtual void DropTable<TModel>() 
        {
            var table = TypeCache.GetTable(typeof(TModel));
            try
            {
                Execute($"drop table {_DBAdapter.KeyWordFormat(table.TableName)}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }

}
