﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CRL.Core;
using CRL.Data.DBAccess;
using CRL.Data.LambdaQuery;
//using MongoDB.Driver.Linq;

namespace CRL.Data
{
    public abstract partial class AbsDBExtend
    {
        public abstract Task<List<TModel>> QueryOrFromCacheAsync<TModel>(ILambdaQuery<TModel> query);

        public abstract Task<List<TResult>> QueryResultAsync<TResult>(LambdaQueryBase query, NewExpression newExpression = null);

        public abstract Task<List<dynamic>> QueryDynamicAsync(LambdaQueryBase query);

        public async Task<List<TModel>> QueryListAsync<TModel>(ILambdaQuery<TModel> query)
        {
            var list = await QueryOrFromCacheAsync(query);
            list.ForEach(b =>
            {
                if (b is IModel)
                {
                    (b as IModel).DbContext = dbContext;
                }
            });
            return list;
        }
        public virtual Task<int> ExecuteAsync(string sql)
        {
            throw new NotImplementedException();
        }
        public virtual Task<int> DeleteAsync<T>(ILambdaQuery<T> query)
        {
            throw new NotImplementedException();
        }
        public virtual Task<int> UpdateAsync<TModel>(ILambdaQuery<TModel> query, ParameCollection updateValue)
        {
            throw new NotImplementedException();
        }
        public virtual Task InsertFromObjAsync<TModel>(TModel obj) where TModel : class
        {
            throw new NotImplementedException();
        }
        public virtual Task BatchInsertAsync<TModel>(List<TModel> details, bool keepIdentity = false) where TModel : class
        {
            throw new NotImplementedException();
        }
    }
}
