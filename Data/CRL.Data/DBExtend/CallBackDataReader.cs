﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CRL.Data
{
    /// <summary>
    /// 可回调取出out参数的DataReader
    /// </summary>
    internal partial class CallBackDataReader
    {
        public IDataReader reader;
        public string Sql;
        Func<int> handler;
        public double runTime = 0;
        public CallBackDataReader(IDataReader _reader, Func<int> _handler,string sql)
        {
            reader = _reader;
            handler = _handler;
            Sql = sql;
        }
        public List<T> GetDataTResult<T>(LambdaQuery.Mapping.QueryInfo<T> queryInfo,out int outParame)
        {
            var data = ObjectConvert.DataReaderToSpecifiedList<T>(reader, queryInfo,out var includeValues);
            ObjectConvert.fillIncludeOneResult(data, queryInfo.__IncludeTypes, includeValues);
            outParame = handler?.Invoke() ?? 0;
            return data;
        }
        public List<dynamic> GetDataDynamic(out int outParame) 
        {
            double runTime;
            var data = Dynamic.DynamicObjConvert.DataReaderToDynamic(reader, out runTime);
            //reader.Close();
            outParame = handler?.Invoke() ?? 0;
            return data;
        }
    }

    internal partial class CallBackDataReader
    {
        public async Task<Tuple<List<T>, int>> GetDataTResultAsync<T>(LambdaQuery.Mapping.QueryInfo<T> queryInfo)
        {
            var data = await ObjectConvert.DataReaderToSpecifiedListAsync<T>(reader, queryInfo);
            var outParame = handler?.Invoke() ?? 0;
            return new Tuple<List<T>, int>(data, outParame);
        }
        public async Task<Tuple<List<dynamic>, int>> GetDataDynamicAsync()
        {
            var data = await Dynamic.DynamicObjConvert.DataReaderToDynamicAsync(reader);
            var outParame = handler?.Invoke() ?? 0;
            return new Tuple<List<dynamic>, int>(data, outParame);
        }
    }
}
