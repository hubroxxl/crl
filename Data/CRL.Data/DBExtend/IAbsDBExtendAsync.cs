﻿/**
* CRL
*/
using CRL.Data.LambdaQuery;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CRL.Data
{
    public partial interface IAbsDBExtend
    {
        Task InsertFromObjAsync<TModel>(TModel obj) where TModel : class;
        Task BatchInsertAsync<TModel>(List<TModel> details, bool keepIdentity = false) where TModel : class;

        Task<List<TResult>> QueryResultAsync<TResult>(LambdaQueryBase query, NewExpression newExpression = null);

        Task<int> ExecuteAsync(string sql);

        Task<int> DeleteAsync<T>(ILambdaQuery<T> query);

        Task<int> UpdateAsync<TModel>(ILambdaQuery<TModel> query, ParameCollection updateValue);
    }
}
