﻿/**
* CRL
*/
using CRL.Data.DBAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRL.Data
{
    public class DBExtendFactory
    {
        public static AbsDBExtend CreateDBExtend(DbContextInner _dbContext)
        {
            return DBConfigRegister.CreateDBExtend(_dbContext);
        }
    }
}
