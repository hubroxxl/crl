﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRL.Data
{
    public class InsertOrUpdateOption
    {
        /// <summary>
        /// 更新时，指定更新的字段
        /// </summary>
        public List<string> UpdateMemberNames { get; set; }
        /// <summary>
        /// 记录存在时不更新
        /// </summary>
        public bool IfExistsNotUpdate { get; set; }
        /// <summary>
        /// 记录唯一判断字段,仅mssql
        /// </summary>
        public string ConstraintMemberName { get; set; }
        public string SqlOut { get; set; }
        /// <summary>
        /// 分割量
        /// </summary>
        public int BatchTake { get; set; }
    }
}
