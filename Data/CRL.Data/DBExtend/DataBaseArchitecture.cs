﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRL.Data
{
    /// <summary>
    /// 数据库架构
    /// </summary>
    internal enum DataBaseArchitecture
    {
        /// <summary>
        /// 关系型
        /// </summary>
        Relation,
        /// <summary>
        /// 非关系型
        /// </summary>
        NotRelation
    }
}
