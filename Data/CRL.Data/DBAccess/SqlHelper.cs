﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CRL.Data.DBAccess
{
    public partial class SqlHelper : DBHelper
    {
        /// <summary>
        /// 根据参数类型实例化
        /// </summary>
        /// <param name="_connectionString">内容</param>
		public SqlHelper(DbProviderFactory dbFactory, DBAccessBuild dBAccessBuild)
            : base(dbFactory, dBAccessBuild)
        { }

        /// <summary>
        /// 根据表插入记录,dataTable需按查询生成结构
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="tableName"></param>
        /// <param name="keepIdentity"></param>
        public void InsertFromDataTable(DataTable dataTable, bool keepIdentity = false)
        {
            SqlBulkCopy sqlBulkCopy;
            var option = keepIdentity ? SqlBulkCopyOptions.KeepIdentity : SqlBulkCopyOptions.KeepNulls;
            option = option | SqlBulkCopyOptions.FireTriggers;
            if (_trans != null)
            {
                SqlTransaction sqlTrans = _trans as SqlTransaction;
                sqlBulkCopy = new SqlBulkCopy(currentConn as SqlConnection, option, sqlTrans);
            }
            else
            {
                GetOrCreateConn();
                sqlBulkCopy = new SqlBulkCopy(currentConn as SqlConnection, option, null);
            }
            sqlBulkCopy.DestinationTableName = dataTable.TableName;
            sqlBulkCopy.BatchSize = dataTable.Rows.Count;
            if (dataTable != null && dataTable.Rows.Count != 0)
            {
                sqlBulkCopy.WriteToServer(dataTable);
            }
            sqlBulkCopy.Close();
        }
    }
}
