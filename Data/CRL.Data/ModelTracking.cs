﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace CRL.Data
{
    public class ModelTracking
    {
        internal object origin;
        Dictionary<string,int> originValues;
        public ModelTracking(object obj)
        {
            obj.CheckNull("obj");
            origin = obj;
            originValues = new  Dictionary<string, int>();
            var fields = TypeCache.GetTable(origin.GetType()).Fields;
            foreach (var f in fields)
            {
                var v = f.GetValue(origin);
                var hash = v == null ? 0 : v.GetHashCode();
                originValues[f.MemberName] = hash;
            }
        }
        public ParameCollection GetDiff()
        {
            var newValues = new ParameCollection();
            var fields = TypeCache.GetTable(origin.GetType()).Fields;
            foreach (var f in fields)
            {
                var v = f.GetValue(origin);
                newValues[f.MemberName] = v;
            }
            foreach (var kv in new ParameCollection(newValues))
            {
                if (originValues[kv.Key] == (kv.Value == null ? 0 : kv.Value.GetHashCode()))
                {
                    newValues.Remove(kv.Key);
                }
            }
            return newValues;
        }
    }
}
