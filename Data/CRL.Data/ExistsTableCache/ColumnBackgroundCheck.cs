﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.Concurrent;
using CRL.Core;

namespace CRL.Data.ExistsTableCache
{
    internal class ColumnBackgroundCheck
    {
        static ConcurrentDictionary<string, AbsDBExtend> dBExtends = new ConcurrentDictionary<string, AbsDBExtend>();
        //static object lockObj = new object();
        static ConcurrentDictionary<Type, string> needCheks = new ConcurrentDictionary<Type, string>();
        static System.Timers.Timer timer;
        public static void Add(AbsDBExtend dBExtend, Type type)
        {
            var dbName = dBExtend.DatabaseName;
            //lock (lockObj)
            //{
                if (!dBExtends.ContainsKey(dbName))
                {
                    dBExtends.TryAdd(dbName, dBExtend);
                }
                if (!needCheks.ContainsKey(type))
                {
                    needCheks.TryAdd(type, dbName);
                }
            //}
            if (timer == null)
            {
                timer = new System.Timers.Timer(15000);
                timer.Elapsed += (a, b) =>
                {
                    DoWatch();
                };
                timer.Start();
            }
        }
        static void DoWatch()
        {
            #region watch
            try
            {
                DoCheck();
            }
            catch(Exception ero)
            {
                EventLog.Log("检查列时发生错误:"+ero,"ColumnCheck");
            }
            #endregion
        }
        static void DoCheck()
        {
            if (needCheks.Count == 0)
            {
                return;
            }
            var list = new Dictionary<Type, string>(needCheks);
            foreach(var item in list)
            {
                var db = dBExtends[item.Value];//todo 线程安全,对象在别的地方被使用过了,导致异常
                //var table = TypeCache.GetTable(item.Key);
                var _DBAdapter = DBAdapter.DBAdapterBase.GetDBAdapterBase(db.dbContext);
                var tableName = TypeCache.GetTableName(item.Key, db.dbContext);
                var dbFileds = ModelCheck.GetDbFields(db, tableName);

                var needCreates = ModelCheck.CheckNotExistsColumns(item.Key, dbFileds);
                if (needCreates.Any())
                {
                    Console.WriteLine($"后台检查字段 {tableName} needCreate {needCreates.Count}");
                }
                //var model = System.Activator.CreateInstance(item.Key) as IModel;
                foreach (var field in needCreates)
                {
                    //ModelCheck.SetColumnDbType(_DBAdapter, field);
                    ModelCheck.CreateColumn(db, field, true);
                }
                string val;
                needCheks.TryRemove(item.Key, out val);
            }
        }
        public static void Stop()
        {
            if (timer != null)
            {
                timer.Stop();
            }
            timer = null;
        }
    }
}
