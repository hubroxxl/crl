﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRL.Data.ExistsTableCache
{
    [Serializable]
    internal class DataBase
    {
        public string Name
        {
            get;
            set;
        }
        public Dictionary<string,Table> Tables
        {
            get;
            set;
        }
    }
}
