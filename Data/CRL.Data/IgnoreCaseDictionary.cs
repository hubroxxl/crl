﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CRL.Data
{
    /// <summary>
    /// 不区分大小的字典
    /// </summary>
    public class IgnoreCaseDictionary<T> : Dictionary<string, T>
    {
        public IgnoreCaseDictionary() : base(StringComparer.OrdinalIgnoreCase)
        {

        }
        public IgnoreCaseDictionary(IDictionary<string, T> dic)
            : base(dic, StringComparer.OrdinalIgnoreCase)
        {
        }
        
    }

    /// <summary>
    /// 键值的集合,不区分大小写
    /// 如果不需要以参数形式处理,名称前加上$ 如 c2["$SoldCount"]="SoldCount+" + num;
    /// </summary>
    public class ParameCollection : IgnoreCaseDictionary<object>
    {
        public ParameCollection()
        {
        }
        public ParameCollection(IDictionary<string, object> dic)
            : base(dic)
        {
        }
    }
    public class ConcurrentDictionaryEx<T,T2> : System.Collections.Concurrent.ConcurrentDictionary<T, T2>
    {

    }
}
