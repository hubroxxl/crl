﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Linq.Expressions;
using System.Collections;
using CRL.Data.LambdaQuery;
namespace CRL.Data
{
    
    /// <summary>
    /// 查询扩展方法,请引用CRL命名空间
    /// </summary>
    public static partial class ExtensionMethod
    {
        /// <summary>
        /// 字符串格式化
        /// </summary>
        /// <param name="source"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string Format(this string source, params object[] args)
        {
            return string.Format(source, args);
        }
        /// <summary>
        /// 对集合进行分页
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="index"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public static IEnumerable<T> Page<T>(this IEnumerable<T> source, int index, int pageSize) where T : class, new()
        {
            return source.Skip((index - 1) * pageSize).Take(pageSize);
        }

        #region IEnumerable Find
        /// <summary>
        /// Find
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static T Find<T>(this IEnumerable<T> source,Func<T, bool> predicate)
        {
            return source.Where(predicate).FirstOrDefault();
        }
        /// <summary>
        /// FindAll
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public static List<T> FindAll<T>(this IEnumerable<T> source, Func<T, bool> predicate)
        {
            return source.Where(predicate).ToList();
        }
        #endregion

        /// <summary>
        /// 判断类型是否为集合类型
        /// </summary>
        /// <param name="type">要处理的类型</param>
        /// <returns>是返回True，不是返回False</returns>
        public static bool IsEnumerable(this Type type)
        {
            if (type == typeof(string))
            {
                return false;
            }
            return typeof(IEnumerable).IsAssignableFrom(type);
        }
        /// <summary>
        /// 判断值为空
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="name"></param>
        public static void CheckNull<T>(this T obj, string name, string message = "")
        {
            bool isNull;
            if (typeof(T) == typeof(string))
            {
                var str = obj as string;
                isNull = string.IsNullOrEmpty(str);
            }
            else
            {
                isNull = obj == null;
            }
            if (isNull)
            {
                message = $"参数:{name}为空 {message}";
                throw new ArgumentNullException(name, message);
            }
        }
    }

    
}
