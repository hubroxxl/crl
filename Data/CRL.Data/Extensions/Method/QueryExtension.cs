﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRL.Data.LambdaQuery;
using System.Linq.Expressions;
namespace CRL.Data
{
    public static partial class ExtensionMethod
    {
        /// <summary>
        /// 按完整查询条件进行删除
        /// goup语法不支持
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public static int Delete<T>(this LambdaQuery<T> query) where T : class
        {
            var db = DBExtendFactory.CreateDBExtend(query.__DbContext);
            return db.Delete(query);
        }
        /// <summary>
        /// 按完整查询条件进行更新
        /// goup语法不支持
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="updateValue"></param>
        /// <returns></returns>
        public static int Update<T>(this LambdaQuery<T> query, ParameCollection updateValue) where T : class
        {
            var db = DBExtendFactory.CreateDBExtend(query.__DbContext);
            return db.Update(query, updateValue);
        }
        //static DbContext getDbContext<T>(string manageName)
        //{
        //    var dbLocation = new DBLocation() { DataAccessType = DataAccessType.Default, ManageType = typeof(T), ManageName = manageName };
        //    var helper = SettingConfig.GetDBAccessBuild(dbLocation).GetDBHelper();
        //    var dbContext = new DbContext(helper, dbLocation);
        //    return dbContext;
        //}
        /// <summary>
        /// 保存更改
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static int Save<T>(this T obj) where T : IModel, new()
        {
            obj.DbContext.CheckNull("DbContext");
            //var dbContext = getDbContext<T>(obj.DbContext.DBLocation.ManageName);
            var db = DBExtendFactory.CreateDBExtend(obj.DbContext);
            return db.Update(obj);
        }
        /// <summary>
        /// 删除
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static int Delete<T>(this T obj) where T : IModel, new()
        {
            obj.DbContext.CheckNull("DbContext");
            //var dbContext = getDbContext<T>(obj.DbContext.DBLocation.ManageName);
            var db = DBExtendFactory.CreateDBExtend(obj.DbContext);
            return db.Delete<T>(TypeCache.GetpPrimaryKeyValue(obj));
        }
    }
}
