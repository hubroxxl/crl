﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRL.Data
{
    public static partial class ExtensionMethod
    {
        #region 查询方法
        /// <summary>
        /// Like("%key%")
        /// 如果包函%通配符,则按通配符算
        /// </summary>
        /// <param name="s"></param>
        /// <param name="likeString"></param>
        /// <returns></returns>
        public static bool Like(this string s, string likeString)
        {
            if (string.IsNullOrEmpty(likeString))
                throw new Exception("参数值不能为空:likeString");
            return s.IndexOf(likeString) > -1;
        }
        /// <summary>
        /// Like("%key")
        /// </summary>
        /// <param name="s"></param>
        /// <param name="likeString"></param>
        /// <returns></returns>
        public static bool LikeLeft(this string s, string likeString)
        {
            if (string.IsNullOrEmpty(likeString))
                throw new Exception("参数值不能为空:likeString");
            return s.IndexOf(likeString) > -1;
        }
        /// <summary>
        /// Like("key%")
        /// </summary>
        /// <param name="s"></param>
        /// <param name="likeString"></param>
        /// <returns></returns>
        public static bool LikeRight(this string s, string likeString)
        {
            if (string.IsNullOrEmpty(likeString))
                throw new Exception("参数值不能为空:likeString");
            return s.IndexOf(likeString) > -1;
        }

        /// <summary>
        /// 按索引和长度检索字符串
        /// </summary>
        /// <param name="s"></param>
        /// <param name="index"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static string Substring(this String s, int index, int length)
        {
            return "";
        }

        /// <summary>
        /// 表示count(字段)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="origin"></param>
        /// <returns></returns>
        public static int COUNT<T>(this T origin)
        {
            return 0;
        }

        /// <summary>
        /// 表示Distinct(字段)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="origin"></param>
        /// <returns></returns>
        public static T DistinctField<T>(this T origin)
        {
            return origin;
        }
        /// <summary>
        /// 表示Count(Distinct(字段))
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="origin"></param>
        /// <returns></returns>
        public static int DistinctCount<T>(this T origin)
        {
            return 0;
        }
        #endregion
    }
}
