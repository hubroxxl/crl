﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Linq.Expressions;
using System.Collections;
using CRL.Data.LambdaQuery;
namespace CRL.Data
{
    public static partial class ExtensionMethod
    {
        /// <summary>
        /// 表示len此字段
        /// </summary>
        /// <param name="origin"></param>
        /// <returns></returns>
        public static int Len(this object origin)
        {
            if (origin == null)
            {
                return 0;
            }
            return origin.ToString().Length;
        }
    }
}
