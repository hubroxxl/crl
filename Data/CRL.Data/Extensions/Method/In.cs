﻿/**
* CRL
*/
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Linq.Expressions;
using System.Collections;
using CRL.Data.LambdaQuery;
using System;
namespace CRL.Data
{
    public static partial class ExtensionMethod
    {
        /// <summary>
        /// 表示in
        /// </summary>
        /// <param name="origin"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public static bool In(this string origin, string values)
        {
            return values.Contains(origin);
        }
        /// <summary>
        /// 表示in
        /// </summary>
        /// <param name="origin"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public static bool In(this string origin, params string[] values)
        {
            return values.Contains(origin);
        }
        /// <summary>
        /// 表示in
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="origin"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public static bool In<T>(this T origin, params T[] values) where T : struct
        {
            return values.Contains(origin);
        }
        /// <summary>
        /// 表示in
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="origin"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public static bool In<T>(this T? origin, params T[] values) where T : struct
        {
            return values.Contains(origin.Value);
        }
        //public static bool In<T>(this T t, IEnumerable<T> c)
        //{
        //    return c.Contains(t);
        //}
    }
}
