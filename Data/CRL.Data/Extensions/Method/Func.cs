﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Linq.Expressions;
using System.Collections;
using CRL.Data.LambdaQuery;
namespace CRL.Data
{
    public static partial class ExtensionMethod
    {
        /// <summary>
        /// 表示函数格式化
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="field"></param>
        /// <param name="format">like cast({0} as datetime)</param>
        /// <returns></returns>
        public static TResult FuncFormat<TResult>(this DateTime field,string format)
        {
            return default(TResult);
        }
        public static TResult FuncFormat<TResult>(this string field, string format)
        {
            return default(TResult);
        }
        public static TResult FuncFormat<TResult>(this int field, string format)
        {
            return default(TResult);
        }
        public static TResult FuncFormat<TResult>(this double field, string format)
        {
            return default(TResult);
        }
        public static TResult FuncFormat<TResult>(this decimal field, string format)
        {
            return default(TResult);
        }
        public static TResult FuncFormat<TResult>(this float field, string format)
        {
            return default(TResult);
        }
        public static TResult FuncFormat<TResult>(this Guid field, string format)
        {
            return default(TResult);
        }
        /// <summary>
        /// 分割字符串取第一组
        /// </summary>
        /// <param name="field"></param>
        /// <param name="par"></param>
        /// <returns></returns>
        public static string SplitFirst(this string field, string par)
        {
            return field;
        }

        //public static string GetResult(this IFuncExpression obj)
        //{
        //    return default(string);
        //}
        //public static TResult GetResult<T,TResult>(this ICaseExpression2<T, TResult> obj)
        //{
        //    return default(TResult);
        //}
        //public static TResult GetResult<T, TResult>(this IWindowFuncExpression<T, TResult> obj)
        //{
        //    return default(TResult);
        //}
    }
}
