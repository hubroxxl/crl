﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using CRL.Core;
using System.Reflection;
using CRL.Data;
using System.Collections.Concurrent;

namespace CRL.Data.LambdaQuery
{
    public partial class ExpressionVisitor
    {
        CRLExpression.CRLExpression ConstantExpressionHandler(Expression exp, ExpressionType? nodeType = null, bool firstLevel = false)
        {
            #region 常量
            ConstantExpression cExp = (ConstantExpression)exp;
            object returnValue;
            object originData = null;
            if (cExp.Value == null)
            {
                returnValue = null;
            }
            else
            {
                if (cExp.Value is bool)
                {
                    if (__DBAdapter.DBType == DBAccess.DBType.NPGSQL)
                    {
                        returnValue = cExp.Value;
                    }
                    else
                    {
                        returnValue = Convert.ToInt32(cExp.Value);
                    }
                    originData = cExp.Value;
                }
                else if (cExp.Value is Enum)
                {
                    returnValue = Convert.ToInt32(cExp.Value);
                    originData = cExp.Value;
                }
                else
                {
                    returnValue = cExp.Value;
                }
            }
            return new CRLExpression.CRLExpression() { Type = CRLExpression.CRLExpressionType.Value, Data = returnValue, IsConstantValue = true, OriginData = originData };
            #endregion
        }
    }
}
