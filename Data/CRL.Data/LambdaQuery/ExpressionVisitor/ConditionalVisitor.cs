﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using CRL.Core;
using System.Reflection;
using CRL.Data;
using System.Collections.Concurrent;
using System.Xml.Linq;

namespace CRL.Data.LambdaQuery
{
    public partial class ExpressionVisitor
    {
        internal CRLExpression.CRLExpression ConditionalExpressionHandler(Expression exp, ExpressionType? nodeType = null, bool firstLevel = false)
        {
            var condExp = exp as ConditionalExpression;
            var sb=new StringBuilder();
            sb.Append("case when ");
            sb.Append(lambdaQueryBase.FormatExpression(condExp.Test).SqlOut);
            sb.Append(" then ");
            sb.Append(lambdaQueryBase.FormatExpression(condExp.IfTrue).SqlOut);
            sb.Append(" else ");
            sb.Append(lambdaQueryBase.FormatExpression(condExp.IfFalse).SqlOut);
            sb.Append(" end");
            return new CRLExpression.CRLExpression() { Type = CRLExpression.CRLExpressionType.Tree, Data = sb.ToString() };
        }
    }
}
