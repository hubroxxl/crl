﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using CRL.Core;
using System.Reflection;
using CRL.Data;
using System.Collections.Concurrent;

namespace CRL.Data.LambdaQuery
{
    public partial class ExpressionVisitor
    {
        CRLExpression.CRLExpression UnaryExpressionHandler(Expression exp, ExpressionType? nodeType = null, bool firstLevel = false)
        {
            #region UnaryExpression
            UnaryExpression ue = ((UnaryExpression)exp);
            if (ue.Operand is MethodCallExpression)
            {
                //方法直接下一步解析
                return RouteExpressionHandler(ue.Operand, ue.NodeType);
            }
            else if (ue.NodeType == ExpressionType.Convert)
            {
                return RouteExpressionHandler(ue.Operand);
            }
            else if (ue.Operand is MemberExpression)
            {
                MemberExpression mExp = (MemberExpression)ue.Operand;
                if (mExp.Expression.NodeType != ExpressionType.Parameter)
                {
                    return RouteExpressionHandler(ue.Operand);
                }
                var parameter = Expression.Parameter(mExp.Expression.Type, "b");
                if (ue.NodeType == ExpressionType.Not)
                {
                    var ex2 = parameter.Property(mExp.Member.Name).Equal(1);
                    return RouteExpressionHandler(ex2);
                }
                else if (ue.NodeType == ExpressionType.Convert)
                {
                    //like Convert(b.Id);
                    var ex2 = parameter.Property(mExp.Member.Name);
                    return RouteExpressionHandler(ex2);

                }
            }
            else if (ue.Operand is ConstantExpression)
            {
                return RouteExpressionHandler(ue.Operand);
            }
            throw new Exception("未处理的一元运算" + ue.NodeType);
            #endregion
        }
    }
}
