﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using CRL.Core;
using System.Reflection;
using CRL.Data;
using System.Collections.Concurrent;

namespace CRL.Data.LambdaQuery
{
    public partial class ExpressionVisitor
    {
        CRLExpression.CRLExpression NewArrayExpressionHandler(Expression exp, ExpressionType? nodeType = null, bool firstLevel = false)
        {
            #region 数组
            NewArrayExpression naExp = (NewArrayExpression)exp;
            var sb = "";
            foreach (Expression expression in naExp.Expressions)
            {
                sb += string.Format(",{0}", RouteExpressionHandler(expression));
            }
            var str = sb.Length == 0 ? "" : sb.Remove(0, 1);
            return new CRLExpression.CRLExpression() { Type = CRLExpression.CRLExpressionType.Value, Data = str };
            #endregion
        }
    }
}
