﻿/**
* CRL
*/
using CRL.Core;
using CRL.Core.Extension;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CRL.Data.LambdaQuery
{
    public abstract partial class LambdaQueryBase
    {
        #region 解析选择的字段
        internal string GetQueryFieldsString(IEnumerable<Attribute.FieldMapping> fields)
        {
            return string.Join(",", fields.Select(b => b.QueryFull));
        }
        #region 筛选缓存
        public class SelectFieldInfo
        {
            public SelectFieldInfo(List<Attribute.FieldMapping> _fields)
            {
                mapping = _fields;
            }
            public SelectFieldInfo(Attribute.FieldMapping field)
            {
                mapping = mapping ?? new List<Attribute.FieldMapping>();
                mapping.Add(field);
            }
            string queryFieldString;
            public List<Attribute.FieldMapping> mapping;
            //ParameCollection parame;
            public void Merge(SelectFieldInfo item)
            {
                mapping.AddRange(item.mapping);
            }
            public void CleanQueryFieldString()
            {
                queryFieldString = null;
            }
            public string GetQueryFieldString()
            {
                if (string.IsNullOrEmpty(queryFieldString))
                {
                    queryFieldString = string.Join(",", mapping.Select(b => b.QueryFull));
                }
                return queryFieldString;
            }

            public SelectFieldInfo Clone()
            {
                var obj = MemberwiseClone() as SelectFieldInfo;
                obj.mapping = new List<Attribute.FieldMapping>(obj.mapping);
                return obj;
            }
            //public Expression expression;
        }
        /// <summary>
        /// 使用函数格式化字段
        /// </summary>
        internal string __FieldFunctionFormat = "";
        //internal static Dictionary<string, SelectFieldInfo> _GetSelectFieldCache = new Dictionary<string, SelectFieldInfo>();
        SelectFieldInfo __CurrentSelectFieldCache;
        internal SelectFieldInfo _CurrentSelectFieldCache
        {
            get
            {
                return __CurrentSelectFieldCache;
            }
            set
            {
                __CurrentSelectFieldCache = value;
            }
        }
        //internal SelectFieldInfo _CurrentUnionSelectFieldCache;
        internal void SetSelectFiled(SelectFieldInfo info, bool overide = false)
        {
            if (_CurrentSelectFieldCache == null || overide)
            {
                _CurrentSelectFieldCache = info;
            }
            else
            {
                _CurrentSelectFieldCache.Merge(info);
            }
            //Console.WriteLine($"SetSelectFiled {info.mapping.ToJson()}");
        }
        #endregion
        /// <summary>
        /// 解析选择的字段
        /// </summary>
        /// <param name="isSelect">查询字段时按属性名生成别名</param>
        /// <param name="expressionBody"></param>
        /// <param name="withTablePrefix">是否生按表生成前辍,关联时用 如Table__Name</param>
        /// <param name="types"></param>
        /// <returns></returns>
        public SelectFieldInfo GetSelectField(bool isSelect, Expression expressionBody, bool withTablePrefix, params Type[] types)
        {
            var allFields = new Dictionary<Type, IgnoreCaseDictionary<Attribute.FieldInnerAttribute>>();
            allFields.Add(__MainType, TypeCache.GetProperties(__MainType, true));
            foreach (var t in types)
            {
                if (!allFields.ContainsKey(t))
                {
                    allFields.Add(t, TypeCache.GetProperties(t, true));
                }
            }

            if (expressionBody is ParameterExpression)//选择所有字段
            {
                var resultFields = new List<Attribute.FieldMapping>();
                var allFieldList = allFields[expressionBody.Type].Values;
                foreach (var item in allFieldList)
                {
                    if (__IsFromResultSelect)
                    {
                        if (!__ResultSelectFields.Contains(item.MapingName))
                        {
                            continue;
                        }
                    }
                    var item2 = item.GetFieldMapping(__DBAdapter, GetPrefix(item.ModelType, ""), withTablePrefix, "");
                    resultFields.Add(item2);
                }
                var selectFieldItem = new SelectFieldInfo(resultFields);
                return selectFieldItem;
            }
            else if (expressionBody is MemberInitExpression)
            {
                var memberInitExp = (expressionBody as MemberInitExpression);

                var members = new List<MemberBindingObj>();
                foreach (MemberAssignment m in memberInitExp.Bindings)
                {
                    members.Add(new MemberBindingObj() { Expression = m.Expression, Member = m.Member });
                }
                return GetNewExpressionMember(memberInitExp.Type, isSelect, allFields, withTablePrefix, members);
            }
            else if (expressionBody is NewExpression)//按匿名对象
            {
                var newExpression = expressionBody as NewExpression;
                var members = new List<MemberBindingObj>();
                for (int i = 0; i < newExpression.Arguments.Count(); i++)
                {
                    var item = newExpression.Arguments[i];
                    members.Add(new MemberBindingObj() { Expression = item, Member = newExpression.Members[i] });
                }
                return GetNewExpressionMember(newExpression.Type, isSelect, allFields, withTablePrefix, members);
            }
            else if (expressionBody is MethodCallExpression)
            {
                #region 方法
                var method = expressionBody as MethodCallExpression;
                var f = new Attribute.FieldInnerAttribute() { ModelType = __MainType, MemberName = "", PropertyType = expressionBody.Type };
                string methodMember;
                var methodQuery = getSelectMethodCall(expressionBody, out methodMember, 0);
                var f2 = f.GetFieldMapping(__DBAdapter, "", withTablePrefix, "", methodQuery);
                f2.MethodName = method.Method.Name;
                #endregion
                var selectFieldItem = new SelectFieldInfo(f2);
                return selectFieldItem;
            }
            else if (expressionBody is BinaryExpression)
            {
                var field = getSeletctBinary(expressionBody);
                var f = new Attribute.FieldInnerAttribute() { ModelType = __MainType, MemberName = "", PropertyType = expressionBody.Type };
                var f2 = f.GetFieldMapping(__DBAdapter, "", withTablePrefix, "", field);
                var selectFieldItem = new SelectFieldInfo(f2);
                return selectFieldItem;
            }
            else if (expressionBody is ConstantExpression)
            {
                var constant = (ConstantExpression)expressionBody;
                var f = new Attribute.FieldInnerAttribute() { ModelType = __MainType, MemberName = "", PropertyType = expressionBody.Type };
                var f2 = f.GetFieldMapping(__DBAdapter, "", withTablePrefix, "", constant.Value + "");
                var selectFieldItem = new SelectFieldInfo(f2);
                return selectFieldItem;
            }
            else if (expressionBody is UnaryExpression)
            {
                var unaryExpression = expressionBody as UnaryExpression;
                return GetSelectField(false, unaryExpression.Operand, withTablePrefix, types);
            }
            else if (expressionBody is MemberExpression)//按成员
            {
                #region MemberExpression
                var mExp = (MemberExpression)expressionBody;
                if (Base.CheckIfAnonymousType(mExp.Expression.Type))
                {
                    //按匿名对象属性,视图关联时用
                    var find2 = __NewExpressionFieldCaches.Find(b => b.OrgionType == mExp.Expression.Type && b.MemberName == mExp.Member.Name);
                    var _f = new Attribute.FieldInnerAttribute() { MemberName = mExp.Member.Name, PropertyType = mExp.Type };
                    var mappingName = "";
                    if (find2 != null)
                    {
                        //修正真实的字段名
                        mappingName = __DBAdapter.KeyWordFormat(find2?.MappingName);
                    }
                    var f3 = _f.GetFieldMapping(__DBAdapter, GetPrefix(mExp.Expression.Type,"", checkExists: true), withTablePrefix, mExp.Member.Name, mappingName);
                    return new SelectFieldInfo(f3);
                }
                Attribute.FieldInnerAttribute f;
                var a = allFields[mExp.Expression.Type].TryGetValue(mExp.Member.Name, out f);
                if (!a)
                {
                    throw new Exception("找不到可筛选的属性 " + mExp.Member.Name + " 在" + mExp.Expression.Type);
                }
                var f2 = f.GetFieldMapping(__DBAdapter, GetPrefix(f.ModelType, ""), withTablePrefix, "");
                return new SelectFieldInfo(f2);
                #endregion
            }
            else
            {
                throw new Exception("不支持此语法解析:" + expressionBody);
            }
        }
        class MemberBindingObj
        {
            public System.Reflection.MemberInfo Member;
            public Expression Expression;
        }
        SelectFieldInfo GetNewExpressionMember(Type originType, bool isSelect, Dictionary<Type, IgnoreCaseDictionary<Attribute.FieldInnerAttribute>> allFields, bool withTablePrefix, List<MemberBindingObj> members)
        {
            var resultFields = new List<Attribute.FieldMapping>();
            var i = -1;
            foreach (var args in members)
            {
                i += 1;
                //new { a1 = b.Name }
                var item = args.Expression;//b.Name
                var selectName = args.Member.Name;//a1

            label1:
                if (item is MethodCallExpression)//group用
                {
                    var methodCallExpression = item as MethodCallExpression;
                    string methodMember;
                    string methodQuery;
                    if (methodCallExpression.Object != null && typeof(IQuery).IsAssignableFrom(methodCallExpression.Object.Type))
                    {
                        __Visitor.MethodCallInvokeBase(methodCallExpression, out var target);
                        methodQuery = ((IQuery)target).GetQuery();
                        methodQuery = $"({methodQuery})";
                    }
                    else
                    {
                        methodQuery = getSelectMethodCall(methodCallExpression, out methodMember, i);
                    }
                    var f = new Attribute.FieldInnerAttribute() { ModelType = __MainType, MemberName = selectName, PropertyType = item.Type };
                    var f2 = f.GetFieldMapping(__DBAdapter, "", withTablePrefix, selectName, methodQuery);
                    f2.MethodName = methodCallExpression.Method.Name;
                    resultFields.Add(f2);
                    checkFieldRef(selectName, f2.QueryField, __MainType);
                }
                else if (item is BinaryExpression)
                {
                    var field = getSeletctBinary(item);
                    var f = new Attribute.FieldInnerAttribute() { ModelType = __MainType, MemberName = "", PropertyType = item.Type };
                    var f2 = f.GetFieldMapping(__DBAdapter, "", withTablePrefix, selectName, field);
                    resultFields.Add(f2);
                    checkFieldRef(selectName, f2.QueryField, __MainType);
                }
                else if (item is ConstantExpression)//常量
                {
                    var constantExpression = item as ConstantExpression;
                    var f = new Attribute.FieldInnerAttribute() { ModelType = __MainType, MemberName = "", PropertyType = item.Type };
                    var value = constantExpression.Value + "";
                    if (constantExpression.Type.IsEnum)
                    {
                        value = __DBAdapter.CastField(Convert.ToInt32(constantExpression.Value).ToString(), typeof(Enum));
                    }
                    else if (constantExpression.Type == typeof(bool))
                    {
                        value = __DBAdapter.CastField(Convert.ToInt32(constantExpression.Value).ToString(), typeof(Boolean));
                    }
                    else if (MethodAnalyze.IsNumeric(constantExpression.Type))
                    {
                        value = __DBAdapter.CastField(constantExpression.Value.ToString(), constantExpression.Type);
                    }
                    else
                    {
                        value = string.Format("'{0}'", value);
                    }
                    var f2 = f.GetFieldMapping(__DBAdapter, "", withTablePrefix, selectName, value);
                    //f.FieldQuery = new Attribute.FieldQuery() { MemberName = memberName, FieldName = value, MethodName = "" };
                    resultFields.Add(f2);
                    checkFieldRef(selectName, f2.QueryField, __MainType);
                }
                else if (item is MemberExpression)
                {
                    var memberExpression = item as MemberExpression;//转换为属性访问表达式
                    var fieldMemberName = memberExpression.Member.Name;


                    if (memberExpression.Expression.NodeType == ExpressionType.Constant)
                    {
                        string parName = __DBAdapter.GetParamName("p", i);
                        //newExpressionParame.Add(parName, i);
                        var obj = ConstantValueVisitor.GetParameExpressionValue(item);
                        var f2 = new Attribute.FieldInnerAttribute() { ModelType = __MainType, PropertyType = item.Type };
                        var f3 = f2.GetFieldMapping(__DBAdapter, "", withTablePrefix, selectName, parName);
                        //f2.FieldQuery = new Attribute.FieldQuery() { MemberName = memberName, FieldName = parName, MethodName = "" };
                        resultFields.Add(f3);
                        __Visitor.AddParame(parName, obj);
                        continue;
                    }
                    else if (Base.CheckIfAnonymousType(memberExpression.Member.ReflectedType))
                    {
                        var typeParameterName = (memberExpression.Expression as ParameterExpression)?.Name;
                        //按匿名对象属性,视图关联时用
                        var f2 = new Attribute.FieldInnerAttribute() { MemberName = fieldMemberName, PropertyType = item.Type, ModelType = memberExpression.Member.ReflectedType };
                        var f3 = f2.GetFieldMapping(__DBAdapter, GetPrefix(memberExpression.Expression.Type, parameterName: typeParameterName), withTablePrefix, selectName);
                        resultFields.Add(f3);
                        checkFieldRef(selectName, fieldMemberName, memberExpression.Expression.Type);
                        continue;

                    }
                    checkFieldRef(selectName, fieldMemberName, memberExpression.Expression.Type);
                    //按属性
                    Attribute.FieldInnerAttribute f;
                    var a2 = allFields[memberExpression.Expression.Type].TryGetValue(memberExpression.Member.Name, out f);
                    if (!a2)
                    {
                        continue;
                        //throw new Exception("找不到可筛选的属性" + fieldMemberName + " 在" + memberExpression.Expression.Type);
                    }
                    Attribute.FieldMapping _f2;
                    if (selectName != memberExpression.Member.Name)//按有别名算
                    {
                        _f2 = f.GetFieldMapping(__DBAdapter, GetPrefix(f.ModelType, ""), withTablePrefix, selectName);
                    }
                    else
                    {
                        //字段名和属性名不一样时才生成别名
                        string fieldName = "";
                        if (isSelect)//查询字段时按属性名生成别名
                        {
                            if (!string.IsNullOrEmpty(f.MapingName))
                            {
                                fieldName = f.MemberName;
                            }
                        }
                        if (f.MemberName != f.MapingName)
                        {
                            __NewExpressionFieldCaches.Add(new newExpressionFieldCache { OrgionType = originType, MemberName = f.MemberName, MappingName = f.MapingName });
                        }
                        _f2 = f.GetFieldMapping(__DBAdapter, GetPrefix(f.ModelType, ""), withTablePrefix, fieldName);
                    }
                    //f.FieldQuery = new Attribute.FieldQuery() { MemberName = memberName, FieldName = f.MapingName, MethodName = "" };
                    resultFields.Add(_f2);
                }
                else if (item is UnaryExpression)
                {
                    var unaryExpression = item as UnaryExpression;
                    item = unaryExpression.Operand;
                    goto label1;
                }
                else if (item is ConditionalExpression)
                {
                    var v = __Visitor.ConditionalExpressionHandler(item);
                    var f = new Attribute.FieldInnerAttribute() { ModelType = __MainType, MemberName = selectName, PropertyType = item.Type };
                    var f2 = f.GetFieldMapping(__DBAdapter, "", withTablePrefix, selectName, $"({v.Data})");
                    resultFields.Add(f2);
                }
                else
                {
                    throw new Exception("不支持此语法解析:" + item);
                }
            }
            var selectFieldItem = new SelectFieldInfo(resultFields);
            return selectFieldItem;
        }
        void checkFieldRef(string selectName,string fieldMemberName, Type modelType)
        {
            fieldRef fRef;
            // new {b.Name}
            //new {ss1=b.Name}
            //new {a1=b.ss1}
            fRef = __DbContext.fieldRefs.Find(b => b.finalField.SelectName == fieldMemberName);
            var f2 = new fieldRef { ModelType = modelType, SelectName = selectName, OriginName = fieldMemberName };
            if (fRef != null)
            {
                fRef.Next = f2;
                if (System.Diagnostics.Debugger.IsAttached)
                {
                    //Console.WriteLine($"fieldRefs: {fRef}");
                }
            }
            else
            {
                __DbContext.fieldRefs.Add(f2);
                if (System.Diagnostics.Debugger.IsAttached)
                {
                    //Console.WriteLine($"fieldRefs: {f2}");
                }
            }
        }
        /// <summary>
        /// 返回方法调用拼接
        /// </summary>
        /// <param name="expression"></param>
        /// <param name="memberName"></param>
        /// <param name="argsIndex"></param>
        /// <param name="remArgsIndex"></param>
        /// <returns></returns>
        string getSelectMethodCall(Expression expression, out string memberName, int argsIndex, bool remArgsIndex = true)
        {
            var cRLExpression = __Visitor.RouteExpressionHandler(expression);
            memberName = "";
            if (cRLExpression.Type == CRLExpression.CRLExpressionType.Value)
            {
                //值类型返回参数值
                var parName = __DBAdapter.GetParamName("cons", __DbContext.parIndex);
                __DbContext.parIndex += 1;
                __Visitor.AddParame(parName, cRLExpression.Data);
                //if (remArgsIndex)
                //{
                //    newExpressionParame.Add(parName, argsIndex);
                //}
                return parName;
            }
            var methodCallObj = cRLExpression.Data as CRLExpression.MethodCallObj;
            memberName = methodCallObj.MethodName;

            var dic = MethodAnalyze.GetMethos(__DBAdapter);
            if (!dic.ContainsKey(methodCallObj.MethodName))
            {
                throw new Exception("LambdaQuery不支持扩展方法" + methodCallObj.MemberQueryName + "." + methodCallObj.MethodName);
            }
            int newParIndex = __DbContext.parIndex;

            var par = dic[methodCallObj.MethodName](methodCallObj, ref newParIndex, __Visitor.AddParame);
            __DbContext.parIndex = newParIndex;
            //if (remArgsIndex)
            //{
            //    newExpressionParame[par] = argsIndex;
            //}
            return par;
            //return string.Format("{0}({1})", methodName, methodField);
        }
        /// <summary>
        /// 返回二元运算调用
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        string getSeletctBinary(Expression expression)
        {
            var str = __Visitor.RouteExpressionHandler(expression).SqlOut;
            return str;
        }
        #endregion

        /// <summary>
        /// 修正匿名对象无法找到真正的字段名
        /// </summary>
        List<newExpressionFieldCache> __NewExpressionFieldCaches = new List<newExpressionFieldCache>();
        class newExpressionFieldCache
        {
            public Type OrgionType;
            public string MemberName;
            public string MappingName;
        }
    }
}
