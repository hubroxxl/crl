﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using CRL.Core;

using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using CRL.Core.Extension;
using System.Reflection;
using System.Collections;
using Newtonsoft.Json.Linq;
using System.Data;

namespace CRL.Data.LambdaQuery
{
    public abstract partial class LambdaQueryBase
    {
        /// <summary>
        /// 当前类型
        /// </summary>
        public Type __MainType;
        internal string __MainTypePrefix;
        internal ExpressionVisitor __Visitor;
        internal bool __FromDbContext = false;
        internal bool __FromSubQuery = false;
        internal Type __SubQueryParentType;
        #region ResultSelect
        //如果是对象筛选，记录筛选字段以供查询返回
        internal bool __IsFromResultSelect = false;
        internal List<string> __ResultSelectFields;
        #endregion
        /// <summary>
        /// 处理后的查询参数
        /// </summary>
        internal List<Tuple<string, object>> QueryParames
        {
            get
            {
                return __Visitor.QueryParames;
            }
        }
        public ExpressionVisitor GetVisitor()
        {
            return __Visitor;
        }

        //public CRLExpression.CRLExpression mongoHavingCount;
        /// <summary>
        /// 条件
        /// </summary>
        public StringBuilder Condition = new StringBuilder();
        /// <summary>
        /// 查询字段是否需要加上前辍,如t1.Id
        /// </summary>
        internal bool __UseTableAliasesName = true;
        /// <summary>
        /// 是否编译为存储过程
        /// </summary>
        internal bool __CompileSp = SettingConfig.CompileSp;
        /// <summary>
        /// 获取记录条数
        /// </summary>
        public int TakeNum = 0;

        /// <summary>
        /// 分页索引,要分页,设为大于0
        /// </summary>
        public int SkipPage = 0;

        //resultSelect分页参数

        //internal int UnionTakeNum = 0;
        //internal int UnionSkipPage = 0;
        internal bool HasPageArgs()
        {
            var a = SkipPage > 0;
            return a;
        }

        ///// <summary>
        ///// 是否自动跟踪对象状态
        ///// </summary>
        //internal bool __TrackingModel = true;
        /// <summary>
        /// group字段 QueryField
        /// </summary>
        public List<Attribute.FieldMapping> __GroupFields;
        public List<Attribute.FieldMapping> __UnionGroupFields;

        public Dictionary<TypeQuery, JoinInfo> __Relations;
        internal DbContextInner __DbContext;
        internal DBAdapter.DBAdapterBase __DBAdapter;
        public bool __DistinctFields = false;
        /// <summary>
        /// 对象转换时间
        /// </summary>
        public double MapingTime = 0;
        /// <summary>
        /// 查询返回的总行数
        /// </summary>
        public int __RowCount = 0;
        /// <summary>
        /// 缓存查询过期时间
        /// </summary>
        internal int __ExpireMinute = 0;

        /// <summary>
        /// 语法解析时间
        /// </summary>
        public double __AnalyticalTime = 0;
        /// <summary>
        /// 语句执行时间
        /// </summary>
        public double ExecuteTime;
        /// <summary>
        /// 排序
        /// </summary>
        internal string __QueryOrderBy = "";

        /// <summary>
        /// group having
        /// </summary>
        internal string Having = "";

        internal Tuple<string, string> joinPackage;

        internal Dictionary<Type, includeInfo> __IncludeTypes;

        /// <summary>
        /// 填充参数
        /// </summary>
        /// <param name="db"></param>
        internal void FillParames(AbsDBExtend db)
        {
            //db.ClearParams();
            foreach (var n in QueryParames)
            {
                db.SetParam(n.Item1, n.Item2);
            }
        }
        internal bool __WithNoLock = SettingConfig.QueryWithNoLock;
        //internal int __AutoInJoin = 0;
        //internal bool __QueryAllField = false;

        public Func<string> __QueryReturn;
        /// <summary>
        /// 输出当前查询语句
        /// </summary>
        /// <param name="uselog">是否生成到文件</param>
        /// <returns></returns>
        public string PrintQuery(bool uselog = false)
        {
            string sql = "";
            if (HasPageArgs())
            {
                var db = DBExtendFactory.CreateDBExtend(__DbContext);
                if (db is DBExtend.RelationDB.DBExtend)
                {
                    var dbExt = db as DBExtend.RelationDB.DBExtend;
                    if (__Unions != null)
                    {
                        sql = dbExt.GetUnionPageReaderSql(this, out var d1, out var c1);
                    }
                    else if (__GroupFields != null)
                    {
                        sql = dbExt.GetGroupPageReaderSql(this, out var d2, out var c2);
                    }
                    else
                    {
                        sql = dbExt.GetPageReaderSql(this, out var d3, out var c3);
                    }
                    goto label1;
                }
            }
            if (__QueryReturn != null)
            {
                var queryReturn = __QueryReturn();
                Console.WriteLine(queryReturn);
                return queryReturn;
            }
            sql = GetQuery();
            label1:
            //string log = string.Format("[SQL]:{0}\r\n", sql);
            foreach (var item in QueryParames)
            {
                var pat = @"\" + item.Item1 + @"(?!\w)";
                sql = Regex.Replace(sql, pat, "'" + item.Item2 + "'");
                //sql = sql.Replace(item.Item1, "'" + item.Item2 + "'");
                //log += string.Format("[{0}]:[{1}]\r\n", item.Item1, item.Item2);
            }
            if (uselog)
            {
                EventLog.Log(sql + "\r\n", "LambdaQuery", false);
            }
            Console.WriteLine(sql);
            return sql;
        }
        #region 别名
        internal const string subQueryPrefix = "st.";
        internal void InitTypePrefix(string parameName, Type type, Expression expression)
        {
            var typeName = type.FullName;
            if (__FromSubQuery && __SubQueryParentType == type)
            {
                if (!__Prefixs.Exists(b => b.typeName == typeName && b.parameterName == parameName))
                {
                    //var prefix = $"st.";
                    __Prefixs.Add(new prefixObj { typeName = typeName, prefix = subQueryPrefix, parameterName = parameName });
                }
            }
            else
            {
                var find = __Prefixs.FindAll(b => b.typeName == typeName).LastOrDefault();
                if (find == null)
                {
                    __Prefixs.Add(new prefixObj { typeName = typeName, prefix = GetPrefix(type, ""), parameterName = parameName });
                }
                else if (find.parameterName != parameName)
                {
                    __Prefixs.Add(new prefixObj { typeName = typeName, prefix = find.prefix, parameterName = parameName });
                }
            }
        }
        public class prefixObj
        {
            public string typeName;
            public string prefix;
            public string parameterName;
            public override string ToString()
            {
                return $"{typeName} - {parameterName} - {prefix}";
            }
        }
        /// <summary>
        /// 别名
        /// </summary>
        internal List<prefixObj> __Prefixs = new List<prefixObj>();
        internal Dictionary<string, Tuple<string, NewExpression>> __PrefixsQuery = new Dictionary<string, Tuple<string, NewExpression>>();
        internal int prefixIndex = 0;
        static Dictionary<int, string> prefixDic = new Dictionary<int, string>() { { 1, "t1." }, { 2, "t2." }, { 3, "t3." }, { 4, "t4." }, { 5, "t5." }, { 6, "t6." }, { 7, "t7." }, { 8, "t8." }, { 9, "t9." }, { 10, "t10." }, { 11, "t11." }, { 12, "t12." } };
        /// <summary>
        /// 获取别名,如t1.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        internal string GetPrefix(Type type ,string parameterName , bool checkExists = false)
        {
            if (type == null)
            {
                type = __MainType;
            }
            if (__FromSubQuery && __SubQueryParentType == type&&string.IsNullOrEmpty(parameterName))
            {
                return subQueryPrefix;
            }
            //from CreateQuery
            if (type == __MainType && !string.IsNullOrEmpty(__MainTypePrefix))
            {
                return __MainTypePrefix + ".";
            }
            var typeName = type.FullName;
            prefixObj prefixObj;
            if (!string.IsNullOrEmpty(parameterName) && Base.CheckIfAnonymousType(type))
            {
                prefixObj = __Prefixs.Find(b => b.typeName == typeName);
                if (prefixObj != null && prefixObj.prefix.StartsWith("tp"))//from AsQuery 优先使用tp前辍
                {
                    return prefixObj.prefix;
                }
                typeName = $"{type.Name}_{parameterName}";
            }
            if (__FromSubQuery && __SubQueryParentType == type)
            {
                //subQuery按参数名判断
                prefixObj = __Prefixs.Find(b => b.typeName == typeName && b.parameterName == parameterName);
            }
            else
            {
                prefixObj = __Prefixs.Find(b => b.typeName == typeName);
            }
            if (checkExists && prefixObj == null)//当为匿名类型,暂无法判断前辍
            {
                return "";
            }
            if (prefixObj == null)
            {
                prefixIndex += 1;
                string prefix2 = prefixDic[prefixIndex];
                if (!__UseTableAliasesName)
                {
                    prefix2 = "";
                }
                __Prefixs.Add(new prefixObj { typeName = typeName, prefix = prefix2, parameterName = parameterName });
                //__Prefixs[key] = prefix2;
                //__PrefixsAllKey = string.Join("-", __Prefixs.Keys);
                return prefix2;
            }
            return prefixObj.prefix;
        }
        #endregion
        internal static System.Collections.Concurrent.ConcurrentDictionary <string, SelectFieldInfo> queryFieldCache = new System.Collections.Concurrent.ConcurrentDictionary<string, SelectFieldInfo>();
        internal void SelectAll(bool cacheAllFieldString = true)
        {
            var cache = false;
            string key = "";
            //当选择所有字段时,进行缓存
            if (GetPrefix(__MainType, "") == "t1.")
            {
                key = __MainType.ToString();
                SelectFieldInfo value;
                var a = queryFieldCache.TryGetValue(key, out value);
                if (a)
                {
                    if (!cacheAllFieldString)
                    {
                        var item = value.Clone();
                        item.CleanQueryFieldString();
                        _CurrentSelectFieldCache = item;
                    }
                    else
                    {
                        _CurrentSelectFieldCache = value;
                    }
                    return;
                }
                cache = true;
            }
            var exp = Expression.Parameter(__MainType, "b");
            var fields = GetSelectField(true, exp, false, __MainType);
            if(cache)
            {
                var clone = fields.Clone();
                clone.GetQueryFieldString();
                queryFieldCache.TryAdd(key, clone);
            }
            _CurrentSelectFieldCache = fields;
            //__QueryFields = fields;
        }
        /// <summary>
        /// 转换为SQL条件，并提取参数
        /// </summary>
        /// <param name="expressionBody"></param>
        /// <returns></returns>
        public CRLExpression.CRLExpression FormatExpression(Expression expressionBody)
        {
            //string condition;
            if (expressionBody == null)
                return null;

            var result = __Visitor.RouteExpressionHandler(expressionBody, firstLevel: true);

            if (string.IsNullOrEmpty(result.SqlOut))//没有构成树
            {
                bool isNullValue;
                result.SqlOut = __Visitor.DealCRLExpression(expressionBody, result, "", out isNullValue);
            }
            return result;
        }
        //internal string FormatJoinExpression(Expression expressionBody)
        //{
        //    return FormatJoinExpression(expressionBody, out var result);
        //}
        internal string FormatJoinExpression(ReadOnlyCollection<ParameterExpression> parameters, Expression expressionBody, out CRLExpression.CRLExpression result)
        {
            result = null;
            string condition;
            if (expressionBody == null)
                return "";

            result = __Visitor.RouteExpressionHandler(expressionBody, firstLevel: true);
            if (string.IsNullOrEmpty(result.SqlOut))//没有构成树
            {
                bool isNullValue;
                result.SqlOut = __Visitor.DealCRLExpression(expressionBody, result, "", out isNullValue);
            }
            condition = result.SqlOut;
            //GetPrefix(typeof(TInner));
            return condition;
        }
        //internal void AddInnerRelationCondition(TypeQuery inner, string condition)
        //{
        //    __Relations[inner] += "  and " + condition;
        //}
        internal void AddInnerRelation(TypeQuery typeQuery, JoinType joinType, CRLExpression.CRLExpression expression)
        {
            var condition = expression.SqlOut;
            if (__Relations == null)
            {
                __Relations = new Dictionary<TypeQuery, JoinInfo>();
            }
            var inner = typeQuery.OriginType;
            if (__Relations.ContainsKey(typeQuery))
            {
                throw new Exception(string.Format("关联查询已包含关联对象 {0} {1}", inner,condition));
                return;
            }
            //if (__MainType == inner)
            //{
            //    throw new CRLException(string.Format("关联查询不能指定自已 {0} {1}" , inner,condition));
            //    return;
            //}
            if (inner.IsSubclassOf(typeof(IModel)))
            {
                DBExtendFactory.CreateDBExtend(__DbContext).CheckTableCreated(inner);
            }
            var tableName = TypeCache.GetTableName(inner, __DbContext);

            //string aliasName = GetPrefix(inner);
            var aliasName = "";
            //别名必须有
            if (typeQuery.TypeQueryEnum == TypeQueryEnum.查询)
            {
                aliasName = typeQuery.aliasName;
                if(string.IsNullOrEmpty(aliasName))
                {
                    throw new Exception("aliasName为空");
                }
                aliasName = aliasName.Substring(0, aliasName.Length - 1);
                tableName = string.Format("({0}) {1} ", typeQuery.InnerQuery, aliasName);
            }
            else
            {
                aliasName = GetPrefix(inner,expression.typeParameterName);
                aliasName = aliasName.Substring(0, aliasName.Length - 1);
                tableName = string.Format("{0} {1}{2}", __DBAdapter.KeyWordFormat(tableName), aliasName, __DBAdapter.GetWithNolockFormat(__WithNoLock));
            }
            //string str = string.Format(" {0} join {1} on {2}", joinType, tableName, condition);
            if (!__Relations.ContainsKey(typeQuery))
            {
                var join = new JoinInfo() { joinType = joinType, tableName = tableName, condition = condition, expression = expression };
                __Relations.Add(typeQuery, join);
            }
        }
        #region Union
        internal class UnionQuery
        {
            public LambdaQueryBase query;
            public UnionType unionType;
        }
        internal List<UnionQuery> __Unions;
        /// <summary>
        /// 在分表情况下,联合查询所有表方式
        /// </summary>
        internal UnionType __ShanrdingUnionType;
        internal void AddUnion(LambdaQueryBase query2, UnionType unionType)
        {
            if (unionType == UnionType.None)
            {
                throw new Exception("没有指定UnionType");
            }
            __Unions = __Unions ?? new List<UnionQuery>();
            __Unions.Add(new UnionQuery() { query = query2, unionType = unionType });
            QueryParames.AddRange(query2.QueryParames);
            //foreach (var kv in query2.QueryParames)
            //{
            //    QueryParames[kv.Item1] = kv.Item2;
            //}
        }
        #endregion

        internal void Where(string condition)
        {
            if (Condition.Length > 0)
            {
                condition = " and " + condition;
            }
            condition = __DBAdapter.ReplaceParameter(null, out var _p, string.Format(" {0} ", condition), true);
            foreach (var kv in _p)
            {
                QueryParames.Add(new Tuple<string, object>(kv.Key, kv.Value));
            }
            Condition.Append(condition);
        }
        internal void Callback(Action<IDataReader> readerFunc)
        {
            var db = DBExtendFactory.CreateDBExtend(__DbContext);
            db.InvokeReaderCallback(this, readerFunc);
        }
        /// <summary>
        /// 获取查询字段字符串,按条件排除
        /// </summary>
        /// <returns></returns>
        public abstract string GetQueryFieldString();
        /// <summary>
        /// 获取查询条件串,带表名
        /// </summary>
        /// <returns></returns>
        public virtual void GetQueryConditions(StringBuilder sb, bool withTableName = true)
        {
            //return "";
        }
        /// <summary>
        /// 获取完整查询
        /// </summary>
        /// <returns></returns>
        public abstract string GetQuery(bool appendOrderBy = true);
        internal SelectFieldInfo GetSelectFieldInfo()
        {
            if (_CurrentSelectFieldCache == null)
            {
                SelectAll();
            }
            //foreach (var item in _CurrentSelectFieldCache.mapping)
            //{
            //    Console.WriteLine($"select field ---------------- {item}");
            //}
            var mainFieldSelected = _CurrentSelectFieldCache.mapping.Exists(b => b.QueryField.StartsWith("t1."));
            //Console.WriteLine($"mainFieldSelected ------------ {mainFieldSelected}");
            //include后，添加原对象字段筛选
            if (!mainFieldSelected && __IncludeTypes?.Any()==true)// from include
            {
                var exp = Expression.Parameter(__MainType, "b");
                var fields = GetSelectField(true, exp, false, __MainType);
                SetSelectFiled(fields);
            }
            return _CurrentSelectFieldCache;
        }
        public IEnumerable<Attribute.FieldMapping> GetFieldMapping()
        {
            var cache = GetSelectFieldInfo();
            return cache.mapping;
        }

        #region 排序
        public List<Tuple<Attribute.FieldMapping, bool>> __sortFields = new List<Tuple<Attribute.FieldMapping, bool>>();
        internal void SetOrder(Attribute.FieldMapping field, bool desc)
        {
            var str = field.QueryField + (desc ? " desc" : " asc");
            if (__QueryOrderBy != "")
            {
                str = "," + str;
            }
            __QueryOrderBy += str;
            __sortFields.Add(new Tuple<Attribute.FieldMapping, bool>(field, desc));
        }
        internal string GetOrder()
        {
            return __QueryOrderBy;
        }
        internal void CleanOrder()
        {
            __QueryOrderBy = "";
            __sortFields.Clear();
        }
        #endregion
    }


    public class JoinInfo
    {
        public JoinType joinType;
        public string tableName;
        public string condition;
        public CRLExpression.CRLExpression expression;
        public override string ToString()
        {
            return string.Format(" {0} join {1} on {2}", joinType, tableName, condition);
        }
    }
}
