﻿/**
* CRL
*/
using System;
using System.Linq.Expressions;

namespace CRL.Data.LambdaQuery
{
    public interface ILambdaQueryViewJoin<T, TJoinResult>
    {
        ILambdaQueryViewJoin<T, TJoinResult> OrderBy<TResult>(Expression<Func<TJoinResult, TResult>> expression, bool desc = true);
        ILambdaQueryResultSelect<TJoinResult2> Select<TJoinResult2>(Expression<Func<T, TJoinResult, TJoinResult2>> resultSelector) where TJoinResult2 : class;
        ILambdaQuery<T> SelectAppendValue<TJoinResult2>(Expression<Func<TJoinResult, TJoinResult2>> resultSelector);
        ILambdaQueryViewJoin<T, TJoinResult> Where(Expression<Func<T, TJoinResult, bool>> expression);
        /// <summary>
        /// 对关联进行take
        /// </summary>
        /// <param name="top"></param>
        /// <returns></returns>
        ILambdaQueryViewJoin<T, TJoinResult> Take(int top);
    }
}
