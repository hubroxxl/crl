﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CRL.Data.LambdaQuery
{
    public abstract partial class LambdaQuery<T> : LambdaQueryBase
    {
        #region 按完整子查询
        /// <summary>
        /// 按查询exists
        /// 等效为exixts(select field from table2)
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public ILambdaQuery<T> Exists<TResult>(ILambdaQueryResultSelect<TResult> query)
        {
            return InnerSelect(null, query, "exists");
        }

        /// <summary>
        /// 按查询not exists
        /// 等效为 not exixts(select field from table2)
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public ILambdaQuery<T> NotExists<TResult>(ILambdaQueryResultSelect<TResult> query)
        {
            return InnerSelect(null, query, "not exists");
        }

        /// <summary>
        /// 按查询in
        /// 等效为table.field in(select field from table2)
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="query"></param>
        /// <param name="outField"></param>
        /// <returns></returns>
        public ILambdaQuery<T> In<TResult>(ILambdaQueryResultSelect<TResult> query, Expression<Func<T, TResult>> outField)
        {
            return InnerSelect(outField, query, "in");
        }

        /// <summary>
        /// 按查询not in
        /// 等效为table.field not in(select field from table2)
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="query"></param>
        /// <param name="outField"></param>
        /// <returns></returns>
        public ILambdaQuery<T> NotIn<TResult>(ILambdaQueryResultSelect<TResult> query, Expression<Func<T, TResult>> outField)
        {
            return InnerSelect(outField, query, "not in");
        }

        /// <summary>
        /// 按=
        /// 等效为table.field =(select field from table2)
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="query"></param>
        /// <param name="outField"></param>
        /// <returns></returns>
        public ILambdaQuery<T> Equal<TResult>(ILambdaQueryResultSelect<TResult> query, Expression<Func<T, TResult>> outField)
        {
            return InnerSelect(outField, query, "=");
        }

        /// <summary>
        /// 按!=
        /// 等效为table.field !=(select field from table2)
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="query"></param>
        /// <param name="outField"></param>
        /// <returns></returns>
        public ILambdaQuery<T> NotEqual<TResult>(ILambdaQueryResultSelect<TResult> query, Expression<Func<T, TResult>> outField)
        {
            return InnerSelect(outField, query, "!=");
        }

        ILambdaQuery<T> InnerSelect<TResult>(Expression<Func<T, TResult>> outField, ILambdaQueryResultSelect<TResult> query, string type, string innerJoinSql = "")
        {
            if (!query.BaseQuery.__FromDbContext)
            {
                throw new Exception("关联需要由LambdaQuery.CreateQuery创建");
            }
            var baseQuery = query.BaseQuery;
            QueryParames.AddRange(baseQuery.QueryParames);
            //foreach (var kv in baseQuery.QueryParames)
            //{
            //    QueryParames[kv.Key] = kv.Value;
            //}
            var query2 = baseQuery.GetQuery();
            return InnerSelect(outField, query2, type);
        }
        ILambdaQuery<T> InnerSelect<TResult>(Expression<Func<T, TResult>> outField, string query, string type)
        {
            string outFieldSql = "";
            if (outField != null)
            {
                if (outField.Body is MemberExpression)
                {
                    var m1 = outField.Body as MemberExpression;
                    var f = TypeCache.GetProperties(typeof(T), true)[m1.Member.Name];
                    outFieldSql = string.Format("{0}{1}", GetPrefix(typeof(T), outField.Parameters[0].Name), __DBAdapter.KeyWordFormat(f.MapingName));
                }
                else
                {
                    outFieldSql = FormatExpression(outField.Body).SqlOut;
                }
            }
            var condition = string.Format("{0} {1}({2})", outFieldSql, type, query);
            if (Condition.Length > 0)
            {
                condition = " and " + condition;
            }
            Condition.Append(condition);
            return this;
        }
        #endregion
        #region 表达式关联
        ILambdaQuery<T> InnerSelect2<TInner>(Expression<Func<T, object>> outField, Expression<Func<TInner, object>> innerField,
    Expression<Func<T, TInner, bool>> expression, string type) where TInner :class
        {
            var prefix = GetPrefix(typeof(TInner), innerField.Parameters[0].Name);
            var tableName = TypeCache.GetTableName(typeof(TInner), __DbContext);
            tableName = __DBAdapter.KeyWordFormat(tableName);
            tableName += " " + prefix.Substring(0, prefix.Length - 1);

            string innerFieldSql = "";
            if (innerField != null && innerField.Body is MemberExpression)
            {
                var m1 = innerField.Body as MemberExpression;
                var f = TypeCache.GetProperties(typeof(TInner), true)[m1.Member.Name];
                innerFieldSql = string.Format("{0}{1}", prefix, __DBAdapter.KeyWordFormat(f.MapingName));
            }
            else
            {
                innerFieldSql = FormatExpression(innerField.Body).SqlOut;
            }
            if(string.IsNullOrEmpty(innerFieldSql))
            {
                throw new Exception($"未找到内联字段{innerField}");
            }
            string condition = FormatJoinExpression(expression.Parameters, expression.Body, out var cRLExpression);
            var query = string.Format("select {0} from {1} where {2}", innerFieldSql, tableName + __DBAdapter.GetWithNolockFormat(__WithNoLock), condition);
            return InnerSelect(outField, query, type); 
        }

        /// <summary>
        /// 按查询exists
        /// 等效为exixts(select field from table2)
        /// </summary>
        /// <typeparam name="TInner"></typeparam>
        /// <param name="innerField"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public ILambdaQuery<T> Exists<TInner>(Expression<Func<TInner, object>> innerField,
Expression<Func<T, TInner, bool>> expression) where TInner :class
        {
            return InnerSelect2<TInner>(null, innerField, expression, "exists");
        }
        /// <summary>
        /// 按查询not exists
        /// 等效为not exixts(select field from table2)
        /// </summary>
        /// <typeparam name="TInner"></typeparam>
        /// <param name="innerField"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public ILambdaQuery<T> NotExists<TInner>(Expression<Func<TInner, object>> innerField,
Expression<Func<T, TInner, bool>> expression) where TInner :class
        {
            return InnerSelect2<TInner>(null, innerField, expression, "not exists");
        }

        /// <summary>
        /// 按查询in
        /// 等效为table.field in(select field from table2)
        /// </summary>
        /// <typeparam name="TInner"></typeparam>
        /// <param name="outField"></param>
        /// <param name="innerField"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public ILambdaQuery<T> In<TInner>(Expression<Func<T, object>> outField, Expression<Func<TInner, object>> innerField,
    Expression<Func<T, TInner, bool>> expression) where TInner :class
        {
            return InnerSelect2<TInner>(outField, innerField, expression, "in");
        }
        /// <summary>
        /// 按查询not in
        /// 等效为table.field not in(select field from table2)
        /// </summary>
        /// <typeparam name="TInner"></typeparam>
        /// <param name="outField"></param>
        /// <param name="innerField"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public ILambdaQuery<T> NotIn<TInner>(Expression<Func<T, object>> outField, Expression<Func<TInner, object>> innerField,
Expression<Func<T, TInner, bool>> expression) where TInner :class
        {
            return InnerSelect2<TInner>(outField, innerField, expression, "not in");
        }
        /// <summary>
        /// 按=
        /// 等效为table.field =(select field from table2)
        /// </summary>
        /// <typeparam name="TInner"></typeparam>
        /// <param name="outField"></param>
        /// <param name="innerField"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public ILambdaQuery<T> Equal<TInner>(Expression<Func<T, object>> outField, Expression<Func<TInner, object>> innerField,
Expression<Func<T, TInner, bool>> expression) where TInner :class
        {
            return InnerSelect2<TInner>(outField, innerField, expression, "=");
        }
        /// <summary>
        /// 按!=
        /// 等效为table.field !=(select field from table2)
        /// </summary>
        /// <typeparam name="TInner"></typeparam>
        /// <param name="outField"></param>
        /// <param name="innerField"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public ILambdaQuery<T> NotEqual<TInner>(Expression<Func<T, object>> outField, Expression<Func<TInner, object>> innerField,
Expression<Func<T, TInner, bool>> expression) where TInner :class
        {
            return InnerSelect2<TInner>(outField, innerField, expression, "!=");
        }

        #endregion
    }
}
