﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRL.Data.LambdaQuery
{
    /// <summary>
    /// 表示查询的类型
    /// </summary>
    public class TypeQuery
    {
        public TypeQuery(Type _OriginType)
        {
            OriginType = _OriginType;
            TypeQueryEnum = TypeQueryEnum.表;
        }
        public TypeQuery(Type _OriginType, string _queryName)
        {
            OriginType = _OriginType;
            TypeQueryEnum = TypeQueryEnum.查询;
            aliasName = _queryName;
            //innerPrefix = _innerPrefix;
        }
        public string _GetKey()
        {
            return $"{TypeQueryEnum}_{OriginType.Name}_{aliasName}";
            //return TypeQueryEnum.ToString() + OriginType.Name;
        }
        public override string ToString()
        {
            return _GetKey();
        }
        public override int GetHashCode()
        {
            var key = _GetKey();
            var h = key.GetHashCode();
            return h;
        }
        public override bool Equals(object obj)
        {
            var obj2 = obj as TypeQuery;
            var a= GetHashCode() == obj2.GetHashCode();
            return a;
        }
        public TypeQueryEnum TypeQueryEnum;
        public Type OriginType;
        internal string aliasName;
        public string InnerQuery;
        /// <summary>
        /// 自定义别名
        /// </summary>
        //string innerPrefix;

        //internal Tuple<string,string> parentQueryAliasNameConvert;
    }
    public enum TypeQueryEnum
    {
        表,
        查询
    }
}
