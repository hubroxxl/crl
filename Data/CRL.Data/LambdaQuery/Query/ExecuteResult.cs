﻿/**
* CRL
*/

using CRL.Core;
using CRL.Core.Extension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CRL.Data.LambdaQuery
{
    public abstract partial class LambdaQuery<T>
    {
        #region 获取一条记录

        /// <summary>
        /// 获取一条
        /// </summary>
        /// <returns></returns>
        public dynamic ToSingleDynamic()
        {
            return Top(1).ToDynamic().FirstOrDefault();
        }
        /// <summary>
        /// 获取一条
        /// </summary>
        /// <returns></returns>
        public T ToSingle()
        {
            return Top(1).ToList().FirstOrDefault();
        }
        /// <summary>
        /// 获取一条
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <returns></returns>
        public TResult ToSingle<TResult>()where TResult : class
        {
            return Top(1).ToList<TResult>().FirstOrDefault();
        }
        #endregion

        /// <summary>
        /// 返回动态对象
        /// 会按GROUP和分页判断
        /// </summary>
        /// <returns></returns>
        public List<dynamic> ToDynamic()
        {
            var db = DBExtendFactory.CreateDBExtend(__DbContext);
            var dbArchitecture = Base.GetDataBaseArchitecture(__DbContext.DBHelper.CurrentDBType);
            if (dbArchitecture == DataBaseArchitecture.NotRelation)
            {
                //like MongoDbQuery.QueryDynamic2
                var method = db.GetType().GetMethod("QueryDynamic2", BindingFlags.Public | BindingFlags.Instance);
                var list = method.MakeGenericMethod(new Type[] { typeof(T) }).Invoke(db, new object[] { this });
                return list as List<dynamic>;
            }
            return db.QueryDynamic(this);
        }
        /// <summary>
        /// 返回指定类型
        /// 会按GROUP和分页判断
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <returns></returns>
        public List<TResult> ToList<TResult>()
            where TResult : class
        {
            var db = DBExtendFactory.CreateDBExtend(__DbContext);
            var list = db.QueryResult<TResult>(this);
            ObjectConvert.fillIncudeManyResult(false, __IncludeTypes, list);
            return list;
        }
        public new void Callback(Action<IDataReader> readerFunc)
        {
            base.Callback(readerFunc);
        }
        internal List<T> __ToList()
        {
            return ToList();
        }
        /// <summary>
        /// 返回当前类型
        /// 会按GROUP和分页判断
        /// </summary>
        /// <returns></returns>
        public List<T> ToList()
        {
            var db = DBExtendFactory.CreateDBExtend(__DbContext);
            var list = db.QueryList(this);
            ObjectConvert.fillIncudeManyResult(false, __IncludeTypes, list);
            return list;
        }

        /// <summary>
        /// 返回字典
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TValue"></typeparam>
        /// <returns></returns>
        public Dictionary<TKey, TValue> ToDictionary<TKey, TValue>()
        {
            var db = DBExtendFactory.CreateDBExtend(__DbContext);
            return db.ToDictionary<T, TKey, TValue>(this);
        }

        #region 返回首列结果
        /// <summary>
        /// 返回首列结果
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <returns></returns>
        public TResult ToScalar<TResult>()
        {
            var db = DBExtendFactory.CreateDBExtend(__DbContext);
            var result = db.QueryScalar(this);
            if (result == null || result is DBNull)
            {
                return default(TResult);
            }
            return (TResult)result;
        }
        /// <summary>
        /// 返回首列结果
        /// </summary>
        /// <returns></returns>
        public dynamic ToScalar()
        {
            if (__FromSubQuery)
            {
                return default(dynamic);
            }
            var db = DBExtendFactory.CreateDBExtend(__DbContext);
            return db.QueryScalar(this);
        }
        #endregion
    }
}
