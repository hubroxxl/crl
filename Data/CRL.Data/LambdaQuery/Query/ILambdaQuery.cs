﻿/**
* CRL
*/
using System;

using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CRL.Data.LambdaQuery
{
    public interface IQuery
    {
        string GetQuery(bool appendOrderBy = true);
    }
    public partial interface ILambdaQuery<T>: IQuery
    {
        double AnalyticalTime { get; }
        double ExecuteTime { get; }
        double MapingTime { get; }
        int RowCount { get; }
        //ILambdaQuery<T> SetPrefix(string prefix);
        ILambdaQuery<T> CompileToSp(bool compileSp);
        /// <summary>
        /// 创建case结构
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="func">可选传入函数，min,max,avg ...</param>
        /// <returns></returns>
        ICaseExpression<T> CreateCase(string func = "");
        /// <summary>
        /// 创建开窗函数结构
        /// </summary>
        /// <returns></returns>
        IWindowFuncExpression<T> CreateWindowFunc();
        /// <summary>
        /// 创建当前查询关联的查询
        /// </summary>
        /// <typeparam name="T2"></typeparam>
        /// <returns></returns>
        ILambdaQuery<T2> CreateQuery<T2>() where T2 :class;

        ILambdaQuery<T2> CreateSubQuery<T2>() where T2 : class;

        ILambdaQuery<T> Equal<TInner>(Expression<Func<T, object>> outField, Expression<Func<TInner, object>> innerField, Expression<Func<T, TInner, bool>> expression) where TInner :class;

        ILambdaQuery<T> Equal<TResult>(ILambdaQueryResultSelect<TResult> query, Expression<Func<T, TResult>> outField);

        ILambdaQuery<T> Exists<TInner>(Expression<Func<TInner, object>> innerField, Expression<Func<T, TInner, bool>> expression) where TInner :class;

        ILambdaQuery<T> Exists<TResult>(ILambdaQueryResultSelect<TResult> query);

        ILambdaQuery<T> Expire(int expireMinute);

        string GetOrderBy();

        IGroupQuery<T> GroupBy<TResult>(Expression<Func<T, TResult>> resultSelector);

        ILambdaQuery<T> In<TInner>(Expression<Func<T, object>> outField, Expression<Func<TInner, object>> innerField, Expression<Func<T, TInner, bool>> expression) where TInner :class;

        ILambdaQuery<T> In<TResult>(ILambdaQueryResultSelect<TResult> query, Expression<Func<T, TResult>> outField);

        ILambdaQueryJoin<T, T2> Join<T2>(Expression<Func<T, T2, bool>> expression, JoinType joinType = JoinType.Inner);

        ILambdaQueryViewJoin<T, TJoinResult> Join<TJoinResult>(ILambdaQueryResultSelect<TJoinResult> resultSelect, Expression<Func<T, TJoinResult, bool>> expression, JoinType joinType = JoinType.Inner);

        ILambdaQuery<T> NotEqual<TInner>(Expression<Func<T, object>> outField, Expression<Func<TInner, object>> innerField, Expression<Func<T, TInner, bool>> expression) where TInner :class;

        ILambdaQuery<T> NotEqual<TResult>(ILambdaQueryResultSelect<TResult> query, Expression<Func<T, TResult>> outField);

        ILambdaQuery<T> NotExists<TInner>(Expression<Func<TInner, object>> innerField, Expression<Func<T, TInner, bool>> expression) where TInner :class;

        ILambdaQuery<T> NotExists<TResult>(ILambdaQueryResultSelect<TResult> query);

        ILambdaQuery<T> NotIn<TInner>(Expression<Func<T, object>> outField, Expression<Func<TInner, object>> innerField, Expression<Func<T, TInner, bool>> expression) where TInner :class;

        ILambdaQuery<T> NotIn<TResult>(ILambdaQueryResultSelect<TResult> query, Expression<Func<T, TResult>> outField);

        ILambdaQuery<T> Or(Expression<Func<T, bool>> expression);

        ILambdaQuery<T> OrderBy(string orderBy);

        ILambdaQuery<T> OrderBy<TResult>(Expression<Func<T, TResult>> expression, bool desc = true);

        ILambdaQuery<T> OrderByPrimaryKey(bool desc);

        ILambdaQuery<T> Page(int pageSize = 15, int pageIndex = 1);

        string PrintQuery(bool uselog = false);

        ILambdaQuery<T> Select(Expression resultSelectorBody);

        ILambdaQueryResultSelect<TResult> Select<TResult>(Expression<Func<T, TResult>> resultSelector = null);

        //ILambdaQuery<T> ShardingUnion(UnionType unionType);

        ILambdaQuery<T> Take(int take);

        Dictionary<TKey, TValue> ToDictionary<TKey, TValue>();

        List<dynamic> ToDynamic();

        List<T> ToList();

        List<TResult> ToList<TResult>() where TResult :class;

        void Callback(Action<IDataReader> readerFunc);

        ILambdaQuery<T> Top(int top);

        dynamic ToScalar();

        TResult ToScalar<TResult>();

        T ToSingle();

        TResult ToSingle<TResult>() where TResult :class;

        dynamic ToSingleDynamic();

        string ToString();

        ILambdaQuery<T> Where(Expression<Func<T, bool>> expression);

        ILambdaQuery<T> Where(string condition);

        ILambdaQuery<T> WhereIf(Expression<Func<T, bool>> expression, bool boolEx);

        //ILambdaQuery<T> WhereNotNull(Expression<Func<T, bool>> expression);

        ILambdaQuery<T> WithNoLock(bool _nolock = true);

        //ILambdaQuery<T> WithTrackingModel(bool trackingModel = true);
        /// <summary>
        /// 重置选择的字段(sort/group/)
        /// </summary>
        void Reset();
        ILambdaQuery<T> Include<TResult>(Expression<Func<T, TResult>> expression);
        ILambdaQuery<T> Include<TResult>(Expression<Func<T, IEnumerable<TResult>>> expression, Expression<Func<TResult, bool>> filter = null);
        ILambdaQuery<T> Include();
        #region func
        long Count();

        TResult Sum<TResult>(Expression<Func<T, TResult>> field);

        TResult Max<TResult>(Expression<Func<T, TResult>> field);
        TResult Min<TResult>(Expression<Func<T, TResult>> field);
        TResult Avg<TResult>(Expression<Func<T, TResult>> field);
        #endregion
    }

    public partial interface ILambdaQuery<T>
    {
        Task<List<T>> ToListAsync();

        Task<List<TResult>> ToListAsync<TResult>() where TResult : class;
    }
}
