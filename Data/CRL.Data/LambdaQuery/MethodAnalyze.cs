﻿/**
* CRL
*/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using CRL.Data.DBAccess;
using CRL.Data.LambdaQuery.CRLExpression;

namespace CRL.Data.LambdaQuery
{
    public delegate string MethodHandler(MethodCallObj methodInfo,ref int parIndex, AddParameHandler addParame);
    public delegate void AddParameHandler(string name, object value);
    internal class MethodAnalyze
    {
        public MethodAnalyze(DBAdapter.DBAdapterBase _dBAdapter)
        {
            //var table = TypeCache.GetTable(typeof(T));
            dBAdapter = _dBAdapter;
            if (dBAdapter == null)
            {
                throw new Exception("dBAdapter尚未初始化");
            }
        }

        DBAdapter.DBAdapterBase dBAdapter;
        static Dictionary<DBAdapter.DBAdapterBase, MethodAnalyze> methodHandlers = new Dictionary<DBAdapter.DBAdapterBase, MethodAnalyze>();
        internal static Dictionary<string, MethodHandler> customExtendMethodHandlers = new Dictionary<string, MethodHandler>();
        public static Dictionary<string, MethodHandler> GetMethos(DBAdapter.DBAdapterBase _dBAdapter)
        {
            if(!methodHandlers.ContainsKey(_dBAdapter))
            {
                methodHandlers.Add(_dBAdapter, new MethodAnalyze(_dBAdapter));
            }
            return methodHandlers[_dBAdapter].Methods();
        }
        internal static void AddCustomExtendMethod(DBType dBType, string methodName, MethodHandler handler)
        {
            customExtendMethodHandlers.Add($"{dBType}_{methodName}", handler);
        }

        Dictionary<string, MethodHandler> methodDic;
        public Dictionary<string, MethodHandler> Methods()
        {
            if (methodDic == null)
            {
                methodDic = new Dictionary<string, MethodHandler>();
                methodDic.Add(nameof(ExtensionMethod.Like), StringLike);
                methodDic.Add(nameof(ExtensionMethod.LikeLeft), StringLikeLeft);
                methodDic.Add(nameof(ExtensionMethod.LikeRight), StringLikeRight);
                methodDic.Add("Contains", Contains);
                methodDic.Add(nameof(ExtensionMethod.Between), Between);
                methodDic.Add(nameof(ExtensionMethod.DateDiff), DateTimeDateDiff);
                methodDic.Add("Length", Length);
                methodDic.Add(nameof(ExtensionMethod.Len), Length);
                methodDic.Add("Trim", Trim);
                methodDic.Add("TrimStart", TrimStart);
                methodDic.Add("TrimEnd", TrimEnd);
                methodDic.Add(nameof(ExtensionMethod.In), In);
                methodDic.Add("Substring", Substring);
                methodDic.Add(nameof(ExtensionMethod.COUNT), Count);
                methodDic.Add("Count", Count);
                methodDic.Add(nameof(ExtensionMethod.SUM), Sum);
                methodDic.Add(nameof(ExtensionMethod.MAX), Max);
                methodDic.Add(nameof(ExtensionMethod.MIN), Min);
                methodDic.Add(nameof(ExtensionMethod.AVG), AVG);
                methodDic.Add("Equals", Equals);
                methodDic.Add("StartsWith", StartsWith);
                methodDic.Add("EndsWith", StringLikeLeft);
                methodDic.Add("IsNullOrEmpty", IsNullOrEmpty);
                methodDic.Add("ToString", CaseToType);
                methodDic.Add("ToInt32", CaseToType);
                methodDic.Add("ToDecimal", CaseToType);
                methodDic.Add("ToDouble", CaseToType);
                methodDic.Add("ToBoolean", CaseToType);
                methodDic.Add("ToDateTime", CaseToType);
                methodDic.Add("ToInt16", CaseToType);
                methodDic.Add("Parse", CaseToType);
                methodDic.Add("ToSingle", CaseToType);
                methodDic.Add("ToUpper", ToUpper);
                methodDic.Add("ToLower", ToLower);
                methodDic.Add(nameof(ExtensionMethod.IsNull), IsNull);
                methodDic.Add("Replace", Replace);
                methodDic.Add(nameof(ExtensionMethod.DistinctField), DistinctField);
                methodDic.Add(nameof(ExtensionMethod.DistinctCount), DistinctCount);
                methodDic.Add(nameof(ExtensionMethod.GreaterThan), GreaterThan);
                methodDic.Add(nameof(ExtensionMethod.LessThan), LessThan);
                methodDic.Add(nameof(ExtensionMethod.GreaterThanOrEqual), GreaterThanOrEqual);
                methodDic.Add(nameof(ExtensionMethod.LessThanOrEqual), LessThanOrEqual);
                methodDic.Add(nameof(ExtensionMethod.FormatTo), DateTimeFormat);
                methodDic.Add(nameof(ExtensionMethod.FuncFormat), FuncFormat);
                methodDic.Add(nameof(ExtensionMethod.SplitFirst), StringSplitFirst);
                //methodDic.Add(nameof(ExtensionMethod.GetResult), GetFuncResult);
                //自定义扩展方法
                foreach (var kv in customExtendMethodHandlers)
                {
                    if (kv.Key.StartsWith(dBAdapter.DBType.ToString()))
                    {
                        var name = kv.Key.Split('_')[1];
                        methodDic[name] = kv.Value;
                    }
                }
            }
            return methodDic;
        }
        private string GetFuncResult(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            return (methodInfo.Args[0] as IQuery).GetQuery();
        }
        private string StringSplitFirst(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            var charindex = dBAdapter.GetSplitFirst(field, methodInfo.Args[0].ToString());
            return charindex;
        }

        private string FuncFormat(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            return string.Format(methodInfo.Args[0].ToString(), field);
        }
        #region compare
        string compare(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame, string op)
        {
            var field = methodInfo.MemberQueryName;
            var args = methodInfo.Args;
            string parName = GetParamName("cmp", parIndex);
            var args1 = args[0];
            if (args1 is ExpressionValueObj)
            {
                parName = args1.ToString();
            }
            else
            {
                addParame(parName, args1);
            }
            parIndex += 1;
            return $"{field}{op}{parName}";
        }
        private string GreaterThan(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            return compare(methodInfo, ref parIndex, addParame, ">");
        }
        private string GreaterThanOrEqual(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            return compare(methodInfo, ref parIndex, addParame, ">=");
        }
        private string LessThan(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            return compare(methodInfo, ref parIndex, addParame, "<");
        }
        private string LessThanOrEqual(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            return compare(methodInfo, ref parIndex, addParame, "<=");
        }

        #endregion
        private string DateTimeFormat(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            return dBAdapter.DateTimeFormat(field, methodInfo.Args[0].ToString());
        }
        private string DistinctCount(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            return dBAdapter.DistinctCount(field);
        }

        private string DistinctField(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            return dBAdapter.Distinct(field);
        }

        string GetParamName(string name, object index)
        {
            return dBAdapter.GetParamName(name, index);
        }
        public string IsNull(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            var args = methodInfo.Args.First();
            string parName = GetParamName("isnull", parIndex);
            parIndex += 1;
            addParame(parName, args);
            return dBAdapter.IsNull(field, parName);
        }
        public string ToUpper(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            return dBAdapter.ToUpperFormat(field);
        }
        public string ToLower(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            return dBAdapter.ToLowerFormat(field);
        }

        public string Length(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            return dBAdapter.LengthFormat(field);
        }
        public string Trim(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            return dBAdapter.Trim(field);
        }
        public string TrimStart(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            return dBAdapter.TrimStart(field);
        }
        public string TrimEnd(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            return dBAdapter.TrimEnd(field);
        }
        public string IsNullOrEmpty(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            var isNot = methodInfo.ExpressionType == ExpressionType.Not;
            var notStr = dBAdapter.IsNotFormat(isNot);
            var result = string.Format("({0} {1} null {3} {0}{2}'')", field, notStr, isNot ? "!=" : "=", isNot ? "and" : "or");
            return result;
        }
        public string CaseToType(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            if (string.IsNullOrEmpty(field))//按转换常量算
            {
                string parName = GetParamName("case", parIndex);
                parIndex += 1;
                addParame(parName, methodInfo.Args.First());
                field = parName;
            }
            return dBAdapter.CastField(field, methodInfo.ReturnType);
        }

        public string Substring(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            var nodeType = methodInfo.ExpressionType;
            var args = methodInfo.Args;
            if (args.Count < 2)
            {
                throw new Exception("Substring扩展方法需要两个参数,index,length");
            }
            return dBAdapter.SubstringFormat(field, (int)args[0], (int)args[1]);
            //return string.Format(" SUBSTRING({0},{1},{2})", field, args[0], args[1]);
        }
        public string StringLike(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            var nodeType = methodInfo.ExpressionType;
            var args = methodInfo.Args;
            return StringLikeFull(methodInfo, ref parIndex, addParame, "%{0}%");
        }
        public string StringLikeLeft(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            //string str = args[0].ToString();
            //str = str.Replace("%", "");
            return StringLikeFull(methodInfo,ref parIndex, addParame, "%{0}");
        }
        public string StringLikeRight(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            //string str = args[0].ToString();
            //str = str.Replace("%","");
            return StringLikeFull(methodInfo, ref parIndex, addParame, "{0}%");
        }
        string StringLikeFull(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame,string likeFormat)
        {
            var field = methodInfo.MemberQueryName;
            var nodeType = methodInfo.ExpressionType;
            var args = methodInfo.Args[0];
            string parName = GetParamName("like", parIndex);
            parIndex += 1;

            if (args is ExpressionValueObj)//like 字段名 '%'+t1.[ProductName1]
            {
                parName = args.ToString();

                likeFormat = likeFormat.Replace("%", "+'%'+");
                parName = string.Format(likeFormat, parName);
                parName = System.Text.RegularExpressions.Regex.Replace(parName,@"^\+","");
                parName = System.Text.RegularExpressions.Regex.Replace(parName, @"\+$", "");
            }
            else
            {
                if (!args.ToString().Contains("%"))
                {
                    args = string.Format(likeFormat, args);
                }
                addParame(parName, args);
            }
            if (nodeType == ExpressionType.Equal)
            {
                return dBAdapter.StringLikeFormat(field, parName);
            }
            else
            {
                return dBAdapter.StringNotLikeFormat(field, parName);
            }
            //return string.Format("{0} LIKE {1}", field, parName);
        }
        /// <summary>
        /// 字符串包含
        /// </summary>
        /// <param name="methodInfo"></param>
        /// <param name="parIndex"></param>
        /// <param name="addParame"></param>
        /// <returns></returns>
        public string Contains(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            var nodeType = methodInfo.ExpressionType;
            var args = methodInfo.Args;
            var firstArgs = args.First();
            if (firstArgs is IEnumerable && firstArgs.GetType() != typeof(string))//如果是集合,按in
            {
                return In(methodInfo, ref parIndex, addParame);
            }
            string parName = GetParamName("contains", parIndex);
            var args1 = args[0];
            if (args1 is ExpressionValueObj)
            {
                parName = args1.ToString();
            }
            else
            {
                addParame(parName, args1);
            }
            parIndex += 1;
            if (nodeType == ExpressionType.Equal)
            {
                return dBAdapter.StringContainsFormat(field, parName);
            }
            else
            {
                return dBAdapter.StringNotContainsFormat(field, parName);
            }
            //return string.Format("CHARINDEX({1},{0})>0", field, parName);
        }
        public string Between(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            var nodeType = methodInfo.ExpressionType;
            var args = methodInfo.Args;
            string parName = GetParamName("between", parIndex);
            var args1 = args[0];
            var args2 = args[1];
            if (args1 is ExpressionValueObj)
            {
                parName = args1.ToString();
            }
            else
            {
                addParame(parName, args1);
            }
            parIndex += 1;
            string parName2 = GetParamName("between", parIndex);
            if (args2 is ExpressionValueObj)
            {
                parName2 = args2.ToString();
            }
            else
            {
                addParame(parName2, args2);
            }
            parIndex += 1;
            if (nodeType == ExpressionType.Equal)
            {
                return dBAdapter.BetweenFormat(field, parName, parName2);
            }
            else
            {
                return dBAdapter.NotBetweenFormat(field, parName, parName2);
            }
            //return string.Format("{0} between {1} and {2}", field, parName, parName2);
        }
        public string DateTimeDateDiff(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            var nodeType = methodInfo.ExpressionType;
            var args = methodInfo.Args;
            string parName = GetParamName("dateDiff", parIndex);
            var args1 = args[1];
            if (args1 is ExpressionValueObj)
            {
                parName = args1.ToString();
            }
            else
            {
                addParame(parName, args[1]);
            }
            parIndex += 1;
            //DateDiff(2015/2/5 17:59:44,t1.AddTime,DateDiff1)>1 
            return dBAdapter.DateDiffFormat(field, args[0].ToString(), parName);
            //return string.Format("DateDiff({0},{1},{2}){3}", args[0], field, parName, args[2]);
        }
        public string Replace(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            var args = methodInfo.Args;
            string parName = GetParamName("repd", parIndex);
            var args1 = args[0];
            if (args1 is ExpressionValueObj)
            {
                parName = args1.ToString();
            }
            else
            {
                addParame(parName, args1);
            }
            var args2= args[1];
            parIndex += 1;
            string parName2 = GetParamName("repd", parIndex);
            addParame(parName2, args2);
            parIndex += 1;
            //DateDiff(2015/2/5 17:59:44,t1.AddTime,DateDiff1)>1 
            return dBAdapter.Replace(field, parName, parName2);
        }
        #region 函数
        public string Count(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            return string.Format("count({0})", field);
        }

        public string Sum(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            return string.Format("sum({0})", field);
        }
        public string Max(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            return string.Format("max({0})", field);
        }
        public string Min(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            return string.Format("min({0})", field);
        }
        public string AVG(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            return string.Format("avg({0})", field);
        }
        #endregion

        internal static bool IsNumeric(Type t)
        {
            if (t.IsEnum) return false;
            var tc = Type.GetTypeCode(t);
            switch (tc)
            {
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Single:
                case TypeCode.Double:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.Byte:
                case TypeCode.Decimal:
                case TypeCode.SByte:
                    return true;
                default:
                    return t == typeof(UIntPtr) || t == typeof(IntPtr);
            }
        }
        string InFormat(object value, ref int parIndex, AddParameHandler addParame)
        {
            value.CheckNull("value");
            string str = "";
            var valueArry = value as IEnumerable;
            var length = 0;
            Type argsType = null;
            foreach (var val in valueArry)
            {
                length++;
                if (val != null)
                {
                    if (argsType == null)
                    {
                        argsType = val.GetType();
                    }
                }
            }
            if (length == 0)
            {
                return str;
                //throw new Exception("in 参数为空");
            }
            if (argsType == null)
            {
                throw new Exception("in 参数为空");
            }
            #region in
            var isNumeric = IsNumeric(argsType);
            if (value is string)
            {
                string parName = GetParamName("in", parIndex);
                addParame(parName, value);
                str = parName;
            }
            else
            {
                var max = length > 1000;//超出直接拼字符串
                foreach (var val in valueArry)
                {
                    string parName;
                    var val2 = val;
                    if (max)
                    {
                        if (argsType.IsEnum && isNumeric)
                        {
                            val2 = (int)val2;
                        }
                        parName = isNumeric ? val2.ToString() : string.Format("'{0}'", val);
                    }
                    else
                    {
                        parName = GetParamName("in", parIndex);
                        addParame(parName, val);
                        parIndex += 1;
                    }
                    str += string.Format("{0},", parName);
                }
                if (str.Length > 1)
                {
                    str = str.Substring(0, str.Length - 1);
                }
            }
            #endregion
            return str;
        }
        public string In(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            var nodeType = methodInfo.ExpressionType;
            var args = methodInfo.Args;
            string str = InFormat(args[0], ref parIndex, addParame);
            if (nodeType == ExpressionType.Equal)
            {
                if (string.IsNullOrEmpty(str))//in 为空
                {
                    return "1=2";
                }
                return dBAdapter.InFormat(field, str);
            }
            else
            {
                if (string.IsNullOrEmpty(str))//in 为空
                {
                    return "1=1";
                }
                return dBAdapter.NotInFormat(field, str);
            }
            //return string.Format("{0} IN ({1})", field, str);
        }
        public string Equals(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            var nodeType = methodInfo.ExpressionType;
            var args = methodInfo.Args;
            string parName = GetParamName("equalEnum", parIndex);
            parIndex += 1;
            var args1 = args[0];
            if (args1 is ExpressionValueObj)
            {
                parName = args1.ToString();
            }
            else
            {
                args1 = ObjectConvert.ConvertObject(args1.GetType(), args1);
                addParame(parName, args1);
            }
            var operate = ExpressionVisitor.ExpressionTypeCast(nodeType);
            return string.Format("{0}{2}{1}", field, parName, operate);
            //if (nodeType == ExpressionType.Equal)
            //{
            //    return string.Format("{0}={1}", field, parName);
            //}
            //else
            //{
            //    return string.Format("{0}!={1}", field, parName);
            //}
        }
        public string StartsWith(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            return StringLikeRight(methodInfo, ref parIndex, addParame);
        }
    }
}
