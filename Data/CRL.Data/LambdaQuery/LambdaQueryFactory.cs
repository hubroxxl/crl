﻿/**
* CRL
*/
using CRL.Data.DBAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRL.Data.LambdaQuery
{
    public class LambdaQueryFactory
    {
        static System.Collections.Concurrent.ConcurrentDictionary<Type, object> notRelationQueryCreaters = new System.Collections.Concurrent.ConcurrentDictionary<Type, object>();
        public static LambdaQuery<T> CreateLambdaQuery<T>(DbContextInner _dbContext)
        {
            //var configBuilder = DBConfigRegister.current;
            var _DBType = _dbContext.DBHelper.CurrentDBType;
            var dbArchitecture = Base.GetDataBaseArchitecture(_DBType);
            if (dbArchitecture== DataBaseArchitecture.Relation)
            {
                return new RelationLambdaQuery<T>(_dbContext);
            }
            var type = typeof(T);
            var a = notRelationQueryCreaters.TryGetValue(type, out object creater);
            if (!a)
            {
                var typeLambdaQuery = DBConfigRegister.GetLambdaQueryType(_DBType);
                var genericType = typeLambdaQuery.MakeGenericType(typeof(T));
                creater = Core.DynamicMethodHelper.CreateCtorFunc<Func<DbContextInner, LambdaQuery<T>>>(genericType, new Type[] { typeof(DbContextInner) });
                notRelationQueryCreaters.TryAdd(type, creater);
            }
            var func = (Func<DbContextInner, LambdaQuery<T>>)creater;
            return func(_dbContext);
        }
    }
}
