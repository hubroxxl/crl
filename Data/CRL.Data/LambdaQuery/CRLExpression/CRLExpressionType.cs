﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRL.Data.LambdaQuery.CRLExpression
{
    /// <summary>
    /// 节点类型
    /// </summary>
    public enum CRLExpressionType
    {
        Tree = 1,
        Binary = 2,
        Name = 4,
        Value = 8,
        MethodCall = 16,
        MethodCallArgs = 32
    }
}
