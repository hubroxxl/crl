﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CRL.Data.LambdaQuery
{
    public partial interface ILambdaQueryResultSelect<TResult>: IQuery
    {
        LambdaQueryBase BaseQuery { get; set; }
        Type InnerType { get; }
        //ILambdaQueryResultSelect<TResult> HavingCount(Expression<Func<TResult, bool>> expression);
        ILambdaQueryResultSelect<TResult> OrderBy<TResult2>(Expression<Func<TResult, TResult2>> expression, bool desc = true);
        List<dynamic> ToDynamic();
        List<TResult> ToList();
        TResult ToSingle();
        List<TResult2> ToList<TResult2>();
        ILambdaQueryResultSelect<TResult> Union<TResult2>(ILambdaQueryResultSelect<TResult2> resultSelect, UnionType unionType = UnionType.UnionAll);
        ///// <summary>
        ///// 指定子查询结果分页
        ///// </summary>
        ///// <param name="pageSize"></param>
        ///// <param name="pageIndex"></param>
        ///// <returns></returns>
        //ILambdaQueryResultSelect<TResult> Page(int pageSize,int pageIndex);

        ILambdaQueryViewJoin<TResult, TResult2> Join<TResult2>(ILambdaQueryResultSelect<TResult2> resultSelect, Expression<Func<TResult, TResult2, bool>> expression, JoinType joinType = JoinType.Inner);

        ILambdaQueryViewJoin<TResult, T2> Join<T2>( Expression<Func<TResult, T2, bool>> expression, JoinType joinType = JoinType.Inner);

        //IGroupQuery<TResult> UnionGroupBy<TResult2>(Expression<Func<TResult, TResult2>> resultSelector);
        ILambdaQuery<TResult> AsQuery();
        ICaseExpression<TResult> CreateCase(string func = "");
        string PrintQuery();
    }

    public partial interface ILambdaQueryResultSelect<TResult>
    {
        Task<List<TResult>> ToListAsync();

        Task<List<TResult2>> ToListAsync<TResult2>();
    }
}
