﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
//using System.Transactions;
using CRL.Data.LambdaQuery;
//using System.Messaging;
using CRL.Core;
//using MongoDB.Driver.Linq;
using System.Collections;
using Newtonsoft.Json.Converters;
using System.Threading.Tasks;

namespace CRL.Data
{
    /// <summary>
    /// 基本业务方法封装
    /// </summary>
    /// <typeparam name="T">源对象</typeparam>
    public abstract partial class ProviderOrigin<T>: IProvider
        where T : class, new()
    {
        public virtual async Task AddAsync(T p, bool asyn = false)
        {
            var db = DBExtend as AbsDBExtend;
            await db.InsertFromObjAsync(p);
            //redis
            //SetToRedis(p);
        }
        public virtual async Task BatchInsertAsync<T2>(List<T2> list, bool keepIdentity = false) where T2 : class, new()
        {
            var db = DBExtend;
            if (list == null || list.Count == 0)
            {
                return;
            }
            if (list.Count == 1)
            {
                await db.InsertFromObjAsync(list.First());
                return;
            }
            await db.BatchInsertAsync(list, keepIdentity);
            //redis
            //var obj = list.First();
            //if (obj is T)
            //{
            //    foreach (var item in list)
            //    {
            //        SetToRedis(item as T);
            //    }
            //}
        }
        public Task<int> DeleteAsync(Expression<Func<T, bool>> expression)
        {
            var query = GetLambdaQuery();
            query.Where(expression);
            return DBExtend.DeleteAsync(query);
        }

        public Task<int> UpdateAsync(Expression<Func<T, bool>> expression, Dictionary<string, object> updateValue)
        {
            var db = DBExtend;
            var query = GetLambdaQuery();
            query.Where(expression);
            var c = new ParameCollection(updateValue);
            var n = db.UpdateAsync(query, c);
            return n;
        }
    }
}
