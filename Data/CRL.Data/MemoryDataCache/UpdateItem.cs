﻿/**
* CRL
*/
using CRL.Core;
using CRL.Data.DBAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CRL.Data.MemoryDataCache
{
    /// <summary>
    /// 更新的项
    /// </summary>
    class UpdateItem
    {
        public string Key;
        public string TableName;
        public DBHelper DBHelper;
        public Dictionary<string, object> Params;
        public DateTime UpdateTime;
        public Type Type;
        public IEnumerable<Attribute.FieldMapping> Mapping;
    }
}
