﻿/**

*/
using CRL.Core.Extension;
using CRL.Data.Attribute;
using CRL.Data.DBAccess;
using CRL.Data.LambdaQuery;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRL.Data.DBAdapter
{
    public class SqliteDBAdapter : DBAdapterBase
    {
        //https://www.runoob.com/sqlite/sqlite-like-clause.html
        public SqliteDBAdapter(DbContextInner _dbContext)
            : base(_dbContext)
        {
        }
        public override bool CanCompileSP
        {
            get
            {
                return false;
            }
        }
        #region 创建结构

        /// <summary>
        /// 创建存储过程脚本
        /// </summary>
        /// <param name="spName"></param>
        /// <returns></returns>
        public override string GetCreateSpScript(string spName, string script)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// 获取字段类型映射
        /// </summary>
        /// <returns></returns>
        public override Dictionary<Type, string> FieldMaping()
        {
            Dictionary<Type, string> dic = new Dictionary<Type, string>();
            //字段类型对应
            dic.Add(typeof(System.String), "TEXT");
            dic.Add(typeof(System.Decimal), "decimal");
            dic.Add(typeof(System.Double), "double");
            dic.Add(typeof(System.Single), "real");
            dic.Add(typeof(System.Boolean), "bool");
            dic.Add(typeof(System.Int32), "int");
            dic.Add(typeof(System.Int16), "short");
            dic.Add(typeof(System.Enum), "int");
            dic.Add(typeof(System.Byte), "byte");
            dic.Add(typeof(System.DateTime), "datetime");
            dic.Add(typeof(System.UInt16), "ushort");
            dic.Add(typeof(System.Int64), "long");
            dic.Add(typeof(System.Object), "TEXT");
            dic.Add(typeof(System.Byte[]), "BLOB");
            dic.Add(typeof(System.Guid), "BLOB");
            return dic;
        }
        /// <summary>
        /// 获取列类型和默认值
        /// </summary>
        /// <param name="info"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public override string GetColumnType(FieldInnerAttribute info, out string defaultValue)
        {
            Type propertyType = info.PropertyType;
            if (info.ValueNeedConvert)
            {
                propertyType = typeof(string);
                info.Length = 8000;
            }
            //Dictionary<Type, string> dic = GetFieldMaping();
            defaultValue = info.DefaultValue;

            //int默认值
            if (string.IsNullOrEmpty(defaultValue))
            {
                if (!info.IsPrimaryKey && propertyType.IsNumeric())
                {
                    defaultValue = "0";
                }
                //datetime默认值
                if (propertyType == typeof(System.DateTime))
                {
                    defaultValue = "datetime('now', 'localtime')";
                }
            }
            string columnType;
            columnType = GetDBColumnType(propertyType);
            //超过3000设为ntext
            if (propertyType == typeof(System.String) && info.Length > 3000)
            {
                //columnType = "ntext";
            }
            if (info.Length > 0)
            {
                columnType = string.Format(columnType, info.Length);
            }
            if (info.IsPrimaryKey)
            {
                if (info.KeepIdentity == true)
                {
                    columnType = columnType + " ";
                }
                else
                {
                    ////todo 只有数值型才能自增
                    //if (info.PropertyType != typeof(string))
                    //{
                    //    columnType = columnType + " IDENTITY(1,1) ";
                    //}
                }
            }
            if (!string.IsNullOrEmpty(info.ColumnType))
            {
                columnType = info.ColumnType;
            }
            return columnType;
        }

        /// <summary>
        /// 创建字段脚本
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        public override string GetCreateColumnScript(DbContextInner dbContext, FieldInnerAttribute field)
        {
            var table = TypeCache.GetTable(field.ModelType);
            var tableName = TypeCache.GetTableName(table.TableName, dbContext);
            var columnType = GetColumnType(field, out var defaultValue);
            string str = string.Format("alter table [{0}] add {1} {2}", tableName, KeyWordFormat(field.MapingName), columnType);
            if (!string.IsNullOrEmpty(defaultValue))
            {
                str += string.Format(" default({0})", defaultValue);
            }
            if (field.NotNull)
            {
                str += " not null";
            }
            else if (field.PropertyType == typeof(string))
            {
                str += " COLLATE NOCASE";
            }
            return str;
        }

        /// <summary>
        /// 创建索引脚本
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        public override string GetColumnIndexScript(DbContextInner dbContext, FieldInnerAttribute field)
        {
            if (field.IsPrimaryKey)
            {
                return "";
            }
            var table = TypeCache.GetTable(field.ModelType);
            var tableName = TypeCache.GetTableName(table.TableName, dbContext);
            string indexScript = string.Format("CREATE {2} INDEX  IX_{1}  ON [{0}]([{1}])", tableName, field.MapingName, field.FieldIndexType == FieldIndexType.非聚集唯一 ? "UNIQUE" : "");
            return indexScript;
        }

        /// <summary>
        /// 创建表脚本
        /// </summary>
        /// <param name="fields"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public override void CreateTable(DbContextInner dbContext, List<FieldInnerAttribute> fields, string tableName)
        {
            string script = string.Format("create table [{0}] (\r\n", tableName);
            List<string> list2 = new List<string>();

            foreach (FieldInnerAttribute item in fields)
            {
                var columnType = GetColumnType(item, out var defaultValue);
                var str = $"{KeyWordFormat(item.MapingName)} {columnType}";
                if (item.IsPrimaryKey)
                {
                    if (item.PropertyType.IsNumeric())
                    {
                        str = $"{item.MapingName} INTEGER";
                    }
                    str += " primary key";
                }
                if (item.NotNull)
                {
                    str += " NOT NULL";
                }
                if (!string.IsNullOrEmpty(defaultValue))
                {
                    str += $" DEFAULT ({defaultValue})";
                }
                if (item.PropertyType == typeof(string) && !item.NotNull)
                {
                    str += " COLLATE NOCASE";
                }
                list2.Add(str);
            }
            script += string.Join(",\r\n", list2.ToArray());
            script += ")";
            var helper = dbContext.DBHelper;
            helper.Execute(script);
        }
        #endregion
        public override DBType DBType
        {
            get { return DBType.SQLITE; }
        }
        #region SQL查询

        public override string GetTableFields(string tableName)
        {
            //https://blog.csdn.net/aflyeaglenku/article/details/50884837
            string sql = $"select name,type from pragma_table_info('{tableName}')";
            return sql;
        }
        //static System.Collections.Concurrent.ConcurrentDictionary <string, DataTable> cacheTables = new System.Collections.Concurrent.ConcurrentDictionary<string, DataTable>();
        protected object valueFormat(FieldInnerAttribute info, object obj)
        {
            object value = info.GetValue(obj);
            value = ObjectConvert.CheckNullValue(value, info.PropertyType);
            if (info.ValueNeedConvert)
            {
                value = SettingConfig.ValueObjSerializer(value);
            }
            return value;
        }
        /// <summary>
        /// 批量插入
        /// </summary>
        /// <param name="details"></param>
        /// <param name="keepIdentity"></param>
        public override void BatchInsert(DbContextInner dbContext, System.Collections.IList details, bool keepIdentity = false)
        {
            if (details.Count == 0)
                return;
            var helper = dbContext.DBHelper;
            var sql = GetBatchInsertSql(dbContext, details, keepIdentity);
            helper.Execute(sql);
        }
        public override void InsertOrUpdate(DbContextInner dbContext, IList items, InsertOrUpdateOption option)
        {
            if (items.Count == 0)
                return;
            option = option ?? new InsertOrUpdateOption();
            var type = items[0].GetType();
            var table = TypeCache.GetTable(type);
            if (table.PrimaryKey == null)
            {
                throw new Exception($"InsertOrUpdate {table.Type} 缺少主键");
            }
            var tableName = KeyWordFormat(TypeCache.GetTableName(table.TableName, dbContext));
            var sb = new StringBuilder();
            string getFields(IEnumerable<FieldInnerAttribute> fields)
            {
                return string.Join(",", fields.Select(b => $"{KeyWordFormat(b.MapingName)}"));
            }
            //var updateFields = table.Fields.AsQueryable();
            if (option?.UpdateMemberNames?.Any() == true)
            {
                throw new Exception($"{DBType}不支持指定更新字段");
            }
            if (!string.IsNullOrEmpty(option.ConstraintMemberName))
            {
                throw new Exception($"{DBType}不支持指定关联约束字段");
            }
            var pIndex = 0;
            string getFieldValues(object item, IEnumerable<FieldInnerAttribute> fields)
            {
                var values = new List<string>();
                foreach (var f in fields)
                {
                    pIndex++;
                    var v = f.GetValue(item);
                    if (v is bool || v is Enum)
                    {
                        v = Convert.ToInt32(v);
                    }
                    //var pName = GetParamName(f.MemberName, pIndex);
                    //dbContext.DBHelper.AddParam(pName, v);
                    var str = f.PropertyType.IsNumeric() ? $"{v}" : $"'{v}'";
                    values.Add(str);
                }
                return string.Join(",", values);
            }

            sb.Append($"REPLACE INTO {tableName}({getFields(table.Fields)})  values ");
            var i = 0;
            if (option.IfExistsNotUpdate)//已经存在的不更新
            {

            }
            foreach (var item in items)
            {
                sb.Append($"({getFieldValues(item, table.Fields)})");
                if (i < items.Count - 1)
                {
                    sb.Append(",");
                }
                i += 1;
            }
            option.SqlOut = sb.ToString();
            dbContext.DBHelper.Execute(option.SqlOut);
        }

        public override async Task BatchInsertAsync(DbContextInner dbContext, IList details, bool keepIdentity = false)
        {
            if (details.Count == 0)
                return;
            var helper = dbContext.DBHelper;
            var sql = GetBatchInsertSql(dbContext, details, keepIdentity);
            await helper.ExecuteAsync(sql);
        }

        /// <summary>
        /// 插入对象,并返回主键
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override object InsertObject<T>(DbContextInner dbContext, T obj)
        {
            Type type = obj.GetType();
            var helper = dbContext.DBHelper;
            helper.ClearParams();
            var table = TypeCache.GetTable(type);
            var primaryKey = table.PrimaryKey;
            var tableName = TypeCache.GetTableName(table.TableName, dbContext);
            var sql = GetInsertSql(dbContext, table, obj);
            if (primaryKey == null)
            {
                SqlStopWatch.Execute(helper, sql);
                return null;
            }
            if (primaryKey.KeepIdentity == true)
            {
                SqlStopWatch.Execute(helper, sql);
                return primaryKey.GetValue(obj);
            }
            else
            {
                sql += $";select LAST_INSERT_ROWID() FROM [{tableName}]";
                return SqlStopWatch.ExecScalar(helper, sql);
            }
        }
        public override async Task<object> InsertObjectAsync<T>(DbContextInner dbContext, T obj)
        {
            Type type = obj.GetType();
            var helper = dbContext.DBHelper;
            helper.ClearParams();
            var table = TypeCache.GetTable(type);
            var primaryKey = table.PrimaryKey;
            var tableName = TypeCache.GetTableName(table.TableName, dbContext);
            var sql = GetInsertSql(dbContext, table, obj);
            if (primaryKey == null)
            {
                await SqlStopWatch.ExecuteAsync(helper, sql);
                return null;
            }
            if (primaryKey.KeepIdentity == true)
            {
                await SqlStopWatch.ExecuteAsync(helper, sql);
                return primaryKey.GetValue(obj);
            }
            else
            {
                sql += $";select LAST_INSERT_ROWID() FROM [{tableName}]";
                return await SqlStopWatch.ExecScalarAsync(helper, sql);
            }
        }
        /// <summary>
        /// 获取 with(nolock)
        /// </summary>
        /// <returns></returns>
        public override string GetWithNolockFormat(bool v)
        {
            return "";
        }
        /// <summary>
        /// 获取前几条语句
        /// </summary>
        /// <param name="fields">id,name</param>
        /// <param name="query">from table where 1=1</param>
        /// <param name="sort"></param>
        /// <param name="top"></param>
        /// <returns></returns>
        public override void GetSelectTop(StringBuilder sb, string fields, Action<StringBuilder> query, string sort, int top)
        {
            sb.Append("select ");
            sb.Append(fields);
            query(sb);
            if (!string.IsNullOrEmpty(sort))
            {
                sb.Append(sort);
            }
            sb.Append(top == 0 ? "" : " LIMIT 0," + top);
        }
        #endregion

        #region 系统查询
        public override string GetAllTablesSql(string db)
        {
            return " select name,name from sqlite_master where type='table' order by name; ";
        }
        public override string GetAllSPSql(string db)
        {
            throw new NotSupportedException();
            return "select name,id from sysobjects where  type='P'";
        }
        #endregion

        #region 模版
        public override string SpParameFormat(string name, string type, bool output)
        {
            throw new NotSupportedException();
        }
        public override string KeyWordFormat(string value)
        {
            return string.Format("[{0}]", value);
        }
        //public override string FieldNameFormat(FieldAttribute field)
        //{
        //    if(string.IsNullOrEmpty(field.MapingNameFormat))
        //    {
        //        return field.MapingName;
        //    }
        //    return field.MapingNameFormat;
        //}
        public override string TemplateGroupPage
        {
            get
            {
                throw new NotSupportedException();

            }
        }

        public override string TemplatePage
        {
            get
            {
                throw new NotSupportedException();
            }
        }

        public override string TemplateSp
        {
            get
            {
                throw new NotSupportedException();
            }
        }
        public override string SqlFormat(string sql)
        {
            return sql;
        }
        #endregion

        #region 函数格式化
        public override string SubstringFormat(string field, int index, int length)
        {
            return string.Format(" substr({0},{1},{2})", field, index + 1, length);
        }

        public override string StringLikeFormat(string field, string parName)
        {
            return string.Format("{0} LIKE {1}", field, parName);
        }

        public override string StringNotLikeFormat(string field, string parName)
        {
            return string.Format("{0} NOT LIKE {1}", field, parName);
        }

        public override string StringContainsFormat(string field, string parName)
        {
            return $"{field} like '%'+{parName}+'%'";
        }
        public override string StringNotContainsFormat(string field, string parName)
        {
            return $"{field} not like '%'+{parName}+'%'";
        }
        public override string BetweenFormat(string field, string parName, string parName2)
        {
            return string.Format("{0} between {1} and {2}", field, parName, parName2);
        }

        public override string DateDiffFormat(string field, string format, string parName)
        {
            return "0";
            //throw new NotSupportedException("DateDiff");
            return string.Format("DateDiff({0},{1},{2})", format, field, parName);
        }

        public override string InFormat(string field, string parName)
        {
            return string.Format("{0} IN ({1})", field, parName);
        }
        public override string NotInFormat(string field, string parName)
        {
            return string.Format("{0} NOT IN ({1})", field, parName);
        }
        #endregion

        public override string PageSqlFormat(DBHelper db, string fields, string rowOver, string condition, int start, int end, string sort)
        {
            start -= 1;
            if (start < 0)
            {
                start = 0;
            }
            db.AddParam(":start", start);
            db.AddParam(":row", end - start);
            string sql = "select {0} {1} {4} limit {2},{3} ";
            return string.Format(sql, fields, condition, ":start", ":row", string.IsNullOrEmpty(sort) ? "" : "order by " + sort);
        }
        public override string GetRelationUpdateSql(string t1, string t2, string condition, string setValue, LambdaQueryBase query)
        {
            if (condition.ToLower().Contains(" join "))
            {
                throw new NotImplementedException();
            }
            else
            {
                setValue = setValue.Replace("t1.", "");
                condition = condition.Replace("t1.", $"{KeyWordFormat(t1)}.");
                return $"update {KeyWordFormat(t1)} set {setValue} from {t2} t2 where {condition}";
            }
        }
        public override string GetRelationDeleteSql(string t1, string t2, string condition, LambdaQueryBase query)
        {
            string table = string.Format("{0} t1", KeyWordFormat(t1), KeyWordFormat(t2));
            string sql = string.Format("delete t1 from {0} {1}", table, condition);
            return sql;
        }
        public override string GetFieldConcat(string field, object value, Type type)
        {
            string str;
            if (type == typeof(string))
            {
                str = string.Format("{0}+'{1}'", field, value);
            }
            else
            {
                str = string.Format("{0}+{1}", field, value);
            }
            return str;
        }
        public override string CastField(string field, Type fieldType)
        {
            var dic = FieldMaping();
            if (!dic.ContainsKey(fieldType))
            {
                throw new Exception(string.Format("没找到对应类型的转换{0} 在字段{1}", fieldType, field));
            }
            var type = dic[fieldType];
            type = string.Format(type, 100);
            return string.Format("CAST({0} as {1})", field, type);
        }
        public override string GetParamName(string name, object index)
        {
            return string.Format(":{0}{1}", name, index);
        }
        public override string GetColumnUnionIndexScript(string tableName, string indexName, List<string> columns, FieldIndexType fieldIndexType)
        {
            var script = string.Format("create index [{1}] on [{0}] ({2})", tableName, indexName, string.Join(",", columns.ToArray()));
            return script;
        }
        public override string DateTimeFormat(string field, string format)
        {
            return string.Format("CONVERT(text, {0}, {1})", field, format);
        }
        public override string GetSplitFirst(string field, string parName)
        {
            return "'NotSupported'";
            //throw new NotSupportedException("SplitFirst");
            return $"substring({field},1,charindex('{parName}',{field})-1)";
        }
        public override Dictionary<string, long> GetFieldLength(DbContextInner dbContext, string tableName)
        {
            return new Dictionary<string, long>();
        }
        public override string GetUpdateColumnScript(string tableName, FieldInnerAttribute field)
        {
            var columnType = GetDBColumnType(field.PropertyType);
            if (field.Length > 0)
            {
                columnType = string.Format(columnType, field.Length);
            }
            var str = GetDropColumnScript(tableName, field.MapingName);
            return str + $";alter table {KeyWordFormat(tableName)} add {field.MapingName} {columnType}";
        }
    }
}
