﻿using CRL.Data;
using CRL.Data.Attribute;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRL.Data.DBAdapter
{
    public partial class MySQLDBAdapter
    {
        public override async Task<object> InsertObjectAsync<T>(DbContextInner dbContext, T obj)
        {
            Type type = obj.GetType();
            var helper = dbContext.DBHelper;
            var table = TypeCache.GetTable(type);
            var primaryKey = table.PrimaryKey;
            var sql = GetInsertSql(dbContext, table, obj);
            if (primaryKey == null)
            {
                await SqlStopWatch.ExecuteAsync(helper, sql);
                return null;
            }
            if (primaryKey.KeepIdentity == true)
            {
                await SqlStopWatch.ExecuteAsync(helper, sql);
                return primaryKey.GetValue(obj);
            }
            else
            {
                sql += ";SELECT LAST_INSERT_ID();";
                return await SqlStopWatch.ExecScalarAsync(helper, sql);
            }
        }
    }
}
