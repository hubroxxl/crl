﻿using CRL.Data.LambdaQuery.Mapping;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace CRL.Data
{
    public class RepositoryFactory
    {
        public static BaseProvider<T> Get<T>(string manageName = "") where T : class, new()
        {
            return new SimpleRepository<T>(manageName);
        }
        class SimpleRepository<T> : BaseProvider<T> where T : class, new()
        {
            string _manageName;
            public override string ManageName => _manageName;
            public SimpleRepository(string manageName)
            {
                _manageName = manageName;
            }
        }
    }
    
}
