﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CRL.Data.Sharding
{
    /// <summary>
    /// 数据定位
    /// </summary>
    public class Location
    {
        public Location(string dataBaseSource,string tablePartName)
        {
            DataBaseSource = dataBaseSource;
            TablePartName = tablePartName;
        }
        /// <summary>
        /// 库名
        /// </summary>
        public string DataBaseSource;
        /// <summary>
        /// 主表名
        /// </summary>
        public string TableName;
        /// <summary>
        /// 当前分表名
        /// </summary>
        public string TablePartName;

        public override string ToString()
        {
            return string.Format("在库[{0}],表[{1}]", DataBaseSource, TablePartName);
        }
    }
}
