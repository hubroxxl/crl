﻿using MySqlConnector;
using CRL.Data;
using CRL.Data.DBAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRL.MySql
{
    public static class Extension
    {
        public static IDbConfigRegister UseMySql(this IDbConfigRegister iBuilder)
        {
            iBuilder.SetDbProviderFactory(MySqlConnectorFactory.Instance);
            return iBuilder;
        }
    }
}
