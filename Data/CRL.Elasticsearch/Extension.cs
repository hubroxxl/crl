﻿/**
* CRL
*/
using CRL.Data;
using CRL.Data.DBAccess;
using CRL.Data.LambdaQuery;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRL.Elasticsearch
{
    public static class Extension
    {
        public static IDbConfigRegister UseElasticsearch(this IDbConfigRegister iBuilder)
        {
            var builder = iBuilder as DBConfigRegister;
            builder.RegisterDBType(DBType.ES, (dBAccessBuild) =>
            {
                return new ESHelper(dBAccessBuild);
            }, (context) =>
            {
                return new ESAdapter(context);
            });
            builder.RegisterDBExtend<ESEx.ESExtend>(DBType.ES, (context) =>
            {
                return new ESEx.ESExtend(context);
            });
            builder.RegisterLambdaQueryType(DBType.ES, typeof(ESLambdaQuery<>));
            return builder;
        }
        /// <summary>
        /// 客户端扩展
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="client"></param>
        /// <returns></returns>
        public static ILambdaQuery<T> GetLambdaQuery<T>(this ElasticClient client) where T : class
        {
            var helper = new ESHelper(client);
            var db = new DbContextInner(helper, new DBLocation());
            var query = LambdaQueryFactory.CreateLambdaQuery<T>(db);
            return query;
        }
        public static IAbsDBExtend GetDBExtend(this ElasticClient client)
        {
            var helper = new ESHelper(client);
            var dbContext2 = new DbContextInner(helper, new DBLocation() { });
            return DBExtendFactory.CreateDBExtend(dbContext2);
        }
    }
}
