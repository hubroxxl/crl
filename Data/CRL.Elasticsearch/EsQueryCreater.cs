﻿using Nest;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace CRL.Elasticsearch
{
    public static class EsQueryCreater
    {
        //https://www.cnblogs.com/yjf512/p/4897294.html
        /// <summary>
        /// id查询
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public static QueryContainer CreateIdsQuery(IEnumerable<Id> values)
        {
            var item = new IdsQuery
            {
                Values = values
            };
            return item;
        }
        /// <summary>
        /// 通配符查询
        /// </summary>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static QueryContainer CreateWildcardQuery(string field, object value)
        {
            var item = new WildcardQuery
            {
                Field = field,
                Value = value
            };
            return item;
        }
        /// <summary>
        /// 词条查询
        /// </summary>
        /// <param name="field"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static QueryContainer CreateTermQuery(string field, object value)
        {
            var item = new TermQuery
            {
                Field = field,
                Value = value
            };
            return item;
        }
        public static QueryContainer CreateTermsQuery(string field, IEnumerable<object> value)
        {
            var item = new TermsQuery
            {
                Field = field,
                Terms = value
            };
            return item;
        }
        public static QueryContainer CreateFuzzyQuery(string field, object value)
        {
            var item = new FuzzyQuery
            {
                Field = field,
                Fuzziness = Fuzziness.Auto,
                Value = value + "",
                Boost = 100
            };
            return item;
        }
        public static QueryContainer CreateMatchQuery(string field, string value, Operator _operator = Operator.And)
        {
            var item = new MatchQuery
            {
                Field = field,
                Query = value,
                Operator = _operator
            };
            return item;
        }
        public static QueryContainer CreateMatchPhraseQuery(string field, string value, int slop = 0)
        {
            var item = new MatchPhraseQuery
            {
                Field = field,
                Query = value,
                Slop = slop
            };
            return item;
        }
        public static QueryContainer CreateEqualQuery(string field, object value)
        {
            if (value == null)
            {
                throw new Exception("值不能为空");
            }
            if (value.GetType() == typeof(string))
            {
                return CreateMatchPhraseQuery(field, value.ToString());
            }
            return CreateTermQuery(field, value);
        }
        public static QueryContainer CreateMultiMatchQuery(string[] fields, string value, Operator Operator, TextQueryType textQueryType, int slop = 0)
        {
            var item = new MultiMatchQuery
            {
                Fields = fields,
                Query = value,
                Operator = Operator,
                Type = textQueryType,
                Slop = slop
            };
            return item;
        }

        #region range
        public static QueryContainer CreateRangeQuery(Type dataType, bool orEq, string field, object lessThan = null, object greaterThan = null)
        {
            if (dataType == typeof(DateTime))
            {
                return CreateDateRangeQuery(orEq, field, lessThan as DateMath, greaterThan as DateMath);
            }
            else
            {
                return CreateLongRangeQuery(orEq, field, lessThan == null ? new long?() : Convert.ToInt64(lessThan.ToString()), greaterThan == null ? new long?() : Convert.ToInt64(greaterThan.ToString()));
            }
        }
        public static QueryContainer CreateDateRangeQuery(bool orEq, string field, DateMath lessThan = null, DateMath greaterThan = null)
        {
            var item = new DateRangeQuery
            {
                Field = field
            };
            if (orEq)
            {
                if (lessThan != null)
                {
                    item.LessThanOrEqualTo = lessThan;
                }
                if (greaterThan != null)
                {
                    item.GreaterThanOrEqualTo = greaterThan;
                }
            }
            else
            {
                if (lessThan != null)
                {
                    item.LessThan = lessThan;
                }
                if (greaterThan != null)
                {
                    item.GreaterThan = greaterThan;
                }
            }
            return item;
        }
        public static QueryContainer CreateLongRangeQuery(bool orEq, string field, long? lessThan = null, long? greaterThan = null)
        {
            var item = new LongRangeQuery
            {
                Field = field
            };
            if (orEq)
            {
                if (lessThan != null)
                {
                    item.LessThanOrEqualTo = lessThan;
                }
                if (greaterThan != null)
                {
                    item.GreaterThanOrEqualTo = greaterThan;
                }
            }
            else
            {
                if (lessThan != null)
                {
                    item.LessThan = lessThan;
                }
                if (greaterThan != null)
                {
                    item.GreaterThan = greaterThan;
                }
            }
            return item;
        }
        #endregion
    }
}
