﻿/**
* CRL
*/
using CRL.Data.LambdaQuery;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CRL.Elasticsearch.ESEx
{
    public sealed partial class ESExtend
    {
        public override Task<List<TResult>> QueryResultAsync<TResult>(LambdaQueryBase query, NewExpression newExpression = null)
        {
            throw new NotImplementedException();
        }

        public override Task<List<dynamic>> QueryDynamicAsync(LambdaQueryBase query)
        {
            throw new NotImplementedException();
        }

        public override Task<List<TModel>> QueryOrFromCacheAsync<TModel>(ILambdaQuery<TModel> query)
        {
            throw new NotImplementedException();
        }
    }
}
