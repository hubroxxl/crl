﻿/**
* CRL
*/
using CRL.Data.LambdaQuery;
using Nest;

namespace CRL.Elasticsearch.ESEx
{
    public sealed partial class ESExtend
    {

        public override int Delete<T>(ILambdaQuery<T> query1)
        {
            var query = query1 as ESLambdaQuery<T>;
            var client = getClient();
            var delQuery = new DeleteByQueryRequest(IndexName);
            delQuery.Query = new BoolQuery { Filter = query.queryContainers };
            var result = client.DeleteByQuery(delQuery);
            return (int)result.Deleted;
        }
    }
}
