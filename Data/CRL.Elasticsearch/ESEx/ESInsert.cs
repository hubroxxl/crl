﻿/**
* CRL
*/
using CRL.Data;
using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRL.Elasticsearch.ESEx
{
    public sealed partial class ESExtend
    {
        public override void BatchInsert<TModel>(List<TModel> details, bool keepIdentity = false)
        {
            if (details.Count == 0)
                return;
            var client = getClient();
            var result = client.IndexMany(details, IndexName);
            if (result.Errors)
            {
                throw new Exception($"{result.ServerError} {result.OriginalException}");
            }
        }
        public override void InsertFromObj<TModel>(TModel obj)
        {
            BatchInsert(new List<TModel> { obj });
            //var client = getClient();
            //var result = client.IndexDocument(obj);
            //if (result.Result == Result.Error)
            //{
            //    throw new Exception($"{result.ServerError} {result.OriginalException}");
            //}
        }
    }
}
