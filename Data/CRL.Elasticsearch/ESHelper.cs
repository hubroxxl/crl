﻿/**
* CRL
*/
using CRL.Data;
using CRL.Data.DBAccess;
using Nest;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace CRL.Elasticsearch
{
    public class ESHelper : DBHelper
    {
        string _dataBaseName;
        internal ElasticClient _client;
        public ESHelper(DBAccessBuild dBAccessBuild)
            : base(null,dBAccessBuild)
        {
            var _connectionString = dBAccessBuild.ConnectionString;
            var lastIndex = _connectionString.LastIndexOf("/");
            var dataBaseName = _connectionString.Substring(lastIndex + 1);//like ES://localhost:27017/db1
            ConnectionString = _connectionString.Substring(0, lastIndex);
            _dataBaseName = dataBaseName;
        }
        public ESHelper(ElasticClient client) : base(null, new DBAccessBuild(DBType.ES, "default"))
        {
            _client = client;
        }

        public override string DatabaseName
        {
            get
            {
                return _dataBaseName;
            }
        }
    }
}
