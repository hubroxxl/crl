﻿/**
* CRL
*/
using CRL.Core;
using CRL.Data;
using CRL.Data.DBExtend.RelationDB;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace EFTest
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleTest.DoCommand(typeof(Program));
        }

        public static void testQuery()
        {
            using (var context = new Context1())
            {
                var set1 = context.Set1;
                context.CreateTable<TestClass>();
                var query = context.GetLambdaQuery<TestClass>();
                var list2 = query.Page(15, 1).ToList();
                Console.WriteLine($"{list2.Count} {query.RowCount}");
                return;
                query.Join<TestClass2>((a, b) => a.Id == b.Id);
                query.PrintQuery();
                var list = query.ToList();
                var result = query.ToSingle();
                var db = context.GetDBExtend();
                db.Update<TestClass>(b => b.Id == 1, new { Name = "222" });
                //db.InsertFromObj(new TestClass() { Id = DateTime.Now.Second, Name = "ddddd" });
                //db.BatchInsert(new List<TestClass>() { new TestClass() { Id = DateTime.Now.Millisecond, Name = "ddddd" } },true);
                //var n = db.Delete<TestClass>(b => b.Id > 0);
                //Console.WriteLine(n);
            }
        }
        public static void testTransaction()
        {
            using (var context = new Context1())
            {

                context.BeginTransaction();
                //context.Add(new TestClass() { Id = DateTime.Now.Second+1, Name = "ddddd" });
                var query = context.GetLambdaQuery<TestClass>().Take(1);
                var list = query.ToList();

                var db = context.GetDBExtend();
                //db.InsertFromObj(new TestClass() { Id = DateTime.Now.Second, Name = "ddddd" });
                db.Update<TestClass>(b => b.Id == 1, new { Name = "ddddd" });
                context.Commit();
            }
        }
        public static void testInsert()
        {
            using (var context = new Context1())
            {
                var db = context.GetDBExtend();
                var list = new List<TestClass>();
                list.Add(new TestClass { Name = "2222" });
                list.Add(new TestClass { Name = "4444" });
                db.BatchInsert(list);
            }

        }
        public static void testDa()
        {
            using (var context = new Context1())
            {
                var db = context.GetDBExtend() as DBExtend;
                var dt = db.dbContext.DBHelper.ExecDataTable("select 1  as name");
                Console.WriteLine(dt.Rows.Count);
            }
        }
    }
}
