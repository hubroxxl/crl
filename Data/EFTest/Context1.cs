﻿/**
* CRL
*/
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using CRL.Data;
using System.Data;
using CRL.Data.LambdaQuery;
using Microsoft.EntityFrameworkCore.Storage;
using System.Linq.Expressions;
using CRL.EFCore.Extensions;

namespace EFTest
{
    public class Context1 : DbContext
    {
        public DbSet<TestClass> Set1 { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var e = modelBuilder.Entity<TestClass>();
            e.ToTable("TestClass", "dbo");
            e.HasKey(b => b.Id);
            e.Property(b => b.Name).HasMaxLength(50) ;
            e.ConfigEntityTypeBuilder();
            base.OnModelCreating(modelBuilder);
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var conn = "server=.;database=testDb; uid=sa;pwd=123;";
            conn = "server=47.105.149.240,61289;database=testDb; uid=hwpg;pwd=dhG2*uwbv;";
            optionsBuilder.UseSqlServer(conn);
            string mySqlDb = "Server=127.0.0.1;Database=testdb;Uid=root;Pwd=123;AllowLoadLocalInfile=true";
            //optionsBuilder.UseMySql(mySqlDb, ServerVersion.AutoDetect(mySqlDb));
            string sqlLiteDb = "Data Source=d:\\sqlliteTest.db;";
            //optionsBuilder.UseSqlite(sqlLiteDb);
            optionsBuilder.UseExtensions();
            base.OnConfiguring(optionsBuilder);
        }
        public override void Dispose()
        {
            dbConnection?.Dispose();
            base.Dispose();
            Console.WriteLine("dbConnection?.Dispose");
        }
        IDbConnection dbConnection;
        //IDbTransaction extenDbTransaction;
        IDbTransaction efDbTransaction;

        internal IDbConnection GetConnection(out IDbTransaction dbTransaction)
        {
            dbTransaction = efDbTransaction;
            if (dbConnection == null)
            {
                dbConnection = this.Database.GetDbConnection();
                if (dbConnection.State != ConnectionState.Open)
                {
                    dbConnection.Open();
                }
            }

            return dbConnection;
        }
        public IAbsDBExtend GetDBExtend()
        {
            var dbConnection = GetConnection(out var dbTrans);
            return dbConnection.GetDBExtend(dbTrans);
        }
        public void CreateTable<T>()
        {
            var dbConnection = GetConnection(out var dbTrans);
            dbConnection.CreateTable<T>();
        }
        public ILambdaQuery<T> GetLambdaQuery<T>() where T : class
        {
            var dbConnection = GetConnection(out var dbTrans);
            return dbConnection.GetLambdaQuery<T>(dbTrans);
        }
        IDbContextTransaction contextTrans;
        public void BeginTransaction()
        {
            contextTrans = Database.BeginTransaction();
            if (efDbTransaction == null)
            {
                efDbTransaction = contextTrans.GetDbTransaction();
            }
        }
        public void Commit()
        {
            try
            {
                SaveChanges();
                contextTrans.Commit();
            }
            catch(Exception ero)
            {
                contextTrans.Rollback();
            }
            contextTrans.Dispose();
        }

        public int Update<TEntity>(Expression<Func<TEntity, bool>> expression, object updateValue) where TEntity : class
        {
            return GetDBExtend().Update(expression, updateValue);
        }
    }
}
