﻿/**
* CRL
*/
using CRL.Data;
using CRL.Data.DBAccess;
using CRL.Data.LambdaQuery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CRL.Mongo
{
    public static class Extension
    {
        public static IDbConfigRegister UseMongoDB(this IDbConfigRegister iBuilder)
        {
            var builder = iBuilder as DBConfigRegister;
            builder.RegisterDBType(DBType.MongoDB, (dBAccessBuild) =>
            {
                return new MongoDBHelper(dBAccessBuild);
            }, (context) =>
            {
                return new MongoDBAdapter(context);
            });
            builder.RegisterDBExtend<MongoDBEx.MongoDBExt>(DBType.MongoDB, (context) =>
            {
                return new MongoDBEx.MongoDBExt(context);
            });
            builder.RegisterLambdaQueryType(DBType.MongoDB, typeof(MongoDBLambdaQuery<>));
            return builder;
        }

        public static ILambdaQueryResultSelect<TResult> HavingCount<TResult>(this ILambdaQueryResultSelect<TResult> resultSelect, System.Linq.Expressions.Expression<Func<TResult, bool>> expression)
        {
            var visitor = resultSelect.BaseQuery.GetVisitor();
            var cRLExpression = visitor.RouteExpressionHandler(expression.Body);
            var mongoQuery = resultSelect.BaseQuery as IMongoDBLambdaQuery;
            mongoQuery.SetHavingCount(cRLExpression);
            return resultSelect;
        }
    }
}
