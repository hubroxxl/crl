﻿/**
* CRL
*/
using CRL.Data;
using CRL.Data.DBAccess;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;

namespace CRL.Mongo
{
    public class MongoDBHelper : DBHelper
    {
        string _dataBaseName;
        public MongoDBHelper(DBAccessBuild dBAccessBuild)
            : base(null, dBAccessBuild)
        {
            var _connectionString = dBAccessBuild.ConnectionString;
            var lastIndex = _connectionString.LastIndexOf("/");
            var dataBaseName = _connectionString.Substring(lastIndex + 1);//like mongodb://localhost:27017/db1
            _dataBaseName = dataBaseName;
        }
        public override string DatabaseName
        {
            get
            {
                return _dataBaseName;
            }
        }

       
    }
}
