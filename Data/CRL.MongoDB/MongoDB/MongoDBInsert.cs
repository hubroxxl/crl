﻿/**
* CRL
*/
using CRL.Core.Extension;
using CRL.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRL.Mongo.MongoDBEx
{
    public sealed partial class MongoDBExt
    {
        public override void BatchInsert<TModel>(List<TModel> details, bool keepIdentity = false)
        {
            if (details.Count == 0)
                return;
            var table = TypeCache.GetTable(typeof(TModel));
            var collection = GetCollection<TModel>();
            if (table.PrimaryKey?.KeepIdentity == true)
            {
                keepIdentity = table.PrimaryKey.KeepIdentity;
            }
            if (!keepIdentity && table.PrimaryKey?.PropertyType?.IsNumeric() == true)
            {
                foreach (var item in details)
                {
                    var index = getId(table.TableName);
                    table.PrimaryKey?.SetValue(item, index);
                }
            }
            collection.InsertMany(details);
        }
        int getId(string tableName)
        {
            var newIndex = _MongoDB.RunCommand<MongoDB.Bson.BsonDocument>(@"{findAndModify:'ids',query:{_id:'" + tableName + @"'}, update:{
$inc:{ 'currentIdValue':1}
        }, new:true,upsert:true}");
            var index = newIndex["value"]["currentIdValue"].AsInt32;
            return index;
        }
        public override void InsertFromObj<TModel>(TModel obj)
        {
            var table = TypeCache.GetTable(typeof(TModel));
            if (table.PrimaryKey != null)
            {
                if (!table.PrimaryKey.KeepIdentity && table.PrimaryKey.PropertyType.IsNumeric())
                {
                    var index = getId(table.TableName);
                    table.PrimaryKey.SetValue(obj, index);
                }
            }

            var collection = GetCollection<TModel>();
            collection.InsertOne(obj);
        }
    }
}
