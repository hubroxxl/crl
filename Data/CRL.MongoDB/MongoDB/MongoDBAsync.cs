﻿/**
* CRL
*/
using CRL.Data.LambdaQuery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Linq.Expressions;
using MongoDB.Driver.Linq;
using System.Reflection;
using System.Reflection.Emit;
using CRL.Core;
using CRL.Data;
using CRL.Data.Attribute;

namespace CRL.Mongo.MongoDBEx
{
    public sealed partial class MongoDBExt
    {
        public override Task<List<TResult>> QueryResultAsync<TResult>(LambdaQueryBase query, NewExpression newExpression = null)
        {
            throw new NotImplementedException();
        }

        public override Task<List<dynamic>> QueryDynamicAsync(LambdaQueryBase query)
        {
            throw new NotImplementedException();
        }

        public override Task<List<TModel>> QueryOrFromCacheAsync<TModel>(ILambdaQuery<TModel> query)
        {
            throw new NotImplementedException();
        }
    }
}
