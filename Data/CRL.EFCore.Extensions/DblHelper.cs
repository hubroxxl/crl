﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
//using Microsoft.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;
using CRL.Data.DBAccess;
using CRL.Data;
using System.Reflection;
using Microsoft.EntityFrameworkCore;

namespace CRL.EFCore.Extensions
{
    class DblHelper : DBHelper
    {
        public DblHelper(DbProviderFactory dbFactory, DBAccessBuild dBAccessBuild)
            : base(dbFactory, dBAccessBuild)
        {
            //dbFactory = _dbFactory;
            //if (dbFactory == null && dBAccessBuild._connection != null)
            //{
            //    var pro = dBAccessBuild._connection.GetType().GetProperty("DbProviderFactory", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            //    dbFactory = (DbProviderFactory)pro.GetValue(dBAccessBuild._connection);
            //}
        }
        public override string DatabaseName
        {
            get
            {
                if (string.IsNullOrEmpty(databaseName))
                {
                    databaseName = currentConn?.Database;
                }
                return databaseName;
            }
        }
        
    }
}
