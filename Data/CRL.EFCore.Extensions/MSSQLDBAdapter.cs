﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CRL.Data.DBAccess;
using CRL.Data.Attribute;
using System.Threading.Tasks;
using System.Collections;
using CRL.Core.Extension;
using CRL.Data.DBAdapter;
using CRL.Data;
namespace CRL.EFCore.Extensions
{

    internal partial class MSSQLDBAdapter2 : MSSQLDBAdapter
    {
        public MSSQLDBAdapter2(DbContextInner _dbContext)
            : base(_dbContext)
        {
        }
        public override void BatchInsert(DbContextInner dbContext, System.Collections.IList details, bool keepIdentity = false)
        {
            if (details.Count == 0)
                return;
            var helper = dbContext.DBHelper;
            var sql = GetBatchInsertSql(dbContext, details, keepIdentity);
            helper.Execute(sql);
        }
    }
}
