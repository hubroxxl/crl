﻿/**
* CRL
*/
using CRL.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRLCoreTest.Code.Sharding
{
    public class OrderSharding : IModel
    {
        public int MemberId
        {
            get;
            set;
        }
        public string Remark
        {
            get;
            set;
        }

    }
    public class OrderRepository : CRL.Data.Sharding.BaseProvider<OrderSharding>
    {
        public static OrderRepository Instance
        {
            get { return new OrderRepository(); }
        }
    }
}
