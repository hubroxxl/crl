﻿/**
* CRL
*/
using CRL.Data.Attribute;
using CRL.Data.Sharding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRLCoreTest.Code.Sharding
{
    public class MemberSharding
    {
        [Field(KeepIdentity=true)]//保持插入主键
        public int Id
        {
            get;
            set;
        }
        public string Name
        {
            get;
            set;
        }
        public string Code;
    }
    public class MemberRepository : BaseProvider<MemberSharding>
    {

    }

}
