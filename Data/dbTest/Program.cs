﻿
using BenchmarkDotNet.Running;
using CRL.Core;
using CRL.Data;
using CRL.Data.DBAccess;
using CRL.Sqlite;
using System.Collections.Generic;
using System;
using System.Reflection;
using BenchmarkDotNet.Configs;
using dbTest.tests;
using dbTest.items;
using dbTest;

public class Program
{
    static void Main(string[] args)
    {
        MyTest.Init();
        new SqlSugarTest().testQueryJoin();
        new MyTest().testQueryJoin();
        //RepositoryFactory.Get<TestEntityItem>().CreateTable();
        ConsoleTest.DoCommand(typeof(Program));
    }
    public static void testQueryResult()
    {
        var summary = BenchmarkRunner.Run<ResultTest>(ManualConfig
                    .Create(DefaultConfig.Instance).WithOptions(ConfigOptions.DisableOptimizationsValidator));
    }
    public static void testQueryAnonymousResult()
    {
        var summary = BenchmarkRunner.Run<AnonymousResultTest>(ManualConfig
                    .Create(DefaultConfig.Instance).WithOptions(ConfigOptions.DisableOptimizationsValidator));
    }
    public static void testQueryCondition()
    {
        var summary = BenchmarkRunner.Run<ConditionTest>(ManualConfig
                    .Create(DefaultConfig.Instance).WithOptions(ConfigOptions.DisableOptimizationsValidator));
    }
    public static void testQueryLoop()
    {
        var summary = BenchmarkRunner.Run<QueryLoopTest>(ManualConfig
                    .Create(DefaultConfig.Instance).WithOptions(ConfigOptions.DisableOptimizationsValidator));
    }
    //public static void testPart()
    //{
    //    var summary = BenchmarkRunner.Run<PartTest>(ManualConfig
    //                .Create(DefaultConfig.Instance).WithOptions(ConfigOptions.DisableOptimizationsValidator));
    //}

    //public static void testPart2()
    //{
    //    new ChloeTest().testQueryJoin();
    //    new FreeSqlTest().testQueryJoin();

    //    new FastFrameworkTest().testQueryJoin();
    //    new CRLTest().testQueryJoin();
    //    new SqlSugarTest().testQueryJoin();
    //}
}
