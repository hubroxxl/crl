﻿using BenchmarkDotNet.Attributes;
using Chloe.Reflection;
using dbTest.items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace dbTest
{
    public abstract class TestBase
    {
        protected List<ITest> tests = new List<ITest>();
        public TestBase()
        {
            MyTest.Init();
            var types = typeof(ITest).GetAssembly().GetTypes().Where(b => typeof(ITest).IsAssignableFrom(b) && !b.IsAbstract);
            foreach (var t in types)
            {
                tests.Add(Activator.CreateInstance(t) as ITest);
            }
            //tests.Add(new SqlSugarTest());
            //tests.Add(new CRLTest());
            //tests.Add(new ChloeTest());
            //tests.Add(new FreeSqlTest());
            //tests.Add(new EfSqlliteTest());
            //tests.Add(new DapperTest());
        }
        public List<string> _needles => tests.Select(b => b.GetType().Name).ToList();
        [ParamsSource(nameof(_needles))]
        public string ProvideType { get; set; }

        public abstract string MethodName { get; }
        //protected MethodInfo method;

        protected void InvokeTest()
        {
            var test = tests.Find(b => b.GetType().Name == ProvideType);
            switch (MethodName)
            {
                case nameof(ITest.testQueryAnonymousResult):
                    test.testQueryAnonymousResult();
                    break;
                case nameof(ITest.testQueryResult):
                    test.testQueryResult();
                    break;
                case nameof(ITest.testQueryCondition):
                    test.testQueryCondition();
                    break;
                case nameof(ITest.testQueryLoop):
                    test.testQueryLoop();
                    break;
            }
            //if (method == null)
            //{
            //    method = test.GetType().GetMethod(MethodName);
            //    //methodFunc = DynamicMethodHelper.CreateMethodInvoker(method);
            //}
            //method.Invoke(test, new object[] { });
        }
    }
}
