﻿using System;
using System.Linq.Expressions;

namespace dbTest.items
{
    public abstract class ITest
    {
        public static string sqlLiteDb = $"Data Source={AppDomain.CurrentDomain.BaseDirectory}sqlliteTest.db;";
        public abstract void testQueryResult();
        public abstract void testQueryAnonymousResult();
        public abstract void testQueryCondition();
        public abstract void testQueryLoop();

        //public abstract void testQueryJoin();
        protected Expression<Func<TestEntity, bool>> GetSelectFilter()
        {
            return b => b.F_String == "111" && b.F_Decimal > 0;
        }
        protected int listTake = 100;
    }

}
