﻿using Chloe;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dbTest.items
{
    public class SqlSugarTest : ITest
    {
        SqlSugarClient getDb()
        {
            return new SqlSugarClient(new ConnectionConfig()
            {
                ConnectionString = sqlLiteDb,
                DbType = DbType.Sqlite,
                IsAutoCloseConnection = true,
                InitKeyType = InitKeyType.SystemTable
            });
        }
        public override void testQueryResult()
        {
            var db = getDb();
            var list = db.Queryable<TestEntity>().Take(listTake).ToList();
        }
        public override void testQueryAnonymousResult()
        {
            var db = getDb();
            var list = db.Queryable<TestEntity>().Take(listTake).Select(b => new
            {
                b.Id,
                b.F_Float,
                b.F_Bool,
                b.F_DateTime,
                b.F_Decimal,
                b.F_Double,
                b.F_Int64
            }).ToList();
        }
        public override void testQueryCondition()
        {
            var db = getDb();
            var filter = GetSelectFilter();
            var query = db.Queryable<TestEntity>().Where(filter);
            var sql = query.ToSqlString();
        }

        public void testQueryJoin()
        {
            var db = getDb();
            try
            {
                var query = db.Queryable<TestEntity>().Take(100);
                var query2 = query.Select(b => new { a1 = b.Id, a2 = b.F_String });
                var query3 = query2.InnerJoin<TestEntityItem>((a, c) => a.a1 == c.TestEntityId);
                var query4 = query3.SelectMergeTable((a, c) => new { a3 = a.a1, a4 = c.Name })
                    .InnerJoin<TestEntity>((d, e) => d.a3 == e.Id).Select((d, e) => new
                    {
                        d.a4,
                        e.Id
                    })
                ;
                var sql = query4.ToSqlString();
                Console.WriteLine($"{GetType().Name}: {sql}");
            }
            catch(Exception e)
            {
                Console.WriteLine($"{GetType().Name}: {e}");
            }
        }
        public override void testQueryLoop()
        {
            for (var i = 0; i < 20; i++)
            {
                var query = getDb().Queryable<TestEntity>();
                var item = query.Where(b => b.Id == i).ToList();
            }
        }
    }

}
