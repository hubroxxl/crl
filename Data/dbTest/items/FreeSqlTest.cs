﻿using Chloe;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dbTest.items
{
    public class FreeSqlTest : ITest
    {

        IFreeSql getDb()
        {
            return new FreeSql.FreeSqlBuilder()
    .UseConnectionString(FreeSql.DataType.Sqlite, sqlLiteDb)
    .UseAutoSyncStructure(false) //自动同步实体结构到数据库
    .Build();
        }
        public override void testQueryAnonymousResult()
        {
            var query = getDb().Select<TestEntity>();
            var list = query.Take(listTake).ToList();
        }

        public override void testQueryCondition()
        {
            var filter = GetSelectFilter();
            var query = getDb().Select<TestEntity>();
            var sql = query.Where(filter).ToSql();
        }

        public override void testQueryResult()
        {
            var query = getDb().Select<TestEntity>();
            var list = query.Take(listTake).ToList(b => new
            {
                b.Id,
                b.F_Float,
                b.F_Bool,
                b.F_DateTime,
                b.F_Decimal,
                b.F_Double,
                b.F_Int64
            });
        }
        public void testQueryJoin()
        {
            var db = getDb();
            db.Aop.CurdAfter += (s, e) =>
            {
                Console.WriteLine($"{GetType().Name}: {e.Sql}");
            };
            var query = db.Select<TestEntity>().Take(100);
            var query2 = query.WithTempQuery(b => new { a1 = b.Id, a2 = b.F_String });
            var query3 = query2.FromQuery(db.Select<TestEntityItem>()).InnerJoin((a, b) => a.a1 == b.TestEntityId);
            var query4 = query3.WithTempQuery((a, b) => new { a3 = a.a1, a4 = b.Name }).FromQuery(db.Select<TestEntity>()).InnerJoin((a, b) => a.a3 == b.Id); ;
            var result = query4.WithTempQuery((a, b) => new
            {
                a.a4,
                b.Id
            }).ToList();
            //var sql = query.ToString();
            //Console.WriteLine($"{GetType().Name}: {sql}");
        }
        public override void testQueryLoop()
        {
            for (var i = 0; i < 20; i++)
            {
                var query = getDb().Select<TestEntity>();
                var item = query.Where(b => b.Id == i).ToList();
            }
        }
    }

}
