﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dbTest.items
{
    class efContext : Microsoft.EntityFrameworkCore.DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(ITest.sqlLiteDb);
            base.OnConfiguring(optionsBuilder);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var e = modelBuilder.Entity<TestEntity>();
            e.ToTable("TestEntity", "dbo");
            e.HasKey(b => b.Id);
            //e.ConfigEntityTypeBuilder();
            base.OnModelCreating(modelBuilder);
        }
    }
    public class EfSqlliteTest : ITest
    {
        public override void testQueryAnonymousResult()
        {
            using (var context = new efContext())
            {
                var query = context.Set<TestEntity>().AsQueryable();
                var list = query.Take(listTake).ToList();
            }
        }

        public override void testQueryCondition()
        {
            using (var context = new efContext())
            {
                var filter = GetSelectFilter();
                var query = context.Set<TestEntity>().AsQueryable();
                var sql = query.Where(filter).ToQueryString();
            }
        }

        public override void testQueryResult()
        {
            using (var context = new efContext())
            {
                var query = context.Set<TestEntity>().AsQueryable();
                var list = query.Take(listTake).Select(b => new
                {
                    b.Id,
                    b.F_Float,
                    b.F_Bool,
                    b.F_DateTime,
                    b.F_Decimal,
                    b.F_Double,
                    b.F_Int64
                }).ToList();
            }
        }
        public  void testQueryJoin()
        {

        }
        public override void testQueryLoop()
        {
            for (var i = 0; i < 20; i++)
            {
                using (var context = new efContext())
                {
                    var query = context.Set<TestEntity>().AsQueryable();
                    var list = query.Where(b => b.Id == i).ToList();
                }
            }
        }
    }
}
