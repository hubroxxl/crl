﻿using CRL.Data;
using CRL.Data.DBAccess;
using CRL.Sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dbTest.items
{
    public class MyTest : ITest
    {
        public static void Init()
        {
            var builder = DBConfigRegister.GetInstance();
            builder.UseSqlite();
            builder.RegisterDBAccessBuild(dbLocation =>
            {
                return new DBAccessBuild(DBType.SQLITE, ITest.sqlLiteDb);
            });
            SettingConfig.CheckModelTableMaping = false;
        }
        public void InitData()
        {
            var rep = RepositoryFactory.Get<TestEntity>();
            var c = rep.Count(b => b.Id > 0);
            if (c == 0)
            {
                var list = new List<TestEntity>();
                for (int i = 0; i < 50000; i++)
                {
                    list.Add(new TestEntity() { F_Bool = true, F_Byte = 1, F_DateTime = DateTime.Now, F_Decimal = 100.23M, F_Double = 23.22, F_Float = 1.22F, F_Int16 = 22, F_Int32 = 333, F_Int64 = 333, F_String = "string" + i });
                }
                rep.BatchInsert(list);
            }
        }
        public override void testQueryResult()
        {
            var rep = RepositoryFactory.Get<TestEntity>();
            var list = rep.GetLambdaQuery().Take(listTake).ToList();
        }
        public override void testQueryAnonymousResult()
        {
            var rep = RepositoryFactory.Get<TestEntity>();
            var list = rep.GetLambdaQuery().Take(listTake).Select(b => new
            {
                b.Id,
                b.F_Float,
                b.F_Bool,
                b.F_DateTime,
                b.F_Decimal,
                b.F_Double,
                b.F_Int64
            }).ToList();
        }
        public override void testQueryCondition()
        {
            var rep = RepositoryFactory.Get<TestEntity>();
            var filter = GetSelectFilter();
            var query = rep.GetLambdaQuery().Where(filter);
            var sql = query.ToString();
        }
        public override void testQueryLoop()
        {
            for (var i = 0; i < 20; i++)
            {
                var rep = RepositoryFactory.Get<TestEntity>();
                var item = rep.GetLambdaQuery().Where(b => b.Id == i).ToList();
            }
        }
        public void testQueryJoin()
        {
            var rep = RepositoryFactory.Get<TestEntity>();
            var query = rep.GetLambdaQuery().Take(100);
            var join = query.Select(b => new { a1 = b.Id, a2 = b.F_String }).Join<TestEntityItem>((a, b) => a.a1 == b.TestEntityId);
            var join2 = join.Select((a, b) => new { a3 = a.a1, a4 = b.Name })
                .Join<TestEntity>((a, b) => a.a3 == b.Id);
            join2.Select((a, b) => new
            {
                a.a4,
                b.Id
            });
            var sql = query.ToString();
            Console.WriteLine($"{GetType().Name}: {sql}");
        }
        public void testQueryJoin2()
        {
            var rep = RepositoryFactory.Get<TestEntity>();
            var query = rep.GetLambdaQuery();
            var view = query.Take(10).Select(b => new { a1 = b.Id, a2 = b.F_String });
            var view2 = query.CreateQuery<TestEntityItem>().Select(b => new { b.TestEntityId, b.Name });
            var query2 = view.Join(view2, (a, b) => a.a1 == b.TestEntityId).Select((a, b) => new { a3 = a.a1, a4 = b.Name }).Join<TestEntity>((a, b) => a.a3 == b.Id).Select((a, b) => new
            {
                a.a4,
                b.Id
            });
            var sql = query.ToString();
            Console.WriteLine($"{GetType().Name}: {sql}");
        }
    }
}
