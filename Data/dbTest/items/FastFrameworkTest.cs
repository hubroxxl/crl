﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fast.Framework.Implements;
using Fast.Framework.Models;

namespace dbTest.items
{
    internal class FastFrameworkTest: ITest
    {
        DbContext getDb()
        {
            return new DbContext(new List<DbOptions> { new DbOptions { ConnectionStrings=sqlLiteDb,
                DbType= Fast.Framework.Enum.DbType.SQLite,
                IsDefault=true,
                  DbId="1",
                ProviderName= "System.Data.SQLite", FactoryName="System.Data.SQLite.SQLiteFactory,System.Data.SQLite", } });
        }
        public override void testQueryAnonymousResult()
        {
            var query = getDb().Query<TestEntity>();
            var list = query.Take(listTake).ToList();
        }

        public override void testQueryCondition()
        {
            var filter = GetSelectFilter();
            var query = getDb().Query<TestEntity>();
            var sql = query.Where(filter).ToSqlString();
        }

        public override void testQueryResult()
        {
            var query = getDb().Query<TestEntity>();
            var list = query.Take(listTake).Select(b => new
            {
                b.Id,
                b.F_Float,
                b.F_Bool,
                b.F_DateTime,
                b.F_Decimal,
                b.F_Double,
                b.F_Int64
            }).ToList();
        }
        public void testQueryJoin()
        {
            var db = getDb();
            var query = db.Query<TestEntity>().Take(100);
            var subQuery = query.Select(b => new { a1 = b.Id, a2 = b.F_String });
            var subQuery2 = db.Query(subQuery).InnerJoin<TestEntityItem>((a, b) => a.a1 == b.TestEntityId).Select((a, b) => new { a3 = a.a1, a4 = b.Name });
            var query2 = db.Query(subQuery2)
                .InnerJoin<TestEntity>((a, b) => a.a3 == b.Id).Select((a, b) => new
                {
                    a.a4,
                    b.Id
                });
            var sql = query2.ToSqlString();
            Console.WriteLine($"{GetType().Name}: {sql}");
        }
        public override void testQueryLoop()
        {
            for (var i = 0; i < 20; i++)
            {
                var query = getDb().Query<TestEntity>();
                var item = query.Where(b => b.Id == i).ToList();
            }
        }
    }
}
