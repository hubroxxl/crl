﻿using Chloe;
using Chloe.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dbTest.items
{
    public class ChloeTest : ITest
    {
        class sqlLiteDbConnectionFactory : IDbConnectionFactory
        {
            public System.Data.IDbConnection CreateConnection()
            {
                return new SQLiteConnection(sqlLiteDb);
            }
        }
        IDbContext getContext()
        {
            return new Chloe.SQLite.SQLiteContext(new sqlLiteDbConnectionFactory());
        }
        public override void testQueryAnonymousResult()
        {
            var query = getContext().Query<TestEntity>();
            var list = query.Take(listTake).ToList();
        }

        public override void testQueryCondition()
        {
            var filter = GetSelectFilter();
            var query = getContext().Query<TestEntity>();
            var sql = query.Where(filter).ToString();
        }

        public override void testQueryResult()
        {
            var query = getContext().Query<TestEntity>();
            var list = query.Take(listTake).Select(b => new
            {
                b.Id,
                b.F_Float,
                b.F_Bool,
                b.F_DateTime,
                b.F_Decimal,
                b.F_Double,
                b.F_Int64
            }).ToList();
        }
        public void testQueryJoin()
        {
            var query = getContext().Query<TestEntity>().Take(100);
            var join = query.Select(b => new { a1 = b.Id, a2 = b.F_String }).Join<TestEntityItem>(JoinType.InnerJoin, (a, b) => a.a1 == b.TestEntityId);
            var query3 = join.Select((a, b) => new { a3 = a.a1, a4 = b.Name })
                .Join<TestEntity>(JoinType.InnerJoin, (a, b) => a.a3 == b.Id).Select((a, b) => new
            {
                a.a4,
                b.Id
            });
            var sql = query3.ToString();
            Console.WriteLine($"{GetType().Name}: {sql}");
        }

        public override void testQueryLoop()
        {
            for (var i = 0; i < 20; i++)
            {
                var item = getContext().Query<TestEntity>().Where(b => b.Id == i).ToList();
            }
        }
    }

}
