﻿using BenchmarkDotNet.Attributes;
using dbTest.items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dbTest.tests
{
    [MemoryDiagnoser]
    public class AnonymousResultTest : TestBase
    {
        public AnonymousResultTest() : base()
        {
        }
        public override string MethodName => nameof(ITest.testQueryAnonymousResult);

        [Benchmark]
        public void TestAnonymousResult()
        {
            InvokeTest();
        }
    }
}
