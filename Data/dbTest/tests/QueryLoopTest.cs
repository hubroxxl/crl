﻿using BenchmarkDotNet.Attributes;
using dbTest.items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dbTest.tests
{
    [MemoryDiagnoser]
    public class QueryLoopTest : TestBase
    {
        public QueryLoopTest() : base()
        {

        }
        public override string MethodName => nameof(ITest.testQueryLoop);
        [Benchmark]
        public void TestQueryLoop()
        {
            InvokeTest();
        }
    }
}
