﻿using BenchmarkDotNet.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRL.Data;
using CRL.Sqlite;
using CRL.Data.Attribute;
using CRL.Data.LambdaQuery.Mapping;
using SqlSugar;
using dbTest.items;

namespace dbTest.tests
{
    [MemoryDiagnoser]
    public class PartTest
    {
        [Benchmark]
        public void test1()
        {
            var provider = new SqlSugarProvider(new ConnectionConfig { ConnectionString = ITest.sqlLiteDb, DbType = DbType.Sqlite });
            var sql = "select * from TestEntity limit 10";
            using (var reader = provider.Ado.GetDataReader(sql))
            {
                var context = new SqlSugar.ContextMethods();
                context.Context = provider;
                context.QueryBuilder = new SqliteQueryBuilder() { Context = provider };
                var data = context.DataReaderToList<TestEntity>(reader);
            }
        }
        [Benchmark]
        public void test2()
        {
            var helper = new SqliteHelper(new DBAccessBuild(CRL.Data.DBAccess.DBType.SQLITE, ITest.sqlLiteDb));
            var sql = "select * from TestEntity limit 10";
            //var dt = helper.ExecDataSet(sql);
            using (var reader = helper.ExecDataReader(sql))
            {
                var pro = TypeCache.GetTable(typeof(TestEntity)).Fields;
                var mapping = pro.Select(b => new FieldMapping() { ResultName = b.MemberName, QueryField = b.MemberName, PropertyType = b.PropertyType }).ToList();
                var queryInfo = new QueryInfo<TestEntity>(false, sql, mapping);
                var data = ObjectConvert.DataReaderToSpecifiedList<TestEntity>(reader, queryInfo, out var includeValues);
            }
        }

        public void test3()
        {
            var helper = new SqliteHelper(new DBAccessBuild(CRL.Data.DBAccess.DBType.SQLITE, ITest.sqlLiteDb));
            var sql = "select * from TestEntity limit 10";
            //var dt = helper.ExecDataSet(sql);
            using (var reader = helper.ExecDataReader(sql))
            {
                var func = Dapper.SqlMapper.GetTypeDeserializer(typeof(TestEntity), reader);
                while (reader.Read())
                {
                    object val = func(reader);
                }
            }
        }
    }
}
