﻿using BenchmarkDotNet.Attributes;
using dbTest.items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dbTest.tests
{
    [MemoryDiagnoser]
    public class ResultTest : TestBase
    {
        public ResultTest() : base()
        {
            
        }
        public override string MethodName => nameof(ITest.testQueryResult);
        [Benchmark]
        public void TestResult()
        {
            InvokeTest();
        }
    }
}
