﻿using BenchmarkDotNet.Attributes;
using dbTest.items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dbTest.tests
{
    [MemoryDiagnoser]
    public class ConditionTest : TestBase
    {
        public ConditionTest() : base()
        {

        }
        public override string MethodName => nameof(ITest.testQueryCondition);
        [Benchmark]
        public void TestCondition()
        {
            InvokeTest();
        }
    }
}
