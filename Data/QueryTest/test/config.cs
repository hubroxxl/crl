﻿using CRL.Data;
using QueryTest.Code;
using System;

namespace QueryTest.test
{
    public class config
    {
        public static void changeToMsSql()
        {
            Base.ClearCache();
			ConfigInit.currentDbType = CRL.Data.DBAccess.DBType.MSSQL;
            Console.WriteLine($"currentDbType is {ConfigInit.currentDbType}");
            Console.Title = $"currentDbType {ConfigInit.currentDbType}";
        }
        public static void changeToMySql()
        {
			Base.ClearCache();
			ConfigInit.currentDbType = CRL.Data.DBAccess.DBType.MYSQL;
            Console.WriteLine($"currentDbType is {ConfigInit.currentDbType}");
            Console.Title = $"currentDbType {ConfigInit.currentDbType}";
        }
        public static void changeToSqlLite()
        {
			Base.ClearCache();
			ConfigInit.currentDbType = CRL.Data.DBAccess.DBType.SQLITE;
            Console.WriteLine($"currentDbType is {ConfigInit.currentDbType}");
            Console.Title = $"currentDbType {ConfigInit.currentDbType}";
        }
        static void changeToPostgreSQL()
        {
			Base.ClearCache();
			ConfigInit.currentDbType = CRL.Data.DBAccess.DBType.NPGSQL;
            Console.WriteLine($"currentDbType is {ConfigInit.currentDbType}");
            Console.Title = $"currentDbType {ConfigInit.currentDbType}";
        }
    }
}
