﻿using CRL.Data;
using QueryTest.Code;
using System;

namespace QueryTest.test
{
    public class page
    {
        public static void testPage()
        {
            var q1 = OrderRepository.Instance.GetLambdaQuery();
            var list = q1.Page(10, 1).ToList();
            Console.WriteLine($"return:{list.Count} total:{q1.RowCount}");
        }
        public static void testPageJoin()
        {
            var q1 = OrderRepository.Instance.GetLambdaQuery();
            var view = q1.Join<Member>((a, b) => a.Id == b.Id).Select((a, b) => new { a.ProductId, b.AccountNo });
            q1.Page(10, 1);
            view.PrintQuery();
            var result = view.ToList();
        }
        public static void testPageGroup()
        {
            var q1 = OrderRepository.Instance.GetLambdaQuery();
            var view = q1.GroupBy(b => b.OrderId).Select(b => new { b.OrderId }).OrderBy(b=>b.OrderId);
            q1.Page(10, 1);
            view.PrintQuery();
            var result = view.ToList();
        }
        public static void testPageUnion()
        {
            var query = ProductDataRepository.Instance.GetLambdaQuery().Where(b => b.Id < 200);
            var query2 = query.CreateQuery<Order>().Where(b => b.Id < 200);
            var view1 = query.Select(b => new { a1 = b.Id, a2 = b.ProductName });
            var view2 = query2.Select(b => new { a1 = b.Id, a2 = b.Remark });
            var view3 = view1.Union(view2);
            var q2 = view3.AsQuery().Page(10, 1).OrderBy(b => b.a1);
            var sql = q2.PrintQuery();
            var result = q2.ToList();
        }
        public static void testPageViewJoin()
        {
            var query = ProductDataRepository.Instance.GetLambdaQuery().Where(b => b.Id < 20);
            var query2 = query.CreateQuery<Order>().Where(b => b.Id > 15);
            var view1 = query.Select(b => new { a1 = b.Id, a2 = b.ProductName });
            var view2 = query2.Select(b => new { a1 = b.Id, a2 = b.Remark });
            var view3 = view1.Join(view2, (a, b) => a.a1 == b.a1).Select((a, b) => new { a.a1, b.a2 });
            var q2 = view3.AsQuery().Page(10, 1).OrderBy(b => b.a1);
            var sql = q2.PrintQuery();
            var result = q2.ToList();
        }
    }
}
