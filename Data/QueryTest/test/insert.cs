﻿using CRL.Data;
using CRL.Data.Attribute;
using QueryTest.Code;
using System;
using System.Collections.Generic;
using System.Management;

namespace QueryTest.test
{
    public class insert
    {
        public static void testInsert()
        {
            var instance = ProductDataRepository.Instance;
            instance.Add(new ProductData { BarCode = "222", OrderItem = new Order { OrderId = "1111" }, Number = 2, ProductName = "test11" });
            instance.Add(new ProductData { BarCode = "333", OrderItem = new Order { OrderId = "1111" }, Number = 2, ProductName = "test33" });
            instance.BatchInsert(new List<ProductData> { new ProductData { BarCode = "222", Number = 2, ProductName = "test11", OrderItem = new Order { OrderId = "1111" } }, new ProductData { BarCode = "333", Number = 2, ProductName = "test33", OrderItem = new Order { OrderId = "333" } } });
            var instance2 = MemberRepository.Instance;
            instance2.BatchInsert(new List<Member> { new Member { Name = "111" }, new Member { Name = "2222" } });
            var instance3 = OrderRepository.Instance;
            var list3 = new List<Order>();
            list3.Add(new Order() { UserId = 1, OrderId = "123", ProductId = 1 });
            list3.Add(new Order() { UserId = 2, OrderId = "456", ProductId = 1 });
            instance3.BatchInsert(list3);
            Console.WriteLine("BatchInsert ok");
            try
            {
                instance.Add(new ProductData { BarCode = "222", Number = 2, ProductName = "超长字符串超长字符串超长字符串超长字符串超长字符串超长字符串超长字符串超长字符串超长字符串超长字符串超长字符串超长字符串超长字符串超长字符串超长字符串超长字符串超长字符串超长字符串超长字符串超长字符串超长字符串超长字符串超长字符串超长字符串超长字符串超长字符串超长字符串超长字符串" });
            }
            catch (Exception ero)
            {
                Console.WriteLine($"BatchInsert fail {ero.Message}");
            }
        }
        public static void testInsert2()
        {
            var rep = RepositoryFactory.Get<insertModel2>();
            rep.CreateTable();
            Func<insertModel2> func = () => new insertModel2 { Id = System.Guid.NewGuid().ToString(), Name = "test" } ;
            rep.Add(func());
            Console.WriteLine("Add ok");
            var list = new List<insertModel2>();
            list.Add(func());
            list.Add(func());
            rep.BatchInsert(list, true);
            Console.WriteLine("BatchInsert ok");
        }

    }

    class insertModel2
    {
        [Field(Length =50)]
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
