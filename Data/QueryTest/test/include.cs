﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CRL.Data;
using QueryTest.Code;

namespace QueryTest.test
{
    public class include
    {
        static include()
        {
            var p1 = new PropertyBuilder<Blog>();
            p1.Relation<Post>((a, b) => a.Id == b.BlogId);
            p1.Relation<BlogUser>((a, b) => a.UserId == b.Id);
            p1.Relation<BlogTag>((a, b) => a.Id == b.BlogId);
            var p2 = new PropertyBuilder<Post>();
            p2.Relation<BlogUser>((a, b) => a.UserId == b.Id);
            initRelation();
        }
        static void initRelation()
        {
            var rep = RepositoryFactory.Get<Blog>();
            if (rep.Count(b => !string.IsNullOrEmpty(b.Id)) > 0)
            {
                return;
            }
            rep.CreateTable();
            var rep2 = RepositoryFactory.Get<Post>();
            var rep3 = RepositoryFactory.Get<BlogUser>();
            var rep4 = RepositoryFactory.Get<BlogTag>();
            rep2.CreateTable();
            rep3.CreateTable();
            rep4.CreateTable();
            rep.Delete(b => !string.IsNullOrEmpty(b.Id));
            rep2.Delete(b => !string.IsNullOrEmpty(b.Id));
            rep3.Delete(b => !string.IsNullOrEmpty(b.Id));
            rep4.Delete(b => !string.IsNullOrEmpty(b.Id));

            rep.Add(new Blog { Id = "b1", Url = "123", UserId = "u1" });
            rep.Add(new Blog { Id = "b2", Url = "123", UserId = "u1" });
            rep2.Add(new Post { Id = "p1", BlogId = "b1", Title = "title", UserId = "u1" });
            rep2.Add(new Post { Id = "p2", BlogId = "b2", Title = "title", UserId = "u1" });

            rep3.Add(new BlogUser { Id = "u1", Name = "name" });
            rep4.Add(new BlogTag { Id = "t1", BlogId = "b1", Tag = "t1" });
            rep4.Add(new BlogTag { Id = "t2", BlogId = "b2", Tag = "t2" });
        }
        public static void testRelationQuery()
        {
            var rep = RepositoryFactory.Get<Blog>();
            var query = rep.GetLambdaQuery();
            query.Where(b => b.Posts.Any(x => x.Title == "222"));
            query.PrintQuery();
            var reslut = query.ToList();
        }
        public static void tsRelationIncludeResult()
        {
            var rep = RepositoryFactory.Get<Blog>();
            var query = rep.GetLambdaQuery();
            query.Include(b => b.Posts, b => !string.IsNullOrEmpty(b.Title));
            query.Include(b => b.Tags);
            query.Include(b => b.BlogUser);
            query.PrintQuery();
            var result = query.ToList();
            var n = result.Sum(b => b.Posts.Count);
            Console.WriteLine(n);
            var result2 = query.Select(b => new { b.Id, b.Posts,b.Tags }).ToList();
        }
        public static void tsRelationIncludeResult2()
        {
            var rep = RepositoryFactory.Get<Post>();
            var query = rep.GetLambdaQuery();
            query.Include(b => b.Blog);
            query.Include(b => b.BlogUser);
            query.PrintQuery();
            var result = query.ToList();
        }
        public static void tsRelationIncludeResult3()
        {
            var rep = RepositoryFactory.Get<Post>();
            var query = rep.GetLambdaQuery();
            query.Include();
            query.PrintQuery();
            var result = query.ToList();
        }
    }
}
