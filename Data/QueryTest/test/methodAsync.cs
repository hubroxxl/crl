﻿using Microsoft.Diagnostics.Runtime.Utilities;
using CRL.Data;
using QueryTest.Code;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace QueryTest.test
{
    public class methodAsync
    {
        public static async Task<int> TestAsyncToList()
        {
            var query = OrderRepository.Instance.GetLambdaQuery();
            query.Take(10);
            var result = await query.Select(b => new { b.OrderId, b.ProductId, b.Id }).ToListAsync();
            Console.WriteLine(result.Count);
            return result.Count;
        }
        public static async Task<int> TestAsyncToList2()
        {
            var query = OrderRepository.Instance.GetLambdaQuery();
            query.Take(10);
            var result = await query.ToListAsync();
            Console.WriteLine(result.Count);
            return result.Count;
        }
        public static async Task<int> TestAsyncDelete()
        {
            var instance = OrderRepository.Instance;
            var n = await instance.DeleteAsync(b => b.Id == 22);
            Console.WriteLine(n);
            return n;
        }
        public static async Task<int> TestAsyncUpdate()
        {
            var instance = OrderRepository.Instance;
            var n = await instance.UpdateAsync(b => b.Id == 22, new Dictionary<string, object> { { "ProductId", 2 } });
            Console.WriteLine(n);
            return n;
        }
        public static async Task TestAsyncInsert()
        {
            var instance = OrderRepository.Instance;
            await instance.AddAsync(new Order { OrderId = "33330", ProductId = 22 });
            await instance.AddAsync(new Order { OrderId = "33331", ProductId = 22 });
            await instance.BatchInsertAsync(new List<Order> { new Order { OrderId = "33330", ProductId = 22 }, new Order { OrderId = "33330", ProductId = 22 } });
        }
    }
}
