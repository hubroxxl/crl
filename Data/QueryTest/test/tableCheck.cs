﻿using BenchmarkDotNet.Running;
using CRL.Data;
using QueryTest.Code;
using System;
using System.Collections.Generic;

namespace QueryTest.test
{
    public class tableCheck
    {
        static void testDropTable()
        {
            RepositoryFactory.Get<ProductData>().DropTable();
            RepositoryFactory.Get<Order>().DropTable();
            RepositoryFactory.Get<Member>().DropTable();
            RepositoryFactory.Get<MemmberAccount>().DropTable();
            RepositoryFactory.Get<Blog>().DropTable();
            RepositoryFactory.Get<BlogUser>().DropTable();
            RepositoryFactory.Get<BlogTag>().DropTable();
            RepositoryFactory.Get<Post>().DropTable();
        }
        public static void testCreateTable()
        {
            new ProductDataRepository().CreateTable();
            new OrderRepository().CreateTable();
            RepositoryFactory.Get<Member>().CreateTable();
            RepositoryFactory.Get<MemmberAccount>().CreateTable();
            RepositoryFactory.Get<Blog>().CreateTable();
            RepositoryFactory.Get<BlogUser>().CreateTable();
            RepositoryFactory.Get<BlogTag>().CreateTable();
            RepositoryFactory.Get<Post>().CreateTable();
        }
        public static void testCreateTableIndex()
        {
            var instance = new ProductDataRepository();
            instance.CreateTableIndex();
        }
        public static void testSyncTableFields()
        {
            var instance = new ProductDataRepository();
            instance.SyncTableFields();
        }

        public static void testSyncFieldComment()
        {
            var instance = new ProductDataRepository();
            instance.UpdateTableFiledComments();
        }
    }
}
