﻿/**
* CRL
*/
using CRL.Data;
using CRL.Data.DBAccess;
using QueryTest.Code;
using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace QueryTest.test
{
    public class method
    {
        class objArgs
        {
            public List<string> args;
        }
        public static void testMethod()
        {
            var ids = new List<int>() { 1, 2, 3 };
            var names = new List<string>() { "111", "222", null };
            var query = ProductDataRepository.Instance.GetLambdaQuery();
            query.WhereIf(b => b.CategoryName.SplitFirst("a") == "123", ConfigInit.isMsSql);
            query.WhereIf(b => b.AddTime.FuncFormat<int>("month({0})") == 1, ConfigInit.isMsSql);
            query.Where(b => b.TransType == TransType.In);
            var item = new ProductData();
            var year = DateTime.Now.Year;
            query.Where(b => b.Id == item.Id);
            query.Where(b => b.InterFaceUser == "222" && b.Id < 20);
            query.Where(b => ids.Contains(b.Id));
            var objArgs = new objArgs { args = new List<string> { "111" } };
            query.Where(b => names.Contains(b.CategoryName));
            query.Where(b => objArgs.args.Contains(b.BarCode));
            query.Where(b => b.IsTop);
            query.Where(b => 0 < b.Id && b.IsTop);
            query.Where(b => b.Id < b.Number);
            query.Where(b => b.ProductName.Contains("123"));
            query.Where(b => !b.ProductName.Contains("122"));
            query.Where(b => b.CategoryName.Contains(b.BarCode));
            query.Where(b => b.ProductName.In("111", "222"));
            query.Where(b => b.AddTime.Between(DateTime.Now, DateTime.Now));
            query.WhereIf(b => b.AddTime.DateDiff(DatePart.dd, DateTime.Now) > 1, !ConfigInit.isSqlLite);
            query.Where(b => b.ProductName.Substring(0, 3) == "222");
            query.Where(b => b.Id.In(1, 2, 3));//in
            query.Where(b => !b.Id.In(1, 2, 3));//not in
            if (!ConfigInit.isNpgSql)
            {
                query.Where(b => b.UserId.Equals("1"));
            }
            query.Where(b => b.ProductName.StartsWith("abc"));
            query.Where(b => b.Id.Between(1, 10));
            query.Where(b => b.ProductName.Like("123"));// %like%
            query.Where(b => b.ProductName.LikeLeft("123"));// %like
            query.Where(b => b.ProductName.LikeRight("123"));// like%
            query.Where(b => b.ProductName.EndsWith("123"));// %like
            query.Where(b => b.Id.ToString() == "123");
            query.Where(b => Convert.ToString(b.Id) == "123");
            query.Where(b => int.Parse(b.InterFaceUser) == 123);
            query.Where(b => b.ProductName.LessThan("sss"));
            query.Where(b => ExtensionMethod.LessThan(b.ProductName, "111"));
            query.Where(b => b.ProductName.GreaterThan("sss"));
            if (!ConfigInit.isNpgSql)
            {
                query.Where(b => b.CategoryName + "_" + Convert.ToInt32(b.Id) == "2222");
            }
            query.Where(b => b.CategoryName == b.ProductName.FuncFormat<string>("{0}"));
            query.Where(b => true);
            query.Where(b => b.CategoryName != null);
            query.Page(2, 1);
            query.OrderBy(b => b.Id * 1);
            var result = query.ToList();
            var sql = query.PrintQuery();
        }
        //public static void whereNotNull()
        //{
        //    var instance = MemberRepository.Instance;
        //    var query = instance.GetLambdaQuery();
        //    string name = null;
        //    //name = "111";
        //    //query.WhereNotNull(b => b.Name == name);
        //    query.WhereNotNull(b => b.Name.Like(name));
        //    query.PrintQuery();
        //}
        public static void whereCondition()
        {
            var instance = MemberRepository.Instance;
            var query = instance.GetLambdaQuery<Member>();
            query.Where("t1.AddTime>'2021-01-1'");
            query.PrintQuery();

            var instance2 = instance.ChangeTo<ProductData>();
            var item2 = instance2.QueryItem(1);
        }

        public static void testCustomMethod()
        {
            var isMsSql = ConfigInit.currentDbType == DBType.MSSQL;
            if (!isMsSql)
            {
                return;
            }
            var query = Code.OrderRepository.Instance.GetLambdaQuery();
            query.Select(b => new { aa = b.Remark.isnull2("default") });
            query.PrintQuery();
        }
 
    }
}
