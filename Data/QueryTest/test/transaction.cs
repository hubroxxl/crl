﻿using CRL.Data;
using QueryTest.Code;
using System;
using System.Threading;

namespace QueryTest.test
{
    public class transaction
    {
        public static void testTrans()
        {
            #region 事务
            string error;
            var item = Code.ProductDataRepository.Instance.QueryItem(1);

            var result = Code.ProductDataRepository.Instance.PackageTrans((out string ex) =>
            {
                ex = "";
                var product = new ProductData();
                product.BarCode = "sdfsdf";
                product.Number = 10;
                ProductDataRepository.Instance.Add(product);
                return false;
            }, out error);
            Console.WriteLine($"{result} {error}");
            #endregion
        }
    }
}
