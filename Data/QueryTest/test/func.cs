﻿using CRL.Data;
using QueryTest.Code;

namespace QueryTest.test
{
    public class func
    {
        public static void testFunc()
        {
            #region 函数
            var instance = Code.ProductDataRepository.Instance;
            //按条件id>0,合计Number列
            var sum = instance.Sum(b => b.Id > 0, b => b.Number * b.UserId);
            //按条件id>0,进行总计
            var count = instance.Count(b => b.Id > 0);
            var max = instance.Max(b => b.Id > 0, b => b.Id);
            var min = instance.Min(b => b.Id > 0, b => b.Id);
            //使用语句进行函数查询
            var query = ProductDataRepository.Instance.GetLambdaQuery();
            query.Select(b => b.Number.SUM());
            var sum2 = query.ToScalar();
            query.PrintQuery();

            var count2 = query.Count();
            query.PrintQuery();
            var sum3 = query.Sum(b => b.Id);
            query.PrintQuery();
            var max2 = query.Max(b => b.Id);
            query.PrintQuery();
            #endregion
        }

    }
}
