﻿using Microsoft.Diagnostics.Tracing.Parsers.Tpl;
using QueryTest.Code;
using System;
using System.Collections.Generic;
using System.Text;

namespace QueryTest.test
{
    public class subQuery
    {
        public static void testSubQuery()
        {
            //按select值
            var query = Code.OrderRepository.Instance.GetLambdaQuery();
            var subQuery = query.CreateSubQuery<ProductData>();
            var v = query.Select(b => new
            {
                b.Id,
                a2 = subQuery.Where(x => x.Id == b.UserId).Where(x => x.BarCode == "222").Max(x => x.Id)
            });
            var list = v.ToList();
            query.PrintQuery();
        }
        public static void testSubQuery2()
        {
            //按条件
            var query = Code.OrderRepository.Instance.GetLambdaQuery();
            var subQuery = query.CreateSubQuery<ProductData>();
            query.Where(b => subQuery.Where(x => x.Id == b.UserId).Count() > 2);
            var list = query.ToList();
            query.PrintQuery();
        }
        public static void testSubQuery3()
        {
            //交叉条件
            var query = Code.OrderRepository.Instance.GetLambdaQuery();
            var subQuery = query.CreateSubQuery<Order>();
            query.Where(b => subQuery.Where(x => x.ProductId == b.UserId && x.Id < b.Id).Count() > 2);
            var list = query.ToList();
            query.PrintQuery();
        }
        public static void testSubQuery5()
        {
            //关联再交叉条件
            var query = Code.OrderRepository.Instance.GetLambdaQuery();
            var query2 = query.CreateQuery<Order>();
            query2.Where(b => b.OrderId == "222");
            var view = query2.Select(b => new { b.OrderId });
            var join = query.Join(view, (a, b) => a.OrderId == b.OrderId);
            var subQuery = query.CreateSubQuery<Order>();
            join.Where((a, b) => subQuery.Where(x => x.Channel == b.OrderId).Count() > 2);
            var sql = query.PrintQuery();
            query.ToList();
        }
        public static void testSubQuery6()
        {
            //同源select
            var query = Code.OrderRepository.Instance.GetLambdaQuery();
            var query2 = query.CreateQuery<Order>();
            query2.Where(b => b.OrderId == "222");
            var view = query2.Select(b => new { b.OrderId });
            var join = query.Join(view, (a, b) => a.OrderId == b.OrderId);
            var subQuery = query.CreateSubQuery<Order>();
            join.Select((a, b) => new {
                a.OrderId,
                a2 = subQuery.Where(x => x.Id == a.UserId).Max(x => x.Id)
            });
            var sql = query.PrintQuery();
            query.ToList();
        }
    }
}
