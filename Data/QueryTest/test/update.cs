﻿using CRL.Data;
using QueryTest.Code;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Management;

namespace QueryTest.test
{
    public class update
    {

        public static void testUpdate()
        {
            var instance = ProductDataRepository.Instance;
            #region 更新
            //按匿名表达式
            var order = new Order { OrderId = "1221212" };
            instance.Update(b => b.Id == 1, b => new ProductData { Number = 2, OrderItem = new Order { OrderId = "1221212" } });
            //按匿名对象
            instance.Update(b => b.Id == 1, new { Number = 2M });

            //要更新属性集合
            ParameCollection c = new ParameCollection();
            c["ProductName"] = "product1";
            c["$UserId"] = "UserId+1";
            ProductDataRepository.Instance.Update(b => b.Id == 4, c);

            var p = new ProductData() { Id = 4 };
            //手动修改值时,指定修改属性以在Update时识别,分以下几种形式
            p.Change(b => b.BarCode);//表示值被更改了
            p.Change(b => b.BarCode, "123");//通过参数赋值
            p.Cumulation(b => b.PurchasePrice, 1);
            ProductDataRepository.Instance.Update(b => b.Id == 4, p);//指定查询更新

            p = ProductDataRepository.Instance.QueryItem(b => b.Id > 0);
            p.UserId += 1;
            ProductDataRepository.Instance.Update(p);//按主键更新,主键值是必须的

            
            #endregion
        }
        public static void testJoinUpdate()
        {
			//关联更新
			var query = OrderRepository.Instance.GetLambdaQuery();
			query.Join<ProductData>((a, b) => a.Id == b.Id && b.Number > 10);
			var c = new ParameCollection();
			c["UserId"] = "$UserId";//order.userid=product.userid
			c["Remark"] = "2222";//order.remark=2222
			if (!ConfigInit.isSqlLite)
			{
				OrderRepository.Instance.Update(query, c);
			}
		}
        public static void updateCache()
        {
            #region 缓存更新
            //按编号为1的数据
            var item = ProductDataRepository.Instance.QueryItemFromCache(b => b.Id > 0);
            item.CheckNull("item");
            var guid = Guid.NewGuid().ToString().Substring(0, 8);
            item.Change(b => b.SupplierName, guid);
            ProductDataRepository.Instance.Update(item);
            item = ProductDataRepository.Instance.QueryItemFromCache(item.Id);
            var item2 = ProductDataRepository.Instance.QueryItem(item.Id);
            var a2 = item.SupplierName == item2.SupplierName && item.SupplierName == guid;
            if (!a2)
            {
                throw new System.Exception("更新缓存失败");
            }
            #endregion
        }
        public static void testUpdate2()
        {
            //以下转换为了直实的字段名
            var instance = RepositoryFactory.Get<MemmberAccount>();
            instance.Update(b => b.Id == 1, b => new MemmberAccount { AccountName = "2222" });
            instance.Update(b => b.Id == 1, new { AccountName = "2222" });
            var c = new ParameCollection();
            c["AccountName"] = "AccountName2";
            c["$Balance"] = "Balance+1";
            instance.Update(b => b.Id == 4, c);

        }
        public static void testInsertOrUpdate()
        {
            if (!ConfigInit.isMsSql)
            {
                return;
            }
            var instance = RepositoryFactory.Get<Order>();
            var op = new InsertOrUpdateOption();
            op.IfExistsNotUpdate = true;
            op.ConstraintMemberName = nameof(Order.OrderId);
            op.BatchTake = 5;
            var updates = new List<Order>();
            for (var i = 1; i < 5; i++)
            {
                //id自增
                updates.Add(new Order { OrderId = i.ToString(), Remark = "code" + i });
            }
            instance.InsertOrUpdate(updates, op);
            Console.WriteLine(op.SqlOut);
        }
        public static void testInsertOrUpdate2()
        {
            if (ConfigInit.isNpgSql)
            {
                return;
            }
            var instance = RepositoryFactory.Get<Order>();
            var op = new InsertOrUpdateOption();
            op.IfExistsNotUpdate = true;
            //id插入
            instance.InsertOrUpdate(new Order { Id = 100, Remark = "code100" }, op);
            Console.WriteLine(op.SqlOut);
        }
        public static void testModelTracking2()
        {
            var instance = ProductDataRepository.Instance;
            var p = instance.QueryItem(b => b.Id > 0);
            var update = new ModelTracking(p);
            p.InterFaceUser = DateTime.Now.ToString();
            p.BarCode = DateTime.Now.Millisecond.ToString();
            instance.Update(update);
        }
        public static void testModelTracking()
        {
            var instance = ProductDataRepository.Instance;
            var p = instance.QueryItem(b => b.Id > 0);
            instance.BeginTracking(p);
			p.InterFaceUser = DateTime.Now.ToString();
			p.BarCode = DateTime.Now.Millisecond.ToString();
			instance.Update(p);
        }
        public static void testUpdateBatch()
        {
            var instance = MemberRepository.Instance;
			var list = instance.GetLambdaQuery().Take(3).ToList();
			instance.BeginTracking(list);
			list.ForEach(p =>
            {
				p.Name = DateTime.Now.ToString();
			});
            MemberRepository.Instance.Update(list);
        }
        public static void testUpdateBatch2()
        {
            var update = new BatchUpdate<ProductData>();

			var updateItem = update.CreateItem();
			updateItem.AddCondition(b => b.Id, 1);
			updateItem.AddCondition(b => b.BarCode, "222");
			updateItem.AddUpdateValue(b => b.OrderId, DateTime.Now.Second.ToString());

			var updateItem2 = update.CreateItem();
			updateItem2.AddCondition(b => b.Id, 2);
			updateItem2.AddCondition(b => b.BarCode, "333");
			updateItem2.AddUpdateValue(b => b.OrderId, DateTime.Now.Second.ToString());

			var updateItem3 = update.CreateItem();
			updateItem3.AddCondition(b => b.Id, 3);
			updateItem3.AddCondition(b => b.BarCode, "444");
			//updateItem3.AddCondition(b => b.InterFaceUser, "444");
			updateItem3.AddUpdateValue(b => b.OrderId, DateTime.Now.Second.ToString());

			ProductDataRepository.Instance.Update(update);
        }
		public static void testUpdateBatch3()
		{
			var update = new BatchUpdate<ProductData>();

            for (var i = 0; i < 1000; i++)
            {
                var updateItem = update.CreateItem();
                updateItem.AddCondition(b => b.Id, i);
                updateItem.AddCondition(b => b.BarCode, "222" + i);
                updateItem.AddUpdateValue(b => b.OrderId, DateTime.Now.Second.ToString());
				updateItem.AddUpdateValue(b => b.InterFaceUser, "InterFaceUser");
				updateItem.AddUpdateValue(b => b.ProductId, "ProductId");
			}
			ProductDataRepository.Instance.Update(update);
		}
	}
}
