﻿using CRL.Data;
using QueryTest.Code;

namespace QueryTest.test
{
    public class queryIn
    {
        #region in
        public static void testIn()
        {
            //按IN查询
            var query = ProductDataRepository.Instance.GetLambdaQuery();
            var query2 = query.CreateQuery<Code.Member>();
            var view = query2.Where(b => b.Name == "123").Select(b => b.Id);
            var result = query.In(view, b => b.UserId + b.Id).ToList();
            var sql = query.PrintQuery();
            var result2 = query.ToList();
        }
        public static void testIn2()
        {
            //按IN查询
            var query = ProductDataRepository.Instance.GetLambdaQuery();
            query.In<Code.Member>(b => b.UserId, b => b.Id, (a, b) => !string.IsNullOrEmpty(b.Name));
            var result = query.ToList();
            var sql = query.PrintQuery();
            var result2 = query.ToList();
        }
        public static void testIn3()
        {
            //按IN查询
            var query = ProductDataRepository.Instance.GetLambdaQuery();
            query.In<Code.Member>(b => b.ProductName, b => b.Name, (a, b) => a.BarCode == b.AccountNo && b.Name == "123");
            var result = query.ToList();
            var sql = query.PrintQuery();
            var result2 = query.ToList();
        }

        public static void testExists()
        {
            //按exists
            var query = ProductDataRepository.Instance.GetLambdaQuery();
            var query2 = query.CreateQuery<Code.Member>();
            var view2 = query2.Where(b => b.Name == "123").Select(b => b.Id);
            var result = query.Exists(view2).ToList();
            var sql = query.PrintQuery();
            var result2 = query.ToList();
        }
        public static void testNotExists()
        {
            var query = ProductDataRepository.Instance.GetLambdaQuery();
            query.NotExists<Member>(b => b.Id, (a, b) => a.Id == b.Id);
            var sql = query.PrintQuery();
            var result2 = query.ToList();
        }
        #endregion

        public static void testSelfIn()
        {
            //同源in
            var query = Code.OrderRepository.Instance.GetLambdaQuery();
            var query2 = query.CreateQuery<Order>();
            query2.Where(b => b.OrderId == "222");
            var view = query2.Select(b => b.OrderId);
            query.In(view, b => b.OrderId);
            var list = query.ToList();
            var sql = query.PrintQuery();
        }

    }
}
