﻿using CRL.Data;
using QueryTest.Code;
using System;

namespace QueryTest.test
{
    public class delete
    {
        public static int testDelete()
        {
            var instance = RepositoryFactory.Get<MemmberAccount>();
            instance.Delete(b => b.Id == 1);
            Code.ProductDataRepository.Instance.Delete(999);
            if (ConfigInit.isSqlLite)
            {
                return 1;
            }
            #region 删除
            //关联删除
            var query2 = Code.ProductDataRepository.Instance.GetLambdaQuery();
            query2.Where(b => b.Id == 10);
            query2.Join<Code.Member>((a, b) => a.SupplierId == "10" && b.Name == "123");
            Code.ProductDataRepository.Instance.Delete(query2);

            #endregion
            return 2;
        }
    }
}
