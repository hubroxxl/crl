﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Text;
using CRL.Data;
using CRL.Data.Sharding;
using CRL.Data.DBAccess;
using CRL.Data.Attribute;
using CRL.Data.LambdaQuery.CRLExpression;
using CRL.Data.LambdaQuery;
using CRL.Sqlite;
using CRL.MySql;
using CRL.Core;

namespace QueryTest
{
    public class ConfigInit
    {
        public static DBType currentDbType = DBType.SQLITE;
		static string sqlLiteDb = CustomSetting.GetConfigKey("sqlLiteDb");
        static string mySqlDb = CustomSetting.GetConfigKey("mySqlDb");
		static string npgSql = CustomSetting.GetConfigKey("npgSql");
		static string msSql = CustomSetting.GetConfigKey("msSql");
		internal static bool isMsSql => currentDbType == DBType.MSSQL;
        internal static bool isMySql => currentDbType == DBType.MYSQL;
        internal static bool isSqlLite => currentDbType == DBType.SQLITE;
        internal static bool isNpgSql => currentDbType == DBType.NPGSQL;
        static DBAccessBuild geDBAccessBuild(string dataBaseName)
        {
            switch(currentDbType)//切换数据库
            {
                case DBType.MSSQL:
                    return new DBAccessBuild(DBType.MSSQL, string.Format(msSql, dataBaseName));
                case DBType.MYSQL:

                    //批量写入问题
                    //https://blog.csdn.net/educast/article/details/89183389
                    //mysql set global local_infile=ON;
                    return new DBAccessBuild(DBType.MYSQL, mySqlDb);
                case DBType.SQLITE:
                    return new DBAccessBuild(DBType.SQLITE, sqlLiteDb);
                case DBType.NPGSQL:
                    return new DBAccessBuild(DBType.NPGSQL, npgSql);
            }
            return null;
        }
        public static void Init()
        {
            var builder = DBConfigRegister.GetInstance();
            builder.UseSqlite();
            builder.UseMySql();
            builder.SetDbProviderFactory(Npgsql.NpgsqlFactory.Instance);
            SettingConfig.CompileSp = false;
            SettingConfig.UseDpaGenerator = true;
            SettingConfig.IsAot = true;
            SettingConfig.CheckModelTableMaping = false;
            //自定义定位
            builder.RegisterLocation<Code.Sharding.MemberSharding>((t, a) =>
            {
                var tableName = t.TableName;
                if (a.Name == "hubro")
                {
                    tableName = "MemberSharding1";
                    return new Location("testdb2", tableName);
                }
                //返回定位库和表名
                return new Location("testdb", tableName);
            });
            builder.RegisterLocation<Code.Sharding.OrderSharding>((t, a) =>
            {
                var tableName = $"{t.TableName}_{a.MemberId}";
                //返回定位库和表名
                return new Location("testdb", tableName);
            });
            builder.RegisterDBAccessBuild(dbLocation =>
            {
                if (dbLocation.ManageName == "mongo")
                {
                    return new DBAccessBuild(DBType.MongoDB, "mongodb://127.0.0.1:27017/admin");
                }
                return null;
            });
            builder.RegisterDBAccessBuild(dbLocation =>
            {
                //定位库
                if (dbLocation.ShardingLocation != null)
                {
                    return geDBAccessBuild(dbLocation.ShardingLocation.DataBaseSource);
                    //return new DBAccessBuild(DBType.MSSQL, "Data Source=.;Initial Catalog=" + dbLocation.ShardingLocation.DataBaseSource + ";User ID=sa;Password=123");
                }
                return geDBAccessBuild("testDb");
                //return new DBAccessBuild(DBType.MSSQL, "server=.;database=testDb; uid=sa;pwd=123;");
            });
            var cb = new ConfigBuilder();
            //编程方式的索引
            var propertyBuilder = new PropertyBuilder<Code.Member>();
            propertyBuilder.AsIndex(b => b.AccountNo);
            propertyBuilder.AsUniqueIndex(b => b.Mobile);
            propertyBuilder.AsUnionIndex("index2", b => new { b.AccountNo, b.Name }, FieldIndexType.非聚集);
            propertyBuilder.Property(b => b.AccountNo).WithColumnLength(100);

            var propertyBuilder2 = new PropertyBuilder<Code.ProductData>();
            propertyBuilder2.AsIndex(b => b.ProductId);

            //自定义扩展方法
            builder.AddCustomExtendMethod(DBType.MSSQL, nameof(ExtendMethod.isnull2), isnull2);
        }

        private static string isnull2(MethodCallObj methodInfo, ref int parIndex, AddParameHandler addParame)
        {
            var field = methodInfo.MemberQueryName;
            var args = methodInfo.Args[0].ToString();
            return $"isnull({field},'{args}')";
        }
    }
    public static class ExtendMethod
    {
        public static string isnull2(this string origin,string v)
        {
            return origin;
        }
    }
}
