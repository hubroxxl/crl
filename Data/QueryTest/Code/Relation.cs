﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QueryTest.Code
{
    public class Blog
    {
        public string Id { get; set; }
        public string UserId { get; set; }
        public virtual BlogUser BlogUser { get; set; }
        public string Url { get; set; }

        public virtual List<Post> Posts { get; set; }
        public virtual List<BlogTag> Tags { get; set; }
    }
    public class BlogUser
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class Post
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string BlogId { get; set; }
        public virtual Blog Blog { get; set; }

        public string UserId { get; set; }
        public virtual BlogUser BlogUser { get; set; }
    }
    public class BlogTag
    {
        public string Id { get; set; }
        public string BlogId { get; set; }
        public string Tag { get; set; }
    }
}
