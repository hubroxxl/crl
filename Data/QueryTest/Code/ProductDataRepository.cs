﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CRL.Data;
namespace QueryTest.Code
{
    /// <summary>
    /// ProductData业务处理类
    /// 这里实现处理逻辑
    /// </summary>
    public class ProductDataRepository : CRL.Data.BaseProvider<ProductData>
    {
        //protected override string GetRedisHashId(ProductData obj)
        //{
        //    return "crlTest";
        //}
        //protected override string GetRedisHashKey(ProductData obj)
        //{
        //    return obj.BarCode;
        //}
        public override string ManageName
        {
            get
            {
                return "test2";
            }
        }
        protected override CRL.Data.LambdaQuery.ILambdaQuery<ProductData> CacheQuery()
        {
            return GetLambdaQuery().Expire(5);
        }

        /// <summary>
        /// 实例访问入口
        /// </summary>
        public static ProductDataRepository Instance
        {
            get { return  new ProductDataRepository(); }
        }
        public void TransactionTest5()
        {
            var db = GetDBExtend();
            var product = new ProductData();
            product.BarCode = "code" + DateTime.Now.Millisecond;
            product.Number = 10;
            var order = new Order() { OrderId="222"  };
            db.BeginTran();
            db.InsertFromObj(order);
            db.BatchInsert(new List<ProductData>() { product, product });
            db.CommitTran();
        }

        public void TestBatchInsert()
        {
            Delete(b => b.Id > 0);
            var p = new ProductData() { Id = 1999, BarCode="sss", Number=12 };
            var list = new List<ProductData>() { p };
            BatchInsert(list, true);
        }
        public void testArgs()
        {
            var db = DBExtend;
            var sql = "select * from ProductData where OrderId=@OrderId";
            db.AddParame("OrderId", null);
            var result = db.ExecList<ProductData>(sql);
        }

    }
}
