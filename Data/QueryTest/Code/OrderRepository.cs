﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QueryTest.Code
{

    /// <summary>
    /// OrderManage
    /// </summary>
    public class OrderRepository : CRL.Data.BaseProvider<Order>
    {
        public static OrderRepository Instance
        {
            get { return new OrderRepository(); }
        }
        public void TestRelationUpdate()
        {
            var query = GetLambdaQuery();
            query.Join<ProductData>((a, b) => a.Id == b.Id && b.Number > 10);
            var c = new CRL.Data.ParameCollection();
            c["UserId"] = "$UserId";//order.userid=product.userid
            c["Remark"] = "2222";//order.remark=2222
            Update(query, c);
        }
        public bool TransactionTest2(out string error)
        {
            
            //简化了事务写法,自动提交回滚
            return PackageTrans((out string ex) =>
            {
                ex = "";
                var product = new ProductData();
                product.BarCode = "code" + DateTime.Now.Millisecond;
                product.Number = 10;
                var list = new List<ProductData>();
                list.Add(product);
                ProductDataRepository.Instance.BatchInsert(list);
                return false; //会回滚
            }, out error);
        }
        public bool TransactionTest3(out string error)
        {

            //简化了事务写法,自动提交回滚
            return PackageTrans((out string ex) =>
            {
                ex = "";
                var product = new ProductData();
                product.BarCode = "code" + DateTime.Now.Millisecond;
                product.Number = 10;
                ProductDataRepository.Instance.Add(product);
                var a = TransactionTest2(out ex);//嵌套事务
                return a; //会回滚
            }, out error);
        }
        public bool TransactionTest(out string error)
        {
            //简化了事务写法,自动提交回滚
            return PackageTrans((out string ex) =>
            {
                ex = "";
                var item = QueryItem(1);
                var product = new ProductData();
                product.BarCode = "code" + DateTime.Now.Millisecond;
                product.Number = 10;
                ProductDataRepository.Instance.Add(product);
                return false; //会回滚
            }, out error);
        }

        public bool TransactionTest4(out string error)
        {
            //简化了事务写法,自动提交回滚
            return PackageTrans((out string ex) =>
            {
                ex = "";
                var item = QueryItem(1);
                var product = new ProductData();
                product.BarCode = "code" + DateTime.Now.Millisecond;
                product.Number = 10;
                ProductDataRepository.Instance.Add(product);
                Add(item);
                return true; //不会回滚
            }, out error);
        }

    }
}
