﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CRL.Data.Sharding;

namespace QueryTest.Code.Sharding
{
    public class MemberSharding : CRL.Data.IModel
    {
        [CRL.Data.Attribute.Field(KeepIdentity=true)]//保持插入主键
        public int Id
        {
            get;
            set;
        }
        public string Name
        {
            get;
            set;
        }

    }
    public class MemberRepository : CRL.Data.Sharding.BaseProvider<MemberSharding>
    {

    }

}
