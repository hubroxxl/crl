﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CRL.Data.Sharding;

namespace QueryTest.Code.Sharding
{
    public class OrderSharding : CRL.Data.IModel
    {
        public int MemberId
        {
            get;
            set;
        }
        public string Remark
        {
            get;
            set;
        }

    }
    public class OrderRepository : CRL.Data.Sharding.BaseProvider<OrderSharding>
    {
        public int tag;
        public static OrderRepository Instance
        {
            get { return new OrderRepository(); }
        }
    }
}
