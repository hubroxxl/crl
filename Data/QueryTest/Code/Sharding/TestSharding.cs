﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Threading.Tasks;

namespace QueryTest.Code.Sharding
{
    public class TestSharding
    {
        static List<OrderRepository> reps = new List<OrderRepository>();
        static TestSharding()
        {
            reps.Add(new OrderRepository() { tag = 1 });
            reps.Add(new OrderRepository() { tag = 2 });
            foreach (var item in reps)
            {
                item.CreateTable();
            }
        }
        public static void test()
        {
            List<Task> tasks = new List<Task>();
            foreach (var item in reps)
            {
                var task = new Task(() =>
                {
                    item.SetLocation(new OrderSharding { MemberId = item.tag });
                    item.BatchInsert(new List<OrderSharding> { new OrderSharding { MemberId = item.tag, Remark = "" } });
                });
                tasks.Add(task);
            }
            tasks.ForEach(b =>
            {
                b.Start();
            });
        }
    }
}
