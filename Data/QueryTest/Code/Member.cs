﻿/**
* CRL
*/
using CRL.Data;
using CRL.Data.Attribute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QueryTest.Code
{
    /// <summary>
    /// 会员
    /// 重新定义了表名和字段名
    /// </summary>
    [Table(TableName = "MM")]
    public class Member : CRL.Data.IModel
    {
        [Field(IsPrimaryKey = true, MapingName = "A")]
        public int Id
        {
            get; set;
        }
        [Field(MapingName = "B")]
        public string Name
        {
            get; set;
        }
        [Field(MapingName = "C")]
        public string Mobile { get; set; }
        [Field(MapingName = "D")]
        public string AccountNo { get; set; }

        protected override System.Collections.IList GetInitData()
        {
            var list = new List<Member>();
            list.Add(new Member() { Name = "hubro" });
            list.Add(new Member() { Name = "hubro2" });
            return list;
        }
    }
    /// <summary>
    /// 启用名称混淆
    /// </summary>
    [Table(EnableObfuscate = true)]
    public class MemmberAccount
    {
        public int Id { get; set; }
        [Field(FieldIndexType = FieldIndexType.非聚集)]
        public string AccountName { get; set; }
        public decimal Balance { get; set; }
    }
}
