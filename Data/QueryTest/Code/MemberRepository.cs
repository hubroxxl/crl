﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QueryTest.Code
{
    public class MemberRepository : CRL.Data.BaseProvider<Member>
    {
        public override string ManageName
        {
            get
            {
                return "test";
            }
        }
        /// <summary>
        /// 实例访问入口
        /// </summary>
        public static MemberRepository Instance
        {
            get { return new MemberRepository(); }
        }
        public void UpdateList()
        {
            var list = GetLambdaQuery().Take(3).ToList();
            list.ForEach(b =>
            {
                b.Name = DateTime.Now.ToString();
            });
            Update(list);
        }
    }
}
