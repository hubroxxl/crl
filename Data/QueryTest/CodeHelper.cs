﻿
using CRL.Core.Mapper;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace QueryTest
{
    public class CodeHelper
    {
        //static Dictionary<string, methodObj> methods = new Dictionary<string, methodObj>();
        public static Dictionary<string, methodObj> LoadFiles(string filePath = "")
        {
            if (string.IsNullOrEmpty(filePath))
            {
                filePath = @"..\QueryTest\test";
            }
            var methods = new Dictionary<string, methodObj>();
            if (!Directory.Exists(filePath))
            {
                return methods;
            }
            var files = Directory.GetFiles(filePath, "*.cs", SearchOption.AllDirectories);
            foreach (var file in files)
            {
                getMethods(methods, File.ReadLines(file).ToList());
            }
            return methods;
        }

        static void getMethods(Dictionary<string, methodObj> methods, List<string> lines)
        {
            var nameSpacePat = @"namespace(\s+)(.+)";
            var regex = new Regex(nameSpacePat, RegexOptions.IgnoreCase);
            var nameSpace = regex.Match(lines.Find(b => b.StartsWith("namespace "))).Groups[2].Value;
            string className = "";
            bool classBegin = false;
            var methodBegin = false;
            methodObj methodObj = null;
            foreach (var line in lines)
            {
                var regex2 = new Regex(@".+?class(\s+)(\w+)", RegexOptions.IgnoreCase);
                if (regex2.IsMatch(line))
                {
                    className = regex2.Match(line).Groups[2].Value;
                    classBegin = true;
                }
                var regex3 = new Regex(@"public(\s+)static(\s+)(.+) (\w+)", RegexOptions.IgnoreCase);
                if (classBegin && regex3.IsMatch(line))
                {
                    methodBegin = true;
                    var methodName = regex3.Match(line).Groups[4].Value;
                    methodObj = new methodObj() { sourceCode = new StringBuilder(), nameSapce = nameSpace, className = className, methodName = methodName };
                }
                if (methodBegin)
                {
                    methodObj.sourceCode.AppendLine(line);
                    foreach (var c in line)
                    {
                        if (c == '{')
                        {
                            methodObj.c = methodObj.c ?? 0;
                            methodObj.c += 1;
                        }
                        else if (c == '}')
                        {
                            methodObj.c -= 1;
                        }
                    }
                    
                    if (methodObj != null && methodObj.c == 0)
                    {
                        methodBegin = false;
                        methods.Add(methodObj.fullName, methodObj.MapTo<methodObj>());
                        methodObj = null;
                    }
                }
            }

        }
    }
    public class methodObj
    {
        public string nameSapce;
        public string className;
        public string methodName;
        public StringBuilder sourceCode;
        public int? c;
        public string fullName
        {
            get
            {
                return $"{nameSapce}.{className}.{methodName}";
            }
        }
    }
}
