﻿/**
* CRL
*/
using CRL.Core;
using CRL.Data.DBAccess;
using QueryTest.Code;
using QueryTest.test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CRL.MySql;
using CRL.Sqlite;
using QueryTest;

namespace QueryTest
{

    class Program
    {
        static void Main(string[] args)
        {
            ConfigInit.Init();
            var asb = Assembly.GetAssembly(typeof(config));
            var types = asb.GetTypes().Where(b => b.Namespace == typeof(config).Namespace && b.IsPublic).ToArray();
            config.changeToSqlLite();
            //config.changeToPostgreSQL();
            //ConsoleTest.DoCommand(types);
            var testAllBefor = typeof(config).GetMethods(BindingFlags.Public | BindingFlags.Static);
            ConsoleTest.DoCommand(types, testAllBefor: testAllBefor.ToList());
		}
	}
}
