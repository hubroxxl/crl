﻿/**
* CRL
*/
using CRL.Data;
using CRL.Data.Attribute;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRL.Mongo;
namespace MongoTest
{
    public class MongoDBModel : IModelBase
    {
       
        public string OrderId
        {
            get;
            set;
        }
        public int Numbrer
        {
            get;
            set;
        }
        public string name
        {
            get; set;
        }
        public int Status
        {
            get;set;
        }
        public double Price
        {
            get;set;
        }
    }

    public class MongoDBModel2 : MongoDBModel
    {
    }
    public class MongoDBModel3 : MongoDBModel
    {
    }
    public class MongoDBTest2Repository : BaseProvider<MongoDBModel2>
    {
        public override string ManageName => "mongo";
        public static MongoDBTest2Repository Instance
        {
            get { return new MongoDBTest2Repository(); }
        }
    }
    public class MongoDBTest3Repository : BaseProvider<MongoDBModel3>
    {
        public override string ManageName => "mongo";
        public static MongoDBTest3Repository Instance
        {
            get { return new MongoDBTest3Repository(); }
        }
    }
    public class MongoDBTestRepository : BaseProvider<MongoDBModel>
    {
        public override string ManageName => "mongo";
        public static MongoDBTestRepository Instance
        {
            get { return new MongoDBTestRepository(); }
        }
        public void GetInitData()
        {

            var list = new List<MongoDBModel2>();
            list.Add(new MongoDBModel2() { name = "test", Numbrer = 1, Price = 1.125, OrderId = "13" });
            for (int i = 0; i < 10; i++)
            {
                var n = i + 0.23 * i;
                list.Add(new MongoDBModel2() { name = "test" + i, Numbrer = i, Price = n, OrderId = "13" });
            }
            BatchInsert(list);
        }
        public void testBsonQuery()
        {
            var list = DBExtend.ExecList<MongoDBModel>("MongoDBModel.{}");
        }

        public void testSelect()
        {
            var query = GetLambdaQuery();
            query.Where(b => !string.IsNullOrEmpty(b.name));
            query.Select(b => new { b.Numbrer, b.name });
            var result2 = query.ToDynamic();
            query.PrintQuery();
        }
        public void testCount()
        {
            var n = Count(b => b.name == "sss");
        }
        public void GroupTest(int page = 1)
        {
            //Delete(b=>b.Numbrer>0);
            GetInitData();
            var query = GetLambdaQuery();
            //query.Page(4, page);
            var result = query.GroupBy(b => new { b.name, b.OrderId }).Select(b => new
            {
                num = b.Numbrer.SUM(),
                b.OrderId,
                b.name
            }).HavingCount(b => b.num > 1).ToList();
            foreach (var item in result)
            {
                Console.WriteLine($"{item.OrderId} {item.num} {item.name}");
            }
            query.PrintQuery();
            //var result = query.ToList<MongoResult>();
        }
        public void GroupTestOrder()
        {
            var query = GetLambdaQuery();
            query.GroupBy(b => b.name);
            var result = query.Select(b => new
            {
                num = b.Numbrer.SUM(),
                b.name
            }).OrderBy(b => b.num, false).ToList();

            foreach (var item in result)
            {
                Console.WriteLine($"{item.name} {item.num}");
            }
        }
        public void GroupTestOrder2()
        {
            var query = GetLambdaQuery();
            //query.Take(1);
            query.GroupBy(b => b.name);
            query.OrderBy(b=> b.Numbrer.SUM(),false);
            var result = query.Select(b => new
            {
                num = b.Numbrer.SUM(),
                b.name
            }).ToList();

            foreach (var item in result)
            {
                Console.WriteLine($"{item.name} {item.num}");
            }
        }
        public void GroupTest2(int page = 1)
        {
            //Delete(b=>b.Numbrer>0);
            //GetInitData();
            var sum = QueryList(b => b.Price > 0).Sum(b => b.Price * b.Numbrer);
            var query = GetLambdaQuery();
            var result = query.GroupBy(b => new { b.name }).Select(b => new
            {
                num2 = b.SUM(x => x.Numbrer * x.Price),
                name2 = b.name
            }).ToList();
            var sql = query.PrintQuery();//输出bson
            var sum2 = result.Sum(b => b.num2);
            foreach (var item in result)
            {
                Console.WriteLine($" name:{item.name2} num:{item.num2}");
            }
        }
        //public void MongoQueryTest()
        //{
        //    var connectionString = "mongodb://127.0.0.1:27017";
        //    var _client = new MongoClient(connectionString);
        //    var _MongoDB = _client.GetDatabase("admin");
        //    var coll= _MongoDB.GetCollection<MongoDBModel2>("MongoDBModel3");
        //    var query = coll.AsQueryable().GroupBy(b => b.name)
        //        .Select(b => new
        //        {
        //            name = b.Key,
        //            sum1 = b.Sum(x => x.Numbrer)
        //        });
        //    var result = query.ToList();
        //}
        public void SumTest()
        {
            var count = Count(b => b.Numbrer >= 0);
            if (count == 0)
            {
                GetInitData();
            }
            var query = GetLambdaQuery();
            var result = query.GroupBy(b => new {  b.OrderId }).Select(b => new
            {
                sum = b.Price.SUM(),
                b.OrderId,
            }).ToList();
            foreach (var item in result)
            {
                Console.WriteLine($"{item.OrderId} {item.OrderId} {item.sum}");
            }

        }
    }
}
