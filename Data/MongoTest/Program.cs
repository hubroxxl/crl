﻿using MongoDB.Bson;
using MongoDB.Driver;
using CRL.Core;
using System;

namespace MongoTest
{
    class Program
    {
        static void Main(string[] args)
        {
            ConfigInit.Init();
            ConsoleTest.DoCommand(typeof(Program));
        }
        public static void initData()
        {
            var instance = new MongoDBTestRepository();
            instance.Add(new MongoDBModel { name = "test1", Numbrer = 10 });
            instance.Add(new MongoDBModel { name = "test2", Numbrer = 20 });

            var instance2 = new MongoDBTest2Repository();
            instance2.Add(new MongoDBModel2 { name = "test1", Numbrer = 10 });
            instance2.Add(new MongoDBModel2 { name = "test2", Numbrer = 20 });

            var instance3 = new MongoDBTest3Repository();
            instance3.Add(new MongoDBModel3 { name = "test1", Numbrer = 10 });
            instance3.Add(new MongoDBModel3 { name = "test2", Numbrer = 20 });
        }
        public static void testBaseQuery()
        {
            var client = new MongoClient(ConfigInit.connectionString);
            var db = client.GetDatabase("admin");
            var collection = db.GetCollection<MongoDBModel>(typeof(MongoDBModel).Name);
            //var filter = Builders<BsonDocument>.Filter.Eq("name", "test1");
            //var projection = Builders<BsonDocument>.Projection.Include("name").Include("Numbrer");
            var projection = new BsonDocument();
            projection.Add("name", 1);
            var query = collection.Find(b => b.name == "test1").Project(projection);
            var result = query.ToList();
        }
        public static void testSelect()
        {
            MongoDBTestRepository.Instance.testSelect();
        }
        public static void testMongoJoin()
        {
            var query = MongoDBTestRepository.Instance.GetLambdaQuery();
            //query.Where(b => b.Id == 51);
            query.Page(1, 1);
            query.Select(b => new { b.name }).ToList();
            var join = query.Join<MongoDBModel2>((a, b) => a.name == b.name);
            var view = join.Select((a, b) => new { a.name, b.Numbrer });
            var list = view.ToList();
            query.PrintQuery();
        }
        public static void testMongoJoin2()
        {
            var query = MongoDBTestRepository.Instance.GetLambdaQuery();
            //query.Where(b => b.Id == 51);
            query.Page(1, 1);
            query.Select(b => new { b.name }).ToList();
            var join = query.Join<MongoDBModel2>((a, b) => a.name == b.name).Join<MongoDBModel3>((a, b, c) => b.name == c.name);
            var view = join.Select((a, b, c) => new { a.name, b.Numbrer });
            var list = view.ToList();
            query.PrintQuery();
        }
        public static void GroupTest()
        {
            MongoDBTestRepository.Instance.GroupTest();
        } 
        public static void testBsonQuery()
        {
            MongoDBTestRepository.Instance.testBsonQuery();
        }
        public static void GroupTestOrder()
        {
            MongoDBTestRepository.Instance.GroupTestOrder();
        }
        public static void GroupTestOrder2()
        {
            MongoDBTestRepository.Instance.GroupTestOrder2();
        }
        public static void testGroup2()
        {
            var query = MongoDBTestRepository.Instance.GetLambdaQuery();
            query.GroupBy(b => new { b.name, b.OrderId });
            var list = query.Select(b => new { b.name, b.OrderId }).ToList();
        }
    }
}
