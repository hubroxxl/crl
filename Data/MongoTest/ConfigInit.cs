﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Text;
using CRL.Mongo;
using CRL.Data;
using CRL.Data.Sharding;
using CRL.Data.DBAccess;
using CRL.Data.Attribute;
using CRL.Data.LambdaQuery.CRLExpression;
using CRL.Data.LambdaQuery;

namespace MongoTest
{
    class ConfigInit
    {
        public static string connectionString= "mongodb://127.0.0.1:27017/admin";
        public static void Init()
        {
            var builder = DBConfigRegister.GetInstance();
            builder.UseMongoDB();
            
            builder.RegisterDBAccessBuild(dbLocation =>
            {
                return new DBAccessBuild(DBType.MongoDB, connectionString);
            });
            
        }
    }
}
