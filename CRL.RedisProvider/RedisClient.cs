﻿/**
* CRL
*/
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CRL.RedisProvider
{
    /// <summary>
    /// redis缓存客户端调用类
    /// </summary>
    [Obsolete("使用 RedisClientFactory.GetCient")]
    public class RedisClient : IRedisClient
    {
        int _id = -1;
        string _name;
        public RedisClient()
        {
            _id = -1;
        }
        public RedisClient(int db = -1, string name = "")
        {
            _id = db;
            _name = name;
        }
        IRedisClient client;
        IRedisClient getClient()
        {
            if (client == null)
            {
                client = RedisClientFactory.GetCient(_id, _name);
            }
            return client;
        }
        public IDatabase GetDatabase()
        {
            return getClient().GetDatabase();
        }
        //internal static Func<string, string> GetRedisConn
        //{
        //    get
        //    {
        //        return ConfigBuilder.GetConfig<Func<string, string>>("redisConn");
        //    }
        //}
        public bool Remove(string key)
        {
            return getClient().Remove(key);
        }

        public T KGet<T>(string key)
        {
            return getClient().KGet<T>(key);
        }
        public T KGet<T>(string key, out bool find)
        {
            return getClient().KGet<T>(key, out find);
        }
        public string KGet(string key)
        {
            return getClient().KGet(key);
        }
        public Dictionary<string, T> KGetAll<T>(IEnumerable<string> keys)
        {
            return getClient().GetAll<T>(keys);
        }
        public List<string> SearchKey(string keyPattern, int pageSize = 50)
        {
            return getClient().SearchKey(keyPattern, pageSize);
        }

        #region hash
        public void HSet(string key, string hashField, object obj)
        {
            getClient().HSet(key, hashField, obj);
        }
        public void HSet(string key, Dictionary<string, object> values)
        {
            getClient().HSet(key, values);
        }
        public bool HRemove(string key, string hashField)
        {
            return getClient().HRemove(key, hashField);
        }

        public T HGet<T>(string key, string hashField)
        {
            var a = getClient().HGet<T>(key, hashField, out var value);
            return value;
        }
        public bool HGet<T>(string key, string hashField, out T value)
        {
            var a = getClient().HGet<T>(key, hashField, out value);
            return a;
        }
        public List<T> HGetAll<T>(string key)
        {
            return getClient().HGetAll<T>(key);
        }
        public Dictionary<string, Dictionary<string, T>> HGetAll<T>(IEnumerable<string> keys)
        {
            return getClient().HGetAll<T>(keys);
        }
        public Dictionary<string, T> HGetDic<T>(string key)
        {
            return getClient().HGetDic<T>(key);
        }
        public Dictionary<string, T> HGet<T>(string key, string[] hashFields)
        {
            return getClient().HGet<T>(key, hashFields);
        }
        public bool HContainsKey(string key, string hashField)
        {
            return getClient().HContainsKey(key, hashField);
        }

        public long GetHashCount(string key)
        {
            return getClient().GetHashCount(key);
        }
        public List<string> HGetAllKeys(string key)
        {
            return getClient().HGetAllKeys(key);
        }
        #endregion
        /// <summary>
        /// 延期
        /// </summary>
        /// <param name="key"></param>
        /// <param name="expiresTime"></param>
        public void KSetEntryIn(string key, TimeSpan expiresTime)
        {
            getClient().KSetEntryIn(key, expiresTime);
        }

        public bool KSet(string key, object obj, TimeSpan? timeSpan)
        {
            return getClient().KSet(key, obj, timeSpan);
        }

        public bool ContainsKey(string key)
        {
            return getClient().ContainsKey(key);
        }
        /// <summary>
        /// 递增
        /// </summary>
        /// <param name="key"></param>
        /// <param name="field"></param>
        /// <param name="timeOut"></param>
        /// <returns></returns>
        public long Increment(string key, string field, TimeSpan? timeOut = null, int num = 1)
        {
            return getClient().Increment(key, field, timeOut, num);
        }

        #region list
        //public long ListRightPush<T>(string key, T value)
        //{
        //    return getClient().ListRightPush(key, value);
        //}
        public long ListRightPush(string key, string value)
        {
            return getClient().ListRightPush(key, value);
        }
        public long ListLeftPush(string key, string value)
        {
            return getClient().ListLeftPush(key, value);
        }
        public long ListRemove(string key, object value)
        {
            return getClient().ListRemove(key, value);
        }
        public IEnumerable<string> ListRange(string key, long start, long end)
        {
            return getClient().ListRange(key, start, end);
        }
        public void ListTrim(string key, long start, long end)
        {
            getClient().ListTrim(key, start, end);
        }
        public long ListLength(string key)
        {
            return getClient().ListLength(key);
        }
        #endregion

        public bool GetBit(string key, long offSet)
        {
            return getClient().GetBit(key, offSet);
        }
        public void SetBit(string key, long offSet, bool bit)
        {
            getClient().SetBit(key, offSet, bit);
        }

        public bool BatchRemove(string keyPattern)
        {
            return getClient().BatchRemove(keyPattern);
        }

        #region geo
        public bool GeoAdd(string key, double longitude, double latitude, string value)
        {
            return getClient().GeoAdd(key, longitude, latitude, value);
        }
        public GeoRadiusResult[] GeoRadius(string key, double longitude, double latitude, double radius)
        {
            return getClient().GeoRadius(key, longitude, latitude, radius);
        }
        public bool GeoRemove(string key, string value)
        {
            return getClient().GeoRemove(key, value);
        }
        #endregion
        public Dictionary<string, T> GetAll<T>(IEnumerable<string> keys)
        {
            return getClient().GetAll<T>(keys);
        }

        bool IRedisClient.HSet(string key, string hashField, object value)
        {
            return getClient().HSet(key, hashField, value);
        }

        public void Publish(string channel, params string[] messages)
        {
            getClient().Publish(channel, messages);
        }

        public Task PublishAsync(string channel, params string[] messages)
        {
            return getClient().PublishAsync(channel, messages);
        }

        public void KSet(Dictionary<string, object> values)
        {
            getClient().KSet(values);
        }

        //public bool KSet(string key, object value, TimeSpan? expireIn)
        //{
        //    return getClient().KSet(key, value, expireIn);
        //}

        public void Subscribe(string channelName, Action<string, string> callBack)
        {
            getClient().Subscribe(channelName, callBack);
        }

        public Task SubscribeAsync(string channelName, Action<string, string> callBack)
        {
            return getClient().SubscribeAsync(channelName, callBack);
        }
        public void UnSubscribe(string channelName)
        {
            getClient().UnSubscribe(channelName);
        }
    }
}
