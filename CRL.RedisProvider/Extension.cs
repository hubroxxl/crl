﻿/**
* CRL
*/
using CRL.Core;
using CRL.Core.Session;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRL.RedisProvider
{
    public static class Extension
    {
        static Dictionary<string, string> conns = new Dictionary<string, string>();

        public static ConfigBuilder UseRedis(this ConfigBuilder builder, string redisConn, string name = "")
        {
            if (string.IsNullOrEmpty(name))
            {
                name = "default__";
            }
            if (conns.ContainsKey(name))
            {
                return builder;
            }
            conns.Add(name, redisConn);
            Func<string, string> func = (type2) =>
            {
                var a = conns.TryGetValue(name, out var c);
                if (a)
                {
                    return c;
                }
                conns.TryGetValue("default__", out c);
                return c;
            };
            builder.AddConfig("redisConn", func);
            return builder;
        }
        public static ConfigBuilder UseRedis(this ConfigBuilder builder, Func<string, string> func)
        {
            builder.AddConfig("redisConn", func);
            return builder;
        }
        /// <summary>
        /// 使用ResionSession
        /// </summary>
        /// <param name="builder"></param>
        public static ConfigBuilder UseRedisSession(this ConfigBuilder builder)
        {
            Func<IWebContext, AbsSession> func = (context) =>
            {
                return new RedisSession(context);
            };
            builder.AddConfig("redisSessionManage", func);
            //builder.__SessionCreater = (context) =>
            //{
            //    return new RedisSession(context);
            //};
            return builder;
        }
        //public static ConfigBuilder UseRedisDelegateCacheContainer(this ConfigBuilder builder)
        //{
        //    DelegateCache.UseCustomCacheContainer(new RedisDelegateCacheContainer());
        //    return builder;
        //}
    }
}
