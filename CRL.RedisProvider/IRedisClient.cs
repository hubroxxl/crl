﻿/**
* CRL
*/
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CRL.RedisProvider
{
    public interface IRedisClient
    {
        IDatabase GetDatabase();
        bool BatchRemove(string keyPattern);
        bool ContainsKey(string key);
        bool GeoAdd(string key, double longitude, double latitude, string value);
        GeoRadiusResult[] GeoRadius(string key, double longitude, double latitude, double radius);
        bool GeoRemove(string key, string value);
        Dictionary<string, T> GetAll<T>(IEnumerable<string> keys);
        bool GetBit(string key, long offSet);
        long GetHashCount(string key);
        bool HContainsKey(string key, string hashField);
        T HGet<T>(string key, string hashField);
        bool HGet<T>(string key, string hashField, out T value);
        Dictionary<string, T> HGet<T>(string key, string[] hashFields);
        Dictionary<string, Dictionary<string, T>> HGetAll<T>(IEnumerable<string> keys);
        List<T> HGetAll<T>(string key);
        List<string> HGetAllKeys(string key);
        Dictionary<string, T> HGetDic<T>(string key);
        bool HRemove(string key, string hashField);
        void HSet(string key, Dictionary<string, object> values);
        bool HSet(string key, string hashField, object value);
        long Increment(string key, string field, TimeSpan? timeOut = null, int num = 1);
        string KGet(string key);
        T KGet<T>(string key);
        T KGet<T>(string key, out bool find);
        void KSetEntryIn(string key, TimeSpan expiresTime);
        long ListLength(string key);
        IEnumerable<string> ListRange(string key, long start, long end);
        long ListRemove(string key, object value);
        long ListRightPush(string key, string value);
        long ListLeftPush(string key, string value);
        void ListTrim(string key, long start, long end);
        void Publish(string channel, params string[] messages);
        Task PublishAsync(string channel, params string[] messages);
        bool Remove(string key);
        List<string> SearchKey(string keyPattern, int pageSize = 10);
        void KSet(Dictionary<string, object> values);
        bool KSet(string key, object value, TimeSpan? expireIn);
        void SetBit(string key, long offSet, bool bit);
        void Subscribe(string channelName, Action<string, string> callBack);
        Task SubscribeAsync(string channelName, Action<string, string> callBack);
        void UnSubscribe(string channelName);
    }
}