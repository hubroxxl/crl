### ORM 对查询外包装

1. 安装nuget包：CRL
2. using CRL;

创建一个查询后，需要对此查询再次包装查询,如：

```sql
select * from (select field,count(0) as count from table1 group by field) a where a.count>10
```

使用ILambdaQuery实现查询

```c#
var query = Code.ProductDataRepository.Instance.GetLambdaQuery();
            query.Where(b => b.Id > 0);
            var query2 = query.GroupBy(b => new { b.ProductId, b.ProductChannel }).Select(b => new
            {
                b.ProductId,
                b.ProductChannel,
                count = b.ProductId.COUNT()
            }).AsQuery();
            
            var view3 = query2.GroupBy(b => b.ProductId).Select(b => new
            {
                b.ProductId,
                sum = b.count.SUM()
            });
            //query2.Page(1, 1).OrderBy(b => b.ProductId);
            query2.PrintQuery();
            var result = view3.ToList();
```

输出SQL为

```sql
SELECT tp1.[ProductId] AS ProductId,
       Sum(tp1.count)  AS sum
FROM   (SELECT t1.[ProductId],
               t1.[ProductChannel],
               Count(t1.[ProductId]) AS count
        FROM   [ProductData] t1 WITH (nolock)
        WHERE  ( t1.[Id] > '0' )
        GROUP  BY t1.[ProductId],
                  t1.[ProductChannel]) tp1
GROUP  BY tp1.[ProductId] 

```

ILambdaQuery能实现子查询和嵌套查询，只要符合T-SQL语义逻辑，可以使用ILambdaQueryResultSelect无限叠加

如：

- join后group
- join后再join
- group后再join
- join一个group结果
- join一个union结果
- 对union进行group再join
- ...

源码示例参考

[Data/QueryTest/test · hubroxxl/CRL - 码云 - 开源中国 (gitee.com)](https://gitee.com/hubroxxl/crl/tree/master/Data/QueryTest/test)