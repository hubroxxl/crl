### ORM case 行转列

1. 安装nuget包：CRL
2. using CRL;

行转列SQL实现[ sql语句实现行转列的3种方法_寸草心的博客-CSDN博客_sql 行转列](https://blog.csdn.net/qq_35531549/article/details/90404986)

使用ILambdaQuery实现查询

```c#
var query = Code.ProductDataRepository.Instance.GetLambdaQuery();
            var caseQuery = query.CreateCase<int>("sum");
            caseQuery.Case(b => b.ProductChannel).When(1).Then(x => 1).Else(x => 0).End();
            var caseQuery2 = query.CreateCase<int>("sum");
            caseQuery2.Case(b => b.ProductChannel).When(0).Then(x => 1).Else(x => 0).End();
            query.GroupBy(b => b.ProductId);
            var view = query.Select(b => new
            {
                b.ProductId,
                count1 = caseQuery.GetCaseResult(),
                count2 = caseQuery2.GetCaseResult()
            });
         
            query.PrintQuery();
            var result = view.ToList();
```

输出SQL为

```sql
SELECT t1.[ProductId],
       Sum(CASE
             WHEN t1.[ProductChannel] = 1 THEN 1
             ELSE 0
           END) AS count1,
       Sum(CASE
             WHEN t1.[ProductChannel] = 0 THEN 1
             ELSE 0
           END) AS count2
FROM   [ProductData] t1 WITH (nolock)
GROUP  BY t1.[ProductId] 
```

ILambdaQuery能实现子查询和嵌套查询，只要符合T-SQL语义逻辑，可以使用ILambdaQueryResultSelect无限叠加

如：

- join后group
- join后再join
- group后再join
- join一个group结果
- join一个union结果
- 对union进行group再join
- ...

源码示例参考

[Data/QueryTest/test · hubroxxl/CRL - 码云 - 开源中国 (gitee.com)](https://gitee.com/hubroxxl/crl/tree/master/Data/QueryTest/test)

