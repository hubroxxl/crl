### Grpc对象转proto代码工具

虽然Grpc.Tools可以将proto文件自动生成代理类，但是proto文件得手敲，还容易出错，如果接口比较复杂，定义比较多，这就很头疼了

为了解决这个问题Class2Proto诞生了，使用标准C#对象转换成proto文件，不管是新写的接口，还是老的API接口转Grpc,都没问题

1. 安装nuget包：CRL.Class2Proto
2. using CRL.Class2Proto;

定义标准接口代码

```c#
[ProtoServiceAttribute("protoTest", "ClassTestAction")]
public interface ClassTestAction
{
    ClassTest getObj(TestObj a);
    //ClassTest getObj2(TestObj a);
    Request getObj3(TestObj2<Request> a);
    TestObj2<List<Request>> getObj4(TestObj2<List<Request>> a);
}
```

运行转换方法生成proto文件

```c#
var convertInfo = ClassConvert.Convert(System.Reflection.Assembly.GetAssembly(typeof(ClassTest)));
convertInfo.ForEach(b => b.CreateCode());
```

程序目录Protos成生了protoTest.proto文件

```protobuf
syntax = "proto3";
option csharp_namespace = "gRPC.gRpcClient.protoTest";
package protoTest;
service ClassTestAction {
    rpc getObj(TestObjDTO) returns (ClassTestDTO);
    rpc getObj3(TestObj2_RequestDTO) returns (RequestDTO);
    rpc getObj4(TestObj2_l_RequestDTO) returns (TestObj2_l_RequestDTO);
}
message StatusDTO {
    ok = 0;
    fail = 1;
}
message TestObjDTO {
    string Id = 1;
}
message ClassTestDTO {
    string Name = 1;
    int32 nullableValue = 2;
    StatusDTO Status = 3;
    TestObjDTO Data = 4;
    repeated string Name2 = 5;
    repeated TestObjDTO Data2 = 6;
    map<string, TestObjDTO> Data3 = 7;
    string time = 8;
    double decimalValue = 9;
    string Id = 10;
}
message RequestDTO {
}
message TestObj2_RequestDTO {
    string Id = 1;
    RequestDTO data = 2;
}
message TestObj2_l_RequestDTO {
    string Id = 1;
    repeated RequestDTO data = 2;
}
```

