## Tcc Saga分布式事务补偿

TCC 事务特点：
* Try 用于资源冻结/预扣；
* Try 全部环节通过，代表业务一定能完成，进入 Confirm 环节；
* Try 任何环节失败，代表业务失败，进入 Cancel 环节；
* Confirm 失败会进行重试N次，直到交付成功，或者人工干预；
* Cancel 失败会进行重试N次，直到取消成功，或者人工干预；

SAGA 事务特点：
* Commit 用于业务提交；
* Commit 全部环节通过，代表业务交付成功；
* Commit 任何环节失败，代表业务失败，进入 Cancel 环节；
* Cancel 失败会进行重试N次，直到取消成功，或者人工干预；
----
此组件实现了以上事务补尝，简化调用，只需要TCC,SAGA事务接口要求实现代码，无需关心事务处理过程和后续处理
并且对事务作了持久化，发生异常时，可以自动或手动继续执行事务，直到完成事务过程

### 配置数据存储
默认使用内存存储事务步骤，若要使用其它方式，实现ITransInfoManage接口
```c#
public interface ITransInfoManage
{
    void CreateTable();
    void DeleteCompleted();
    List<TransMasterInfo> QueryChecks(string projectName);
    TransMasterInfo QueryChek(TransOptions option);
    List<TransStepInfo> QuerySteps(IEnumerable<string> tIds);
    bool SaveSteps(TransMasterInfo info, List<TransStepInfo> steps, out string error);
    void UpdateStatus(string tId, int stepIndex, TransStatus status, string msg);
    void UpdateStatus(string tId, TransStatus status, string msg);
}
//接口实现
public class TransInfoManageRedis : ITransInfoManage
{
	....
}
* 配置
TransConfig.RegisterTransInfoManage("test", () => new TransInfoManageRedis());
```
### 事务调用
```c#
var tccControl = new TccControl();
tccControl.Then<TccStep1>().Then<TccStep2>().Then<TccStep3>();
var result = tccControl.Execute();
```
### 以step对象的方式
```c#
    public class TccStep1 : AbsTccStep<string>
    {
        public TccStep1()
        {

        }
        public override string Name => GetType().Name;
#if NETSTANDARD
        IServiceProvider provider;
        public Step4(IServiceProvider _provider)
        {
            provider = _provider;
        }
#endif
        public override void Try()
        {
            Program.print("tcc", "try", 1);
        }
        public override void Cancel()
        {
            Program.print("tcc", "cancel", 1);
        }

        public override void Commit()
        {
            Program.print("tcc", "commit", 1);
        }
    }
    public class TccStep2 : AbsTccStep<string>
    {
        public TccStep2()
        {

        }
        public override string Name => GetType().Name;
#if NETSTANDARD
        IServiceProvider provider;
        public Step5(IServiceProvider _provider)
        {
            provider = _provider;
        }
#endif
        public override void Try()
        {
            Program.print("tcc", "try", 2);
        }
        public override void Cancel()
        {
            Program.print("tcc", "cancel", 2);
        }

        public override void Commit()
        {
            Program.print("tcc", "commit", 2);
        }
    }
    public class TccStep3 : AbsTccStep<string>
    {
        public TccStep3()
        {

        }
        public override string Name => GetType().Name;
#if NETSTANDARD
        IServiceProvider provider;
        public Step6(IServiceProvider _provider)
        {
            provider = _provider;
        }
#endif
        public override void Try()
        {
            Program.print("tcc", "try", 3);
        }
        public override void Cancel()
        {
            Program.print("tcc", "cancel", 3);
        }

        public override void Commit()
        {
            Program.print("tcc", "commit", 3);
        }
    }
```
### 以委托方法的形式
注：委托方法无法存储到数据库中
```c#
tccControl.Then(() =>
{
    print("tcc", "try", 1);
}, () =>
{
    print("tcc", "commit", 1);
}, () =>
{
    print("tcc", "cancel", 1);
}).Then(() =>
{
    print("tcc", "try", 2);
}, () =>
{
    print("tcc", "commit", 2);
}, () =>
{
    print("tcc", "cancel", 2);
}).Then(() =>
{
    print("tcc", "try", 3);
}, () =>
{
    print("tcc", "commit", 3);
}, () =>
{
    print("tcc", "cancel", 3);
});
```
### 事务的干预
事务方法(Execute)运行后会得到一个同步结果
```c#
public class ExecuteResult
{
    public bool AbstractResult { get; set; }
    public bool NeedRepair { get; set; }
    public string LastError { get; set; }
    public TransStatus Status { get; set; }
    public IStep ErrorStep { get; internal set; }
}
```
外部调用根据此结果反馈到用户
对于需要干预的事务，可以使用以下代码手动处理
```c#
var all = SagaLoader.LoadAll("test");
all.ForEach(b => b.Execute());
```
### .net core 注入
```c#
services.AddTransControl(Assembly.GetAssembly(typeof(Neptunus.Transaction.Test.Step1)), Assembly.GetAssembly(typeof(Program)));
app.UseTransControl("test");
//调用
var saga = provider.GetService<SagaControl>();
```