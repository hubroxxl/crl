## 异步缓存

异步或同步更新缓存实现
数据存储为内存和redis
1. 同步更新：数据过期后，在本次调用更新
2. 异步更新：数据过期后，后台线程更新
----
对于同步更新是一种很常见的处理方式，但是数据更新过程比如果较慢，调用者要一直等待，造成卡顿
因此引入了异步更新，由后台线程调用数据更新方法生成缓存数据，调用者无需等待，缺点是本次无法获取新的缓存数据

### 配置redis
```c#
var cb = new ConfigBuilder();
cb.UseRedis("Server_204@127.0.0.1:6389");
```
### 获取实例
```c#
var instance = DelegateCacheFactory.GetRedisInstance()
var instance2 = DelegateCacheFactory.GetMemoryInstance()
```
### 同步更新：
委托内返回的值在过期后本次更新
```c#
var data = instance.Init("test", 0.1, () =>
{
    return new List<string> { val1 };
});
Console.WriteLine($"get {val1} return {data}");
```
### 异步更新：
委托内返回的值在过期后，下次调用生效
后台有轮循线程检查数据是否过期或是否需要更新
如果需要更新，则反射注册委托更新缓存数据
```c#
static string getTestArgs(DateTime time)
{
    return time.ToString();
}
var val1 = "testDelegateCache2";
var time = DateTime.Now;
var data = instance.InitAsyncUpdate("test22", 0.1, false, getTestArgs, time);
Console.WriteLine($"get {val1} return {data}");
```
### 移除缓存
```c#
instance.Remove(key)
```