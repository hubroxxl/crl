## ORM特性
CRL是一个效率高，易于扩展的ORM，支持.NetFramework 4.6+ .NetCore 2.0+
### 数据源可扩展性
除了支持关系型数据库生成SQL外，也支持非关系型数据库
通过解析生成内置表达多结构，轻松实现实现非SQL类型查询表达，如json
已支持非关系型数据库有:
	1. MongoDb
	2. Elasticsearch
### 注入式数据连接源
和常用的实例化访问对象传参比较，如:
```c#
	var conn= new MySqlConnection(ConnectionString);
```
CRL使用委托注入的方式定义数据连接
```c#
	builder.RegisterLocation<Code.Sharding.OrderSharding>((t, a) =>
	{
		var tableName = $"{t.TableName}_{a.MemberId}";
		//返回定位库和表名
		return new Location("testdb", tableName);
	});
```
使用什么连接访问数据库不是在调用时就固定了，而是在注入的委托里实现
因此很容易实现，分库，分表，主从模式

### 作为扩展使用
CRL LambdaQuery支持丰富的查询语法，所写即生成，如果只想用LambdaQuery，可直接使用查询扩展

	1. 基于IDbConnection的扩展
	2. 基于Elasticsearch ElasticClient的扩展

### 内置异步缓存
按实体或LambdaQuery查询实现的内置缓存
对于非及时数据，轻松实现数据缓存和自动更新，使数据库压力大大减轻

### 效率
以BenchmarkDotNet测试为例（仅供参考）
数据库为sqlite
测试程序见[](tree/master/Data/dbTest)
表达式解析 query.Where(filter).ToString()
| Method        | ProvideType       | Mean            | Error         | StdDev        | Gen0    | Gen1   | Allocated |
|---------------|-------------------|-----------------|---------------|---------------|---------|--------|-----------|
| TestCondition | orm1              | 566,803.40 ns   | 12,890.384 ns | 36,984.927 ns | 9.7656  | -      | 31943 B   |
| TestCondition | DapperTest        | 44.37 ns        | 0.927 ns      | 2.537 ns      | 0.0204  | -      | 64 B      |
| TestCondition | EfSqlliteTest     | 166,142.66 ns   | 4,631.944 ns  | 13,584.690 ns | 17.5781 | -      | 55778 B   |
| TestCondition | orm2              | 604,019.17 ns   | 12,861.250 ns | 37,516.871 ns | 8.7891  | -      | 30030 B   |
| TestCondition | orm3              | 1,047,346.11 ns | 18,131.667 ns | 34,497.380 ns | 5.8594  | 1.9531 | 45996 B   |
| TestCondition | CRLTest           | 14,003.68 ns    | 278.607 ns    | 399.570 ns    | 1.7548  | 0.4272 | 5514 B    |
| TestCondition | orm4              | 188,462.25 ns   | 3,757.174 ns  | 4,326.768 ns  | 21.7285 | -      | 68492 B   |


----
## 使用
### 配置数据源
数据连接采用委托注入的方式
```c#
	var builder = DBConfigRegister.GetInstance();
	builder.RegisterDBAccessBuild(dbLocation =>
			{
				if (dbLocation.ManageName == "mongo")
				{
					return new DBAccessBuild(DBType.MongoDB, "mongodb://127.0.0.1:27017/admin");
				}
				return new DBAccessBuild(DBType.MSSQL, "server=.;database=testDb; uid=sa;pwd=123;");
			});
	builder.RegisterLocation<Code.Sharding.OrderSharding>((t, a) =>
	{
		var tableName = $"{t.TableName}_{a.MemberId}";
		//返回定位库和表名
		return new Location("testdb", tableName);
	});
```
RegisterDBAccessBuild方法内可根据dbLocation返回参数进行判断，返回不同的DBAccessBuild
达到连接不同的数据源
RegisterLocation可以定义访问对象使用什么Location规则，从而影响到dbLocation
使用此方式达到动态切换数据源的目的

### 定义数据管理对象
```c#
	public class OrderRepository : BaseProvider<Order>
	{
		public override string ManageName => "default";
	}
	var rep = new OrderRepository();
```
按上文说明，OrderRepository明确了两个意义

	1. 关联实体是Order
	2. ManageName为 default
回传到dbLocation，以区分数据源

或使用简单工厂实例化
```c#
	var instance = RepositoryFactory.Get<Order>();
```

### 定义实体
```c#
	public class Order
	{
		[Field(FieldIndexType = FieldIndexType.非聚集)]
		public string InterFaceUser
		{
			get;
			set;
		}
		[Field(MapingName = "ProductName1")]
		public string ProductName
		{
			get;
			set;
		}
	}
```

实体属性可标记Field特性
以编程方式定义特性
```c#
	var propertyBuilder = new PropertyBuilder<Code.Member>();
	propertyBuilder.AsIndex(b => b.AccountNo);
	propertyBuilder.AsUniqueIndex(b => b.Mobile);
	propertyBuilder.AsUnionIndex("index2", b => new { b.AccountNo, b.Name }, FieldIndexType.非聚集);
	propertyBuilder.Property(b => b.AccountNo).WithColumnLength(100);
```
### 基本数据操作
#### 插入
```c#
	var instance = ProductDataRepository.Instance;
	instance.Add(new ProductData { BarCode = "222", Order = new Order { OrderId = "1111" }, Number = 2, ProductName = "test11" });
	instance.BatchInsert(new List<ProductData> { new ProductData { BarCode = "222", Number = 2, ProductName = "test11", Order = new Order { OrderId = "1111" } }, new ProductData { BarCode = "222", Number = 2, ProductName = "test11", Order = new Order { OrderId = "1111" } } });
```
#### 查询
```c#
	var p = instance.QueryItem(b => b.Id > 0);
	//使用表达式查询
	var query = instance.GetLambdaQuery();
	query.Where(b => (b.Id + b.Number) * 3 < 110);
	query.Select(b => new
	{
		s1 = (b.Id + b.Number) * 3,
		s2 = b.Id * b.Number,
		s3 = b.Id
	}).ToSingle();
```
#### 更新
```c#
	var instance = RepositoryFactory.Get<MemmberAccount>();
	instance.Update(b => b.Id == 1, b => new MemmberAccount { AccountName = "2222" });
	instance.Update(b => b.Id == 1, new { AccountName = "2222" });
	var c = new ParameCollection();
	c["AccountName"] = "AccountName2";
	c["$Balance"] = "Balance+1";
	instance.Update(b => b.Id == 4, c);
```
#### 删除
```c#
	var instance = RepositoryFactory.Get<MemmberAccount>();
	instance.Delete(b => b.Id == 1);
```
#### LambdaQuery进阶
	-ILambdaQuery 
	通过instance.GetLambdaQuery()获取查询对象
	- ILambdaQueryResultSelect 
	通过ILambdaQuery.Select得到ILambdaQueryResultSelect
	等效sql为 select field1,field2 from table,表示确认一个查询输出
	
所有关联嵌套都是这两种类型组合

##### 表之间关联
	query.Jon<T>((a,b)=>a.id==b.id,JoinType)
	
##### 查询间关联
	var query2=query.CreateQuery<T2>();
	var resultSelect=query2.Select(...)
	query.Join(resultSelect,(a,b)=>a.id==b.id,JoinType)
	
##### 开窗
```c#
	var windowFunc = query.CreateWindowFunc().Sum(b => b.Id).Over().PartitionBy(b => b.Remark).OrderBy(b => b.AddTime, true).OrderBy(b => b.ProductId, false);
	var v = query.Select(b => new { ss = windowFunc.End() });
```
##### case
```c#
	 var caseQuery = query.CreateCase().Case(x => x.Remark).When("1")//按字段值
		 .Then(x => "11").Else(x => "22");
	 query.Where(b => caseQuery.End() == "22");
	 query.Select(b => new { ss = caseQuery.End() });
 ```
##### case实现行转列
```c#
	 var caseQuery = query.CreateCase("sum").Case(b => b.ProductChannel).When(1).Then(x => 1).Else(x => 0);
	 var caseQuery2 = query.CreateCase("sum").Case(b => b.ProductChannel).When(0).Then(x => 1).Else(x => 0);
	 query.GroupBy(b => b.ProductId).OrderBy(b => b.ProductId);
	 query.Page(1, 1);
	 var view = query.Select(b => new
	 {
		 b.ProductId,
		 count1 = caseQuery.End(),
		 count2 = caseQuery2.End()
	 });
```
#### 事务
CRL只支持单库事务，但支持嵌套调用
```c#
//简化了事务写法,自动提交回滚
instace.PackageTrans((out string ex) =>
{
    ex = "";
    PackageTrans(...)
    return false; //会回滚
}, out error);
```
上例PackageTrans内又调用了PackageTrans方法，最终会合并成一个事务

### 使用缓存
重写方法以定义缓存数据查询范围，如未重写，则查询所有
以下示例为缓存5分钟过期
```c#
	protected override ILambdaQuery<ProductData> CacheQuery()
	{
		return GetLambdaQuery().Expire(5);
	}
```
查询缓存数据
```c#
	instance.QueryItemFromCache(1);
	instance.QueryListFromCache(b=>b.id==1);
```

### 编译存储过程(仅SQLSERVER)
所有LambdaQuery都可以编译为存储过程
query.CompileToSp(true).Where(...).ToList()
调用后，会在数据库生成开头为ZautoSp_的存储过程
再次调用则会直接按存储过程返回结果
示例:
```sql
 ALTER PROCEDURE [dbo].[ZautoSp_20C1F148B75C2C9A]
(@p0 nvarchar(500))
AS
set  nocount  on
	select t1.[InterFaceUser],t1.[OrderId],t1.[Date2],t1.[UserId],t1.[BarCode],t1.[TransType],t1.[ProductId],t1.[ProductName1] as ProductName,t1.[SupplierId],t1.[SupplierName],t1.[CategoryName],t1.[PurchasePrice],t1.[SoldPrice],t1.[Style],t1.[Remark],t1.[IsTop],t1.[Number],t1.[ProductChannel],t1.[Object2],t1.[Id],t1.[AddTime] from [ProductData] t1  with (nolock)  where t1.[ProductName1]+t1.[CategoryName] in(select t2.[B]+t2.C from MM t2 with (nolock) where (t1.[BarCode]=t2.[D]) AND (t2.[B]=@p0)) 
```
	- 查询条件或返回字段变更后会生成新的存储过程
	- 手动删除后会重新生成

### 结构同步
配置好实体字段特性后，可用以下代码同步表结构

	- 检查并创建表 instance.CreateTable
	- 检查并创建索引 instance.CreateTableIndex
	- 同步表结构 instance.SyncTableFields
	- 同步字段注释 instance.UpdateTableFiledComments
----
## 扩展
### ado扩展
基于IDbConnection的扩展
获取IAbsDBExtend数据操作对象
```c#
public static IAbsDBExtend GetDBExtend(this IDbConnection dbConnection, IDbTransaction dbTransaction = null)

var dbExtend = GetDBExtend(dbConnection, dbTransaction);
dbExtend.QueryItem<Goods>(b=>b.Id==1);
```
获取ILambdaQuery数据查询对象
```c#
public static ILambdaQuery<T> GetLambdaQuery<T>(this IDbConnection dbConnection, IDbTransaction dbTransaction = null) where T : class

var query=GetLambdaQuery<Goods>(dbConnection);
query.Where(b=>b.Id==1);
```
如果要对EF扩展，使用EFCore.Extensions

### Elasticsearch扩展
Elasticsearch扩展可以简化Elasticsearch客户端的调用，并且可以输出原始查询结构

```c#
public static ILambdaQuery<T> GetLambdaQuery<T>(this ElasticClient client) where T : class
```
Elasticsearch原始查询结构嵌套严重，稍复杂点，数括号都数不过来，逻辑关系位置不同，bool查询嵌套层数也会不同，很是麻烦
CRL扩展极大简化了这种查询
以一个bool查询为例
```c#
query.Where(b=> b.DataType == "Goods");
query.Where(b=> b.Id == "2" || b.Id == "3");
query.PrintQuery();
var list = query.ToList();
Console.WriteLine($"count should 2 {list.Count == 2}");
```
等效Elasticsearch查询
```json
{
    "BoolQuery": {
        "Filter": [
            {
                "MatchPhraseQuery": {
                    "Query": "Goods",
                    "Slop": 0,
                    "Field": "DataType"
                }
            },
            {
                "BoolQuery": {
                    "Should": [
                        {
                            "MatchPhraseQuery": {
                                "Query": "2",
                                "Slop": 0,
                                "Field": "Id"
                            }
                        },
                        {
                            "MatchPhraseQuery": {
                                "Query": "3",
                                "Slop": 0,
                                "Field": "Id"
                            }
                        }
                    ]
                }
            }
        ]
    },
    "sort": []
}
```
除了自动逻辑转换，还可以使用原始Elasticsearch对象查询
```c#
var bq = new BoolQuery()
{
    Filter = new List<QueryContainer> { EsQueryCreater.CreateMatchPhraseQuery("DataType", "Goods") },
    Should = new List<QueryContainer> { EsQueryCreater.CreateMatchPhraseQuery("Name", "123") }
};
query.Where(b => b.WithQueryBase(bq));
query.PrintQuery();

var bqCreater = new BoolQueryCreater<GoodsInfo>(query);
bqCreater.SetFilter(x => x.Number == 4);
bqCreater.SetShould(x => x.Number < 3);
query.WithBoolQuery(bqCreater);
query.PrintQuery();

```

### 使用MongoDB
同关系系数据库一样配置，调试方式也一样
对使用者来说，没有数据库之分，方法都一样
```c#
string connectionString= "mongodb://127.0.0.1:27017/admin";
var builder = DBConfigRegister.GetInstance();
builder.UseMongoDB();

builder.RegisterDBAccessBuild(dbLocation =>
{
    return new DBAccessBuild(DBType.MongoDB, connectionString);
});

var query = MongoDBTestRepository.Instance.GetLambdaQuery();
//query.Where(b => b.Id == 51);
query.Page(1, 1);
query.Select(b => new { b.name }).ToList();
var join = query.Join<MongoDBModel2>((a, b) => a.name == b.name);
var view = join.Select((a, b) => new { a.name, b.Numbrer });
var list = view.ToList();
query.PrintQuery();

```