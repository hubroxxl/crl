## EventBus 组件
采用发布，订阅的模式，支持多种数据容器
可作为事件处理，数据处理队列，rpc使用
支持的数据容器
	- 关系型数据库
	- rabbitmq
	- redis
	- mongo
	- memory
	- rocketmq
----
对于rabbitmq，redis，memory 消费事件由订阅的方式触发,适合实时性比较高的场景
对于数据库类的容器，消费事件由轮循的方式触发,适合数据重复处理任务
	- DB类型 任何条件下都能保证消息顺序性
	- MQ类型 在异常重试情况下，无法保证消息的顺序性

### 配置
```c#
config = new QueueConfig();
config.UseRabbitMQ(cfg =>
{
    cfg.HostName = "127.0.0.1";
    cfg.UserName = "guest";
    cfg.Password = "guest";
}, opt =>
{
    opt.ConsumerChannelFunc = channel => channel.BasicQos(0, 1, false);
});
//可以使用多种配置，订阅时需要指定
config.UseRedis(cfg =>
{
    cfg.ConnString = "Server_204@127.0.0.1:6389";
    cfg.UseList = true;
});
config.UseDb(cfg =>
{
    cfg.DBType = DBType.SQLITE;
    cfg.ConnString = sqlLiteDb;
    cfg.ConsumerByNameSingle = true;
});
config.UseMemory();
```
### 创建发布器
```c#
//默认发布器
client = new Publisher(config);

client.Publish("test", DateTime.Now.Second);
//优先级发布
for (int i = 0; i < 50; i++)
{
    var priority = i % 2 == 0 ? 0 : 10;
    client.Publish("test", priority, priority);
}
//延迟发布
client.Publish("testDelay", DateTime.Now, opt => { opt.DelayMs = 3000; });//延迟队列发送
//rpc 调用,等待订阅返回值
var res = client.Request<string>("testSendReq3", DateTime.Now.ToString());
```
### 订阅数据
```c#
var subService = new SubscribeService(config);

//同步订阅
subService.AddSubscribe<int>(new SubscribeAttribute { Name = "test", RetryTimes = 2 }, b =>
{
    //返回false时会重试
    Console.WriteLine($"testRetry2 recieve {b}");
    return false;
});
//异步订阅
subService.AddSubscribeAsync<DateTime>(new SubscribeAttribute { Name = "testSendReq" }, async b =>
{
    await Task.Delay(10);
    //Console.WriteLine($"testSendReq recieve {b}");
    return $"reqResponse1 {DateTime.Now}";
});
```
### 按声明方法自动订阅
```c#
注册订阅所在的程序集
config.RegisterSubscribe(Assembly.GetAssembly(typeof(Program2)));
声明方法
[Subscribe]
public class SubscribeTest
{
    [Subscribe("test")]
    public void test(int a)
    {
        Console.WriteLine($"test receive {a}");
    }
	[Subscribe("testAsync")]
	public async Task testAsync(DateTime time)
	{
		Console.WriteLine($"testAsync receive {time}");
		await Task.Delay(100);
	}
}
```
### 订阅可选参数
| 属性              | 说明             |
|-------------------|---------------------------|
| Name              | 订阅的名称             |
| ListTake          | 批量订阅时条数           |
| QueueName         | 自定义队列名,仅rabbitmq  |
| ThreadSleepSecond | 轮循线程间隔时间          |
| MQType            | 指定MQ类型，如果配置了多种    |
| RetryTimes        | 消费重试次数            |
| DelayQueue        | 是否为延迟队列，仅rabbitmq |
### 批量处理
事件发布时，可单个发布(Publish)或批量发布(BatchPublish)
消费时同样可以单个消费和批量消费,如批量发布=>批量消费 批量发布=>单个消费
对于数据库队列这种方式比较实用如：
 - 为了增加写入效率，使用批量发布
 - 为了增加处理效率，使用批量消费处理
 
### 异常补尝
当消息订阅处理时发生异常，系统将会按以下处理
 1. MQ类型队列 将消息发送到延迟队列，重新进行消费
 2. DB类型队列 原始队列数据保留，更新处理次数，在下个周期再次消费
 3. 通过重写订阅延迟时间方法，控制重发间隔

### .net core 注入
```c#
services.AddEventBus(config =>
{
    config.UseMemory();
    config.RegisterSubscribe(Assembly.GetAssembly(typeof(Program)));
}
);
app.RunEventBusSubscribe();
//调用
var client = provider.GetService<IPublisher>();
var subService = provider.GetService<SubscribeService>();
```
