### ORM case 查询

1. 安装nuget包：CRL
2. using CRL;

case 语法分两种情况

按字段值

```sql
 case field when 1 then '结果1' else '结果2' end
```

按自定义条件

```sql
case when table.field=1 then '结果1' else '结果2' end
```

此结构用代码定义为

```c#
caseQuery.When(x => x.Remark == "1").Then(x => "11").Else(x => "22").End();//按自定义条件
caseQuery.Case(x => x.Remark).When("1").Then(x => "11").Else(x => "22").End();//按字段值
```

使用ILambdaQuery实现查询

```c#
var query = Code.OrderRepository.Instance.GetLambdaQuery();
            var caseQuery = query.CreateCase<string>();
            //caseQuery.When(x => x.Remark == "1").Then(x => "11").Else(x => "22").End();//按自定义条件
            caseQuery.Case(x => x.Remark).When("1").Then(x => "11").Else(x => "22").End();//按字段值
            query.Where(b => caseQuery.GetCaseResult() == "22");
            query.Select(b => new { ss = caseQuery.GetCaseResult() });
            query.PrintQuery();
```

输出SQL为

```sql
SELECT CASE t1.[Remark]
         WHEN '1' THEN '11'
         ELSE '22'
       END AS ss
FROM   [OrderProduct] t1 WITH (nolock)
WHERE  ( CASE t1.[Remark]
           WHEN '1' THEN '11'
           ELSE '22'
         END = '22' ) 
```

ILambdaQuery能实现子查询和嵌套查询，只要符合T-SQL语义逻辑，可以使用ILambdaQueryResultSelect无限叠加

如：

- join后group
- join后再join
- group后再join
- join一个group结果
- join一个union结果
- 对union进行group再join
- ...

源码示例参考

[Data/QueryTest/test · hubroxxl/CRL - 码云 - 开源中国 (gitee.com)](https://gitee.com/hubroxxl/crl/tree/master/Data/QueryTest/test)

