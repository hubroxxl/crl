### 对EF Core进行扩展使支持批量操作/复杂查询

EF Core的问题一言难尽，然后有了各种插件，批量插入扩展，批量更新扩展，查询扩展。。。然后一个项目引入一堆扩展

解决此问题

1. 安装nuget包：CRL.EFCore.Extensions
2. using CRL;
   using CRL.EFCore.Extensions;

### 实现数据批量操作

配置实体映射,调用ConfigEntityTypeBuilder扩展方法

```c#
protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var e = modelBuilder.Entity<TestClass>();
            e.ToTable("TestClass", "dbo");
            e.HasKey(b => b.Id);
            e.Property(b => b.Name).HasMaxLength(50) ;
            e.ConfigEntityTypeBuilder();
            base.OnModelCreating(modelBuilder);
        }
```

获取DbContext的IDbConnection dbConnection

通过扩展方法**GetDBExtend**获取IAbsDBExtend对象

```c#
    public IAbsDBExtend GetDBExtend()
    {
        return dbConnection.GetDBExtend(dbTrans);
    }
```


使用IAbsDBExtend实现批量操作

```
var db = context.GetDBExtend();
db.BatchInsert(new List<TestClass>() { new TestClass() { Id = DateTime.Now.Millisecond, Name = "ddddd" } },true);
db.Update(b => b.Id == 1, new { Number = 2 });
```

**IAbsDBExtend**可以实现所有数据操作，如：

1. 批量更新
2. 批量删除
3. 批量插入
4. 存储过程
5. 表字段、索引检查

### 实现复杂查询

通过扩展方法**GetLambdaQuery<T>**获取ILambdaQuery对象

```c#
public ILambdaQuery<T> GetLambdaQuery<T>() where T : class
        {
            return dbConnection.GetLambdaQuery<T>(dbTrans);
        }
```

使用ILambdaQuery实现查询

```c#
var query = context.GetLambdaQuery<TestClass>();
                query.Join<TestClass2>((a, b) => a.Id == b.Id);
                query.PrintQuery();
```

ILambdaQuery能实现子查询和嵌套查询，只要符合T-SQL语义逻辑，可以使用ILambdaQueryResultSelect无限叠加

如：

- join后group
- join后再join
- group后再join
- join一个group结果
- join一个union结果
- 对union进行group再join
- ...

**示例查询:**

```c#
var q1 = GetLambdaQuery();
            var q2 = q1.CreateQuery<Code.ProductData>();
            q2.Where(b => b.Id > 0);
            var view = q2.CreateQuery<Code.Member>().Where(b => b.Id > 0).GroupBy(b => b.Name).Select(b => new { b.Name, aa = b.Id.COUNT() });//GROUP查询
            var view2 = q2.Join(view, (a, b) => a.CategoryName == b.Name).Select((a, b) => new { ss1 = a.UserId, ss2 = b.aa });//关联GROUP
            q1.Join(view2, (a, b) => a.Id == b.ss1).Select((a, b) => new { a.Id, b.ss1 }).ToList();//再关联
            //var result = view2.ToList();
            var sql = q1.PrintQuery();
```

源码示例参考[](tree/master/Data/EFTest)

高级的查询方法见源码示例

[](tree/master/Data/QueryTest/test)
