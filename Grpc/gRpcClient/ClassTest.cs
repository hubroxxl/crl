﻿using CRL.Class2Proto;
using System;
using System.Collections.Generic;
using System.Text;

namespace gRpcClient
{
    [ProtoServiceAttribute("protoTest", "ClassTestAction")]
    public interface ClassTestAction
    {
        ClassTest getObj(TestObj a);
        //public abstract ClassTest getObj2(TestObj a);
        Request getObj3(TestObj2<Request> a);
        TestObj2<List<Request>> getObj4(TestObj2<List<Request>> a);
    }
    //[ProtoServiceAttribute("protoTest", "ClassTestAction2")]
    //public abstract class ClassTestAction2
    //{
    //    public abstract ClassTest getObj(TestObj a);
    //    public abstract ClassTest getObj2(TestObj a);
    //}
    public class ClassTestBase
    {
        public string Id { get; set; }
    }
    //[ProtoClassConvert]
    public class ClassTest: ClassTestBase
    {
        public string Name { get; set; }
        public int? nullableValue { get; set; }
        public Status Status { get; set; }
        public TestObj Data { get; set; }
        public List<string> Name2 { get; set; }
        public List<TestObj> Data2 { get; set; }
        public Dictionary<string, TestObj> Data3 { get; set; }
        public DateTime time { get; set; }
        public decimal decimalValue { get; set; }

    }
    public enum Status
    {
        ok,fail
    }
    public class TestObj
    {
        public string Id { get; set; }
    }
    public class TestObj2<T>
    {
        public string Id { get; set; }

        public object obj { get; set; }
        public T data { get; set; }
    }
    public class Request
    {

    }
}
