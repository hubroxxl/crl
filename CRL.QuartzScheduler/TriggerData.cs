﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRL.QuartzScheduler
{
    public class TriggerData
    {
        /// <summary>
        /// 任务数据项
        /// </summary>
        public object TagData;
        internal Action<object> CustomFunc;

        //string JobName { get; }
        //string JobGroup { get; }
        /// <summary>
        /// Cron表达式,如果为空,则按重复间隔
        /// </summary>
        public string CronExpression;
        /// <summary>
        /// 重复间隔
        /// </summary>
        public TimeSpan RepeatInterval;
        /// <summary>
        /// 重复次数,-1为不限次数
        /// </summary>
        public int RepeatCount = -1;
    }
}
