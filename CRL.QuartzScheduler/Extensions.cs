﻿using System;
#if NETSTANDARD
using CRL.Core.Remoting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
#endif
namespace CRL.QuartzScheduler
{
#if NETSTANDARD
    public static class Extensions
    {
        public static void AddQuartzScheduler(this IServiceCollection services, params Assembly[] assemblies)
        {
            services.AddScoped<_CustomWork>();
            foreach (var assembyle in assemblies)
            {
                var types = assembyle.GetTypes();
                foreach (var type in types)
                {
                    if (type.IsSubclassOf(typeof(QuartzJob)) && !type.IsAbstract)
                    {
                        services.AddScoped(type);
                    }
                }
            }

        }
    }
#endif
}
