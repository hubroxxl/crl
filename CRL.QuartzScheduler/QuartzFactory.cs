﻿#if NETSTANDARD
using Microsoft.Extensions.DependencyInjection;
#endif
using Quartz;
using Quartz.Spi;
using System;
namespace CRL.QuartzScheduler
{
#if NETSTANDARD
    class QuartzFactory : IJobFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public QuartzFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            return (IJob)_serviceProvider.GetService(bundle.JobDetail.JobType);
        }

        public void ReturnJob(IJob job)
        {
            var disposable = job as IDisposable;
            disposable?.Dispose();
        }
    }
#endif
}
