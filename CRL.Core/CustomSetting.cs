﻿/**
* CRL
*/
using CRL.Core.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace CRL.Core
{
    /// <summary>
    /// 获取自定义配置值
    /// /CustomSetting.config
    /// 文本 key=value
    /// </summary>
    public class CustomSetting
    {
        static System.IO.FileSystemWatcher watch;
        static System.IO.FileSystemWatcher watch2;
        static Dictionary<string, string> keyCaches;
        static Dictionary<string, string> GetSettings()
        {
            string confgiFile = "Config/CustomSetting.config";
            //var cache = System.Web.HttpRuntime.Cache;
            //string configKey = "$CustomSetting";
            //var cacheObj = cache.Get(configKey);
            //Dictionary<string, string> keyCaches;
            //if (cacheObj != null)
            //{
            //    keyCaches = cacheObj as Dictionary<string, string>;
            //}
            if (keyCaches == null)
            {
                keyCaches = new Dictionary<string, string>();
                var path = RequestHelper.GetFilePath("Config");
                if (!System.IO.Directory.Exists(path))
                {
                    System.IO.Directory.CreateDirectory(path);
                }

                //string file = System.Web.Hosting.HostingEnvironment.MapPath(confgiFile);
                string file = RequestHelper.GetFilePath(confgiFile);
                if (!System.IO.File.Exists(file))
                {

                    System.IO.File.WriteAllText(file, "key=value");
                    throw new Exception("配置文件不存在:" + file);
                }
                if (watch == null)
                {
                    watch = new System.IO.FileSystemWatcher(path, "CustomSetting.config");
                    watch.NotifyFilter = System.IO.NotifyFilters.LastWrite;
                    watch.Changed += (s, e) =>
                    {
                        keyCaches = null;
                    };
                    watch.EnableRaisingEvents = true;
                }
                var arry = System.IO.File.ReadLines(file);
                foreach (string str in arry)
                {
                    if (str.StartsWith("//"))
                        continue;
                    int index = str.IndexOf("=");
                    if (index == -1)
                    {
                        index = str.IndexOf(";");
                        if (index == -1)
                        {
                            continue;
                        }
                    }
                    string name = str.Substring(0, index).Trim().ToUpper().Trim();
                    string value = str.Substring(index + 1).Trim();

                    //value = DesString(value);
                    keyCaches.Add(name, value);
                }
                //cache.Insert(configKey, keyCaches, new System.Web.Caching.CacheDependency(file), DateTime.Now.AddDays(1), System.Web.Caching.Cache.NoSlidingExpiration);
            }
            return keyCaches;
        }

        //const string confgiFile = "/Config/CustomSetting.config";

        ///// <summary>
        ///// 是否包含有键值
        ///// </summary>
        ///// <param name="key"></param>
        ///// <returns></returns>
        //public static bool ContainsKey(string key)
        //{

        //    return ContainsKey(key, out var value);
        //}
        public static bool ContainsKey(string key, out string value)
        {
            key = key.ToUpper().Trim();
            var keyCaches = GetSettings();
            return keyCaches.TryGetValue(key, out value);
        }
        public static T GetConfigKey<T>(string key)
        {
            string value = GetConfigKey(key);
            return (T)Convert.ChangeType(value, typeof(T));
        }
        /// <summary>
        /// 获取自定义配置值
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetConfigKey(string key)
        {
            key = key.ToUpper().Trim();
            var keyCaches = GetSettings();
            if (keyCaches.ContainsKey(key))
                return keyCaches[key];
            throw new Exception("CustomSetting 找不到匹配的KEY:" + key + "");
        }
        public static Dictionary<string,string> GetAll()
        {
            return keyCaches;
        }
        
    }
}
