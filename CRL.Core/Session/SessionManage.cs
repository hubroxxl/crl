﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CRL.Core.Session
{
    public class SessionManage
    {
        public static AbsSession GetSessionClient(IWebContext context)
        {
            var config = ConfigBuilder.GetConfig<Func<IWebContext, AbsSession>>("redisSessionManage");
            if (config == null)
            {
                return new WebSession(context);
            }
            return config.Invoke(context);
        }
    }
}
