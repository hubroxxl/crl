﻿/**
* CRL
*/
using CRL.Core.Log;
using CRL.Core.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CRL.Core
{
    public class ConfigBuilder
    {
        public ConfigBuilder()
        {
            //__Current = this;
        }
        static ConfigBuilder()
        {
            //__Current = new ConfigBuilder();
            configs = new Dictionary<string, object>();
        }
        //internal static ConfigBuilder __Current;
        static Dictionary<string, object> configs;
        public void AddConfig(string type, object objFunc)
        {
            configs.Remove(type);
            configs.Add(type, objFunc);
        }
        public static T GetConfig<T>(string type)
        {
            configs.TryGetValue(type, out var config);
            if (config == null)
            {
                return default(T);
            }
            return (T)config;
        }
        //public Func<IWebContext, AbsSession> __SessionCreater;
    }
}
