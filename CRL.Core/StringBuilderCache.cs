﻿#if NETSTANDARD
using Microsoft.Extensions.ObjectPool;
#endif
using System;
using System.Text;
namespace CRL.Core
{
    public static class StringBuilderCache
    {
#if NETSTANDARD
        static ObjectPool<StringBuilder> pool;
#endif
        static StringBuilderCache()
        {
#if NETSTANDARD
            var provider = new DefaultObjectPoolProvider();
            pool = provider.CreateStringBuilderPool(256, 8192);
#endif
        }
        public static StringBuilder Acquire()
        {
#if NETSTANDARD
            return pool.Get();
#endif
            return new StringBuilder();
        }

        public static string GetStringAndRelease(StringBuilder sb)
        {
            string result = sb.ToString();
#if NETSTANDARD
            //sb.Clear();
            pool.Return(sb);
#endif
            return result;
        }

        public static string GetSimpleString(Action<StringBuilder> func)
        {
            var sb = Acquire();
            func(sb);
            return GetStringAndRelease(sb);
        }
    }
}
