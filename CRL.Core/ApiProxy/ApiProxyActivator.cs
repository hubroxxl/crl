﻿using CRL.Core.Remoting;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;

namespace CRL.Core.ApiProxy
{

    public sealed class ApiProxyActivator<T>
    {
        private readonly ActionInvoker[] actionInvokers;
        private readonly Func<IApiInterceptor, ActionInvoker[], T> activator;

        public ApiProxyActivator(Func<MethodInfo, DefaultActionInvoker> inoker)
        {
            var httpApiType = typeof(T);
            var all = httpApiType.GetInterfaces().ToList();
            all.Add(httpApiType);

            var apiMethods = all.SelectMany(item => item.GetMethods())
                //.Select(item => item.EnsureApiMethod())
                .ToArray();

            this.actionInvokers = apiMethods
                .Select(b => inoker(b))
                .ToArray();

            var proxyType = BuildProxyType(apiMethods);
            var args = new Type[] { typeof(IApiInterceptor), typeof(ActionInvoker[]) };
            var ctor = proxyType.GetConstructor(args);
            var parameters = args.Select(t => Expression.Parameter(t)).ToArray();
            var body = Expression.New(ctor, parameters);
            this.activator = Expression.Lambda<Func<IApiInterceptor, ActionInvoker[], T>>(body, parameters).Compile();
        }
        public T CreateInstance(AbsClient client)
        {
            return this.activator.Invoke(new ApiInterceptor(client), this.actionInvokers);
        }

        private static readonly MethodInfo interceptMethod = typeof(IApiInterceptor).GetMethod(nameof(IApiInterceptor.Intercept)) ?? throw new MissingMethodException(nameof(IApiInterceptor.Intercept));

        private static readonly Type[] proxyTypeCtorArgTypes = new Type[] { typeof(IApiInterceptor), typeof(ActionInvoker[]) };


        private static Type BuildProxyType(MethodInfo[] apiMethods)
        {
            var interfaceType = typeof(T);
            if (interfaceType.IsVisible == false)
            {
                var message = "interfaceType is not visible";
                throw new NotSupportedException(message);
            }

            var moduleName = Guid.NewGuid().ToString();
            var assemblyName = new AssemblyName(Guid.NewGuid().ToString());

            AssemblyBuilder asb;
            ModuleBuilder module;
#if NET45_OR_GREATER
            //from https://blog.csdn.net/xiaouncle/article/details/52890037
            moduleName = interfaceType.Name + "_il";
            assemblyName = new AssemblyName(interfaceType.Name + "_il");
            var dllName = $"{interfaceType.Name}_il.dll";
            asb = AppDomain.CurrentDomain
                .DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.RunAndSave);
            module = asb
                .DefineDynamicModule(moduleName, dllName);
#else
            asb = AssemblyBuilder
                           .DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
            module = asb
 .DefineDynamicModule(moduleName);
#endif
            var typeName = interfaceType.FullName ?? Guid.NewGuid().ToString();
            var builder = module.DefineType(typeName, System.Reflection.TypeAttributes.Class);
            builder.AddInterfaceImplementation(interfaceType);

            var fieldApiInterceptor = BuildField(builder, "interceptor", typeof(IApiInterceptor));
            var fieldActionInvokers = BuildField(builder, "invokers", typeof(ActionInvoker[]));

            BuildCtor(builder, fieldApiInterceptor, fieldActionInvokers);
            BuildMethods(builder, apiMethods, fieldApiInterceptor, fieldActionInvokers);

            var proxyType = builder.CreateTypeInfo();
#if NET45_OR_GREATER
            if (System.Diagnostics.Debugger.IsAttached)
            {
                asb.Save(dllName);
            }
#endif
            return proxyType ?? throw new Exception("not build type");
        }

        private static FieldBuilder BuildField(TypeBuilder builder, string fieldName, Type fieldType)
        {
            const FieldAttributes filedAttribute = FieldAttributes.Private | FieldAttributes.InitOnly;
            return builder.DefineField(fieldName, fieldType, filedAttribute);
        }

        private static void BuildCtor(TypeBuilder builder, FieldBuilder fieldApiInterceptor, FieldBuilder fieldActionInvokers)
        {
            // .ctor(IHttpApiInterceptor apiInterceptor, ApiActionInvoker[] actionInvokers)
            var ctor = builder.DefineConstructor(MethodAttributes.Public, CallingConventions.Standard, proxyTypeCtorArgTypes);

            var il = ctor.GetILGenerator();

            // this.apiInterceptor = 第一个参数
            il.Emit(OpCodes.Ldarg_0);
            il.Emit(OpCodes.Ldarg_1);
            il.Emit(OpCodes.Stfld, fieldApiInterceptor);

            // this.actionInvokers = 第二个参数
            il.Emit(OpCodes.Ldarg_0);
            il.Emit(OpCodes.Ldarg_2);
            il.Emit(OpCodes.Stfld, fieldActionInvokers);

            il.Emit(OpCodes.Ret);
        }

        private static void BuildMethods(TypeBuilder builder, MethodInfo[] actionMethods, FieldBuilder fieldApiInterceptor, FieldBuilder fieldActionInvokers)
        {
            // private final hidebysig newslot virtual
            const MethodAttributes implementAttribute = MethodAttributes.Private | MethodAttributes.Final | MethodAttributes.HideBySig | MethodAttributes.NewSlot | MethodAttributes.Virtual;

            for (var i = 0; i < actionMethods.Length; i++)
            {
                var actionMethod = actionMethods[i];
                var actionParameters = actionMethod.GetParameters();
                var parameterTypes = actionParameters.Select(p => p.ParameterType).ToArray();
                var actionMethodName = $"{actionMethod.DeclaringType?.FullName}.{actionMethod.Name}";

                var methodBuilder = builder.DefineMethod(actionMethodName, implementAttribute, CallingConventions.Standard | CallingConventions.HasThis, actionMethod.ReturnType, parameterTypes);
                builder.DefineMethodOverride(methodBuilder, actionMethod);
                var iL = methodBuilder.GetILGenerator();

                // this.apiInterceptor
                iL.Emit(OpCodes.Ldarg_0);
                iL.Emit(OpCodes.Ldfld, fieldApiInterceptor);

                // this.actionInvokers[i]
                iL.Emit(OpCodes.Ldarg_0);
                iL.Emit(OpCodes.Ldfld, fieldActionInvokers);
                iL.Emit(OpCodes.Ldc_I4, i);
                iL.Emit(OpCodes.Ldelem_Ref);

                // var arguments = new object[parameters.Length]
                var arguments = iL.DeclareLocal(typeof(object[]));
                iL.Emit(OpCodes.Ldc_I4, actionParameters.Length);
                iL.Emit(OpCodes.Newarr, typeof(object));
                iL.Emit(OpCodes.Stloc, arguments);

                for (var j = 0; j < actionParameters.Length; j++)
                {
                    iL.Emit(OpCodes.Ldloc, arguments);
                    iL.Emit(OpCodes.Ldc_I4, j);
                    iL.Emit(OpCodes.Ldarg, j + 1);

                    var parameterType = parameterTypes[j];
                    if (parameterType.IsValueType || parameterType.IsGenericParameter)
                    {
                        iL.Emit(OpCodes.Box, parameterType);
                    }
                    iL.Emit(OpCodes.Stelem_Ref);
                }

                // 加载 arguments 参数
                iL.Emit(OpCodes.Ldloc, arguments);

                // Intercept(actionInvoker, arguments)
                iL.Emit(OpCodes.Callvirt, interceptMethod);

                if (actionMethod.ReturnType == typeof(void))
                {
                    iL.Emit(OpCodes.Pop);
                }

                iL.Emit(OpCodes.Castclass, actionMethod.ReturnType);
                iL.Emit(OpCodes.Ret);
            }
        }
    }
}