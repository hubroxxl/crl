﻿using CRL.Core.Remoting;
using System;
using System.Reflection;

namespace CRL.Core.ApiProxy
{
    public abstract class ActionInvoker
    {
        public abstract object Invoke(AbsClient client, object[] arguments);
    }
    public class DefaultActionInvoker : ActionInvoker
    {
        MethodInfo _method;
        public DefaultActionInvoker(MethodInfo method)
        {
            _method = method;
        }
        public override object Invoke(AbsClient client, object[] arguments)
        {
            var a = client.TryInvokeMember(_method.Name, arguments, out var result);
            return result;
        }
    }

    public interface IApiInterceptor
    {
        object Intercept(ActionInvoker actionInvoker, object[] arguments);
    }
    public class ApiInterceptor : IApiInterceptor
    {
        AbsClient _client;
        public ApiInterceptor(AbsClient client)
        {
            _client = client;
        }
        public object Intercept(ActionInvoker actionInvoker, object[] arguments)
        {
            //Console.WriteLine($"Interceptor {actionInvoker}");
            return actionInvoker.Invoke(_client, arguments);
        }
    }
}
