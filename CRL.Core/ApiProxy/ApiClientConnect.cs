﻿/**
* CRL
*/
using CRL.Core.Remoting;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace CRL.Core.ApiProxy
{
    public class ApiClientConnect : AbsClientConnect
    {
        string host;

        //internal Action<ImitateWebRequest, Dictionary<string, object>,string> OnBeforRequest;
        internal Action<string, string> OnAfterRequest;
        internal string Apiprefix = "api";
        internal Encoding Encoding = Encoding.UTF8;
        ///// <summary>
        ///// 发送前处理
        ///// </summary>
        //public ApiClientConnect UseBeforRequest(Action<ImitateWebRequest, Dictionary<string, object>,string> action)
        //{
        //    OnBeforRequest = action;
        //    return this;
        //}
        /// <summary>
        /// 发送后回调
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public ApiClientConnect UseAfterRequest(Action<string, string> action)
        {
            OnAfterRequest = action;
            return this;
        }
        public ApiClientConnect(string _host)
        {
            host = _host;
        }
        /// <summary>
        /// 设置编码
        /// </summary>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public ApiClientConnect SetEncoding(Encoding encoding)
        {
            Encoding = encoding;
            return this;
        }
        /// <summary>
        /// 直接使用网关时,传入服务调用前辍
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="gatewayPrefix"></param>
        /// <returns></returns>
        public T GetClient<T>(string gatewayPrefix, Dictionary<string, object> requestHeads, HttpClient httpClient = null) where T : class
        {
            var type = typeof(T);
            //var serviceName = type.Name;
            //var key = string.Format("{0}_{1}", host, serviceName);
            //var a = _services.TryGetValue(key, out object instance);
            //if (a)
            //{
            //    return instance as T;
            //}
            var info = serviceInfo.GetServiceInfo("api", type);
            var client = new ApiClient(this, httpClient)
            {
                HostAddress = new HostAddress() { address = host, serviceNamePrefix = gatewayPrefix },
                serviceInfo = info,
                requestHeads = requestHeads
            };
            //创建代理
            //var instance2 = client.ActLike<T>();
            ApiProxyActivator<T> at;
            var b = ilCache.TryGetValue(type, out var _at);
            if (!b)
            {
                at = new ApiProxyActivator<T>((m) => new DefaultActionInvoker(m));
                ilCache.TryAdd(type, at);
            }
            else
            {
                at = _at as ApiProxyActivator<T>;
            }
            var instance = at.CreateInstance(client);
            //_services[key] = instance;
            return instance as T;
        }
        System.Collections.Concurrent.ConcurrentDictionary<Type, object> ilCache = new System.Collections.Concurrent.ConcurrentDictionary<Type, object>();
        public override T GetClient<T>(Dictionary<string, object> requestHeads = null)
        {
            return GetClient<T>("", requestHeads);
        }
    }
}
