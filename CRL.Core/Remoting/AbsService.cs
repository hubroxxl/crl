﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace CRL.Core.Remoting
{
    public abstract class AbsService
    {
        object authData;

        internal void __SetAuthData(object user)
        {
            authData = user;
        }
        protected T GetAuthData<T>()
        {
            if (authData == null)
            {
                return default(T);
            }
            return (T)authData;
        }

        //HttpPostedFile postFile;
        //public void SetPostFile(HttpPostedFile file)
        //{
        //    postFile = file;
        //}
        ///// <summary>
        ///// 获取发送的文件
        ///// </summary>
        ///// <returns></returns>
        //protected HttpPostedFile GetPostFile()
        //{
        //    return postFile;
        //}
    }
}
