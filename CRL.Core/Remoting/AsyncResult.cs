﻿/**
* CRL
*/
using Newtonsoft.Json.Bson;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRL.Core.Remoting
{
    public abstract class AsyncResult
    {
        public abstract Task InvokeAsync();
        public Func<Task<object>> ResultCreater;
        public abstract object GetResult(Task task);
        public abstract Task<object> GetResult2(Task task);
    }
    public class AsyncResult<T> : AsyncResult
    {
        public async Task<T> InvokeAsync2()
        {
            var result = await ResultCreater();
            return (T)result;
        }

        public override Task InvokeAsync()
        {
            return InvokeAsync2();
        }
        public override object GetResult(Task task)
        {
            var task2 = task as Task<T>;
            task2.ConfigureAwait(false);
            return task2.Result;
        }
        public override async Task<object> GetResult2(Task task)
        {
            var task2 = task as Task<T>;
            return await task2;
        }
    }
}
