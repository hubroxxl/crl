﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace CRL.Core.Remoting.WorkConsole
{
    public interface IWorkManageClient
    {
        void InitWork(WorkDetail work);
        void UpdateTaskStatus(string workName, TaskStatus status, long elMs, string error);
        void UpdateWorkStatus(string workName, WorkStatus status);
        void RemoveWork(string workName);
        Action<string> OnPauseWork { get; set; } 
        Action<string> OnResumeWork { get; set; }
        void StartMonitor();
    }
    public interface IWorkManageConsole
    {
        List<WorkDetail> GetWorks();
        void SendPauseWork(string workName);
        void SendResumeWork(string workName);
        void RemoveWork(string workName);
    }
    public class WorkDetail
    {
        public string MachineName { get; set; }
        public string WorkName { get; set; }
        public string Repeat { get; set; }
        public string Args { get; set; }
        public long Times { get; set; }
        WorkStatus _WorkStatus;
        public WorkStatus WorkStatus
        {
            get
            {
                //var nextTime = getNextWorkTime(HeartTime);
                ////nextTime = getNextWorkTime(nextTime);
                //if (nextTime < DateTime.Now)
                //{
                //    return WorkStatus.OffLine;
                //}
                return _WorkStatus;
            }
            set
            {
                _WorkStatus = value;
            }
        }
        public TaskStatus TaskStatus { get; set; }
        public long LastElMs { get; set; }
        public string LastError { get; set; }
        public DateTime LastWorkTime { get; set; }
        public DateTime HeartTime { get; set; }
        public override string ToString()
        {
            return $"{WorkName} WorkStatus:{WorkStatus} TaskStatus:{TaskStatus}";
        }
        public string Remark { get; set; }
        //DateTime getNextWorkTime(DateTime time)
        //{
        //    if (Repeat.Contains(":"))
        //    {
        //        var ts = TimeSpan.Parse(Repeat);
        //        return time.AddSeconds(ts.TotalSeconds);
        //    }
        //    var cronExpression = new CronExpression(Repeat);
        //    return cronExpression.GetNextValidTimeAfter(time).Value.ToLocalTime().DateTime;
        //}
        //public DateTime NextWorkTime
        //{
        //    get
        //    {
        //        return getNextWorkTime(DateTime.Now);
        //    }
        //}
    }
    public class WorkCommand
    {
        public string WorkName;
        public Command Command;
        public DateTime Time;
    }
    public enum WorkStatus
    {
        Runing,
        Pause,
        OffLine
    }
    public enum TaskStatus
    {
        Wait,
        Runing,
    }
    public enum Command
    {
        Pause,
        Resume
    }
}
