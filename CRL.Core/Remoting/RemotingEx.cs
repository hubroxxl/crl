﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRL.Core.Remoting
{
    class RemotingEx : Exception
    {
        public RemotingEx(string msg, Exception innerException = null) : base(msg, innerException)
        {
        }
        public string Code { get; set; }
        public override string ToString()
        {
            return $"{Code} {base.Message}";
        }
    }
}
