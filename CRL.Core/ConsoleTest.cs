﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CRL.Core
{
    public class ConsoleTest
    {
        static List<string> charArry;
        static ConsoleTest()
        {
            var _charArry = "1234567890abcdefghijklmnopqrstuvwxyz";
            charArry = _charArry.Select(b => b.ToString()).ToList();
            for (var i = 10; i < 200; i++)
            {
                charArry.Add(i.ToString());
            }
        }
        public class methodDetail
        {
            public string text;
            public Func<methodDetail,object> func;
            public object data;
            public string typeName;
        }
        public static void DoCommand(Type currentType, List<methodDetail> customMethods = null)
        {
            DoCommand(new Type[] { currentType }, customMethods);
        }
        public static void DoCommand(Type[] currentTypes, List<methodDetail> customMethods = null, List<MethodInfo> testAllBefor = null)
        {
            //Console.WriteLine("======================= Methods =======================");
            var allMethods = new List<methodDetail>();
            foreach (var currentType in currentTypes)
            {
                var allMethods2 = currentType.GetMethods(BindingFlags.Static | BindingFlags.Public).Where(b => b.Name != "Main").Select(b => new methodDetail
                {
                    func = (d) => b.Invoke(null, null),
                    text = b.Name,
                    typeName = currentType.Name
                }).ToList();
                allMethods.AddRange(allMethods2);
            }
            
            if (customMethods != null)
            {
                allMethods.AddRange(customMethods);
            }
            var allMethods_ = allMethods.Where(x => x.typeName != "config");
            allMethods.Add(new methodDetail
            {
                text = "invokeAll",
                typeName = "invokeAll",
                func = (b) =>
            {
                if (testAllBefor != null)
                {
                    //循环前置方法
                    foreach (var m1 in testAllBefor)
                    {
                        Console.WriteLine($"{DateTime.Now}调用前置参数方法:{m1.Name}");
                        m1.Invoke(null, null);
                        foreach (var m in allMethods_)
                        {
                            if (testAllBefor.Exists(x => x.Name == m.text))
                            {
                                continue;
                            }
                            if (m.typeName == "config" || m.text == "invokeAll")
                            {
                                continue;
                            }
                            var a2 = invokeMethod(m);
                            if (!a2)
                            {
                                break;
                            }
                        }
                        System.Threading.Thread.Sleep(1000);
                    }
                }
                else
                {
                    foreach (var m in allMethods_)
                    {
                        if (m.typeName == "config" || m.text == "invokeAll")
                        {
                            continue;
                        }
                        var a2 = invokeMethod(m);
                        if (!a2)
                        {
                            break;
                        }
                    }
                }
                return null;
            }
            });
        label1:
            var dicMethod = new Dictionary<string, methodDetail>();
            var i = 0;
            var take = 2;
            var maxNameLength = allMethods.Max(b => b.text.Length);
            if (allMethods.Count > 20)
            {
                take = 2;
            }
            if (maxNameLength > 30)
            {
                take = 1;
            }
            var n = 0;
            var group = allMethods.GroupBy(b => b.typeName);
            var defaultColor = Console.ForegroundColor;
            foreach (var g in group)
            {
                Console.ForegroundColor = ConsoleColor.Magenta;
                Console.WriteLine($"---------------------[ {g.Key} ]---------------------");
                Console.ForegroundColor = defaultColor;
                var typeList = g.ToList();
                var i2 = 0;
                while (i2 < typeList.Count && i < charArry.Count)
                {
                    var takes = typeList.Skip(i2).Take(take);
                    var cmd = "";
                    foreach (var m in takes)
                    {
                        var char1 = charArry[n].ToString();
                        dicMethod.Add(char1, m);
                        var keyText = $"[{char1.ToUpper()}]";
                        cmd += $"{keyText.PadRight(4, ' ')} {m.text.PadRight(30, ' ')}";
                        n += 1;
                    }
                    i += take;
                    i2+= take;
                    Console.WriteLine(cmd);
                }
            }
            //Console.WriteLine("================ select a method to invoke ================");
            string key;
            Console.Write("invoke method: ");
            //if (allMethods.Count < 36)
            //{
            //    key = Console.ReadKey().KeyChar.ToString();
            //}
            //else
            //{
            //    key = Console.ReadLine();
            //}
            key = Console.ReadLine();
            Console.Write(" --->\r\n");
            if (key.Length > 2)//按分组或名称
            {
                var finds = allMethods.FindAll(b => b.typeName.ToLower() == key.ToLower());
                if(!finds.Any())
                {
                    finds = allMethods.FindAll(b => b.text.ToLower() == key.ToLower());
                }
                foreach(var method in finds)
                {
                    var a2 = invokeMethod(method);
                    if(!a2)
                    {
                        break;
                    }
                }
                Console.ReadKey();
            }
            else
            {
                var a = dicMethod.TryGetValue(key, out var method);
                if (a)
                {
                    invokeMethod(method);
                    Console.ReadKey();
                }
            }
            goto label1;
        }

        static bool invokeMethod(methodDetail method)
        {
            try
            {
                var result = method.func(method);
                if (result is Task)
                {
                    var task = (Task)result;
                    task.Wait();
                }
                Console.WriteLine($"{method.text} invoke completed");
                Console.WriteLine($"-----------------------------------------");
                //Console.ReadKey();
                return true;
            }
            catch (Exception ero)
            {
                Console.WriteLine($"{method.text} invoke Error");
                Console.WriteLine(ero.InnerException?.ToString());
                return false;
                //Console.ReadKey();
            }
        }
    }
}
