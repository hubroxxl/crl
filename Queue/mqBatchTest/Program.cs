﻿/**
* CRL
*/
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using CRL.Core;
using CRL.EventBus;
using CRL.EventBus.NetCore;
using CRL.EventBus.RocketMQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
namespace MqTest
{
    class Program
    {
        static IServiceProvider provider;
        static Program()
        {
            var builder = new ConfigurationBuilder();

            var configuration = builder.Build();

            var services = new ServiceCollection();
            services.AddOptions();
            services.AddEventBus(config =>
            {
                config.UseRocketMQ(b =>
                {
                    b.Server = "127.0.0.1:8080";
                    b.AccessKey = "rocketmq2";
                    b.SecretKey = "12345678";
                    b.Topic = "eventBusTopic";
                    b.GroupName = "group1";
                    b.InvisibleDuration = TimeSpan.FromSeconds(10);
                    //b.ConsumerByTag = true;
                });
                config.RegisterSubscribe(Assembly.GetAssembly(typeof(Program)));
            }
            );

            provider = services.BuildServiceProvider();
        }
        static SubscribeService subService;
        static void Main(string[] args)
        {
            var client = provider.GetService<IPublisher>();
            subService = provider.GetService<SubscribeService>();

            //自定义订阅
            subService.AddSubscribe<List<int>>(new SubscribeAttribute { Name = "testBatch", BatchSize = 10, RetryTimes = 3 }, b =>
            {
                Console.WriteLine($"{DateTime.Now} throw new Exception");
                throw new Exception("eee");
                Console.WriteLine($"testBatch recieve {b.Count()}");
                return true;
            });
            provider.RunEventBusSubscribe();
            ConsoleTest.DoCommand(typeof(Program));
        }
        public static void testSend()
        {
            var client = provider.GetService<IPublisher>();
            for (var i = 0; i < 1; i++)
            {
                client.Publish("testBatch", i);
            }
        }
    }
}
