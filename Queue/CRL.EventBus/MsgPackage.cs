﻿using CRL.Core.Extension;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRL.EventBus
{
    public interface IData
    {
        int RetryTimes { get; set; }
        string RoutingKey { get; set; }
        string MsgKey { get; set; }
        int DelayMs { get; set; }
        int Priority { get; set; }
        DateTime Time { get; set; }
    }
    public class MsgPackage: IData
    {
        public string Id { get; set; } = Guid.NewGuid().ToString("N");
        public string RoutingKey { get; set; }
        public string OriginRoutingKey { get; set; }
        public string _InnerData { get; set; }
        public string MsgKey { get; set; }
        public int RetryTimes { get; set; }
        public int DelayMs { get; set; }
        public int Priority { get; set; }
        public DateTime Time { get; set; } = DateTime.Now;
        public object GetData(Type type)
        {
            return _InnerData.ToString().ToObject(type);
        }
    }
}
