﻿/**
* CRL
*/
#if NETSTANDARD 
using Microsoft.Extensions.DependencyInjection;
//using Microsoft.AspNetCore.Builder;
#endif
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace CRL.EventBus.NetCore
{
#if NETSTANDARD 
    public static class EventBusExtensions
    {
        /// <summary>
        /// 添加EventBus
        /// </summary>
        /// <param name="services"></param>
        /// <param name="setupAction"></param>
        public static void AddEventBus(this IServiceCollection services, Action<QueueConfig> setupAction)
        {
            Action<QueueConfig> setupAction2 = b =>
            {
                b.fromNetCoreInjection = true;
            };
            setupAction += setupAction2;
            var _queueConfig = new QueueConfig();
            setupAction(_queueConfig);
            services.AddSingleton(typeof(QueueConfig), _queueConfig);

            services.AddSingleton<IPublisher, Publisher>();
            services.AddSingleton<PublisherFactory>();
            RegisterSubscribeService(services, _queueConfig);
        }

        /// <summary>
        /// 添加订阅
        /// </summary>
        static void RegisterSubscribeService(IServiceCollection services, QueueConfig _queueConfig)
        {
            services.AddSingleton<SubscribeService>();
            //services.AddHostedService<SubscribeBackgroundService>();
            foreach (var assembyle in _queueConfig.SubscribeAssemblies)
            {
                var types = assembyle.GetTypes();
                foreach (var type in types)
                {
                    SubscribeService.Register(_queueConfig, type, true);
                    var atr = type.GetCustomAttribute(typeof(SubscribeAttribute));
                    if (atr != null)
                    {
                        services.AddScoped(type);
                    }
                }
            }
            SubscribeService.SetTypeRegistered();
            //services.AddHostedService<SubscribeBackgroundService>();
        }
        /// <summary>
        /// 启动订阅
        /// </summary>
        /// <param name="provider"></param>
        public static void RunEventBusSubscribe(this IServiceProvider provider)
        {
            var subscribeService = provider.GetService<SubscribeService>();
            subscribeService.StartSubscribe();
        }
    }

    //class SubscribeBackgroundService : BackgroundService
    //{
    //    SubscribeService subscribeService;
    //    QueueConfig queueConfig;
    //    IServiceProvider serviceProvider;
    //    public SubscribeBackgroundService(Microsoft.Extensions.Options.IOptions<QueueConfig> options, SubscribeService _subscribeService, IServiceProvider _serviceProvider)
    //    {
    //        queueConfig = options.Value;
    //        subscribeService = _subscribeService;
    //        serviceProvider = _serviceProvider;
    //    }
    //    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    //    {
    //        subscribeService.Register(queueConfig.SubscribeAssemblies);
    //        stoppingToken.Register(() =>
    //        {
    //            subscribeService.StopSubscribe();
    //        });

    //        subscribeService.StartSubscribe();
    //        return Task.CompletedTask;
    //    }
    //}
#endif
}
