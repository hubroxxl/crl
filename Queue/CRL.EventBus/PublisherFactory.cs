﻿using System;
using System.Collections.Generic;
using System.Text;
#if NETSTANDARD 
using Microsoft.Extensions.DependencyInjection;
#endif
using CRL.EventBus.Queue;

namespace CRL.EventBus
{

    public class PublisherFactory
    {
        QueueConfig queueConfig;
        public PublisherFactory(QueueConfig _queueConfig)
        {
            queueConfig = _queueConfig;
        }
        static Dictionary<MQType, IPublisher> allPublisher = new Dictionary<MQType, IPublisher>();
        public IPublisher GetPublisher(MQType mQType)
        {
            return GetPublisherBase(mQType);
        }
        IPublisher GetPublisherBase(MQType mQType)
        {
            if (!queueConfig.settings.Exists(b => b.MQType == mQType))
            {
                throw new Exception($"未配置 {mQType}");
            }
            IPublisher publisher = null;
            //只能创建一次
            var a = allPublisher.TryGetValue(mQType, out publisher);
            if (a)
            {
                return publisher;
            }
            publisher = new Publisher(queueConfig, mQType);
            if (publisher == null)
            {
                throw new Exception($"未找到指定类型的配置 {mQType}");
            }
            allPublisher.Add(mQType, publisher);
            return publisher;
        }

    }

}
