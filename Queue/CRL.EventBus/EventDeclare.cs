﻿/**
* CRL
*/
using CRL.Core;
using CRL.EventBus.Queue;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace CRL.EventBus
{
    public class EventDeclare
    {
        public override string ToString()
        {
            return $"Name:{Name} IsArray:{IsArray} EventDataType:{EventDataType}";
        }
        //internal AbsQueue IQueue;
        public SubscribeAttribute SubscribeAttribute;
        public string Name;
        public Type EventDataType;
        //internal MethodInfo Method;
        internal Func<object, object[], object> MethodInvoke;
        internal bool IsAsync;
        public bool IsArray;
        internal Type ServiceInstanceType;
        //internal Func<Type, object> ServiceInstanceCtor = null;
        /// <summary>
        /// 委托实例化
        /// </summary>
        internal Func<object> ServiceInstanceCtor2 = null;
        //public int BatchSize;
        //internal Queue<object> CacheData = new Queue<object>();
        //DateTime CacheDataTime = DateTime.Now;
        //internal string QueueName;

        //public double ThreadSleepSecond = 1;
        //public int RetryTimes;
        //public int RetryInterval;
        //internal MQType MQType;
        internal bool FromManual;
        //public bool DelayQueue;
        internal object Clone()
        {
            return MemberwiseClone();
        }
        public object CreateServiceInstance()
        {
            //if (ServiceInstanceCtor != null)
            //{
            //    try
            //    {
            //        return ServiceInstanceCtor(ServiceInstanceType);
            //    }
            //    catch(Exception ero)
            //    {
            //        throw ero;
            //    }
            //}
            //else
            //{
            //    return ServiceInstanceCtor2?.Invoke();
            //}
            return ServiceInstanceCtor2?.Invoke();
        }
        internal string GetArrayName()
        {
            return $"{Name}_Array";
        }
    }
}
