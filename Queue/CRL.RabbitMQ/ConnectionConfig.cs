﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRL.RabbitMQ
{
    public class ConnectionConfig
    {
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string VirtualHost { get; set; } = "/";
        public int Timeout { get; set; }
        public string HostName { get; set; }
        public TimeSpan NetworkRecoveryInterval { get; set; } = TimeSpan.FromSeconds(5.0);
        /// <summary>
        /// rabbitmq在创建连接时就需要确定是使用同步还是异步
        /// </summary>
        public bool ConsumersAsync { get; set; }
    }
}
