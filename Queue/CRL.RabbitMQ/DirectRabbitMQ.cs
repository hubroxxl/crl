﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRL.Core.Extension;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace CRL.RabbitMQ
{
    /// <summary>
    /// 直连队列
    /// 直连交换机，根据Routing Key(路由键)进行投递到不同队列
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DirectRabbitMQ : AbsRabbitMQ
    {
        protected override string MqExchangeType => ExchangeType.Direct;

        public DirectRabbitMQ(ConnectionConfig config, string exchangeName) : base(config)
        {
            __exchangeName = exchangeName;
            Log($"{MqExchangeType}队列:初始化");
        }
        public void Publish<T>(string routingKey, params T[] msgs)
        {
            BasePublish(routingKey, msgs);
        }
        public void Publish<T>(string routingKey, Action<IBasicProperties> basicPropertiesFunc, params T[] msgs)
        {
            BasePublish(routingKey, basicPropertiesFunc, msgs);
        }
        public void BeginReceive<T>(string queueName, string routingKey, Action<T,string> onReceive, ConsumeOption option = null)
        {
            var channel = CreateConsumerChannel();
            //channel.QueueDeclare(queueName, true, false, false, null);
            QueueDeclare(channel, queueName, option);
            ExchangeDeclare(channel, option);
            QueueBind(channel, queueName, routingKey, option);
            Log($"开始消费,类型:{MqExchangeType} 队列:{queueName} Key:{routingKey}");
            base.BaseBeginConsumer(channel, queueName, onReceive, option);
        }
        //public void BeginBatchReceive<T>(string queueName, string routingKey, Action<List<T>, string> onReceive, ConsumeOption option = null)
        //{
        //    var channel = CreateConsumerChannel();
        //    //channel.QueueDeclare(queueName, true, false, false, null);
        //    QueueDeclare(channel, queueName, option);
        //    ExchangeDeclare(channel, option);
        //    QueueBind(channel, queueName, routingKey, option);
        //    channel.Dispose();
        //    Log($"开始批量消费,类型:{MqExchangeType} 队列:{queueName} Key:{routingKey}");
        //    base.BatchConsumer(queueName, onReceive, option);
        //}
        public void BeginReceiveString(string queueName, string routingKey, Action<string, string> onReceive, ConsumeOption option = null)
        {
            var channel = CreateConsumerChannel();
            //channel.QueueDeclare(queueName, true, false, false, null);
            QueueDeclare(channel, queueName, option);
            ExchangeDeclare(channel, option);
            QueueBind(channel, queueName, routingKey, option);
            Log($"开始消费,类型:{MqExchangeType} 队列:{queueName} Key:{routingKey}");
            base.BaseBeginConsumerString(channel, queueName, onReceive, option);
        }
        public void BeginReceiveAsync(string queueName, string routingKey, Func<string, string, Task> onReceive, ConsumeOption option = null)
        {
            var channel = CreateConsumerChannel();

            //channel.QueueDeclare(queueName, true, false, false, null);
            QueueDeclare(channel, queueName, option);
            ExchangeDeclare(channel, option);
            QueueBind(channel, queueName, routingKey, option);
            Log($"开始消费,类型:{MqExchangeType} 队列:{queueName} Key:{routingKey}");
            base.BaseBeginConsumerAsync(channel, queueName, onReceive, option);
        }
    }
}
