﻿/**
* CRL
*/
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CRL.RabbitMQ
{
    public class SimpleRabbitMQ : AbsRabbitMQ
    {
        protected override string MqExchangeType => ExchangeType.Direct;
        public SimpleRabbitMQ(ConnectionConfig config) : base(config)
        {
            Log($"SimpleDirect队列:初始化");
        }
        public void Publish<T>(string queueName, params T[] msgs)
        {
            BasePublish(queueName, msgs);
        }

        public void BeginReceive<T>(string queueName, Action<T, string> onReceive, ConsumeOption option = null)
        {
            var channel = CreateConsumerChannel();
            //channel.QueueDeclare(queueName, true, false, false, null);
            QueueDeclare(channel, queueName, option);
            Log($"开始消费,类型:SimpleDirect 队列:{queueName}");
            base.BaseBeginConsumer(channel, queueName, onReceive, option);
        }

        public void BeginReceiveString(string queueName, Action<string, string> onReceive, ConsumeOption option = null)
        {
            var channel = CreateConsumerChannel();

            //channel.QueueDeclare(queueName, true, false, false, null);
            QueueDeclare(channel, queueName, option);
            Log($"开始消费,类型:SimpleDirect 队列:{queueName}");
            base.BaseBeginConsumerString(channel, queueName, onReceive, option);
        }
        public void BeginReceiveAsync(string queueName, Func<string, string, Task> onReceive, ConsumeOption option = null)
        {
            var channel = CreateConsumerChannel();

            //channel.QueueDeclare(queueName, true, false, false, null);
            QueueDeclare(channel, queueName, option);
            Log($"开始消费,类型:SimpleDirect 队列:{queueName}");
            base.BaseBeginConsumerAsync(channel, queueName, onReceive, option);
        }
    }
}
