﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRL.RabbitMQ
{
    public class ConsumeOption
    {
        public Action<IModel> ConsumerChannelFunc { get; set; }
        public int ConsumerRetryTimes { get; set; }
        internal string RoutingKey { get; set; }
        internal bool IsAsync { get; set; }
        public bool LazyConsume { get; set; }
        //public int BatchTask { get; set; } = 10;
        /// <summary>
        /// 持久化
        /// </summary>
        public bool QueueDurable { get; set; }
        public bool ExchangeDurable { get; set; }
        /// <summary>
        /// 对首次声明它的连接可见，并且在连接断开时自动删除
        /// </summary>
        public bool QueueExclusive { get; set; }
        /// <summary>
        /// 自动删除
        /// </summary>
        public bool AutoDelete { get; set; }
        public IDictionary<string, object> QueueDeclareArgs { get; set; }
        public IDictionary<string, object> ExchangeDeclareArgs { get; set; }
    }
}
