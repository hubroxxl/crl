﻿/**
* CRL
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRL.Core.Extension;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Framing.Impl;

namespace CRL.RabbitMQ
{
    /// <summary>
    /// 头交换机，不处理路由键。而是根据发送的消息内容中的headers属性进行匹配
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class HeaderRabbitMQ : AbsRabbitMQ
    {
        //匹配规则x-match有下列两种类型：
        //x-match = all ：表示所有的键值对都匹配才能接受到消息
        //x-match = any ：表示只要有键值对匹配就能接受到消息
        protected override string MqExchangeType => ExchangeType.Headers;
        public HeaderRabbitMQ(ConnectionConfig config, string exchangeName) : base(config)
        {
            __exchangeName = exchangeName;
            Log($"{MqExchangeType}队列:初始化");
        }
        public void Publish<T>(IDictionary<string, object> headers, params T[] msgs)
        {
            var routingKey = "";
            Action<IBasicProperties> basicPropertiesFunc = p =>
            {
                p.Headers = headers;
            };
            BasePublish(routingKey, basicPropertiesFunc, msgs);
        }
        //void Publish<T>(string routingKey, Action<IBasicProperties> basicPropertiesFunc, params T[] msgs)
        //{
        //    BasePublish(ExchangeType.Direct, routingKey, basicPropertiesFunc, msgs);
        //}

        public void BeginReceive<T>(string queueName, IDictionary<string, object> headers, Action<T,string> onReceive, ConsumeOption option = null)
        {
            var routingKey = "";
            var channel = CreateConsumerChannel();
            //channel.QueueDeclare(queueName, true, false, false, null);
            QueueDeclare(channel, queueName, option);
            ExchangeDeclare(channel, option);
            //channel.QueueBind(queueName, __exchangeName, routingKey, headers);
            QueueBind(channel,queueName,routingKey, option, headers);
            Log($"开始消费,类型:{MqExchangeType} 队列:{queueName} Key:{routingKey}");
            base.BaseBeginConsumer(channel, queueName, onReceive, option);
        }

        public void BeginReceiveString(string queueName, IDictionary<string, object> headers, Action<string, string> onReceive, ConsumeOption option = null)
        {
            var routingKey = "";
            var channel = CreateConsumerChannel();
            //channel.QueueDeclare(queueName, true, false, false, null);
            QueueDeclare(channel, queueName, option);
            ExchangeDeclare(channel, option);
            QueueBind(channel, queueName, routingKey, option, headers);
            Log($"开始消费,类型:{MqExchangeType} 队列:{queueName} Key:{routingKey}");
            base.BaseBeginConsumerString(channel, queueName, onReceive, option);
        }
        public void BeginReceiveAsync(string queueName, IDictionary<string, object> headers, Func<string, string, Task> onReceive, ConsumeOption option = null)
        {
            var routingKey = "";
            var channel = CreateConsumerChannel();

            //channel.QueueDeclare(queueName, true, false, false, null);
            QueueDeclare(channel, queueName, option);
            ExchangeDeclare(channel, option);
            QueueBind(channel, queueName, routingKey, option, headers);
            Log($"开始消费,类型:{MqExchangeType} 队列:{queueName} Key:{routingKey}");
            base.BaseBeginConsumerAsync(channel, queueName, onReceive, option);
        }
    }
}
