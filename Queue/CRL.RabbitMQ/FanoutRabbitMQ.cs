﻿/**
* CRL
*/
using CRL.Core.Extension;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRL.RabbitMQ
{
    /// <summary>
    /// 广播队列
    /// 扇形交换机，采用广播模式，根据绑定的交换机，路由到与之对应的所有队列
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class FanoutRabbitMQ: AbsRabbitMQ
    {
        protected override string MqExchangeType => ExchangeType.Fanout;
        public FanoutRabbitMQ(ConnectionConfig config, string exchangeName) : base(config)
        {
            __exchangeName = exchangeName;
            Log($"{MqExchangeType}队列:初始化");
        }

        public void Publish<T>(params T[] msgs)
        {
            BasePublish("", msgs);
        }
        public void Publish<T>(string routingKey, Action<IBasicProperties> basicPropertiesFunc, params T[] msgs)
        {
            BasePublish("", basicPropertiesFunc, msgs);
        }
        public void BeginReceive<T>(Action<T, string> onReceive, ConsumeOption option = null)
        {
            var channel = CreateConsumerChannel();
    
            var queueName = channel.QueueDeclare().QueueName;
            ExchangeDeclare(channel, option);
            //绑定队列到指定fanout类型exchange，无需指定路由键
            QueueBind(channel, queueName, "", option);
            Log($"开始消费,类型: {MqExchangeType} 队列:{queueName} Key:");
            base.BaseBeginConsumer(channel, queueName, onReceive, option);

        }
    }
}
