﻿/**
* CRL
*/
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRL.Core.Extension;
using RabbitMQ.Client.Events;

namespace CRL.RabbitMQ
{
    /// <summary>
    /// 主题队列
    /// 主题交换机，对路由键进行模式匹配后进行投递，符号#表示一个或多个词，*表示一个词
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class TopicRabbitMQ : AbsRabbitMQ
    {
        protected override string MqExchangeType => ExchangeType.Topic;
        public TopicRabbitMQ(ConnectionConfig config, string exchangeName) : base(config)
        {
            __exchangeName = exchangeName;
            Log($"{MqExchangeType}队列: 初始化");
        }

        public void Publish<T>(string routingKey, params T[] msgs)
        {
            BasePublish(routingKey, msgs);
        }
        public void Publish<T>(string routingKey, Action<IBasicProperties> basicPropertiesFunc, params T[] msgs)
        {
            BasePublish(routingKey, basicPropertiesFunc, msgs);
        }
        public void BeginReceive<T>(string routingKey, Action<T, string> onReceive, ConsumeOption option = null)
        {
            var channel = CreateConsumerChannel();

            var queueName = channel.QueueDeclare().QueueName;
            ExchangeDeclare(channel, option);
            //绑定队列到topic类型exchange，需指定路由键routingKey
            //channel.QueueBind(queueName, __exchangeName, routingKey);
            QueueBind(channel, queueName, routingKey, option);
            Log($"开始消费,类型:{MqExchangeType} 队列:{queueName} Key:{routingKey}");
            base.BaseBeginConsumer(channel, queueName, onReceive, option);
        }
    }
}
