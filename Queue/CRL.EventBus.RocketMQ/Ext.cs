﻿using System;
using System.Collections.Generic;
using System.Text;
using CRL.EventBus;
using StackExchange.Redis;
namespace CRL.EventBus.RocketMQ
{
    public static class Ext
    {
        public static void UseRocketMQ(this QueueConfig queueConfig, Action<RocketMQConfig> configFunc)
        {
            var config = new RocketMQConfig()
            {
                MQType = MQType.RocketMQ,
                InstanceType = (config2, args) =>
                {
                    return new RocketMQ(config2 as RocketMQConfig);
                }
            };
            configFunc(config);
            queueConfig.AddSetting(config);
        }
    }
}
