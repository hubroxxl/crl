﻿/**
* CRL
*/
using CRL.EventBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MqTest
{
    [Subscribe]
    public class SubscribeTest
    {

        [Subscribe("test")]
        public void test(int a)
        {
            Console.WriteLine($"test receive {a}");
        }

        [Subscribe("testAsync")]
        public async Task testAsync(DateTime time)
        {
            Console.WriteLine($"testAsync receive {time}");
            await Task.Delay(100);
        }
        [Subscribe(Name = "testAsyncThrowException", RetryTimes = 2)]
        public async Task testAsyncThrowException(DateTime time)
        {
            Console.WriteLine($"testAsyncThrowException {time}");
            await Task.Delay(100);
            throw new Exception("testAsyncThrowException Exception");
        }

        [Subscribe("testSendReq3", RetryTimes = 2)]
        public async Task<string> testSendReq3(string name)
        {
            Console.WriteLine($"testSendReq3ThrowException {name}");
            await Task.Delay(10);
            throw new Exception("testSendReq3ThrowException Exception");
            return $"reqResponse3 {name}";
        }

    }
}
