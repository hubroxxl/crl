﻿using Org.Apache.Rocketmq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Unicode;

namespace MqTest
{
    internal class ApacheRocketmq
    {
        static ApacheRocketmq()
        {
            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
        }
        public static async void publish()
        {
            var credentialsProvider = new StaticSessionCredentialsProvider("rocketmq2", "12345678");
            string endpoints = "127.0.0.1:8080";
            var clientConfig = new ClientConfig.Builder()
                .SetEndpoints(endpoints)
                .SetCredentialsProvider(credentialsProvider)
                .Build();
            var topic = "eventBusTopic";
            var builder2 = new Producer.Builder();
            builder2.SetTopics(topic);
            builder2.SetClientConfig(clientConfig);

            Producer producer;
            try
            {
                producer = await builder2.Build();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        
            for(var i=0;i<10;i++)
            {
                var bytes = System.Text.Encoding.UTF8.GetBytes("hellow"+i);
                var builder = new Message.Builder()
                        .SetTopic(topic)
                        .SetBody(bytes)
                        .SetTag("tag")
                        // You could set multiple keys for the single message actually.
                        .SetKeys(Guid.NewGuid().ToString());
                //if (opt.DelayMs > 0)
                //{
                //    builder.SetDeliveryTimestamp(DateTime.Now + TimeSpan.FromMilliseconds(opt.DelayMs));
                //}
                var message = builder.Build();
                var sendReceipt = await producer.Send(message);
            }
          
        }

        public static async void receive()
        {
            string consumerGroup = "yourConsumerGroup";
            var topic = "eventBusTopic";
            var subscription = new Dictionary<string, FilterExpression>
                            { { topic, new FilterExpression("*") } };
            // In most case, you don't need to create too many consumers, single pattern is recommended.
            var credentialsProvider = new StaticSessionCredentialsProvider("rocketmq2", "12345678");
            string endpoints = "127.0.0.1:8080";
            var clientConfig = new ClientConfig.Builder()
                .SetEndpoints(endpoints)
                .SetCredentialsProvider(credentialsProvider)
                .Build();
            var builder = new SimpleConsumer.Builder()
                .SetClientConfig(clientConfig)
                .SetConsumerGroup(consumerGroup)
                .SetAwaitDuration(TimeSpan.FromMinutes(15))
                .SetSubscriptionExpression(subscription);
            //if (eventDeclare.ThreadSleepSecond > 0)
            //{
            //    builder.SetAwaitDuration(TimeSpan.FromSeconds(eventDeclare.ThreadSleepSecond));
            //}
            SimpleConsumer simpleConsumer;
            try
            {
                simpleConsumer = await builder.Build();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //var ed = eventDeclare;
            //var instance = ed.CreateServiceInstance();
            //var innerType = ed.EventDataType.GenericTypeArguments[0];
            while (true)
            {
                var messageViews = await simpleConsumer.Receive(10, TimeSpan.FromSeconds(15));
                foreach(var m in messageViews)
                {
                    Console.WriteLine(Encoding.UTF8.GetString(m.Body));
                    await simpleConsumer.Ack(m);
                }
            }
        }
    }
}
