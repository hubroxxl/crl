﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using CRL.Core;
using CRL.Core.Extension;
using CRL.Data.DBAccess;
using CRL.EventBus;
using CRL.EventBus.NetCore;
using CRL.EventBus.Queue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MqTest
{
    /// <summary>
    /// 使用实例化调用
    /// </summary>
    class Program2
    {
        static QueueConfig config;
        static IPublisher client;
        static void Main1(string[] args)
        {
            config = new QueueConfig();
            config.UseRabbitMQ(cfg =>
            {
                cfg.HostName = "127.0.0.1";
                cfg.UserName = "guest";
                cfg.Password = "guest";
            }, opt =>
            {
                opt.ConsumerChannelFunc = channel => channel.BasicQos(0, 1, false);
            });
            //可以使用多种配置，订阅时需要指定
            //config.UseMongoDb("mongodb://127.0.0.1:27017/admin");
            //config.UseRedis("Server_204@127.0.0.1:6389");
            //config.UseDb(DBType.MSSQL, "server=.;database=testDb; uid=sa;pwd=123;");
            //config.UseMemory();
            config.RegisterSubscribe(Assembly.GetAssembly(typeof(Program2)));
            var subService = new SubscribeService(config);

            //默认发布器
            client = new Publisher(config);
 
            subService.StartSubscribe();
      
            ConsoleTest.DoCommand(typeof(Program2));
        }

        public static void testPublish()
        {
            client.Publish("timeTest", DateTime.Now);
            client.Publish("delayQueue", DateTime.Now.ToString(), opt =>
            {
                opt.DelayMs = 1000;
            });//延迟队列发送

            var factory = new PublisherFactory(config);//按类型获取发布器
            //var client2 = factory.GetPublisher(MQType.Db);
            //client2.Publish("manual", DateTime.Now);
        }

    }
    class reSend
    {
        public object data;
        public string name;
    }
}
