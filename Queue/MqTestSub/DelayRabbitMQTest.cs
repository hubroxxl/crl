﻿/**
* CRL
*/
using CRL.RabbitMQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MqTest
{
    class DelayRabbitMQTest
    {
        static string queueName = "testqueueDelay";
        static string routingKey = "delayTest";
        static string exchangeName = "delayExchangeName2";
        static DelayRabbitMQ client;
        static DelayRabbitMQTest()
        {
            var config = new ConnectionConfig { HostName = "127.0.0.1", UserName = "guest", Password = "guest", Port = 5672 };
            client = new DelayRabbitMQ(config, exchangeName);
        }
        public static void send()
        {
            client.Publish(routingKey, 3000, DateTime.Now.ToString());
        }
        public static void StartReceive()
        {
            //client.Publish(DateTime.Now.ToString());
            client.BeginReceive<string>(queueName, routingKey, (msg, key) =>
            {
                Console.WriteLine($"{DateTime.Now} receive:{msg}");
            });
        }
    }
}
