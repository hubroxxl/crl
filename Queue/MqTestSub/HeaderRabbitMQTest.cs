﻿/**
* CRL
*/
using CRL.RabbitMQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MqTest
{
    class HeaderRabbitMQTest
    {
        static string queueName = "queueHeader";
        static string routingKey = "";
        static string exchangeName = "headExchange";
        static HeaderRabbitMQ client;
        static HeaderRabbitMQTest()
        {
            var config = new ConnectionConfig { HostName = "127.0.0.1", UserName = "guest", Password = "guest", Port = 5672 };
            client = new HeaderRabbitMQ(config,exchangeName);
            //client.ConsumerRetryTimes = 2;
        }
        public static void send()
        {
            var headers = new Dictionary<string, object>();
            headers.Add("user",1);
            client.Publish(headers, "user1");

            var headers2 = new Dictionary<string, object>();
            headers2.Add("user", 2);
            client.Publish(headers, "user2");

        }
        public static void StartReceive()
        {
            var headers = new Dictionary<string, object>();
            headers.Add("user", 1);
            var consumeOption = new ConsumeOption { ConsumerRetryTimes =2 };
            //client.Publish(DateTime.Now.ToString());
            client.BeginReceive<string>(queueName, headers, (msg, key) =>
            {
                //throw new Exception("throw");
                Console.WriteLine($"header1 receive:{msg}");
            }, consumeOption);

            var headers2 = new Dictionary<string, object>();
            headers2.Add("user", 2);
            //client.Publish(DateTime.Now.ToString());
            client.BeginReceive<string>(queueName, headers2, (msg, key) =>
            {
                //throw new Exception("throw");
                Console.WriteLine($"header2 receive:{msg}");
            }, consumeOption);
        }
    }
}
