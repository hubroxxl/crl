﻿/**
* CRL
*/
using CRL.RabbitMQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MqTest
{
    class SimpleRabbitMQTest
    {
        static string queueName = "queueSimple";
        //static string routingKey = "simpleTest";
        static string exchangeName = "exchangeSimple";
        static SimpleRabbitMQ client;
        static SimpleRabbitMQTest()
        {
            var config = new ConnectionConfig { HostName = "127.0.0.1", UserName = "guest", Password = "guest", Port = 5672 };
            client = new SimpleRabbitMQ(config);
            //client.ConsumerRetryTimes = 2;
        }
        public static void send()
        {
            client.Publish(queueName, DateTime.Now.ToString());
        }
        public static void StartReceive()
        {
            var consumeOption = new ConsumeOption { ConsumerRetryTimes =2 };
            //client.Publish(DateTime.Now.ToString());
            client.BeginReceive<string>(queueName, (msg, key) =>
            {
                //throw new Exception("throw");
                Console.WriteLine($"{DateTime.Now} receive:{msg}");
            }, consumeOption);
        }
    }
}
