﻿/**
* CRL
*/
using CRL.RabbitMQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MqTest
{
    class DirectRabbitMQTest
    {
        static string queueName = "testqueue2";
        static string routingKey = "queueTest2";
        static string exchangeName = "queueExchangeName21";
        static DirectRabbitMQ client;
        static DirectRabbitMQTest()
        {
            var config = new ConnectionConfig { HostName = "127.0.0.1", UserName = "guest", Password = "guest", Port = 5672, ConsumersAsync = false };
            client = new DirectRabbitMQ(config, exchangeName);
            //client.ConsumerRetryTimes = 2;
        }
        public static void send()
        {
            client.Publish(routingKey, DateTime.Now.ToString());
        }
        public static void StartReceive()
        {
            var consumeOption = new ConsumeOption { ConsumerRetryTimes =2 };
            //client.Publish(DateTime.Now.ToString());

            //client.BeginReceiveAsync(queueName, routingKey, (msg, key) =>
            //{
            //    //throw new Exception("throw");
            //    Console.WriteLine($"{DateTime.Now} receive:{msg}");
            //    return Task.CompletedTask;
            //}, consumeOption);
            client.BeginReceive<string>(queueName, routingKey, (msg, key) =>
            {
                //throw new Exception("throw");
                Console.WriteLine($"{DateTime.Now} receive:{msg}");
                //return Task.CompletedTask;
            }, consumeOption);

            //client.BeginBatchReceive<string>(queueName, routingKey, (msg, key) =>
            //{
            //    //throw new Exception("throw");
            //    Console.WriteLine($"{DateTime.Now} receive:{msg}");
            //}, consumeOption);
        }
    }
}
