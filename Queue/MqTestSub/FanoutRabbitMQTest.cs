﻿/**
* CRL
*/
using CRL.RabbitMQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MqTest
{
    class FanoutRabbitMQTest
    {
        static string queueName = "queueFanout";
        static string routingKey = "fanoutTest";
        static string exchangeName = "exchangeFanout";
        static FanoutRabbitMQ client;
        static FanoutRabbitMQTest()
        {
            var config = new ConnectionConfig { HostName = "127.0.0.1", UserName = "guest", Password = "guest", Port = 5672 };
            client = new FanoutRabbitMQ(config,exchangeName);
            //client.ConsumerRetryTimes = 2;
        }
        public static void send()
        {
            client.Publish(DateTime.Now.ToString());
        }
        public static void StartReceive()
        {
            var consumeOption = new ConsumeOption { ConsumerRetryTimes =2 };
            //client.Publish(DateTime.Now.ToString());
            client.BeginReceive<string>((msg, key) =>
            {
                //throw new Exception("throw");
                Console.WriteLine($"{DateTime.Now} receive:{msg}");
            }, consumeOption);
        }
    }
}
