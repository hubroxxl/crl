﻿using CRL.Transaction;
using System;
using System.Collections.Generic;
using System.Text;

namespace transTest
{
    public class SagaStep1 : AbsSagaStep<string>
    {
        public SagaStep1()
        {

        }
        public override string Name => GetType().Name;
#if NETSTANDARD
        IServiceProvider provider;
        public Step1(IServiceProvider _provider)
        {
            provider = _provider;
        }
#endif
        public override void Cancel()
        {
            Program.print("saga", "cancel", 1);
        }

        public override void Commit()
        {
            Program.print("saga", "commit", 1);
        }
    }
    public class SagaStep2 : AbsSagaStep<string>
    {
        public SagaStep2()
        {

        }
        public override string Name => GetType().Name;
#if NETSTANDARD
        IServiceProvider provider;
        public Step2(IServiceProvider _provider)
        {
            provider = _provider;
        }
#endif
        public override void Cancel()
        {
            Program.print("saga", "cancel", 2);
        }

        public override void Commit()
        {
            Program.print("saga", "commit", 2);
        }
    }
    public class SagaStep3 : AbsSagaStep<string>
    {
        public SagaStep3()
        {

        }
        public override string Name => GetType().Name;
#if NETSTANDARD
        IServiceProvider provider;
        public Step3(IServiceProvider _provider)
        {
            provider = _provider;
        }
#endif
        public override void Cancel()
        {
            Program.print("saga", "cancel", 3);
        }

        public override void Commit()
        {
            Program.print("saga", "commit", 3);
        }
    }

    public class TccStep1 : AbsTccStep<string>
    {
        public TccStep1()
        {

        }
        public override string Name => GetType().Name;
#if NETSTANDARD
        IServiceProvider provider;
        public Step4(IServiceProvider _provider)
        {
            provider = _provider;
        }
#endif
        public override void Try()
        {
            Program.print("tcc", "try", 1);
        }
        public override void Cancel()
        {
            Program.print("tcc", "cancel", 1);
        }

        public override void Commit()
        {
            Program.print("tcc", "commit", 1);
        }
    }
    public class TccStep2 : AbsTccStep<string>
    {
        public TccStep2()
        {

        }
        public override string Name => GetType().Name;
#if NETSTANDARD
        IServiceProvider provider;
        public Step5(IServiceProvider _provider)
        {
            provider = _provider;
        }
#endif
        public override void Try()
        {
            Program.print("tcc", "try", 2);
        }
        public override void Cancel()
        {
            Program.print("tcc", "cancel", 2);
        }

        public override void Commit()
        {
            Program.print("tcc", "commit", 2);
        }
    }
    public class TccStep3 : AbsTccStep<string>
    {
        public TccStep3()
        {

        }
        public override string Name => GetType().Name;
#if NETSTANDARD
        IServiceProvider provider;
        public Step6(IServiceProvider _provider)
        {
            provider = _provider;
        }
#endif
        public override void Try()
        {
            Program.print("tcc", "try", 3);
        }
        public override void Cancel()
        {
            Program.print("tcc", "cancel", 3);
        }

        public override void Commit()
        {
            Program.print("tcc", "commit", 3);
        }
    }

    public class TccStepNoTry : AbsTccStep<string>
    {
        public TccStepNoTry()
        {

        }
        public override string Name => GetType().Name;
#if NETSTANDARD
        IServiceProvider provider;
        public Step6(IServiceProvider _provider)
        {
            provider = _provider;
        }
#endif
        public override void Try()
        {
            var args = GetArgs();
            Program.print("tcc", "try", 3);
        }
        public override void Cancel()
        {
            Program.print("tcc", "cancel", 3);
        }

        //public override void Commit()
        //{
        //    Program.print("tcc", "commit", 3);
        //}
    }
}
