﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CRL.Core;
using CRL.Core.Log;
namespace DockerTest.Controllers
{
    [Route("[controller]/[action]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        public string testLog()
        {
            EventLog.Log("testConfig");
            return "log ok";
        }
        public string testConfig()
        {
            EventLog.Log("testConfig");
            try
            {
                var v = CustomSetting.GetConfigKey("test");
                return $"testConfig4 {v}";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }
    }
}
