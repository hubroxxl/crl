﻿/**
* CRL
*/
using CRL.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CRL.RedisProvider;
using CRL.Core.Extension;
namespace coreTest
{
    /// <summary>
    /// 简单REDIS消息订阅
    /// 存储为list数据
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class RedisMessage<T>
    {
        public virtual int Take
        {
            get
            {
                return 500;
            }
        }
        public virtual int SleepSecond
        {
            get
            {
                return 3;
            }
        }
        public static bool created = false;
        protected RedisClient client = new RedisClient();
        public Type GetMessageType()
        {
            return typeof(T);
        }
        public RedisMessage(bool start = true)
        {
            if (start)
            {
                Start();
            }
        }
        public void Start()
        {
            var hashId = GetHashId();
            if (!created)
            {
                created = true;
                Console.WriteLine("RedisMessage" + typeof(T) + "启动订阅");
                //client.Subscribe<T>(OnSubscribe);
                new ThreadWork().Start(hashId, () =>
                {
                    //var len = GetLength();
                    //wait = false;
                    var all = client.ListRange(hashId, 0, Take - 1);
                    if (all.Count() == 0)
                    {
                        return true;
                    }
                    //var a = OnSubscribe(all);
                    var a = true;
                    if (a)
                    {
                        client.ListTrim(hashId, all.Count(), -1);
                        //var len2 = GetLength();
                        //Console.WriteLine($"删除前{len} 获取:{all.Count} 删除后:{len2} 差异{len - all.Count != len2}", GetType().Name);
                    }
                    return true;
                }, SleepSecond);
                created = true;
            }
        }
        string GetHashId()
        {
            return string.Format("RedisMessage_{0}",typeof(T).Name);
        }
        public virtual void Publish(T message)
        {
            if (message == null)
            {
                return;
            }
            var hashId = GetHashId();
            client.ListRightPush(hashId, message.ToJson());
        }
        public void DeleteAll()
        {
            var hashId = GetHashId();
            client.Remove(hashId);
        }
        public long GetLength()
        {
            var hashId = GetHashId();
            return client.ListLength(hashId);
        }
        public void PublishList(string jsonData)
        {
            if (string.IsNullOrEmpty(jsonData))
            {
                return;
            }
            var list = SerializeHelper.DeserializeFromJson<List<T>>(jsonData);
            PublishList(list);
        }
        public virtual void PublishList(List<T> messages)
        {
           
            if (messages == null)
            {
                return;
            }
            if (messages.Count == 0)
            {
                return;
            }
            var hashId = GetHashId();
            foreach (var m in messages)
            {
                client.ListRightPush(hashId, m.ToJson());
            }

        }
        protected virtual bool OnSubscribe(List<T> message)
        {
            var msg = string.Join(",", message.Select(b => b.ToString()));
            EventLog.Log(msg, typeof(T).Name);
            return true;
        }
    }
}
