﻿using coreTest.NRediSearchClient;
using NRediSearch;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using static NRediSearch.Client;
using CRL.Core.Extension;
namespace coreTest
{
    public class NRedisSearch
    {
        static Client client;
        static Client GetClient()
        {
            client = client ?? new RediSearchTestBase().GetClient();
            return client;
        }
        public static void createIndex()
        {
            var client = GetClient();

            try { client.DropIndex(); } catch { /* Intentionally ignored */ } // reset DB

            var schema = new Schema();

            schema
                .AddSortableTextField("title")
                .AddTextField("country")
                .AddTextField("author")
                .AddTextField("aka")
                .AddTagField("language");

            client.CreateIndex(schema, new ConfiguredIndexOptions());

            var doc = new Document("1");

            doc
                .Set("title", "Le Petit Prince")
                .Set("country", "France")
                .Set("author", "Antoine de Saint-Exupéry")
                .Set("language", "fr_FR")
                .Set("aka", "The Little Prince, El Principito");

            client.AddDocument(doc);
        }
        public static void testQuery()
        {
            var client = GetClient();
            var term = "petit*";

            var query = new Query(term);
            query.Limit(0, 10);
            query.WithScores = true;
            query.Scoring = "TFIDF";
            query.ExplainScore = true;

            var searchResult = client.Search(query);

            var docResult = searchResult.Documents.FirstOrDefault();
            Console.WriteLine(docResult.ToJson());
        }
    }
}
