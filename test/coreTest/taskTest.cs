﻿using CRL.QuartzScheduler;
using System;
using System.Collections.Generic;
using System.Text;

namespace coreTest
{
    public class taskTest
    {
        static QuartzWorker quartzWorker;
        public static void initTask()
        {
            var taskName = "task1";
            quartzWorker = new QuartzWorker();

            Action<object> func = (b) => { Console.WriteLine($"custom func args is {b}"); };
            quartzWorker.AddCustomWork(func, new TriggerData() { RepeatInterval = new TimeSpan(0, 0, 5), TagData = taskName }, taskName);
            //quartzWorker.SetJobFactory(app.ApplicationServices);
            quartzWorker.Start();
        }
        public static void addTask()
        {
            var taskName = "task2";
            Action<object> func = (b) => { Console.WriteLine($"custom2 func args is {b}"); };
            quartzWorker.AddCustomWork(func, new TriggerData() { RepeatInterval = new TimeSpan(0, 0, 5), TagData = taskName }, taskName);
        }
        public static void removeTask()
        {
            var taskName = "task2";
            quartzWorker.RemoveCustomWork(taskName);
        }
    }
}
