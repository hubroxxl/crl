﻿using System;
using System.Collections.Generic;
using System.Text;

namespace coreTest
{
    public class GCTest
    {
        public static void objectInit()
        {
            AA aa1 = new AA("1");
            AA aa2 = new AA("2");
            AA aa3 = new AA("3");
            aa1 = null;
            aa2 = null;

            var tmp = aa3;
        }
        public static void gc()
        {            
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
        public class AA
        {
            public string id = "";
            public AA(string s)
            {
                id = s;
                Console.WriteLine("对象AA_" + s + "被创建了");
            }
            ~AA()
            {
                Console.WriteLine(id + " 析构函数被执行了");
            }
        }
    }
}
