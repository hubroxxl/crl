﻿using ConsoleTables;
using InfluxData.Net.Common.Enums;
using InfluxData.Net.InfluxDb;
using InfluxData.Net.InfluxDb.Models;
using InfluxData.Net.InfluxDb.Models.Responses;
using InfluxDB.Client.Api.Domain;
using InfluxDB.Client;
using CRL.Core;
using Newtonsoft.Json.Linq;
using InfluxDB.Client.Core;
using InfluxDB.Client.Linq;
using Measurement = InfluxDB.Client.Core.Measurement;
namespace testInfluxDB
{
    [Measurement("requestInfo")]
    public class requestInfo
    {
        [Column(IsTag = true)]
        public string appType { get; set; }

        [Column(IsTag = true)]
        public string projectName { get; set; }

        [Column(IsTag = true)]
        public string orgId { get; set; }

        [Column(IsTag = true)]
        public string customerId { get; set; }

        [Column(IsTag = true)]
        public string userMobile { get; set; }

        [Column(IsTag = true)]
        public string requestPath { get; set; }

        [Column]
        public string responseCode { get; set; }

        [Column]
        public long requestElMs { get; set; }

        [Column(IsTimestamp = true)]
        public DateTime time { get; set; }
    }
    public class testv1
    {
        private static InfluxDbClient clientDb;
        static string infuxUrl = "http://localhost:8086/";
       
        static string infuxUser = "test";
        static string infuxPwd = "test1234";
        static string dbName => bucket;
        const string bucket = "request2024";
        //https://github.com/tihomir-kit/InfluxData.Net
        static testv1()
        {
            clientDb = new InfluxDbClient(infuxUrl, infuxUser, infuxPwd, InfluxDbVersion.Latest);
        }
        public static async void createDataBase()
        {
            var response = await clientDb.Database.CreateDatabaseAsync(dbName);
        }
        static async Task<bool> addDataBase<T>(string tableName,List<T> datas, List<string> keyFields)
        {
            var refInfo = ReflectionHelper.GetInfo<T>();
            var pros = typeof(T).GetProperties();
            var point_model = new Point()
            {
                Name = tableName,
                Tags = new
               Dictionary<string, object>(),
                Fields = new
               Dictionary<string, object>(),
                Timestamp = DateTime.UtcNow
            };
            foreach (var item in datas)
            {
                foreach(var f in pros)
                {
                    var isKey = keyFields.Contains(f.Name);
                    if(isKey)
                    {
                        point_model.Tags.Add(f.Name, refInfo.GetValue(item,f.Name));
                    }
                    else
                    {
                        point_model.Fields.Add(f.Name, refInfo.GetValue(item, f.Name));
                    }
                }
            }
            var response = await clientDb.Client.WriteAsync(point_model, dbName);
            return response.Success;
        }
        public static async void addData()
        {
            var list = new List<requestInfo>();
            list.Add(new requestInfo { projectName = "projectName1", requestPath = "/api/user", responseCode = "200", time = DateTime.UtcNow });
            var v = await addDataBase("requestInfo", list, new List<string> { "projectName", "requestPath" });
        }
        static async Task<Serie> query(string sql)
        {
            var response = await clientDb.Client.QueryAsync(sql, dbName);
            var series = response.ToList();
            var data = series.FirstOrDefault();
            Console.WriteLine(string.Join("  -  ",data.Columns));
            foreach(var v in data.Values)
            {
                Console.WriteLine(string.Join("  -  ", v));
            }
            return data;
        }
        public static async void testGroup()
        {
            var interval = 10;
            var length = 20;
            var time = DateTime.Now.AddMinutes(-length/(60/interval)).AddHours(-8);
            var timeStr = time.ToString("yyyy-MM-ddTHH:mm:ss.000000000Z");
            var sql = $"SELECT time,count(statusCode) as count FROM requestInfo where time>'{timeStr}' group by time({interval}s) order by time desc";
            var result = await query(sql);
        }
        public static async void test22()
        {
            var serialNumber = "F2EA2B0CDFF";
            var queryTemplate = "SELECT * FROM cpuTemp WHERE \"serialNumber\" = @SerialNumber";

            var response = await clientDb.Client.QueryAsync(
                queryTemplate: queryTemplate,
                parameters: new
                {
                    @SerialNumber = serialNumber
                },
                dbName: "yourDbName"
            );
        }

    }
}