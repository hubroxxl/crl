﻿using CRL.Core;
using System.Reflection;

namespace testInfluxDB
{

    class Program
    {
        static void Main(string[] args)
        {
            var asb = Assembly.GetAssembly(typeof(testv1));
            var types = asb.GetTypes().Where(b => b.Namespace == typeof(testv1).Namespace && b.IsPublic).ToArray();
            ConsoleTest.DoCommand(types);
        }
    }
}
