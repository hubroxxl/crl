# CRL组件库

## ORM 框架
[](tree/master/Data/crl)
CRL是一个效率高，易于扩展的ORM，支持.NetFramework 4.6+ .NetCore 2.0+
- 数据源可扩展性
除了支持关系型数据库生成SQL外，也支持非关系型数据库
通过解析生成内置表达多结构，轻松实现实现非SQL类型查询表达，如json
已支持非关系型数据库有:
	1. MongoDb
	2. Elasticsearch
- 注入式数据连接源
CRL使用委托注入的方式定义数据连接
```c#
	builder.RegisterLocation<Code.Sharding.OrderSharding>((t, a) =>
	{
		var tableName = $"{t.TableName}_{a.MemberId}";
		//返回定位库和表名
		return new Location("testdb", tableName);
	});
```
很容易实现，分库，分表，主从模式
- 复杂查询支持
ILambdaQuery 提供的语法支持 嵌套，关联，子查询，函数扩展
- 作为扩展使用
CRL LambdaQuery支持丰富的查询语法，所写即生成，如果只想用LambdaQuery，可直接使用查询扩展
	1. 基于IDbConnection的扩展
	2. 基于Elasticsearch ElasticClient的扩展

## EventBus 组件
[](tree/master/Queue/CRL.EventBus)
采用发布，订阅的模式，支持多种数据容器
可作为事件处理，数据处理队列，rpc使用
- 关系型数据库
- rabbitmq
- redis
- mongo
- memory
```c#
client.Publish("test", DateTime.Now.Second);
subService.AddSubscribe<int>(new SubscribeAttribute { Name = "test", RetryTimes = 2 }, b =>
{
    //返回false时会重试
    Console.WriteLine($"testRetry2 recieve {b}");
    return false;
});
```
## Tcc Saga分布式事务补偿
[](tree/master/CRL.Transaction)
自定义事务数据存储方式,快速实现事务补偿

TCC 事务特点：
* Try 用于资源冻结/预扣；
* Try 全部环节通过，代表业务一定能完成，进入 Confirm 环节；
* Try 任何环节失败，代表业务失败，进入 Cancel 环节；
* Confirm 失败会进行重试N次，直到交付成功，或者人工干预；
* Cancel 失败会进行重试N次，直到取消成功，或者人工干预；

SAGA 事务特点：
* Commit 用于业务提交；
* Commit 全部环节通过，代表业务交付成功；
* Commit 任何环节失败，代表业务失败，进入 Cancel 环节；
* Cancel 失败会进行重试N次，直到取消成功，或者人工干预；
```c#
var tccControl = new TccControl();
tccControl.Then<TccStep1>().Then<TccStep2>().Then<TccStep3>();
var result = tccControl.Execute();
```
## 异步缓存
[](tree/master/CRL.Cache)
异步或同步更新缓存实现
支持内存和redis
同步更新：数据过期后，在本次调用更新
异步更新：数据过期后，后台线程更新
```c#
static string getTestArgs(DateTime time)
{
    return time.ToString();
}
var val1 = "testDelegateCache2";
var time = DateTime.Now;
var data = DelegateCacheFactory.GetRedisInstance().InitAsyncUpdate("test22", 0.1, false, getTestArgs, time);
Console.WriteLine($"get {val1} return {data}");
```
## Quartz任务组件
[](tree/master/CRL.QuartzScheduler)
简单创建Quartz任务
```c#
var taskName = "task1";
quartzWorker = new QuartzWorker();

Action<object> func = (b) => { Console.WriteLine($"custom func args is {b}"); };
quartzWorker.AddCustomWork(func, new TriggerData() { RepeatInterval = new TimeSpan(0, 0, 5), TagData = taskName }, taskName);
//quartzWorker.SetJobFactory(app.ApplicationServices);
quartzWorker.Start();
```
## api调用代理
[](tree/master/api)
使用interface代理简化http接口调用
```c#
    /// <summary>
    /// 微信获取用户信息
    /// </summary>
    [Service(HostName = "weixin", HostUrl = "https://api.weixin.qq.com")]
    public interface IUser
    {
        [Method(Path = "/cgi-bin/user/info", Method = HttpMethod.GET)]
        string info(string access_token, string openid, string lang = "zh_CN");

        [Method(Path = "/cgi-bin/user/info", Method = HttpMethod.GET)]
        Task<string> infoAsync(string access_token, string openid, string lang = "zh_CN");
    }
var client = provider.GetService<IUser>();
 var reply = client.info("123", "123");
 Console.WriteLine("服务返回数据: " + reply);
```

## 文档集合
[](tree/master/doc)