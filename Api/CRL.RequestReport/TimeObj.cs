﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace CRL.RequestReport
{
    class timeObj2
    {
        public string name;
        public string type;
        public string key
        {
            get
            {
                return $"{name}_{type}".ToLower();
            }
        }
        public long count;
        public DateTime time;
        public int minuteKey ;
    }
}
