﻿#if NETSTANDARD
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
#endif
namespace CRL.RequestReport
{
#if NETSTANDARD
    public static class EXT
    {
        internal static Func<HttpContext, bool> _config;
        public static void RequestCount(this IApplicationBuilder app, Func<HttpContext, bool> config = null)
        {
            _config = config;
            app.UseMiddleware<RequestCountMiddleware>();
        }
    }
#endif
}
