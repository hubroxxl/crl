﻿#if NETSTANDARD
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
#endif
namespace CRL.RequestReport
{
#if NETSTANDARD
    class RequestCountMiddleware
    {
        private readonly RequestDelegate _next;
        public RequestCountMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var config = EXT._config;
            if (config == null)
            {
                config = (context) =>
                {
                    return context.Response.StatusCode != 404;
                };
                EXT._config = config;
            }
            if (config.Invoke(httpContext))
            {
                var path = httpContext.Request.Path.Value.ToLower();
                if (!path.Contains('.'))
                {
                    var countInstance = CountService.GetInstance("pathCount");
                    countInstance.Add(httpContext.Request.Path.Value);
                }
            }
            await _next(httpContext);
        }
    }
#endif
}
