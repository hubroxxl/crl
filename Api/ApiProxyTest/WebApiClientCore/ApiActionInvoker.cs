﻿using System;
using System.Reflection;

namespace WebApiClientCore
{
    /// <summary>
    /// 表示Action执行器
    /// </summary>
    public abstract class ApiActionInvoker
    {
        /// <summary>
        /// 执行Action
        /// </summary>
        /// <param name="context">服务上下文</param>
        /// <param name="arguments">action参数值</param>
        /// <returns></returns>
        public abstract object Invoke(object[] arguments);
    }
    public class DefaultApiActionInvoker : ApiActionInvoker
    {
        MethodInfo _method;
        public DefaultApiActionInvoker(MethodInfo method)
        {
            _method = method;
        }
        public override object Invoke(object[] arguments)
        {
            Console.WriteLine($"invoke {_method.Name}");
            return $"invoke {_method.Name}";
        }
    }
    /// <summary>
    /// 接口方法的拦截器
    /// </summary>
    public interface IHttpApiInterceptor
    {
        /// <summary>
        /// 拦截方法的调用
        /// </summary>
        /// <param name="actionInvoker">action执行器</param> 
        /// <param name="arguments">action的参数集合</param>
        /// <returns></returns>
        object Intercept(ApiActionInvoker actionInvoker, object[] arguments);
    }
    public class HttpApiInterceptor : IHttpApiInterceptor
    {
        public object Intercept(ApiActionInvoker actionInvoker, object[] arguments)
        {
            Console.WriteLine($"Interceptor {actionInvoker}");
            return actionInvoker.Invoke(arguments);
        }
    }
}
