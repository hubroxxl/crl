﻿/**
* CRL
*/
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using CRL.Core;
using CRL.Core.ApiProxy;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace ApiProxyTest
{
    class Program
    {
        static IServiceProvider provider;
        static Program()
        {
            var builder = new ConfigurationBuilder();

            var configuration = builder.Build();

            var services = new ServiceCollection();
            services.RegisterApiHost(new HostInfo("weixin", "https://api.weixin.qq.com"));
            //services.RegisterApiClient<IUser>();
            services.RegisterApiClient((hostName) =>
            {
                return new Dictionary<string, object>();
            }, System.Reflection.Assembly.GetExecutingAssembly());
            provider = services.BuildServiceProvider();
        }

        static void Main(string[] args)
        {

            ConsoleTest.DoCommand(typeof(Program));
        }

        public static void testInfo()
        {
            var client = provider.GetService<IUser>();
            var reply = client.info("123", "123");
            Console.WriteLine("服务返回数据: " + reply);
        }
        public static async void testInfoAsync()
        {
            var client = provider.GetService<IUser>();
            var reply = await client.infoAsync("123", "123");
            Console.WriteLine("服务返回数据: " + reply);
        }
        public static void testGetPost()
        {
            var client = provider.GetService<IUser>();
            client.getPost("123", new msgObj { id = "222" });
            client.testForm(new msgObj { id = "222", id2 = "2121" });
            client.testForm2("1111", "22222");
            client.testGet("1111", "22222");
        }
        public static void testError()
        {
            var client = provider.GetService<IUser>();
            client.testError();
        }
        public static void testApiIntercept()
        {
            var at = new WebApiClientCore.ILEmitHttpApiActivator<IUser>(m => new WebApiClientCore.DefaultApiActionInvoker(m));
            var client = at.CreateInstance(new WebApiClientCore.HttpApiInterceptor());
            var result = client.info("123", "123");
            Console.WriteLine($"result is {result}");
        }
    }

    /// <summary>
    /// 微信获取用户信息
    /// </summary>
    [Service(HostName = "weixin", HostUrl = "https://api.weixin.qq.com")]
    public interface IUser
    {
        [Method(Path = "/cgi-bin/user/info", Method = HttpMethod.GET)]
        string info(string access_token, string openid, string lang = "zh_CN");

        [Method(Path = "/cgi-bin/user/info", Method = HttpMethod.GET)]
        Task<string> infoAsync(string access_token, string openid, string lang = "zh_CN");

        [Method(Path = "/cgi-bin/message/template/send", Method = HttpMethod.POST)]
        string getPost([MethodGet] string access_token, msgObj data);

        [Method(Path = "/cgi-bin/message/template/send", Method = HttpMethod.POST, ContentType = ContentType.FORM)]
        string testForm(msgObj data);

        [Method(Path = "/cgi-bin/message/template/send", Method = HttpMethod.POST, ContentType = ContentType.FORM)]
        string testForm2(string id, string id2);

        [Method(Path = "/cgi-bin/message/template/send", Method = HttpMethod.GET, ContentType = ContentType.NONE)]
        string testGet([MethodGet] string id, string id2);

        [Method(Path = "/cgi-bin/abc/error", Method = HttpMethod.GET, ContentType = ContentType.NONE)]
        string testError();

    }
    public class msgObj
    {
        public string id { get; set; }
        public string id2 { get; set; }
    }
}
