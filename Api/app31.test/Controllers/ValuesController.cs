﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CRL.Core;

using CRL.Transaction;
using CRL.Transaction.Test;
using CRL.Data;
using CRL.Core.Extension;
using System.IO;
namespace app31.test.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [ActionFiler]
    public class ValuesController : ControllerBase
    {
        //IPublisher publisher;
        SagaControl SagaControl;
        TransLoader SagaLoader;
        public ValuesController(SagaControl sagaControl, TransLoader sagaLoader)
        {
            //publisher = _publisher;
            SagaControl = sagaControl;
            SagaLoader = sagaLoader;
        }
        public ActionResult Index()
        {
            var rep = RepositoryFactory.Get<BlogUser>();
            var ids = new List<string>() { "rrrr" };
            rep.GetLambdaQuery().Where(b => ids.Contains(b.Id)).ToSingle();
            var sum = rep.Count(b => !string.IsNullOrEmpty(b.Name));
            return Content("");
        }
        public ActionResult GetAllSQLTrack(string path = "", int take = 50, int min = 0)
        {
            var all = SQLTrack.GetAllSqlSQLTrack() as IDictionary<string, Dictionary<int, SQLTrack.SqlInfo>>;
            if (!string.IsNullOrEmpty(path))
            {
                all = all.Where(b => b.Key == path).ToDictionary(b => b.Key, b => b.Value);
            }
            var query = all.OrderByDescending(b => b.Value.Values.Max(x => x.MaxElTime)).Take(take);
            if (min > 0)
            {
                query = query.Where(b => b.Value.Values.Max(x => x.MaxElTime) > min);
            }
            var result2 = query.Select(b => new
            {
                path = b.Key,
                items = b.Value.Values.Where(x => x.MaxElTime > min).OrderByDescending(x => x.MaxElTime)
            });
            return Content(result2.ToJson(), "application/json");
        }
        public ActionResult test()
        {
            CallContext.SetData("time", DateTime.Now);
            var time = CallContext.GetData<DateTime>("time");
            return Content("ok");
        }

        public ActionResult testSaga()
        {
            var control = SagaControl;
            control.Start(new TransOptions { Name = "testSaga", MaxRetryCount = 10, RetryInterval = TimeSpan.FromSeconds(1) });
            control.Then<Step1>("Step1").Then<Step2>("Step2").Then<Step3>("Step3");
            var result = control.Execute();
            Console.WriteLine($"SagaTest 执行 {result}");
            return Content("ok");
        }
        public ActionResult TestSagaLoad()
        {
            var all = SagaLoader.LoadAll("test");
            all.ForEach(b => b.Execute());
            return Content("ok");
        }
    }
    public class argsObj
    {
        public string name;
    }
    public class BlogUser
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
