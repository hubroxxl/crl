﻿using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc.Filters;
using CRL.Data;
namespace app31.test
{
    public class ActionFiler : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var path = context.HttpContext.Request.Path;
            SQLTrack.SetSQLTrackContext(path);
            base.OnActionExecuting(context);
        }
    }
}
