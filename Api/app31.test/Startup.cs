﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using CRL.Transaction.NetCore;
using CRL.Core;
using CRL.Data;
using CRL.Data.DBAccess;
using CRL.Sqlite;

namespace app31.test
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddOptions();


            //services.AddEventBusSubscribe();

            services.AddTransControl(Assembly.GetAssembly(typeof(CRL.Transaction.Test.Step1)), Assembly.GetAssembly(typeof(Program)));
        }

        //public static QuartzWorker quartzWorker;
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            SettingConfig.LogSql = true;
            var builder = DBConfigRegister.GetInstance();
            builder.UseSqlite();
            builder.RegisterDBAccessBuild(dbLocation =>
            {
                return new DBAccessBuild(DBType.SQLITE, "Data Source=d:\\sqlliteTest.db;");
            });
            app.UseDeveloperExceptionPage();
            //ConfigHelper.Init(projectName, "platform");
            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors(builder =>
            {
                builder.WithOrigins(new string[]
                {
                    "*"
                });
                builder.AllowAnyHeader();
                builder.AllowAnyMethod();
            });

            app.UseTransControl("test");
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();

            });
        }
    }
}
