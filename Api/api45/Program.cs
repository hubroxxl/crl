﻿using CRL.Core;
using CRL.Core.ApiProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace api45
{
    internal class Program
    {
        static IUser client;
        static void Main(string[] args)
        {
            var clientConnect = new ApiClientConnect("");
            client = clientConnect.GetClient<IUser>();
            ConsoleTest.DoCommand(typeof(Program));
        }
        public static void test()
        {
            var reply = client.info("123", "123");
            Console.WriteLine("服务返回数据: " + reply);
        }
        public static void test2()
        {
            AssemblyName assemblyName = new AssemblyName("Study");
            AssemblyBuilder assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.RunAndSave);
            ModuleBuilder moduleBuilder = assemblyBuilder.DefineDynamicModule("StudyModule", "StudyOpCodes.dll");
            TypeBuilder typeBuilder = moduleBuilder.DefineType("StudyOpCodes", TypeAttributes.Public);
            MethodBuilder showMethod = typeBuilder.DefineMethod("Show", MethodAttributes.Public | MethodAttributes.Static, null, new Type[] { typeof(int), typeof(string) });

            ILGenerator ilOfShow = showMethod.GetILGenerator();
            ilOfShow.Emit(OpCodes.Ldstr, "姓名：{1} 年龄：{0}");
            //静态方法必参数索引从0开始
            ilOfShow.Emit(OpCodes.Ldarg_0);
            ilOfShow.Emit(OpCodes.Ldarg_1);
            ilOfShow.Emit(OpCodes.Call, typeof(string).GetMethod("Format", new Type[] { typeof(string), typeof(int), typeof(string) }));
            ilOfShow.Emit(OpCodes.Pop);
            ilOfShow.Emit(OpCodes.Ret);
            Type t = typeBuilder.CreateType();
            assemblyBuilder.Save("StudyOpCodes.dll");
        }
    }

    /// <summary>
    /// 微信获取用户信息
    /// </summary>
    [Service(HostName = "weixin", HostUrl = "https://api.weixin.qq.com")]
    public interface IUser
    {
        [Method(Path = "/cgi-bin/user/info", Method = HttpMethod.GET)]
        string info(string access_token, string openid, string lang = "zh_CN");

        [Method(Path = "/cgi-bin/user/info", Method = HttpMethod.GET)]
        Task<string> infoAsync(string access_token, string openid, string lang = "zh_CN");

        [Method(Path = "/cgi-bin/message/template/send", Method = HttpMethod.POST)]
        string getPost([MethodGet] string access_token, msgObj data);

        [Method(Path = "/cgi-bin/message/template/send", Method = HttpMethod.POST, ContentType = ContentType.FORM)]
        string testForm(msgObj data);

        [Method(Path = "/cgi-bin/message/template/send", Method = HttpMethod.POST, ContentType = ContentType.FORM)]
        string testForm2(string id, string id2);

        [Method(Path = "/cgi-bin/message/template/send", Method = HttpMethod.GET, ContentType = ContentType.NONE)]
        string testGet([MethodGet] string id, string id2);

        [Method(Path = "/cgi-bin/abc/error", Method = HttpMethod.GET, ContentType = ContentType.NONE)]
        string testError();

    }
    public class msgObj
    {
        public string id { get; set; }
        public string id2 { get; set; }
    }
}
