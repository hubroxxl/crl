﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CRL.Core.Remoting;
namespace app2._2test
{
    public interface IApiServerTest
    {
        string Test1(string a, int b);
        TestObj Test2(TestObj obj);
    }
    public abstract class ServiceBase: AbsService
    {

    }
    public interface Iface2
    {

    }

    [AllowAnonymous]
    [Service]
    public class ApiServerTest : ServiceBase, IApiServerTest, Iface2
    {
        public User tokenUser
        {
            get
            {
                return GetAuthData<User>();
            }
        }
        public string Test1(string a, int b)
        {
            var u = tokenUser;
            return a + b.ToString();
        }
        public Task<int> TestAsync()
        {
            return Task.FromResult(1);
        }
        [AllowAnonymous]
        public TestObj Test2(TestObj obj)
        {
            //var u = tokenUser;
            return obj;
        }
    }
    public class User
    {
        public string name;
    }
}
