﻿using CRL.EventBus;
using CRL.QuartzScheduler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace app2._2test
{
    public class TaskTest : QuartzJob
    {
        //public TaskTest()
        //{
        //    this.RepeatInterval = new TimeSpan(0, 30, 0);
        //    //this.CronExpression = "0 0/28 * * * ?";
        //}
        IPublisher publisher;
        public TaskTest(IPublisher _publisher)
        {
            publisher = _publisher;
        }

        public override void DoWork()
        {
            Console.WriteLine($"DoWork {TagData}");
        }
    }
}
