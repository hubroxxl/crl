﻿/**
* CRL
*/
using CRL.EventBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace app2._2test
{
    [Subscribe]
    public class SubscribeTest
    {
        [Subscribe("timeTest")]
        public void Test(DateTime time)
        {
            //throw new Exception("error");
            Console.WriteLine($"receive {time}");
            CRL.Core.EventLog.Log(time.ToString());
        }
        [Subscribe("testSendReq")]
        public string testSendReq(string name)
        {
            return $"reqResponse1 {name}";
        }

    }
}
