﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CRL.Core;
using CRL.EventBus;

namespace app2._2test.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        IPublisher publisher;

        public ValuesController(IPublisher _publisher)
        {
            publisher = _publisher;

        }

        [HttpGet]
        public ActionResult test1()
        {
            CRL.Core.CallContext.SetData("2222", true);
            var obj = CRL.Core.CallContext.GetData<bool>("2222");
            return Content(obj + "");
        }
        [HttpGet]
        public ActionResult test2()
        {
            var obj = CRL.Core.CallContext.GetData<bool>("2222");
            return Content(obj + "");
        }
        [HttpGet]
        public ActionResult test3()
        {
            Startup.quartzWorker.RemoveWork<TaskTest>();
            return Content("");
        }
        argsObj getArgs()
        {
            return new argsObj { name = Request.Query["name"] };
        }
        [HttpGet]
        public ActionResult testEventBusRequest(string name)
        {
            var res = publisher.Request<string>("testSendReq", name);
            return Content(res);
        }

    }
    public class argsObj
    {
        public string name;
    }
}
