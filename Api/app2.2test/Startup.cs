﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using CRL.EventBus.NetCore;
using CRL.QuartzScheduler;
using CRL.ApiGenerate.NetCore;
using CRL.DynamicWebApi.NetCore;
//using CRL.Transaction.NetCore;
using CRL.Core;
using CRL.Data;
using CRL.Data.DBAccess;
using CRL.EventBus;

namespace app2._2test
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddOptions();
            services.AddEventBus(config =>
            {
                //config.UseRabbitMQ(cfg =>
                //{
                //    cfg.HostName = "127.0.0.1";
                //    cfg.UserName = "guest";
                //    cfg.Password = "guest";
                //});
                config.UseMemory();
                config.RegisterSubscribe(Assembly.GetAssembly(typeof(Program)));
            }
            );
            //services.AddScoped<IApiServerTest,ApiServerTest>();
            services.AddApiGenerate(server =>
            {
                server.SetDefaultApiPrefix("api");
                //server.UseCustomTokenCheck(checkToken);
            }, System.Reflection.Assembly.GetAssembly(typeof(Startup)));

            services.AddDynamicApi(server =>
            {
                server.SetDefaultApiPrefix("api2");
                server.UseCustomTokenCheck(checkToken);
            }, System.Reflection.Assembly.GetAssembly(typeof(Startup)));

            services.AddScoped<SubscribeTest>();
            services.AddQuartzScheduler(System.Reflection.Assembly.GetAssembly(typeof(Startup)));
            //services.AddTransControl(System.Reflection.Assembly.GetAssembly(typeof(CRL.Transaction.Test.Step1)));
        }
        bool checkToken(CRL.Core.Remoting.MessageBase req, out object user, out string error)
        {
            error = "";
            var token = req.Token;
            user = new User { name = "name" };
            return req.Token == "123";
        }
        public static QuartzWorker quartzWorker;
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            var host = app.ApplicationServices.GetService<Microsoft.Extensions.Hosting.IHost>();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            #region eventbus
            app.ApplicationServices.RunEventBusSubscribe();
            #endregion
            app.UseMvc();
            app.UseApiGenerate();
            app.UseDynamicApi();
            //app.ApplicationServices.UseTransControl("test", DBType.MSSQL, "Server=.;Database=testdb;Uid=sa;Pwd=123;");
            quartzWorker = new QuartzWorker();
            //works
            quartzWorker.AddWorkBase<TaskTest>(new TriggerData() { RepeatInterval = new TimeSpan(0, 0, 5), TagData = "123" }, "123");
            //quartzWorker.AddWorkBase<TaskTest>(new TriggerData() { RepeatInterval = new TimeSpan(0, 0, 5), TagData = "456" }, "456");
            Action<object> func = (b) => { Console.WriteLine($"custom func args is {b}"); };
            quartzWorker.AddCustomWork(func, new TriggerData() { RepeatInterval = new TimeSpan(0, 0, 5), TagData = "789" }, "789");
            quartzWorker.SetJobFactory(app.ApplicationServices);
            quartzWorker.Start();

        }
    }
}
