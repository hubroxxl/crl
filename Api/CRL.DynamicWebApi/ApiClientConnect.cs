﻿using CRL.Core.ApiProxy;
using CRL.Core.Remoting;
using System.Collections.Generic;

namespace CRL.DynamicWebApi
{
    public class ApiClientConnect : AbsClientConnect
    {
        string host;
        string apiPrefix;
        public ApiClientConnect(string _host, string _apiPrefix = "")
        {
            host = _host;
            apiPrefix = _apiPrefix;
        }
        /// <summary>
        /// 使用网关调用前辍
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="gatewayPrefix"></param>
        /// <returns></returns>
        public T GetClient<T>(string gatewayPrefix) where T : class
        {
            var type = typeof(T);
            var serviceName = type.Name;
            var key = string.Format("{0}_{1}", host, serviceName);
            var a = _services.TryGetValue(key, out object instance);
            if (a)
            {
                return instance as T;
            }
            var info = serviceInfo.GetServiceInfo(apiPrefix, type);
            var client = new ApiClient(this)
            {
                HostAddress = new HostAddress() { address = host, serviceNamePrefix = gatewayPrefix },
                serviceInfo = info,
            };
            //创建代理
            //instance = client.ActLike<T>();
            var at = new ApiProxyActivator<T>((m) => new DefaultActionInvoker(m));
            instance = at.CreateInstance(client);
            _services[key] = instance;
            return instance as T;
        }
        public override T GetClient<T>(Dictionary<string, object> requestHeads = null)
        {
            return GetClient<T>("");
        }
    }
}
