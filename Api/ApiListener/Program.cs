﻿using System;

namespace ApiListener
{
    class Program
    {
        static void Main(string[] args)
        {
            var server = new ServerListener(9080);
            server.Start();
        }
    }
}
