﻿//using CRL.Data.Attribute;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRL.Transaction.DbStatus
{
    //[Table(TableName = "_TransMasterInfo")]
    public class TransMasterInfo
    {
        public int Id { get; set; }

        //[Field(FieldIndexType = FieldIndexType.非聚集)]
        public TransactionType TransactionType { get; set; }

        //[Field(Length = 200)]
        public string Name { get; set; }

        //[Field(Length = 50, FieldIndexType = FieldIndexType.非聚集)]
        public string TId { get; set; }
        public string CheckKey { get; set; }
        //[Field(Length = 100, FieldIndexType = FieldIndexType.非聚集)]
        public string ProjectName { get; set; }

        public int MaxRetryCount { get; set; }
        public double RetryInterval { get; set; }

        //[Field(Length = 5000)]
        public string Args { get; set; }

        //[Field(Length = 300)]
        public string ArgsType { get; set; }

        public DateTime CreateTime { get; set; } = DateTime.Now;
        public DateTime UpdateTime { get; set; }
        public TransStatus Status { get; set; }
        public string StatusName { get; set; }
        public string Msg { get; set; }
    }
    //[Table(TableName = "_TransStepInfo")]
    public class TransStepInfo
    {
        public int Id { get; set; }
        //[Field(Length = 200)]
        public string Name { get; set; }

        //[Field(FieldIndexType = FieldIndexType.非聚集)]
        public TransactionType TransactionType { get; set; }

        //[Field(Length = 50, FieldIndexType = FieldIndexType.非聚集)]
        public string TId { get; set; }

        //[Field(Length = 200)]
        public string TypeName { get; set; }

        //[Field(Length = 5000)]
        public string Args { get; set; }

        //[Field(Length = 300)]
        public string ArgsType { get; set; }

        public bool FromNetCoreInjection { get; set; }
        public DateTime CreateTime { get; set; } = DateTime.Now;
        public DateTime UpdateTime { get; set; }
        public TransStatus Status { get; set; }
        public string StatusName { get; set; }

        //[Field(FieldIndexType = FieldIndexType.非聚集)]
        public int Step { get; set; }
        public string Msg { get; set; }
    }

}
