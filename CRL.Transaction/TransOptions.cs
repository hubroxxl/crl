﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRL.Transaction
{
    public class TransOptions
    {
        internal string TId { get; set; }
        public string Name { get; set; }
        public int MaxRetryCount { get; set; } = 10;
        public TimeSpan RetryInterval { get; set; } = TimeSpan.FromSeconds(1);
        /// <summary>
        /// 是否持久化
        /// </summary>
        internal bool Persistent { get; set; } = true;
        public object Args { get; set; }
        public bool UseLog { get; set; }
        /// <summary>
        /// 事务重复检查唯一标识
        /// </summary>
        public string CheckKey { get; set; }

        //internal Dictionary<string, object> _LocalState = new Dictionary<string, object>();
        //public void SetState(string key, object value)
        //{
        //    _LocalState.Add(key, value);
        //}
        //public T GetState<T>(string key)
        //{
        //    var a = _LocalState.TryGetValue(key, out var v);
        //    if(a)
        //    {
        //        return (T)v;
        //    }
        //    return default(T);
        //}
    }
}
