﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRL.Transaction
{
    public class ExecuteResult
    {
        public bool AbstractResult { get; set; }
        public bool NeedRepair { get; set; }
        public string LastError { get; set; }
        public TransStatus Status { get; set; }
        public IStep ErrorStep { get; internal set; }
    }
}
