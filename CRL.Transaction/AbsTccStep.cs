﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CRL.Transaction
{

    public interface ITccStep: IStep
    {
        void Try();
    }
    public abstract class AbsTccStepBase : ITccStep
    {
        public TransOptions TransOptions { get; set; }

        public abstract string Name { get; }
        public int Step { get; set; }
        public TransStatus Status { get; set; }
        public object Args { get; set; }
        public void SetArgs(object args)
        {
            Args = args;
        }
        public abstract Type GetArgsType();

        //public object GetArgs() => Args;

        //以判断方法有没重载
        internal bool defaultTry = false;
        internal bool defaultCommit = false;
        internal bool defaultCancel = false;
        public virtual void Try()
        {
            defaultTry = true;
        }
        public virtual void Commit()
        {
            defaultCommit = true;
        }
        public virtual void Cancel()
        {
            defaultCancel = true;
        }
    }
    public abstract class AbsTccStep<TArgs> : AbsTccStepBase
    {
        public T GetMainArgs<T>()
        {
            return (T)TransOptions.Args;
        }
        public override Type GetArgsType()
        {
            return typeof(TArgs);
        }
        public TArgs GetArgs() => (TArgs)Args;
    }
    public abstract class AbsTccStep : AbsTccStep<string>
    {

    }
    class CustomTccStep : AbsTccStep<object>
    {
        internal Action try_;
        internal Action commit;
        internal Action cancel;
        public override string Name => "CustomTccStep";
        public CustomTccStep(Action _try, Action _commit, Action _cancel)
        {
            commit = _commit;
            try_ = _try;
            cancel = _cancel;
        }
        public override void Try()
        {
            try_?.Invoke();
        }
        public override void Cancel()
        {
            cancel?.Invoke();
        }

        public override void Commit()
        {
            commit?.Invoke();
        }
    }
}
