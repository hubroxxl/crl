﻿using CRL.Core.Remoting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using CRL.Core.Extension;
using CRL.Transaction.DbStatus;

namespace CRL.Transaction
{
    public class TccControl: ITransControl
    {
        TransOptions _option;
        List<ITccStep> steps = new List<ITccStep>();
        public bool FromLoad { get; set; }
        bool fromNetCoreInjection;
        public TransStatus Status { get; set; }

        public TccControl()
        {

        }
#if NETSTANDARD
        IServiceProvider provider;
        public TccControl(IServiceProvider _provider, ITransInfoManage _transInfoManage)
        {
            provider = _provider;
            fromNetCoreInjection = true;
            //transInfoManage = _transInfoManage;
        }
        public void SetServiceProvider(IServiceProvider _provider)
        {
            provider = _provider;
        }
#endif
        public TransactionType TransactionType => TransactionType.Tcc;
        public string LastError { get; set; }
        public ITransControl Start(TransOptions option)
        {
            if (string.IsNullOrEmpty(option.TId))
            {
                option.TId = Guid.NewGuid().ToString();
            }
            _option = option;
            return this;
        }
        public TccControl Then<T>(object args = null) where T : ITccStep
        {
            return Then(typeof(T), args, null) as TccControl;
        }
        bool isDelegate;
        public TccControl Then(Action _try, Action commit, Action cancel)
        {
            isDelegate = true;
            var tccStep = new CustomTccStep(_try, commit, cancel);
            //_option.Persistent = false;
            steps.Add(tccStep);
            tccStep.Step = steps.Count;
            //sagaStep.SetArgs(args);
            return this;
        }
        public ITransControl Then(Type type, object args, TransStatus? newStatus)
        {
            if (type == null)
            {

                throw new ArgumentNullException("type");
            }
            ITccStep tccStep;
#if NETSTANDARD
            if (provider != null && fromNetCoreInjection)
            {
                tccStep = provider.GetService(type) as ITccStep;
            }
            else
            {
                tccStep = System.Activator.CreateInstance(type) as ITccStep;
            }
#else
            tccStep = System.Activator.CreateInstance(type) as ITccStep;
#endif
            tccStep.TransOptions = _option;
            steps.Add(tccStep);
            tccStep.Step = steps.Count;
            tccStep.SetArgs(args);
            if (newStatus != null)
            {
                tccStep.Status = newStatus.Value;
            }
            return this;
        }
        void Log(IStep step, string msg)
        {
            if (!_option.UseLog)
            {
                return;
            }
            var logMsg = $"{_option.Name} step{step.Step}[{step.Name}] {msg}";
            Console.WriteLine($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} {logMsg}");
            Core.EventLog.Log(logMsg, GetType().Name);
        }
        /// <summary>
        /// TCC事务，try失败则返回失败，否则一直返回成功,commit失败由后台重复处理
        /// </summary>
        /// <returns></returns>
        public ExecuteResult Execute()
        {
            var infoManage = TransConfig.GetTransInfoManage();
            if(isDelegate)
            {
                infoManage = null;
            }
            //delegate不写入状态维护
            if (!string.IsNullOrEmpty(_option.CheckKey) && infoManage != null)
            {
                TransLoader loader;
#if NETSTANDARD
                if (fromNetCoreInjection)
                {
                    loader = provider.GetService(typeof(TransLoader)) as TransLoader;
                }
                else
                {
                    loader = new TransLoader();
                }
#else
                loader = new TransLoader();
#endif
                var info = infoManage.QueryChek(_option);
                if (info != null)
                {
                    var allSteps = infoManage.QuerySteps(new List<string> { info.TId });
                    return loader.Load(info, allSteps).Execute();
                }
            }
            if (_option.Persistent && infoManage != null)
            {
                var masterInfo = new DbStatus.TransMasterInfo
                {
                    TId = _option.TId,
                    Name = _option.Name,
                    ProjectName = TransConfig.projectName,
                    MaxRetryCount = _option.MaxRetryCount,
                    RetryInterval = _option.RetryInterval.TotalSeconds,
                    StatusName = TransStatus.Pending.ToString(),
                    TransactionType = TransactionType,
                    Args = _option.Args.ToJson(),
                    ArgsType = _option.Args?.GetType().AssemblyQualifiedName,
                    CheckKey = _option.CheckKey
                };
                var steps2 = steps.Select(b => new DbStatus.TransStepInfo
                {
                    Args = b.Args.ToJson(),
                    ArgsType = b.GetArgsType().AssemblyQualifiedName,
                    TId = _option.TId,
                    FromNetCoreInjection = fromNetCoreInjection,
                    TypeName = b.GetType().AssemblyQualifiedName,
                    Step = b.Step,
                    StatusName = TransStatus.Pending.ToString(),
                    TransactionType = TransactionType,
                    Name = b.Name
                }).ToList();
                bool a;
                string error = "";
                try
                {
                    a = infoManage.SaveSteps(masterInfo, steps2, out error);
                }
                catch (Exception ex)
                {
                    a = false;
                    error = ex.Message;
                }
                if (!a)
                {
                    Core.EventLog.Log($"提交{TransactionType} 失败 {error} {new { master = masterInfo, setps = steps2 }.ToJson()}", GetType().Name);
                    throw new Exception($"事务持久化失败 {error}");
                }
            }
            int maxCancelStep = 0;
            if (FromLoad)
            {
                //取消失败，步骤前需要重新取消
                var maxError = steps.FindAll(b => b.Status == TransStatus.CancelError).OrderByDescending(b => b.Step).FirstOrDefault();
                if (maxError != null)
                {
                    maxCancelStep = maxError.Step;
                }
            }
            FromLoad = true;//使可重复执行
            IStep errorStep = null;
            for (var i = 0; i < steps.Count; i++)
            {
                var current = steps[i] as AbsTccStepBase;
                if (current.Status == TransStatus.Canceled)
                {
                    //二次处理发现有取消过，退出
                    break;
                }
                var cancelSetp = -1;
                try
                {
                    if (maxCancelStep > 0 && i == maxCancelStep - 1)
                    {
                        Log(current, "加载上次异常，开始自动取消");
                        cancelSetp = i;
                        goto label1;
                    }
                    if (current.Status == TransStatus.Pending)
                    {
                        current.Try();
                        current.Status = TransStatus.Tried;
                        var msg = $"{current.Status} 成功";
                        if (!current.defaultTry)
                        {
                            infoManage?.UpdateStatus(_option.TId, i, current.Status, "Try 成功");
                        }
                        else
                        {
                            msg += " 未重载Try方法，不写入数据库状态";
                        }
                        Log(current, msg);
                    }
                    continue;
                }
                catch (Exception e)
                {
                    cancelSetp = i - 1;
                    Log(current, $"Try 异常，开始取消 {e}");
                    current.Status = TransStatus.TryError;
                    infoManage?.UpdateStatus(_option.TId, i, TransStatus.TryError, $"{e.Message} {e.InnerException?.Message}");
                    //errorStep = current;
                    goto label1;
                }
            label1:
                //try 出错，取消之前所有环节
                var cancelStatus = TransStatus.Canceled;
                for (var n = cancelSetp; n >= 0; n--)
                {
                    var pre = steps[n];
                    var pollyKey = $"{_option.TId}_step_{n}";
                    var pollyData = PollyExtension.Invoke(new PollyAttribute { RetryCount = _option.MaxRetryCount, RetryInterval = _option.RetryInterval, }, () =>
                    {
                        if (pre.Status == TransStatus.Tried || pre.Status == TransStatus.CancelError)
                        {
                            pre.Cancel();
                            pre.Status = TransStatus.Canceled;
                            var msg = $"{pre.Status} 成功";
                            if (!current.defaultCancel)
                            {
                                infoManage?.UpdateStatus(_option.TId, n, pre.Status, "Cancel 成功");
                            }
                            else
                            {
                                msg += " 未重载Cancel方法，不写入数据库状态";
                            }
                            Log(pre, msg);
                        }
                        return new PollyExtension.PollyData<string>() { Data = "ok" };
                    }, pollyKey);
                    if (!string.IsNullOrEmpty(pollyData.Error))
                    {
                        LastError = $"Cancel 失败 {pollyData.Error}";
                        Log(pre, LastError);
                        errorStep = pre;
                        pre.Status = TransStatus.CancelError;
                        infoManage?.UpdateStatus(_option.TId, n, TransStatus.CancelError, pollyData.Error);
                        //infoManage.UpdateStatus(_option.TId, SagaStatus.CancelError);
                        cancelStatus = TransStatus.CancelError;
                    }
                }
                Status = cancelStatus;
                infoManage?.UpdateStatus(_option.TId, cancelStatus, "预处理失败，已取消");
                LastError = "预处理失败，已取消";
                return new ExecuteResult
                {
                    AbstractResult = false,
                    LastError = LastError,
                    NeedRepair = cancelStatus == TransStatus.CancelError,
                    Status = cancelStatus,
                    ErrorStep = errorStep
                };
            }
            //确认所有环节
            var commitStatus = TransStatus.Commited;
            for (var i = 0; i < steps.Count; i++)
            {
                var current = steps[i] as AbsTccStepBase;
                if (current.Status == TransStatus.Canceled)
                {
                    //二次处理发现有取消过，退出
                    commitStatus = TransStatus.Canceled;
                    break;
                }
                var pollyKey = $"{_option.TId}_step_{i}";
                var pollyData = PollyExtension.Invoke(new PollyAttribute { RetryCount = _option.MaxRetryCount, RetryInterval = _option.RetryInterval, }, () =>
                {
                    if (current.Status == TransStatus.Tried || current.Status == TransStatus.CommitError)
                    {
                        current.Commit();
                        current.Status = TransStatus.Commited;
                        var msg = $"{current.Status} 成功";
                        if (!current.defaultCommit)
                        {
                            infoManage?.UpdateStatus(_option.TId, i, current.Status, "Commit 成功");
                        }
                        else
                        {
                            msg += " 未重载Commit方法，不写入数据库状态";
                        }
                        Log(current, msg);
                    }
                    return new PollyExtension.PollyData<string>() { Data = "ok" };
                }, pollyKey);
                if (!string.IsNullOrEmpty(pollyData.Error))
                {
                    errorStep = current;
                    LastError = $"Commit 失败 {pollyData.Error}";
                    Log(current, $"Commit 失败 {pollyData.InnerException}");
                    current.Status = TransStatus.CommitError;
                    infoManage?.UpdateStatus(_option.TId, i, TransStatus.CommitError, pollyData.Error);
                    commitStatus = TransStatus.CommitError;
                }
            }
            Status= commitStatus;
            infoManage?.UpdateStatus(_option.TId, commitStatus, LastError);
            //if (commitStatus != TransStatus.Commited)
            //{
            //    LastError = "确认失败";
            //}
            return new ExecuteResult
            {
                AbstractResult = true,
                LastError = LastError,
                NeedRepair = commitStatus == TransStatus.CommitError,
                Status = commitStatus,
                ErrorStep = errorStep
            };
        }

    }

}
