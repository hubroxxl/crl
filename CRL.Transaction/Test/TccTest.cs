﻿using CRL.Core.Extension;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRL.Transaction.Test
{
    public class TccTest
    {
        public static void Test()
        {
            var control = new TccControl();
            control.Start(new TransOptions { Name = "testTcc", MaxRetryCount = 2, RetryInterval = TimeSpan.FromSeconds(1), Args = DateTime.Now, UseLog = true });
            control.Then<Step4>().Then<Step5>().Then<Step6>();
            var result = control.Execute();
            Console.WriteLine($"testTcc 执行 {result.ToJson()}");

        }
    }
    public class Step4 : AbsTccStep<string>
    {
        public Step4()
        {

        }
        public override string Name => GetType().Name;
#if NETSTANDARD
        IServiceProvider provider;
        public Step4(IServiceProvider _provider)
        {
            provider = _provider;
        }
#endif
        public override void Try()
        {
            var ss = GetMainArgs<DateTime>();
            throw new Exception("eee");
        }
        public override void Cancel()
        {
            //throw new Exception("Cancel fail");
            Console.WriteLine($"{GetType().Name} {Args} Cancel");
        }

        public override void Commit()
        {
            Console.WriteLine($"{GetType().Name} {Args} Commit");
        }
    }
    public class Step5 : AbsTccStep<string>
    {
        public Step5()
        {

        }
        public override string Name => GetType().Name;
#if NETSTANDARD
        IServiceProvider provider;
        public Step5(IServiceProvider _provider)
        {
            provider = _provider;
        }
#endif
        public override void Try()
        {
            Console.WriteLine($"{GetType().Name} {Args} Try");
        }
        public override void Cancel()
        {
            Console.WriteLine($"{GetType().Name} {Args} Cancel");
        }

        public override void Commit()
        {
            //throw new Exception("error");
            Console.WriteLine($"{GetType().Name} {Args} Commit");
        }
    }
    public class Step6 : AbsTccStep<string>
    {
        public Step6()
        {

        }
        public override string Name => GetType().Name;
#if NETSTANDARD
        IServiceProvider provider;
        public Step6(IServiceProvider _provider)
        {
            provider = _provider;
        }
#endif
        public override void Try()
        {
            throw new Exception("error");
        }
        public override void Cancel()
        {
            Console.WriteLine($"{GetType().Name} {Args} Cancel");
        }

        public override void Commit()
        {
            //throw new Exception("error");
            Console.WriteLine($"{GetType().Name} {Args} Commit");
        }
    }
}
