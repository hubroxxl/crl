﻿using CRL.Core.Extension;
using System;
using System.Collections.Generic;
using System.Text;

namespace CRL.Transaction.Test
{
    public class SagaTest
    {
        public static void Test()
        {
            var control = new SagaControl();
            control.Start(new TransOptions { Name = "testSaga", MaxRetryCount = 2, RetryInterval = TimeSpan.FromSeconds(1), UseLog = true, CheckKey="11111" });
            control.Then<Step1>("Step1").Then<Step2>("Step2").Then<Step3>("Step3");
            var result = control.Execute();
            Console.WriteLine($"SagaTest 执行 {result.ToJson()}");

        }
        public static void Test2()
        {
            var control2 = new SagaControl();
            control2.Start(new TransOptions { TId = "testSaga", MaxRetryCount = 10, RetryInterval = TimeSpan.FromSeconds(1) });
            control2.Then(() =>
            {
                Console.WriteLine("step1 commit");
            }, () =>
            {
                Console.WriteLine("step1 cancel");
            });
            control2.Then(() =>
            {
                throw new Exception("ssss");
                Console.WriteLine("step2 commit");
            }, () =>
            {

                Console.WriteLine("step2 cancel");
            });
            var result2 = control2.Execute();
            Console.WriteLine($"SagaTest 执行 {result2}");
        }
    }
    public class Step1 : AbsSagaStep<string>
    {
        public Step1()
        {

        }
        public override string Name => GetType().Name;
#if NETSTANDARD
        IServiceProvider provider;
        public Step1(IServiceProvider _provider)
        {
            provider = _provider;
        }
#endif
        public override void Cancel()
        {
            throw new Exception("Cancel fail");
            Console.WriteLine($"{GetType().Name} {Args} Cancel");
        }

        public override void Commit()
        {
            Console.WriteLine($"{GetType().Name} {Args} Commit");
        }
    }
    public class Step2 : AbsSagaStep<string>
    {
        public Step2()
        {

        }
        public override string Name => GetType().Name;
#if NETSTANDARD
        IServiceProvider provider;
        public Step2(IServiceProvider _provider)
        {
            provider = _provider;
        }
#endif
        public override void Cancel()
        {
            Console.WriteLine($"{GetType().Name} {Args} Cancel");
        }

        public override void Commit()
        {
            //throw new Exception("error");
            Console.WriteLine($"{GetType().Name} {Args} Commit");
        }
    }
    public class Step3 : AbsSagaStep<string>
    {
        public Step3()
        {

        }
        public override string Name => GetType().Name;
#if NETSTANDARD
        IServiceProvider provider;
        public Step3(IServiceProvider _provider)
        {
            provider = _provider;
        }
#endif
        public override void Cancel()
        {
            Console.WriteLine($"{GetType().Name} {Args} Cancel");
        }

        public override void Commit()
        {
            throw new Exception("error");
            Console.WriteLine($"{GetType().Name} {Args} Commit");
        }
    }
}
