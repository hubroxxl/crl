﻿
using CRL.Transaction.DbStatus;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace CRL.Transaction
{
    public class TransConfig
    {
        internal static Func<ITransInfoManage> _manageFunc = () => new MemoryTransInfoManage();
        static TransLoader loader;
        internal static string projectName;
        public static void RegisterTransInfoManage(string projectName, Func<ITransInfoManage> manageFunc)
        {
            _manageFunc = manageFunc;
            if (loader == null)
            {
                loader = new TransLoader();
                loader.StartCheckThread(projectName);
            }
        }
        internal static ITransInfoManage GetTransInfoManage()
        {
            if (_manageFunc == null)
            {
                throw new Exception("未初始化");
            }
            return _manageFunc.Invoke();
        }
    }
}
