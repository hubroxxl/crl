﻿using System;

namespace CRL.Transaction
{
    public enum TransactionType
    {
        Tcc, Saga
    }
    public interface ITransControl
    {
        bool FromLoad { get; set; }
        TransStatus Status { get; set; }

        string LastError { get; set; }
        TransactionType TransactionType { get; }
        ExecuteResult Execute();
        ITransControl Start(TransOptions option);
        ITransControl Then(Type type, object args, TransStatus? newStatus);
#if NETSTANDARD
        void SetServiceProvider(IServiceProvider _provider);
#endif
    }
}